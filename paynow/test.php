<?php

require 'paynow/vendor/autoload.php';

$p = new Paynow\Paynow(3105, 'eb01b0bc-c551-466b-b5e4-c39e5a7d98b8');

$reference = '123';
$amount = 10.00;
$additionalInfo = 'Payment for order '.$reference;
$returnUrl = 'http://localhost/admin/paynow/thankyou.php';
$resultUrl = 'http://finsec.co.zw/paynow/result.php';

$res = $p->initiatePayment(
    $reference,
    $amount,
    $additionalInfo,
    $returnUrl,
    $resultUrl
);

echo "<a href='".$res->browserurl."'>Make payment</a>";

?>