<?php
function WereCreatingATransaction()
{
	global $_POST;
	global $integration_id;
	global $integration_key;
	global $authemail;
	global $initiate_transaction_url;
	global $orders_data_file;
	global $checkout_url;
	$local_order_id = $_POST['order_id'];
	
	
	//set POST variables
	$values = array('resulturl' => $_POST['resulturl'],
			'returnurl' =>  $_POST['returnurl'],
			'reference' =>  $_POST['reference'],
			'amount' =>  $_POST['amount'],
			'id' =>  $integration_id,
			'additionalinfo' =>  $_POST['additionalinfo'],
			'authemail' =>  $authemail,
			'status' =>  'Message'); //just a simple message
			
	$fields_string = CreateMsg($values, $integration_key);
	
	//open connection
	$ch = curl_init();
	$url = $initiate_transaction_url;
	
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	//execute post
	$result = curl_exec($ch);

	if($result)
	{
		$msg = ParseMsg($result);
		
		//first check status, take appropriate action
		if ($msg["status"] == ps_error){
			header("Location: $checkout_url");
			exit;
		}
		else if ($msg["status"] == ps_ok){
		
			//second, check hash
			$validateHash = CreateHash($msg, $integration_key);
			if($validateHash != $msg["hash"]){
				$error =  "Paynow reply hashes do not match : " . $validateHash . " - " . $msg["hash"];
			}
			else
			{
				
				$theProcessUrl = $msg["browserurl"];

				/***** IMPORTANT ****
				On User has approved paying you, maybe they are awaiting delivery etc
				
					Here is where you
					1. Save the PollURL that we will ALWAYS use to VERIFY any further incoming Paynow Notifications FOR THIS PARTICULAR ORDER
					1. Update your local shopping cart of Payment Status etc and do appropriate actions here, Save any other relavant data to DB
					2. Email, SMS Notifications to customer, merchant etc
					3. Any other thing
				
				*** END OF IMPORTANT ****/
				
				//1. Saving mine to a PHP.INI type of file, you should save it to a db etc
				$orders_array = array();
				if (file_exists($orders_data_file))
				{
					$orders_array = parse_ini_file($orders_data_file, true);
				}
				
				$orders_array['OrderNo_'.$local_order_id] = $msg;
				
				write_ini_file($orders_array, $orders_data_file, true);				
				
			}
		}
		else {						
			//unknown status or one you dont want to handle locally
			$error =  "Invalid status in from Paynow, cannot continue.";
		}

	}
	else
	{
	   $error = curl_error($ch);
	}
	
	//close connection
	curl_close($ch);

			
	//Choose where to go
	if(isset($error))
	{
		//back to checkout, show the user what they need to do
		header("Location: $checkout_url");
	}
	else
	{
		//redirect to paynow for user to complete payment
		header("Location: $theProcessUrl");
	}
	exit;	
	
}