<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ; 
$extra_css = "" ; 
$page_number = "10" ; 
$extra_js = "" ;
$thy_stage = "3" ;
@$msg=$_GET['msg'];
@$balance_msg =$_GET['balance_msg'];
$main_page_title = "Advanced Stage" ; 
require_once("require/header.php") ;
require_once 'func/controlDAO.php' ;
?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                         <h1> 3YR HISTORICAL FINANCIALS   4 - 6</h1>
                        <form action="form/login.php"  class="login-form" method="post">

                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>Income Statement: <a href="stage_3_historical_financials_edit.php?action=add_income" >[ Add New ]</a> <p><?php echo "<br><font color='green'  >".@$msg. " </font>" ;?></p> </h5> 
                                    <table class="table table-bordered">
                                      <thead>

                                      <tr>
                                         <th>Year</th>
                                         <th>% Gross Income </th>
                                         <th>Gross Income</th>
                                         <th>Cost of sales</th>
                                         <th>Gross Profit</th>
                                         <th>Net Profit</th>                                 
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                      $ifPopulated =  (new controlDAO())->getincomeStatement()->selectincomeStatementByEmail($_SESSION['email'] , 'history') ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="6" style="text-align:center">No income statement entry found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //#
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>".@$ifPopulated[$i]['year']."</td>
                                                         <td>".@$ifPopulated[$i]['gross_income_per']."</td>
                                                         <td>".@$ifPopulated[$i]['gross_income']."</td>
                                                         <td>".@$ifPopulated[$i]['cost_of_sales']."</td>
                                                         <td>".@$ifPopulated[$i]['gross_profit']."</td>
                                                         <td>".@$ifPopulated[$i]['net_profit']."</td>
                                                         <td><a href='stage_3_historical_financials_edit.php?id=".@$ifPopulated[$i]['income_statement_id']."'>[Edit]</a> | <a href ='./func/controller/incomeStatementController.php?id=".@$ifPopulated[$i]['income_statement_id']."' >[Delete]</a></td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>


                                      </tbody>
                                    </table>
                                </div>

                            </div>









                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>Balance sheet : <a href="stage_3_historical_financials_balancesheet_edit.php?action=add_balancesheet" >[ Add New ]</a>  <p><?php echo "<br><font color='green'  >".@$balance_msg. " </font>" ;?></p></h5> 
                                    <table class="table table-bordered">
                                      <thead>

                                      <tr>
                                         <th>Year</th>
                                         <th>Fixed Assets </th>
                                         <th>Current Assets</th>
                                         <th>Total Assets</th>
                                         <th>Current Liabilities</th>
                                         <th>Other Liabilities</th>
                                         <th>Total Liabilities</th>
                                         <th>NET ASSETS</th>

                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                      $ifPopulated =  (new controlDAO())->getbalanceSheet()->selectBalanceSheetByEmail($_SESSION['email'] , 'history') ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="8" style="text-align:center">No balance sheet entry found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# balance_sheet_id, user_id, , , , , , , , ,
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>".@$ifPopulated[$i]['year']."</td>
                                                         <td>".@$ifPopulated[$i]['fixed_assets']."</td>
                                                         <td>".@$ifPopulated[$i]['current_assets']."</td>
                                                         <td>".@$ifPopulated[$i]['total_assets']."</td>
                                                         <td>".@$ifPopulated[$i]['current_liabilities']."</td>
                                                         <td>".@$ifPopulated[$i]['other_liabilities']."</td>
                                                         <td>".@$ifPopulated[$i]['total_liabilities']."</td>
                                                         <td>".@$ifPopulated[$i]['net_assets']."</td>
                                                        <td><a href='stage_3_historical_financials_balancesheet_edit.php?id=".@$ifPopulated[$i]['balance_sheet_id']."'>[Edit]</a> | <a href ='./func/controller/balanceSheetController.php?id=".@$ifPopulated[$i]['balance_sheet_id']."' >[Delete]</a></td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>


                                      </tbody>
                                    </table>
                                </div>

                            </div>


                        </form>


                    </div>

                    <!--  First form of the dialog
                          Income Statement  -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                          <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="exampleModalLabel1">Income Statement</h4> </div>
                                  <div class="modal-body">
                                      <form>
                                          <div class="form-group">
                                              <label for="recipient-name" class="control-label">Year  :</label>
                                              <input type="text" class="form-control" id="recipient-name1"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name2" class="control-label">% Gross Income :</label>
                                              <input type="text" class="form-control" id="recipient-name3" > </div>
                                          <div class="form-group">
                                              <label for="recipient-name4" class="control-label">Gross Income  :</label>
                                              <input type="text" class="form-control" id="recipient-name5"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name6" class="control-label">Cost of sales :</label>
                                              <input type="text" class="form-control" id="recipient-name7"> </div>
                                          <div class="form-group">
                                              <label for="recipient-name8" class="control-label">Gross Profit  :</label>
                                              <input type="text" class="form-control" id="recipient-name9"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name10" class="control-label">Net Profit  :</label>
                                              <input type="text" class="form-control" id="recipient-name11"> </div>                                                                                      
                                      </form>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save</button>
                                  </div>
                              </div>
                          </div>
                    </div>


                    <!--  Second form of the dialog
                          Balance sheet  -->
                    <div class="modal fade" id="exampleModaltwo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
                          <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="exampleModalLabel1">Balance sheet</h4> </div>
                                  <div class="modal-body">
                                      <form>
                                          <div class="form-group">
                                              <label for="recipient-name" class="control-label">Year  :</label>
                                              <input type="text" class="form-control" id="recipient-name1"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name2" class="control-label">Fixed Assets :</label>
                                              <input type="text" class="form-control" id="recipient-name3"> </div>
                                          <div class="form-group">
                                              <label for="recipient-name4" class="control-label">Current Assets  :</label>
                                              <input type="text" class="form-control" id="recipient-name5"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name6" class="control-label">Total Assets :</label>
                                              <input type="text" class="form-control" id="recipient-name7"> </div>
                                          <div class="form-group">
                                              <label for="recipient-name8" class="control-label">Current Liabilities  :</label>
                                              <input type="text" class="form-control" id="recipient-name9"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name10" class="control-label">Other Liabilities  :</label>
                                              <input type="text" class="form-control" id="recipient-name11"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name12" class="control-label">Total Liabilities  :</label>
                                              <input type="text" class="form-control" id="recipient-name13"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name14" class="control-label">NET ASSETS  :</label>
                                              <input type="text" class="form-control" id="recipient-name15"> </div>                                        
                                                 
                                      </form>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save</button>
                                  </div>
                              </div>
                          </div>
                    </div>                    



                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_3_material_litigation_claims.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <a href="stage_3_projected_financials.php" class="btn green btn-outline pull-right" >[ Next >>] </a>
                            </div>
                        </div>
                    </div>


                </div>
<?php

require_once("require/footer.php") ;

?>