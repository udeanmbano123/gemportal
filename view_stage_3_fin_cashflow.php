<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ;
$extra_css = "" ;
$page_number = "10" ;
$extra_js = "" ;
$thy_stage = "3" ;
$thy_qsn = "183/472" ;
$thy_qp = round(183/472 * 100, 2);
$my_title = "CASH FLOW" ;
@$msg=$_GET['msg'];
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg'];

$cashflow = "true" ;

require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header_admin.php") ;


$main=(new controlDAO())->getlast2yrs();
$get_financials_cashflows= $main->selectCashflows($_GET['email']) ;
$get_net_cashflows= $main->NetCash($_GET['email']) ;
$ifPopulated =  (new controlDAO())->getuploadFinancials()->selectOneuploadFinancialsByEmailCashFlow($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;


}else{

    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
    }

    //echo $user_id ;
}


?>
    <div class="col-md-8 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
        <div class="login-content" style ="margin-top:0px;padding:0px;">

            <div class="bg-title my_custom_header_main">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                <!-- /.col-lg-12 -->
            </div>


            <!-- /row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                        <h3 class="box-title">Cash Flow Checked</h3>
                        <form action="#"     class="login-form" method="post">

                            <table class="table table-bordered responsive">
                                <thead>
                                <th class="c-gray">#</th>
                                <th class="c-gray">Is Audited</th>
                                <th class="c-gray"></th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>  <input type="checkbox"
                                            <?php
                                            if(@$is_audited =="yes"){
                                                echo "checked" ;
                                            }

                                            ?> value="yes" name="is_audited" disabled /></td>

                                    <td> <a target="_blank" href="<?=$doc_url;?>"> <?=@$doc_url;?></a> </td>
                                </tr>


                                </tbody>
                            </table>

                        </form>
                    </div>
                </div>
            </div>

            <!-- /row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box-main">
                        <h3 class="box-title">CONSOLIDATED STATEMENT OF CASH FLOWS</h3>



                        <table id="cashflow_table" class="table editable-table table-bordered m-b-0">
                            <thead class="text-uppercase">
                            <th width="330" class="c-gray"></th>
                            <th colspan="3" class="c-gray">HISTORICAL</th>
                            <th colspan="5" class="c-gray">PROJECTED</th>
                            </thead>
                            <tbody>
                            <colgroup>
                                <col  style="background-color:#f5f5f5;">
                                <col span="3" style="background-color:yellow">
                                <col span="5" style="background-color:pink">
                            </colgroup>

                            <tr>
                                <td>Year</td><td>2015</td><td>2016</td><td>2017</td><td>2018</td><td>2019</td><td>2020</td><td>2021</td><td>2022</td>
                            </tr>
                            <tr>
                                <td>(Loss)/Profit  before tax</td>
                                <?php

                                    for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                        echo "<td data-name = 'cash_flow_LossProfitbeforetax' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_LossProfitbeforetax']."</td>" ;
                                    }
                                ?>

                            </tr>

                            <tr>
                                <td>Adjustments to reconcile profit before tax to net cash flows (+/-) </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'cash_flow_Adjustments' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_Adjustments']."</td>" ;
                                }
                                ?>

                            </tr>
                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;">
                                <td>Net cashflows from operating activities</td>
                                <?php

                                for($i = 0  ; $i < count($get_net_cashflows) ; $i++ ) {
                                    echo "<td >".$get_net_cashflows[$i]['Total']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Net cash used in investing activities (+/-) </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'cash_flow_NetCashInvestingactivities' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_NetCashInvestingactivities']."</td>" ;

                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Net cashflows used in financing activities (+/-)</td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'cash_flow_Netfinancingactivities' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_Netfinancingactivities']."</td>" ;

                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Cash and cash equivalents at the beginning of the year</td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'cash_flow_Cashequivalents' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_Cashequivalents']."</td>" ;

                                }
                                ?>
                            </tr>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <div class="login-footer">
            <div class="row bs-reset">
                <div  style="padding: 0 15px;" class="col-xs-4 bs-reset">
                    <?php
                    if($others->getUserPemission($_SESSION['email']) =="Analyst") {
                        ?>
                        <a href="analyst_pending_applications.php" class="btn green btn-outline">[<< Back
                            Pending Applications]</a>
                        <?php
                    }
                    if($others->getUserPemission($_SESSION['email']) == "Financier") {
                        ?>
                        <a href="fin_applicants.php" class="btn green btn-outline">[<< Back
                            View Applicants Lists]</a>
                        <?php
                    }
                    ?>
                </div>
                <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                    <button type="button"  class="js-print-link btn blue btn-outline pull-right" >[ PRINT ] </button>
                </div>
            </div>


        </div>

    </div>



<?php
require_once("require/footer_admin.php") ;
?>