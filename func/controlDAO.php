<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:23 AM
 */


class controlDAO
{


    function __construct()
    {
        set_include_path('data/');

    }

    function getUser(){
        require_once('data/usersDAO.php') ;
        $userDAO = new usersDAO() ;
        return $userDAO ;
    }
    function getCapitalRequirements(){
        include ('data/capitalRequirementsDAO.php') ;
        $capitalreqDAO = new capitalRequirementsDAO() ;
        return $capitalreqDAO ;
    }
    function getfinRatios(){
        include ('data/finRatiosDAO.php') ;
        $capitalreqDAO = new finRatiosDAO() ;
        return $capitalreqDAO ;
    }
    function getCorporateDirectory(){
        include ('data/corporateDirectoryDAO.php') ;
        $capitalreqDAO = new corporateDirectoryDAO() ;
        return $capitalreqDAO ;
    }
    function getenterpriseSizeCategorisation(){
        include ('data/enterpriseSizeCategorisationDAO.php') ;
        $capitalreqDAO = new enterpriseSizeCategorisationDAO() ;
        return $capitalreqDAO ;
    }
    function getbalanceSheet(){
        include ('data/balanceSheetDAO.php') ;
        $capitalreqDAO = new balanceSheetDAO() ;
        return $capitalreqDAO ;
    }
    function getcommittees(){
        include ('data/committeesDAO.php') ;
        $capitalreqDAO = new committeesDAO() ;
        return $capitalreqDAO ;
    }
    function getcompanyOverview(){
        include ('data/companyOverviewDAO.php') ;
        $capitalreqDAO = new companyOverviewDAO() ;
        return $capitalreqDAO ;
    }
    function getcompanyOverviewCustomers(){
        include ('data/companyOverviewCustomersDAO.php') ;
        $capitalreqDAO = new companyOverviewCustomersDAO() ;
        return $capitalreqDAO ;
    }
    function getcompanyOverviewProductsServices(){
        include ('data/companyOverviewProductsServicesDAO.php') ;
        $capitalreqDAO = new companyOverviewProductsServicesDAO() ;
        return $capitalreqDAO ;
    }
    function getcompanyOverviewRawMaterials(){
        include ('data/companyOverviewRawMaterialsDAO.php') ;
        $capitalreqDAO = new companyOverviewRawMaterialsDAO() ;
        return $capitalreqDAO ;
    }
    function getcompanyOverviewSuppliers(){
        include ('data/companyOverviewSuppliersDAO.php') ;
        $capitalreqDAO = new companyOverviewSuppliersDAO() ;
        return $capitalreqDAO ;
    }
    function getcorporateStructure(){
                include ('data/corporateStructureDAO.php') ;
        $capitalreqDAO = new corporateStructureDAO() ;
        return $capitalreqDAO ;
    }
    function getdirectors(){
                include ('data/directorsDAO.php') ;
        $capitalreqDAO = new directorsDAO() ;
        return $capitalreqDAO ;
    }
    function getdirectorsShareholding(){
                include ('data/directorsShareholdingDAO.php') ;
        $capitalreqDAO = new directorsShareholdingDAO() ;
        return $capitalreqDAO ;
    }
    function gethumanResourceOrganogram(){
                include ('data/humanResourceOrganogramDAO.php') ;
        $capitalreqDAO = new humanResourceOrganogramDAO() ;
        return $capitalreqDAO ;
    }
    function getincomeStatement(){
                include ('data/incomeStatementDAO.php') ;
        $capitalreqDAO = new incomeStatementDAO() ;
        return $capitalreqDAO ;
    }
    function getmaterialAssetTransactions(){
                include ('data/materialAssetTransactionsDAO.php') ;
        $capitalreqDAO = new materialAssetTransactionsDAO() ;
        return $capitalreqDAO ;
    }
    function getmaterialLitigation(){
                include ('data/materialLitigationDAO.php') ;
        $capitalreqDAO = new materialLitigationDAO() ;
        return $capitalreqDAO ;
    }
    function getpersonalFinancial(){
                include ('data/personalFinancialDAO.php') ;
        $capitalreqDAO = new personalFinancialDAO() ;
        return $capitalreqDAO ;
    }
    function getuploadFinancials(){
                include ('data/uploadFinancialsDAO.php') ;
        $capitalreqDAO = new uploadFinancialsDAO() ;
        return $capitalreqDAO ;
    }
    function getshareholdersShareholding(){
                include ('data/shareholdersDAO.php') ;
        $capitalreqDAO = new directorsShareholdingDAO() ;
        return $capitalreqDAO ;
    }
    function getmaterialContracts(){
                include ('data/materialContractsDAO.php') ;
        $capitalreqDAO = new materialContractsDAO() ;
        return $capitalreqDAO ;
    }
     function getPermissions(){
                include ('data/permissionsDAO.php') ;
        $capitalreqDAO = new permissionsDAO() ;
        return $capitalreqDAO ;
    }
     function getFinancialAdvisors(){
                include ('data/financialadvisornominationDAO.php') ;
        $capitalreqDAO = new  FinancialadvisorDAO () ;
        return $capitalreqDAO ;
    }
    function getApplications(){
                include ('data/applicationsDAO.php') ;
        $capitalreqDAO = new  applicationsDAO () ;
        return $capitalreqDAO ;
    }
    function getDetails(){
                include ('data/viewDetailsDAO.php') ;
        $capitalreqDAO = new  viewDetailsDAO () ;
        return $capitalreqDAO ;
    }
    function getOthers(){
        include ('data/othersDAO.php') ;
        $capitalreqDAO = new  othersDAO () ;
        return $capitalreqDAO ;
    }
    function getOthersNew(){
        include ('data/othersDAONew.php') ;
        $capitalreqDAO = new  othersDAONew () ;
        return $capitalreqDAO ;
    }
    function getParameters(){
        include ('data/parametersDAO.php') ;
        $capitalreqDAO = new  parametersDAO () ;
        return $capitalreqDAO ;
    }
    function getCountry(){
                include ('data/countriesDAO.php') ;
        $capitalreqDAO = new  countriesDAO () ;
        return $capitalreqDAO ;
    }
    function getVerification(){
                include ('data/verificationDAO.php') ;
        $capitalreqDAO = new verificationDAO() ;
        return $capitalreqDAO ;
    }
    function getverificationScoringSettings(){
                include ('data/verificationScoringSettingsDAO.php') ;
        $capitalreqDAO = new verificationScoresSettingsDAO() ;
        return $capitalreqDAO ;
    }

   
     function  getReview(){
                include ('data/reviewDAO.php') ;
        $capitalreqDAO = new reviewDAO() ;
        return $capitalreqDAO ;
    }

     function  getIncomeStatementValues(){
                include ('data/retrieveIncomeStatementDAO.php') ;
        $capitalreqDAO = new retrieveIncomeStatementDAO() ;
        return $capitalreqDAO ;
    }
    function  getCashflowValues(){
                include ('data/retrieveCashFlowDAO.php') ;
        $capitalreqDAO = new retrieveCashFlowDAO() ;
        return $capitalreqDAO ;
    }
    function  getBalanceSheetValues(){
                include ('data/retrieveBalanceSheetDAO.php') ;
        $capitalreqDAO = new retrieveBalanceSheetDAO() ;
        return $capitalreqDAO ;
    }
    function  getlast2yrs(){
                include ('data/last2yrsDAO.php') ;
        $capitalreqDAO = new last2yrsDAO() ;
        return $capitalreqDAO ;
    }
    function  getlastyr(){
                include ('data/lastyrDAO.php') ;
        $capitalreqDAO = new lastyrDAO() ;
        return $capitalreqDAO ;
    }
    function  getthisyr(){
                include ('data/thisyrDAO.php') ;
        $capitalreqDAO = new thisyrDAO() ;
        return $capitalreqDAO ;
    }
     function  getnextyr(){
                include ('data/nextyrDAO.php') ;
        $capitalreqDAO = new nextyrDAO() ;
        return $capitalreqDAO ;
    }
    function  getnext2yrs(){
                include ('data/next2yrsDAO.php') ;
        $capitalreqDAO = new next2yrsDAO() ;
        return $capitalreqDAO ;
    }
     function  getFinancialRatiosByValues(){
                include ('data/financialratiosDAO.php') ;
        $capitalreqDAO = new financialratiosDAO () ;
        return $capitalreqDAO ;
    }
    function  getUsertype(){
                include ('data/getUsertypeDAO.php') ;
        $capitalreqDAO = new usertypeDAO () ;
        return $capitalreqDAO ;
    }
    function  getClientStatus(){
                include ('data/clientStatusDAO.php') ;
        $capitalreqDAO = new clientStatusDAO() ;
        return $capitalreqDAO ;
    }
    function getScores(){
        include ('data/scoresDAO.php') ;
        $capitalreqDAO = new  scoresDAO () ;
        return $capitalreqDAO ;
    }
}