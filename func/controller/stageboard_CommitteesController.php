<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 12/6/2018
 * Time: 2:08 AM
 */


include '../controlDAO.php' ;
session_start() ;

$my_others = (new controlDAO())->getOthers() ;
if(@$_POST['board_committees'] != "" ){
    $my_others->setScore($_SESSION['email'] , "board_committees") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "board_committees") ;
}
if(@$_POST['board_committees_count'] >= 4){
    $my_others->setScore($_SESSION['email'] , "committees_at_least_4_board_committees") ;
}else if(@$_POST['board_committees_count'] <= 3 && @$_POST['board_committees_count'] >= 2 ){
    $my_others->setScore($_SESSION['email'] , "committees_2_to_3_board_committees") ;
}else if(@$_POST['board_committees_count'] == 1 ){
    $my_others->setScore($_SESSION['email'] , "committees_one_board_committee") ;
}else{
    $my_others->setScore($_SESSION['email'] , "committees_no_board_committees") ;
}

header("Location: ../../stage_2_corporate_structure.php") ;