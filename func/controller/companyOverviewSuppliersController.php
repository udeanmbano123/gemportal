<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:33 AM
 */





include '../controlDAO.php' ;

//$u = (new controlDAO())->getUser();

if (isset($_POST['create_supply'])){

    $c_u =  createSuppliers(@$_POST['user_id'] ,@$_POST['customer_name'] ,@$_POST['customer_location'],@$_POST['customer_years_rel'],@$_POST['customer_sales'],@$_POST['customer_trade_terms'],@$_POST['customer_distr_channel']) ;
    if($c_u){
       // echo"Product created successfully " ;
        
       header("Location: ../../stage_1_overview_of_company_grid.php?supplier_msg=Supplier created successfully") ;
    }else{
        echo "failed to create product " ;
    }

}
if (isset($_GET['delete_supply'])){
    // echo "Delete product". @$_GET['id'] ; 
    $c_u = deleteSuppliers(@$_GET['id']) ;
    if($c_u){
       echo"Customer deleted successfully " ;
        header("Location: ../../stage_1_overview_of_company_grid.php?customer_msg=Customer deleted successfully") ;
    }else{
        echo "failed to delete product " ;
    }

}
if (isset($_POST['edit_supply'])){

 

    $c_u = editSuppliers(@$_POST['company_overview_suppliers_id'] ,@$_POST['customer_name'] ,@$_POST['customer_location'],@$_POST['customer_years_rel'],@$_POST['customer_sales'],@$_POST['customer_trade_terms'],@$_POST['customer_distr_channel']) ;
    if($c_u){
        
          header("Location: ../../stage_1_overview_of_company_grid.php?supplier_msg=Supplier edited  successfully") ;
    }else{
        echo "failed to edit product " ;
    }

}


function createSuppliers($user_id,$customer_name,$customer_location,$customer_years_rel,$customer_sales,$customer_trade_terms,$customer_distr_channel){
    include("../models/companyOverviewSuppliers.php") ;
    $uc  = new companyOverviewSuppliers();
    $uc->setuser_id($user_id);
    $uc->setcustomer_name($customer_name);
    $uc->setcustomer_location($customer_location);
    $uc->setcustomer_years_rel($customer_years_rel);
    $uc->setcustomer_sales($customer_sales);
    $uc->setcustomer_trade_terms($customer_trade_terms);
    $uc->setcustomer_distr_channel($customer_distr_channel);
    
    return (new controlDAO())->getcompanyOverviewSuppliers()->createcompanyOverviewSuppliers($uc) ;

}
function editSuppliers($company_overview_suppliers_id,$customer_name,$customer_location,$customer_years_rel,$customer_sales,$customer_trade_terms,$customer_distr_channel){
    include("../models/companyOverviewSuppliers.php") ;
    
    $uc  = new companyOverviewSuppliers();
    $uc->setcompany_overview_suppliers_id($company_overview_suppliers_id);
    $uc->setcustomer_name($customer_name);
    $uc->setcustomer_location($customer_location);
    $uc->setcustomer_years_rel($customer_years_rel);
    $uc->setcustomer_sales($customer_sales);
    $uc->setcustomer_trade_terms($customer_trade_terms);
    $uc->setcustomer_distr_channel($customer_distr_channel);   
    //return false ;
    return (new controlDAO())->getcompanyOverviewSuppliers()->updatecompanyOverviewSuppliers($uc) ;

}
function deleteSuppliers($company_overview_suppliers_id){
    include("../models/companyOverviewSuppliers.php") ;
    $uc  = new companyOverviewSuppliers();
    $uc->setcompany_overview_suppliers_id($company_overview_suppliers_id) ;
    return (new controlDAO())->getcompanyOverviewSuppliers()->deletecompanyOverviewSuppliers($uc) ;

}