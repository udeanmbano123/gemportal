<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:33 AM
 */
//echo $_POST['company_name'] ;
include '../controlDAO.php' ;
session_start() ;

$my_others = (new controlDAO())->getOthers() ;
if(@$_POST['company_name'] != "" ){
    $my_others->setScore($_SESSION['email'] , "company_name") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "company_name") ;
}
if(@$_POST['company_registration_number'] != "" ){
    $my_others->setScore($_SESSION['email'] , "company_registration_number") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "company_registration_number") ;
}
if(@$_POST['date_of_incorporation'] != "" ){
    $my_others->setScore($_SESSION['email'] , "date_of_incorporation") ;
    $d1 = new DateTime(date("Y-m-d"));
    $d2 = new DateTime($_POST['date_of_incorporation']);
    $diff = $d2->diff($d1);
    $date_diff =  $diff->y;
    if($date_diff > 5) {
        $my_others->setScore($_SESSION['email'] , "more_than_5_years") ;
        $my_others->deleteSetScore($_SESSION['email'] , "3_to_5years") ;
        $my_others->deleteSetScore($_SESSION['email'] , "less_than_3years") ;
    }else if($date_diff < 5 && $date_diff > 3) {
        $my_others->deleteSetScore($_SESSION['email'] , "more_than_5_years") ;
        $my_others->deleteSetScore($_SESSION['email'] , "3_to_5years") ;
        $my_others->setScore($_SESSION['email'] , "3_to_5years") ;
    }else{
        $my_others->deleteSetScore($_SESSION['email'] , "more_than_5_years") ;
        $my_others->deleteSetScore($_SESSION['email'] , "3_to_5years") ;
        $my_others->setScore($_SESSION['email'] , "less_than_3years") ;
    }
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "date_of_incorporation") ;
    $my_others->deleteSetScore($_SESSION['email'] , "more_than_5_years") ;
    $my_others->deleteSetScore($_SESSION['email'] , "3_to_5years") ;
    $my_others->deleteSetScore($_SESSION['email'] , "less_than_3years") ;
}
if(@$_POST['country_of_incorporation'] != "" ){
    $my_others->setScore($_SESSION['email'] , "country_of_incorporation") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "country_of_incorporation") ;
}
if(@$_POST['region_of_incorporation'] != "" ){
    $my_others->setScore($_SESSION['email'] , "region_of_incorporation") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "region_of_incorporation") ;
}

if(@$_POST['type_of_entity'] != "" ){
    $my_others->setScore($_SESSION['email'] , "type_of_entity") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Corporation") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Partnership") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Proprietorship") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Trust") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Association") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Non_rofit") ;
    if($_POST['type_of_entity'] == "Corporation"){
        $my_others->setScore($_SESSION['email'] , "Entity_type_Corporation") ;
    }
    if($_POST['type_of_entity'] == "Trust"){
        $my_others->setScore($_SESSION['email'] , "Entity_type_Trust") ;
    }
    if($_POST['type_of_entity'] == "Partnership"){
        $my_others->setScore($_SESSION['email'] , "Entity_type_Partnership") ;
    }
    if($_POST['type_of_entity'] == "Association"){
        $my_others->setScore($_SESSION['email'] , "Entity_type_Association") ;
    }
    if($_POST['type_of_entity'] == "Non Profit"){
        $my_others->setScore($_SESSION['email'] , "Entity_type_Non_rofit") ;
    }
    if($_POST['type_of_entity'] == "Proprietorship"){
        $my_others->setScore($_SESSION['email'] , "Entity_type_Proprietorship") ;
    }
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "type_of_entity") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Corporation") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Partnership") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Proprietorship") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Trust") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Association") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Entity_type_Non_rofit") ;
}
if(@$_POST['business_sector'] != "" ){
    $my_others->setScore($_SESSION['email'] , "business_sector") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_sector_Mining") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_sector_Manufacturing") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_sector_Agriculture") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_sector_Services") ;
    if($_POST['business_sector'] == 'Mining') {
        $my_others->setScore($_SESSION['email'] , "Buz_sector_Mining") ;
    }
    if($_POST['business_sector'] == 'Manufacturing') {
        $my_others->setScore($_SESSION['email'] , "Buz_sector_Manufacturing") ;
    }
    if($_POST['business_sector'] == 'Agriculture') {
        $my_others->setScore($_SESSION['email'] , "Buz_sector_Agriculture") ;
    }
    if($_POST['business_sector'] == 'Services') {
        $my_others->setScore($_SESSION['email'] , "Buz_sector_Services") ;
    }

}else{
    $my_others->deleteSetScore($_SESSION['email'] , "business_sector") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_sector_Mining") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_sector_Manufacturing") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_sector_Agriculture") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_sector_Services") ;
}
if(@$_POST['nature_of_business'] != "" ){
    $my_others->setScore($_SESSION['email'] , "nature_of_business") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Extractive") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Manufacturing") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Services") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Retail") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Other") ;
    if($_POST['nature_of_business'] == 'Extractive') {
        $my_others->setScore($_SESSION['email'] , "Buz_Nature_Extractive") ;
    }
    if($_POST['nature_of_business'] == 'Manufacturing') {
        $my_others->setScore($_SESSION['email'] , "Buz_Nature_Manufacturing") ;
    }
    if($_POST['nature_of_business'] == 'Services') {
        $my_others->setScore($_SESSION['email'] , "Buz_Nature_Services") ;
    }
    if($_POST['nature_of_business'] == 'Retail') {
        $my_others->setScore($_SESSION['email'] , "Buz_Nature_Retail") ;
    }
    if($_POST['nature_of_business'] == 'Other') {
        $my_others->setScore($_SESSION['email'] , "Buz_Nature_Other") ;
    }

}else{
    $my_others->deleteSetScore($_SESSION['email'] , "nature_of_business") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Extractive") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Manufacturing") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Services") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Retail") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Buz_Nature_Other") ;
}
if(@$_POST['business_nature'] != "" ){
    $my_others->setScore($_SESSION['email'] , "business_nature") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "business_nature") ;
}
if(@$_POST['telephone'] != "" ){
    $my_others->setScore($_SESSION['email'] , "telephone") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "telephone") ;
}
if(@$_POST['fax_number'] != "" ){
    $my_others->setScore($_SESSION['email'] , "fax_number") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "fax_number") ;
}
if(@$_POST['registered_office_physical_address'] != "" ){
    $my_others->setScore($_SESSION['email'] , "registered_office_physical_address") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "registered_office_physical_address") ;
}
if(@$_POST['postal_address'] != "" ){
    $my_others->setScore($_SESSION['email'] , "postal_address") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "postal_address") ;
}
if(@$_POST['principal_place_of_business'] != "" ){
    $my_others->setScore($_SESSION['email'] , "principal_place_of_business") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "principal_place_of_business") ;
}
if(@$_POST['raised_equity'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_equity") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_equity") ;
}
if(@$_POST['raised_equity_bd'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_equity_bd") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_equity_bd") ;
}
if(@$_POST['raised_equity_gwc'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_equity_gwc") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_equity_gwc") ;
}
if(@$_POST['raised_equity_capex'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_equity_capex") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_equity_capex") ;
}
//raised Debt
if(@$_POST['raised_debt'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_debt") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_debt") ;
}
if(@$_POST['raised_debt_bd'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_debt_bd") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_debt_bd") ;
}
if(@$_POST['raised_debt_gwc'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_debt_gwc") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_debt_gwc") ;
}
if(@$_POST['raised_debt_capex'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_debt_capex") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_debt_capex") ;
}

if(@$_POST['raised_other'] != "" ){
    $my_others->setScore($_SESSION['email'] , "raised_other") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "raised_other") ;
}
if(@$_POST['purpose_of_funds'] != "" ){
    $my_others->setScore($_SESSION['email'] , "purpose_of_funds") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "purpose_of_funds") ;
}
if(@$_POST['share_on_offer'] != "" ){
    $my_others->setScore($_SESSION['email'] , "share_on_offer") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "share_on_offer") ;
}

$u = (new controlDAO())->getUser();

$_SESSION['company_name'] =  @$_POST['company_name'] ;
if(isset($_POST['fax_number'])){
   $faxvaildate= ctype_alpha(@$_POST['fax_number']);
   if ($faxvaildate)
   {

      die(header("Location: ../../stage_1_capital_requirements.php?fax_error=Fax number cannot contain alphabet characters") );
   }
}
if(isset($_POST['company_registration_number'])){
   $faxvaildate= ctype_alpha(@$_POST['company_registration_number']);
   if ($faxvaildate)
   {

      die(header("Location: ../../stage_1_capital_requirements.php?reg_error=Registration  number cannot contain alphabet characters") );
   }
}
if(isset($_POST['telephone'])){
   $faxvaildate= ctype_alpha(@$_POST['telephone']);
   if ($faxvaildate)
   {

      die(header("Location: ../../stage_1_capital_requirements.php?tele_error=Telephone  number cannot contain alphabet characters") );
   }
}

if (isset($_POST['create'])){

    $c_u = createCapitalReq(@$_POST['user_id'] , @$_POST['company_name'] , @$_POST['company_registration_number'] ,
        @$_POST['date_of_incorporation'] , @$_POST['country_of_incorporation'] , @$_POST['region_of_incorporation'] , @$_POST['type_of_entity'] ,
        @$_POST['business_sector'] , @$_POST['nature_of_business'] , @$_POST['business_nature'] , @$_POST['telephone'] ,
        @$_POST['fax_number'] , @$_POST['registered_office_physical_address'] , @$_POST['postal_address'] , @$_POST['principal_place_of_business'] ,
        @$_POST['raised_equity'], @$_POST['raised_equity_capex'] ,@$_POST['raised_equity_bd'] ,@$_POST['raised_equity_gwc']  ,
        @$_POST['raised_debt'] ,@$_POST['raised_debt_capex'] , @$_POST['raised_debt_bd'], @$_POST['raised_debt_gwc']  ,
        @$_POST['raised_other'] , @$_POST['purpose_of_funds'],@$_POST['share_on_offer']) ;

    if($c_u){
        echo"Created successfully " ;
        header("Location: ../../stage_1_corporate_directory.php") ;
    }else{
        echo "failed to create user " ;
    }
}
if (isset($_POST['update'])){

    $c_u = updateCapitalReq(@$_POST['user_id'] , @$_POST['company_name'] , @$_POST['company_registration_number'] ,
        @$_POST['date_of_incorporation'] , @$_POST['country_of_incorporation'] ,@$_POST['region_of_incorporation'] , @$_POST['type_of_entity'] ,
        @$_POST['business_sector'] , @$_POST['nature_of_business'] , @$_POST['business_nature'] , @$_POST['telephone'] ,
        @$_POST['fax_number'] , @$_POST['registered_office_physical_address'] , @$_POST['postal_address'] ,
        @$_POST['principal_place_of_business'] ,
        @$_POST['raised_equity'] ,@$_POST['raised_equity_capex'] ,@$_POST['raised_equity_bd'] ,@$_POST['raised_equity_gwc']  ,
        @$_POST['raised_debt']  ,@$_POST['raised_debt_capex']  , @$_POST['raised_debt_bd'],@$_POST['raised_debt_gwc'] ,
        @$_POST['raised_other'] , @$_POST['purpose_of_funds'], @$_POST['capital_req_id'],@$_POST['share_on_offer']) ;

    if($c_u){
        echo"Updated successfully " ;
       header("Location: ../../stage_1_corporate_directory.php") ;
    }else{
        echo "failed to create user " ;
    }
}


function createCapitalReq($user_id,$company_name,$company_registration_number,$date_of_incorporation,$country_of_incorporation,$region_of_incorporation,
                          $type_of_entity,$business_sector,$nature_of_business,$business_nature,$telephone,$fax_number,
                          $registered_office_physical_address,$postal_address,$principal_place_of_business,
                          $raised_equity, $raised_equity_capex, $raised_equity_bd, $raised_equity_gwc,
                          $raised_debt,$raised_debt_capex,$raised_debt_bd,$raised_debt_gwc,
                          $raised_other,$purpose_of_funds,$share_on_offer){
    include("../models/capitalRequirements.php") ;
    $uc  = new capitalRequirements();
    $uc->setuser_id($user_id) ;
    $uc->setcompany_name($company_name) ;
    $uc->setcompany_registration_number($company_registration_number) ;
    $uc->setdate_of_incorporation($date_of_incorporation) ;
    $uc->setcountry_of_incorporation($country_of_incorporation) ;
    $uc->setregion_of_incorporation($region_of_incorporation) ;
    $uc->settype_of_entity($type_of_entity) ;
    $uc->setbusiness_sector($business_sector) ;
    $uc->setnature_of_business($nature_of_business) ;
    $uc->setbusiness_nature($business_nature) ;
    $uc->settelephone($telephone) ;
    $uc->setfax_number($fax_number) ;
    $uc->setregistered_office_physical_address($registered_office_physical_address) ;
    $uc->setpostal_address($postal_address) ;
    $uc->setprincipal_place_of_business($principal_place_of_business) ;
    $uc->setraised_equity($raised_equity) ;
    $uc->setraised_equity_capex($raised_equity_capex) ;
    $uc->setraised_equity_bd($raised_equity_bd) ;
    $uc->setraised_equity_gwc($raised_equity_gwc) ;
    $uc->setraised_debt($raised_debt) ;
    $uc->setraised_debt_capex($raised_debt_capex) ;
    $uc->setraised_debt_bd($raised_debt_bd) ;
    $uc->setraised_debt_gwc($raised_debt_gwc) ;
    $uc->setraised_other($raised_other) ;
    $uc->setpurpose_of_funds($purpose_of_funds) ;
    $uc->setshare_on_offer($share_on_offer);
    return (new controlDAO())->getCapitalRequirements()->createcapitalRequirements($uc) ;

}
function updateCapitalReq($user_id,$company_name,$company_registration_number,$date_of_incorporation,$country_of_incorporation,$region_of_incorporation,
                          $type_of_entity,$business_sector,$nature_of_business,$business_nature,$telephone,$fax_number,
                          $registered_office_physical_address,$postal_address,$principal_place_of_business,
                          $raised_equity, $raised_equity_capex, $raised_equity_bd, $raised_equity_gwc,
                          $raised_debt,$raised_debt_capex,$raised_debt_bd,$raised_debt_gwc,
                          $raised_other,$purpose_of_funds,$capital_req_id,$share_on_offer){
    include("../models/capitalRequirements.php") ;
    $uc  = new capitalRequirements();
    $uc->setuser_id($user_id) ;
    $uc->setcompany_name($company_name) ;
    $uc->setcompany_registration_number($company_registration_number) ;
    $uc->setdate_of_incorporation($date_of_incorporation) ;
    $uc->setcountry_of_incorporation($country_of_incorporation) ;
    $uc->setregion_of_incorporation($region_of_incorporation) ;
    $uc->settype_of_entity($type_of_entity) ;
    $uc->setbusiness_sector($business_sector) ;
    $uc->setnature_of_business($nature_of_business) ;
    $uc->setbusiness_nature($business_nature) ;
    $uc->settelephone($telephone) ;
    $uc->setfax_number($fax_number) ;
    $uc->setregistered_office_physical_address($registered_office_physical_address) ;
    $uc->setpostal_address($postal_address) ;
    $uc->setprincipal_place_of_business($principal_place_of_business) ;
    $uc->setraised_equity($raised_equity) ;
    $uc->setraised_equity_capex($raised_equity_capex) ;
    $uc->setraised_equity_bd($raised_equity_bd) ;
    $uc->setraised_equity_gwc($raised_equity_gwc) ;
    $uc->setraised_debt($raised_debt) ;
    $uc->setraised_debt_capex($raised_debt_capex) ;
    $uc->setraised_debt_bd($raised_debt_bd) ;
    $uc->setraised_debt_gwc($raised_debt_gwc) ;
    $uc->setraised_other($raised_other) ;
    $uc->setpurpose_of_funds($purpose_of_funds) ;
    $uc->setcapital_req_id($capital_req_id) ;
    $uc->setshare_on_offer($share_on_offer) ;
    return (new controlDAO())->getCapitalRequirements()->updateCapitalRequirements($uc) ;

}
// */