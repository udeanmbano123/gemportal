<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:33 AM
 */





include '../controlDAO.php' ;

//$u = (new controlDAO())->getUser();

if (isset($_POST['create_customer'])){

    $c_u =  createCustomers(@$_POST['user_id'] ,@$_POST['customer_name'] ,@$_POST['customer_location'],@$_POST['customer_years_rel'],@$_POST['customer_sales'],@$_POST['customer_trade_terms'],@$_POST['customer_distr_channel']) ;
    if($c_u){
       // echo"Product created successfully " ;
        
       header("Location: ../../stage_1_overview_of_company_grid.php?customer_msg=Customer created successfully") ;
    }else{
        echo "failed to create product " ;
    }

}
if (isset($_GET['delete_customer'])){
    // echo "Delete product". @$_GET['id'] ; 
    $c_u = deleteCustomers(@$_GET['id']) ;
    if($c_u){
       echo"Customer deleted successfully " ;
        header("Location: ../../stage_1_overview_of_company_grid.php?customer_msg=Customer deleted successfully") ;
    }else{
        echo "failed to delete product " ;
    }

}
if (isset($_POST['edit_customer'])){

 

    $c_u = editCustomers(@$_POST['company_overview_customers_id'] ,@$_POST['customer_name'] ,@$_POST['customer_location'],@$_POST['customer_years_rel'],@$_POST['customer_sales'],@$_POST['customer_trade_terms'],@$_POST['customer_distr_channel']) ;
    if($c_u){
        
          header("Location: ../../stage_1_overview_of_company_grid.php?customer_msg=Customer edited  successfully") ;
    }else{
        echo "failed to edit product " ;
    }

}


function createCustomers($user_id,$customer_name,$customer_location,$customer_years_rel,$customer_sales,$customer_trade_terms,$customer_distr_channel){
    include("../models/companyOverviewCustomers.php") ;
    $uc  = new companyOverviewCustomers();
    $uc->setuser_id($user_id);
    $uc->setcustomer_name($customer_name);
    $uc->setcustomer_location($customer_location);
    $uc->setcustomer_years_rel($customer_years_rel);
    $uc->setcustomer_sales($customer_sales);
    $uc->setcustomer_trade_terms($customer_trade_terms);
    $uc->setcustomer_distr_channel($customer_distr_channel);
    
    return (new controlDAO())->getcompanyOverviewCustomers()->createcompanyOverviewCustomers($uc) ;

}
function editCustomers($company_overview_customers_id,$customer_name,$customer_location,$customer_years_rel,$customer_sales,$customer_trade_terms,$customer_distr_channel){
    include("../models/companyOverviewCustomers.php") ;
    
    $uc  = new companyOverviewCustomers();
    $uc->setcompany_overview_customers_id($company_overview_customers_id);
    $uc->setcustomer_name($customer_name);
    $uc->setcustomer_location($customer_location);
    $uc->setcustomer_years_rel($customer_years_rel);
    $uc->setcustomer_sales($customer_sales);
    $uc->setcustomer_trade_terms($customer_trade_terms);
    $uc->setcustomer_distr_channel($customer_distr_channel);   
    //return false ;
    return (new controlDAO())->getcompanyOverviewCustomers()->updatecompanyOverviewCustomers($uc) ;

}
function deleteCustomers($company_overview_customers_id){
    include("../models/companyOverviewCustomers.php") ;
    $uc  = new companyOverviewCustomers();
    $uc->setcompany_overview_customers_id($company_overview_customers_id) ;
    return (new controlDAO())->getcompanyOverviewCustomers()->deletecompanyOverviewCustomers($uc) ;

}