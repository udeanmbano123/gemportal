<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:33 AM
 */


include '../controlDAO.php' ;

//$u = (new controlDAO())->getUser();
session_start() ;

$my_others = (new controlDAO())->getOthers() ;

if(@$_POST['staff_levels'] != ""){
    $my_others->setScore($_SESSION['email'] , "staff_levels") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Employees_Up_to_5_employees") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Employees_6_to_40_employees") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Employees_41_to_75_employees") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Employees_76_and_above") ;
    if($_POST['staff_levels'] == "Up to 5 employees") {
        $my_others->setScore($_SESSION['email'] , "Employees_Up_to_5_employees") ;
    }
    if($_POST['staff_levels'] == "6 to 40 employees") {
        $my_others->setScore($_SESSION['email'] , "Employees_6_to_40_employees") ;
    }
    if($_POST['staff_levels'] == "41 to 75 employees") {
        $my_others->setScore($_SESSION['email'] , "Employees_41_to_75_employees") ;
    }
    if($_POST['staff_levels'] == "76 and above") {
        $my_others->setScore($_SESSION['email'] , "Employees_76_and_above") ;
    }
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "staff_levels") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Employees_Up_to_5_employees") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Employees_6_to_40_employees") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Employees_41_to_75_employees") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Employees_76_and_above") ;
}
if(@$_POST['annual_turnover'] != ""){
    $my_others->setScore($_SESSION['email'] , "annual_turnover") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Annual_turnover_Up_to_50000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Annual_turnover_50001_to_500000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Annual_turnover_500001_to_1000000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Annual_turnover_1000001_and_above") ;
    if($_POST['annual_turnover'] == "Up to $50,000") {
        $my_others->setScore($_SESSION['email'] , "Annual_turnover_Up_to_50000") ;
    }
    if($_POST['annual_turnover'] == "$50,001 to $500,000") {
        $my_others->setScore($_SESSION['email'] , "Annual_turnover_50001_to_500000") ;
    }
    if($_POST['annual_turnover'] == "$500,001 to $1,000,000") {
        $my_others->setScore($_SESSION['email'] , "Annual_turnover_500001_to_1000000") ;
    }
    if($_POST['annual_turnover'] == "$1,000,001 and above") {
        $my_others->setScore($_SESSION['email'] , "Annual_turnover_1000001_and_above") ;
    }

}else{
    $my_others->deleteSetScore($_SESSION['email'] , "annual_turnover") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Annual_turnover_Up_to_50000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Annual_turnover_50001_to_500000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Annual_turnover_500001_to_1000000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Annual_turnover_1000001_and_above") ;
}
if(@$_POST['gross_value_of_assets'] != ""){
    $my_others->setScore($_SESSION['email'] , "gross_value_of_assets") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Gross_Value_Up_to_50000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Gross_Value_50001_to_1000000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Gross_Value_1000001_to_2000000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Gross_Value_2000001_and_above") ;
    if($_POST['gross_value_of_assets'] == "Up to $50,000") {
        $my_others->setScore($_SESSION['email'] , "Gross_Value_Up_to_50000") ;
    }
    if($_POST['gross_value_of_assets'] == "$50,001 to $1,000,000") {
        $my_others->setScore($_SESSION['email'] , "Gross_Value_50001_to_1000000") ;
    }
    if($_POST['gross_value_of_assets'] == "$1,000,001 to $2,000,000") {
        $my_others->setScore($_SESSION['email'] , "Gross_Value_1000001_to_2000000") ;
    }
    if($_POST['gross_value_of_assets'] == "$2,000,001 and above") {
        $my_others->setScore($_SESSION['email'] , "Gross_Value_2000001_and_above") ;
    }

}else{
    $my_others->deleteSetScore($_SESSION['email'] , "gross_value_of_assets") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Gross_Value_Up_to_50000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Gross_Value_50001_to_1000000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Gross_Value_1000001_to_2000000") ;
    $my_others->deleteSetScore($_SESSION['email'] , "Gross_Value_2000001_and_above") ;
}

if (isset($_POST['create'])){

    $c_u = createCorpDir(@$_POST['user_id'], @$_POST['staff_levels'], @$_POST['annual_turnover'], @$_POST['gross_value_of_assets']) ;

    if($c_u){
        echo"Created successfully " ;
        header("Location: ../../stage_1_overview_of_company.php") ;
    }else{
        echo "failed to save " ;
    }
}
if (isset($_POST['update'])){

    $c_u = updateCorpDir(@$_POST['user_id'], @$_POST['staff_levels'], @$_POST['annual_turnover'], @$_POST['gross_value_of_assets'], @$_POST['enterprise_size_categorisation_id'] ) ;

    if($c_u){
        echo"Updated successfully " ;
        header("Location: ../../stage_1_overview_of_company.php") ;
    }else{
        echo "failed to create user " ;
    }
}


function createCorpDir($user_id , $staff_levels , $annual_turnover , $gross_value_of_assets){
    include("../models/enterpriseSizeCategorisation.php") ;
    $uc  = new enterpriseSizeCategorisation();
    $uc->setuser_id($user_id) ;
    $uc->setstaff_levels($staff_levels) ;
    $uc->setannual_turnover($annual_turnover) ;
    $uc->setgross_value_of_assets($gross_value_of_assets) ;
    return (new controlDAO())->getenterpriseSizeCategorisation()->createEnterpriseSizeCategorisation($uc) ;

}
function updateCorpDir( $user_id , $staff_levels , $annual_turnover , $gross_value_of_assets ,$enterprise_size_categorisation_id ){
    include("../models/enterpriseSizeCategorisation.php") ;
    $uc  = new enterpriseSizeCategorisation();
    $uc->setenterprise_size_categorisation_id($enterprise_size_categorisation_id);
    $uc->setuser_id($user_id) ;
    $uc->setstaff_levels($staff_levels) ;
    $uc->setannual_turnover($annual_turnover) ;
    $uc->setgross_value_of_assets($gross_value_of_assets) ;
    return (new controlDAO())->getenterpriseSizeCategorisation()->updateEnterpriseSizeCategorisation($uc) ;

}