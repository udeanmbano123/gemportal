<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 12/6/2018
 * Time: 2:08 AM
 */


include '../controlDAO.php' ;
session_start() ;

$my_others = (new controlDAO())->getOthers() ;
if(@$_POST['list_of_directors'] != "" ){
    $my_others->setScore($_SESSION['email'] , "list_of_directors") ;
}else{
    $my_others->deleteSetScore($_SESSION['email'] , "list_of_directors") ;
}
if($_POST['directors_count'] >= 7){
    $my_others->setScore($_SESSION['email'] , "directors_more_than_7_directors") ;
}else if($_POST['directors_count'] >= 3 && $_POST['directors_count'] <= 6){
    $my_others->setScore($_SESSION['email'] , "directors_6_to_3_directors") ;
}
else if($_POST['directors_count'] < 3){
    $my_others->setScore($_SESSION['email'] , "directors_less_than_3_directors") ;
}else{
    echo "" ;
}
header("Location: ../../stage_2_directors_shareholding.php") ;