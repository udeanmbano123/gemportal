<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:33 AM
 */





include '../controlDAO.php' ;

//$u = (new controlDAO())->getUser();

if (isset($_POST['create_raw'])){

    $c_u =  createMajorRawMaterials(@$_POST['user_id'] ,@$_POST['name'],@$_POST['desc'],@$_POST['volume']) ;
    if($c_u){
       // echo"Product created successfully " ;
        
       header("Location: ../../stage_1_overview_of_company_grid.php?raw_msg=Raw Materials created successfully") ;
    }else{
        echo "failed to create product " ;
    }

}
if (isset($_GET['delete_raw'])){
    // echo "Delete product". @$_GET['id'] ; 
    $c_u = deleteMajorRawMaterials(@$_GET['id']) ;
    if($c_u){
       echo"Product deleted successfully " ;
        header("Location: ../../stage_1_overview_of_company_grid.php?raw_msg=Raw Materials deleted successfully") ;
    }else{
        echo "failed to delete product " ;
    }

}
if (isset($_POST['edit_raw'])){

 

    $c_u = editMajorRawMaterials(@$_POST['company_overview_raw_materials_id'] ,@$_POST['name'] ,@$_POST['desc'],@$_POST['volume']) ;
    if($c_u){
        
          header("Location: ../../stage_1_overview_of_company_grid.php?raw_msg=Raw material edited  successfully") ;
    }else{
        echo "failed to edit product " ;
    }

}


function createMajorRawMaterials($user_id,$name,$desc,$volume){
    include("../models/companyOverviewRawMaterials.php") ;
    $uc  = new companyOverviewRawMaterials();
    $uc->setuser_id($user_id);
    $uc->setname($name);
    $uc->setdesc($desc);
    $uc->setvolume($volume);
    $uc->setcompany_overview_id("");
    
    return (new controlDAO())->getcompanyOverviewRawMaterials()->createcompanyOverviewRawMaterials($uc) ;

}
function editMajorRawMaterials($company_overview_raw_materials_id ,$name,$desc,$volume){
    include("../models/companyOverviewRawMaterials.php") ;
    $uc  = new companyOverviewRawMaterials();
    $uc->setname($name);
    $uc->setdesc($desc);
    $uc->setvolume($volume);    
    $uc->setcompany_overview_raw_materials_id($company_overview_raw_materials_id);    
    //return false ;
    return (new controlDAO())->getcompanyOverviewRawMaterials()->updatecompanyOverviewRawMaterials($uc) ;

}
function deleteMajorRawMaterials($company_overview_raw_materials_id){
    include("../models/companyOverviewRawMaterials.php") ;
    $uc  = new companyOverviewRawMaterials();
    $uc->setcompany_overview_raw_materials_id($company_overview_raw_materials_id) ;
    return (new controlDAO())->getcompanyOverviewRawMaterials()->deletecompanyOverviewRawMaterials($uc) ;

}