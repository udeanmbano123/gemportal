<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:33 AM
 */





include '../controlDAO.php' ;

//$u = (new controlDAO())->getUser();
@$type=$_POST['type'];
@$year=$_POST['year'];
@$income=$_POST['income'];
@$gincome=$_POST['gincome'];
@$sales=$_POST['sales'];
@$gross_profit=$_POST['gprofit'];
@$net_profit=$_POST['nprofit'];
@$user_id=$_POST['user_id'];
@$tab_id=$_POST['id'];
@$delete_id=$_GET['id'];
if(!@$tab_id  and !@$delete_id){

    $action='create';
}
if (@$delete_id)
{
    $operation='delete';
}

//$u = (new controlDAO())->getUser();

if (@$action){

    $c_u =  createIncomeStatement(@$user_id ,@$type,@$year,@$income,@$gincome ,@$sales,@$gross_profit,@$net_profit) ;
    if($c_u){
       // echo"Product created successfully " ;
        
       header("Location: ../../stage_3_projected_financials.php?msg=Income Statement  added  successfully") ;
    }else{
        echo "failed to create product " ;
    }

}
if (@$operation){
    // echo "Delete product". @$_GET['id'] ; 
    $c_u = deleteIncomeStatement(@$_GET['id']) ;
    if($c_u){
      
        header("Location: ../../stage_3_projected_financials.php?msg=Income Statement  deleted  successfully") ;
    }else{
        echo "failed to delete product " ;
    }

}
if (@$tab_id){

 

    $c_u = editIncomeStatement(@$tab_id ,@$type,@$year,@$income,@$gincome ,@$sales,@$gross_profit,@$net_profit) ;
    if($c_u){
        
          header("Location: ../../stage_3_projected_financials.php?msg=Income Statement  edited  successfully") ;
    }else{
        echo "failed to edit product " ;
    }

}


function createIncomeStatement($user_id ,$type,$year,$income,$gincome ,$sales,$gross_profit,$net_profit){
    include("../models/IncomeStatement.php") ;
    $uc  = new IncomeStatement();
    $uc->setuser_id($user_id);
    $uc->setincome_statement_type($type);
    $uc-> setyear($year);    
    $uc->setgross_income_per($income);
    $uc->setgross_income($gincome);
    $uc->setcost_of_sales($sales);
    $uc-> setgross_profit($gross_profit);
    $uc-> setnet_profit($net_profit);
   
    return (new controlDAO())->getincomeStatement()->createincomeStatement($uc)  ;

}
function editIncomeStatement($tab_id ,$type,$year,$income ,$gincome,$sales,$gross_profit,$net_profit){
   include("../models/IncomeStatement.php") ;
    $uc  = new IncomeStatement();
    $uc->setincome_statement_id($tab_id);
    $uc->setincome_statement_type($type);
    $uc-> setyear($year);    
    $uc->setgross_income_per($income);
    $uc->setgross_income($gincome);
    $uc->setcost_of_sales($sales);
    $uc-> setgross_profit($gross_profit);
    $uc-> setnet_profit($net_profit);
   
    return (new controlDAO())->getincomeStatement()->updateincomeStatement($uc)   ;
       
    //return false ;
    

}
function deleteIncomeStatement($tab_id){
    include("../models/IncomeStatement.php") ;
    $uc  = new IncomeStatement();
    $uc->setincome_statement_id($tab_id) ;
    return (new controlDAO())->getincomeStatement()->deleteincomeStatement($uc)   ;

}

?>