<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:33 AM
 */

include '../controlDAO.php' ;


session_start() ;
$my_others = (new controlDAO())->getOthers() ;
try {
    if (isset($_POST['company_overview_history']) != ""){
        $my_others->setScore($_SESSION['email'] , "company_overview_history") ;
    }else{
        $my_others->deleteSetScore($_SESSION['email'] , "company_overview_history") ;
    }
}
//catch exception
catch(Exception $e) {

}
try {
    if (isset($_POST['products']) != ""){
        $my_others->setScore($_SESSION['email'] , "products") ;
    }else{
        $my_others->deleteSetScore($_SESSION['email'] , "products") ;
    }
}

//catch exception
catch(Exception $e) {

}
try {
    if (isset($_POST['rawmaterials']) != ""){
        $my_others->setScore($_SESSION['email'] , "rawmaterials") ;
    }else{
        $my_others->deleteSetScore($_SESSION['email'] , "rawmaterials") ;
    }
}

//catch exception
catch(Exception $e) {

}
try {
    if (isset($_POST['distributionchannels']) != ""){
        $my_others->setScore($_SESSION['email'] , "distributionchannels") ;
    }else{
        $my_others->deleteSetScore($_SESSION['email'] , "distributionchannels") ;
    }
}

//catch exception
catch(Exception $e) {

}
try {
    if (isset($_POST['supplychannels']) != ""){
        $my_others->setScore($_SESSION['email'] , "supplychannels") ;
    }else{
        $my_others->deleteSetScore($_SESSION['email'] , "supplychannels") ;
    }
}

//catch exception
catch(Exception $e) {

}
try {
    if (isset($_POST['company_overview_rights']) != ""){
        $my_others->setScore($_SESSION['email'] , "company_overview_rights") ;
    }else {
        $my_others->deleteSetScore($_SESSION['email'], "company_overview_rights");
    }
}

//catch exception
catch(Exception $e) {

}
try {
    if(isset($_POST['company_overview_count_products']) >= 5){
        $my_others->setScore($_SESSION['email'] , "product_more_than_5_products") ;
    }else if(isset($_POST['company_overview_count_products']) >= 2 && $_POST['company_overview_count_products'] <= 4){
        $my_others->setScore($_SESSION['email'] , "product_2_to_4_products") ;
    }
    else if(isset($_POST['company_overview_count_products']) == 1){
        $my_others->setScore($_SESSION['email'] , "product_reliance_on_1_product") ;
    }else{

    }
}

//catch exception
catch(Exception $e) {

}


try {
    if(isset($_POST['company_overview_count_raw_materials']) >= 5){
        $my_others->setScore($_SESSION['email'] , "raw_material_more_than_5_products") ;
    }else if(isset($_POST['company_overview_count_raw_materials']) >= 2 && $_POST['company_overview_count_raw_materials'] <= 4){
        $my_others->setScore($_SESSION['email'] , "raw_material_2_to_4_products") ;
    }
    else if(isset($_POST['company_overview_count_raw_materials']) == 1){
        $my_others->setScore($_SESSION['email'] , "raw_material_reliance_on_1_product") ;
    }else{

    }
}

//catch exception
catch(Exception $e) {

}


if (isset($_POST['create'])){
    if(!empty($_FILES['company_overview_rights']['name']))
    {
        $path = "upload/";
        $temp = explode(".", $_FILES["company_overview_rights"]["name"]);
        $newfilename = round(microtime(true)).'.'. end($temp);
        $db_path ="$path".$newfilename  ;

        $path = $path . basename( $_FILES['company_overview_rights']['name']);
        if(move_uploaded_file($_FILES['company_overview_rights']['tmp_name'], $db_path)) {
            echo "The file ".  basename( $_FILES['company_overview_rights']['name']).
                " has been uploaded";
        } else{
            echo "There was an error uploading the file, please try again!";
        }



      }
    if(!empty(@$db_path)) {
        $c_u = createCorpDir(@$_POST['user_id'], @$_POST['company_overview_history'], "func/controller/" . @$db_path);
    }
    if($c_u){
        echo "Created successfully " ;
        header("Location: ../../stage_1_overview_of_company_grid.php") ;
    }else{
        header("Location: ../../stage_1_overview_of_company_grid.php") ;
    }
}
if (isset($_POST['update'])){

    if(!empty($_FILES['company_overview_rights']['name']))
    {
        $path = "upload/";
        $temp = explode(".", $_FILES["company_overview_rights"]["name"]);
        $newfilename = round(microtime(true)).'.'. end($temp);
        $db_path ="$path".$newfilename  ;

        if(move_uploaded_file($_FILES['company_overview_rights']['tmp_name'], $db_path)) {
            echo "The file ".  basename( $_FILES['company_overview_rights']['name']).
                " has been uploaded";
        } else{
            echo "There was an error uploading the file, please try again!";
        }



    }
    if(!empty(@$db_path)) {
        $c_u = updateCorpDir(@$_POST['user_id'], @$_POST['company_overview_history'], "func/controller/" . @$db_path, @$_POST['company_overview_id']);
    }
    if($c_u){
        echo"Updated successfully " ;
        header("Location: ../../stage_1_overview_of_company_grid.php") ;
    }else{

        header("Location: ../../stage_1_overview_of_company_grid.php") ;
    }
}


function createCorpDir($user_id ,$company_overview_history , $company_overview_rights ){
    include("../models/companyOverview.php") ;
    $uc  = new companyOverview();
    $uc->setuser_id($user_id) ;
    $uc->setcompany_overview_history($company_overview_history) ;
    $uc->setcompany_overview_rights($company_overview_rights) ;
    return (new controlDAO())->getcompanyOverview()->createCompanyOverview($uc) ;

}
function updateCorpDir( $user_id , $company_overview_history , $company_overview_rights,$company_overview_id ){
    include("../models/companyOverview.php") ;
    $uc  = new companyOverview();
    $uc->setuser_id($user_id) ;
    $uc->setcompany_overview_history($company_overview_history) ;
    $uc->setcompany_overview_rights($company_overview_rights) ;
    $uc->setcompany_overview_id($company_overview_id) ;
    return (new controlDAO())->getcompanyOverview()->updateCompanyOverview($uc) ;

}

?>