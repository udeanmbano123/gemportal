<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 28/2/2018
 * Time: 9:33 AM
 */





include '../controlDAO.php' ;

//$u = (new controlDAO())->getUser();

if (isset($_POST['create_prod'])){

    $c_u = createProductnService(@$_POST['user_id'] ,@$_POST['product_service_name'],@$_POST['description_of_product'],@$_POST['sales_volume']) ;
    if($c_u){
       // echo"Product created successfully " ;
       header("Location: ../../stage_1_overview_of_company_grid.php?msg=Product created successfully") ;
    }else{
        echo "failed to create product " ;
    }

}
if (isset($_GET['delete_prod'])){
    // echo "Delete product". @$_GET['id'] ; 
    $c_u = deleteProductnService(@$_GET['id']) ;
    if($c_u){
       echo"Product deleted successfully " ;
        header("Location: ../../stage_1_overview_of_company_grid.php?msg=Product deleted successfully") ;
    }else{
        echo "failed to delete product " ;
    }

}
if (isset($_POST['edit_prod'])){



    $c_u = editProductnService(@$_POST['company_overview_products_services_id'] ,@$_POST['user_id'] ,@$_POST['product_service_name'],@$_POST['description_of_product'],@$_POST['sales_volume']) ;
    if($c_u){
         header("Location: ../../stage_1_overview_of_company_grid.php?msg=Product edited  successfully") ;
    }else{
        echo "failed to edit product " ;
    }

}


function createProductnService($user_id ,$product_service_name,$description_of_product,$sales_volume){
    include("../models/companyOverviewProductsServices.php") ;
    $uc  = new companyOverviewProductsServices();
    $uc->setuser_id($user_id) ;
    $uc->setproduct_service_name($product_service_name) ;
    $uc->setdescription_of_product($description_of_product) ;
    $uc->setsales_volume($sales_volume) ;
    return (new controlDAO())->getcompanyOverviewProductsServices()->createcompanyOverviewProductsServices($uc) ;

}
function editProductnService($company_overview_products_services_id ,$user_id ,$product_service_name,$description_of_product,$sales_volume){
    include("../models/companyOverviewProductsServices.php") ;
    $uc  = new companyOverviewProductsServices();
    $uc->setuser_id($user_id);
    $uc->setcompany_overview_products_services_id($company_overview_products_services_id) ;
    $uc->setproduct_service_name($product_service_name) ;
    $uc->setdescription_of_product($description_of_product) ;
    $uc->setsales_volume($sales_volume) ;
    //return false ;
    return (new controlDAO())->getcompanyOverviewProductsServices()->updatecompanyOverviewProductsServices($uc) ;

}
function deleteProductnService($company_overview_products_services_id){
    include("../models/companyOverviewProductsServices.php") ;
    $uc  = new companyOverviewProductsServices();
    $uc->setcompany_overview_products_services_id($company_overview_products_services_id) ;
    return (new controlDAO())->getcompanyOverviewProductsServices()->deletecompanyOverviewProductsServices($uc) ;

}