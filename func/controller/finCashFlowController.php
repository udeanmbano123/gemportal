<?php
session_start();
include '../controlDAO.php' ;

if (isset($_POST['create'])){
    if(!empty($_FILES['upload_file']['name']))
    {
        $path = "upload/";
        $temp = explode(".", $_FILES["upload_file"]["name"]);
        $newfilename = round(microtime(true)).'.'. end($temp);
        $db_path ="$path".$newfilename  ;

        $path = $path . basename( $_FILES['upload_file']['name']);
        if(move_uploaded_file($_FILES['upload_file']['tmp_name'], $db_path)) {
            echo "The file ".  basename( $_FILES['upload_file']['name']).
                " has been uploaded";
        } else{
            echo "There was an error uploading the file, please try again!";
        }
    }

    $c_u = createCorpDir(@$_POST['user_id'], @$_POST['is_audited'], "func/controller/".@$db_path) ;

    if($c_u){
        echo"Created successfully " ;
        header("Location: ../../stage_3_fin_cashflow.php") ;
    }else{
        echo "failed to save " ;
    }

}
if (isset($_POST['update'])){

    if(!empty($_FILES['upload_file']['name']))
    {
        $path = "upload/";
        $temp = explode(".", $_FILES["upload_file"]["name"]);
        $newfilename = round(microtime(true)).'.'. end($temp);
        $db_path ="$path".$newfilename  ;

        if(move_uploaded_file($_FILES['upload_file']['tmp_name'], $db_path)) {
            echo "The file ".  basename( $_FILES['upload_file']['name']).
                " has been uploaded";
        } else{
            echo "There was an error uploading the file, please try again!";
        }
    }

    $c_u = updateCorpDir(@$_POST['user_id'], @$_POST['is_audited'], "func/controller/".@$db_path, @$_POST['id'] ) ;

    if($c_u){
        echo"Updated successfully " ;
        header("Location: ../../stage_3_fin_cashflow.php") ;
    }else{
        echo "failed to upload file " ;
    }
}


function createCorpDir($user_id , $is_audited , $upload_file ){
    include("../models/fin_cash_flow_upload.php") ;
    $uc  = new fin_cash_flow_upload();
    $uc->setUserId($user_id) ;
    $uc->setIsAudited($is_audited) ;
    $uc->setDocUrl($upload_file) ;
    return (new controlDAO())->getuploadFinancials()->createuploadFinancialsCashFlow($uc) ;

}
function updateCorpDir( $user_id , $is_audited , $upload_file,$id ){
    include("../models/fin_cash_flow_upload.php") ;
    $uc  = new fin_cash_flow_upload();
    $uc->setUserId($user_id) ;
    $uc->setDocUrl($upload_file) ;
    $uc->setIsAudited($is_audited) ;
    $uc->setId($id) ;
    return (new controlDAO())->getuploadFinancials()->updateuploadFinancialsCashFlow($uc) ;

}


