<?php

/**
 * Handling database connection
 *
 * @author Tinashe Makaza
 * @link URL Tutorial link
 */
class DbConnect {

    private $conn;

    function __construct() {        
    }

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
        include_once('Config.php');

        // Connecting to mysql database
        $this->conn = new mysqli("localhost", "root", "", "capitalonline");

        // Check for database connection error
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        // returing connection resource
        return $this->conn;
    }

}
include("db.class.php");

// Open the base (construct the object):
$base="capitalonline";
$server="localhost";
$user="root";
$pass="";
$db = new DB($base, $server, $user, $pass);

?>
