<?php

class balanceSheet{
    var $balance_sheet_id ;
    var $user_id ;
    var $balance_sheet_type ;
    var $year ;
    var $fixed_assets ;
    var $current_assets ;
    var $total_assets ;
    var $equity_and_liability ;
    var $capital_and_reserves ;
    var $issued_share_capital ;
    var $share_premium ;
    var $revenue_reserves ;
    var $capital_reserves ;
    var $equity_attributed ;
    var $non_controlling_interest ;
    var $shareholders_equity ;
    var $non_current_liabilities ;
    var $long_term_borrowings ;
    var $deffered_tax_liabilities ;
    var $short_term_borrowings ;
    var $trade_and_other_payab ;
    var $current_tax_liability ;
    var $total_equity_and_liabilities ;
    var $current_liabilities ;
    var $other_liabilities ;
    var $total_liabilities ;
    var $net_assetsles ;

    /**
     * @return mixed
     */
    public function getBalanceSheetId()
    {
        return $this->balance_sheet_id;
    }

    /**
     * @param mixed $balance_sheet_id
     */
    public function setBalanceSheetId($balance_sheet_id)
    {
        $this->balance_sheet_id = $balance_sheet_id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getBalanceSheetType()
    {
        return $this->balance_sheet_type;
    }

    /**
     * @param mixed $balance_sheet_type
     */
    public function setBalanceSheetType($balance_sheet_type)
    {
        $this->balance_sheet_type = $balance_sheet_type;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getFixedAssets()
    {
        return $this->fixed_assets;
    }

    /**
     * @param mixed $fixed_assets
     */
    public function setFixedAssets($fixed_assets)
    {
        $this->fixed_assets = $fixed_assets;
    }

    /**
     * @return mixed
     */
    public function getCurrentAssets()
    {
        return $this->current_assets;
    }

    /**
     * @param mixed $current_assets
     */
    public function setCurrentAssets($current_assets)
    {
        $this->current_assets = $current_assets;
    }

    /**
     * @return mixed
     */
    public function getTotalAssets()
    {
        return $this->total_assets;
    }

    /**
     * @param mixed $total_assets
     */
    public function setTotalAssets($total_assets)
    {
        $this->total_assets = $total_assets;
    }

    /**
     * @return mixed
     */
    public function getEquityAndLiability()
    {
        return $this->equity_and_liability;
    }

    /**
     * @param mixed $equity_and_liability
     */
    public function setEquityAndLiability($equity_and_liability)
    {
        $this->equity_and_liability = $equity_and_liability;
    }

    /**
     * @return mixed
     */
    public function getCapitalAndReserves()
    {
        return $this->capital_and_reserves;
    }

    /**
     * @param mixed $capital_and_reserves
     */
    public function setCapitalAndReserves($capital_and_reserves)
    {
        $this->capital_and_reserves = $capital_and_reserves;
    }

    /**
     * @return mixed
     */
    public function getIssuedShareCapital()
    {
        return $this->issued_share_capital;
    }

    /**
     * @param mixed $issued_share_capital
     */
    public function setIssuedShareCapital($issued_share_capital)
    {
        $this->issued_share_capital = $issued_share_capital;
    }

    /**
     * @return mixed
     */
    public function getSharePremium()
    {
        return $this->share_premium;
    }

    /**
     * @param mixed $share_premium
     */
    public function setSharePremium($share_premium)
    {
        $this->share_premium = $share_premium;
    }

    /**
     * @return mixed
     */
    public function getRevenueReserves()
    {
        return $this->revenue_reserves;
    }

    /**
     * @param mixed $revenue_reserves
     */
    public function setRevenueReserves($revenue_reserves)
    {
        $this->revenue_reserves = $revenue_reserves;
    }

    /**
     * @return mixed
     */
    public function getCapitalReserves()
    {
        return $this->capital_reserves;
    }

    /**
     * @param mixed $capital_reserves
     */
    public function setCapitalReserves($capital_reserves)
    {
        $this->capital_reserves = $capital_reserves;
    }

    /**
     * @return mixed
     */
    public function getEquityAttributed()
    {
        return $this->equity_attributed;
    }

    /**
     * @param mixed $equity_attributed
     */
    public function setEquityAttributed($equity_attributed)
    {
        $this->equity_attributed = $equity_attributed;
    }

    /**
     * @return mixed
     */
    public function getNonControllingInterest()
    {
        return $this->non_controlling_interest;
    }

    /**
     * @param mixed $non_controlling_interest
     */
    public function setNonControllingInterest($non_controlling_interest)
    {
        $this->non_controlling_interest = $non_controlling_interest;
    }

    /**
     * @return mixed
     */
    public function getShareholdersEquity()
    {
        return $this->shareholders_equity;
    }

    /**
     * @param mixed $shareholders_equity
     */
    public function setShareholdersEquity($shareholders_equity)
    {
        $this->shareholders_equity = $shareholders_equity;
    }

    /**
     * @return mixed
     */
    public function getNonCurrentLiabilities()
    {
        return $this->non_current_liabilities;
    }

    /**
     * @param mixed $non_current_liabilities
     */
    public function setNonCurrentLiabilities($non_current_liabilities)
    {
        $this->non_current_liabilities = $non_current_liabilities;
    }

    /**
     * @return mixed
     */
    public function getLongTermBorrowings()
    {
        return $this->long_term_borrowings;
    }

    /**
     * @param mixed $long_term_borrowings
     */
    public function setLongTermBorrowings($long_term_borrowings)
    {
        $this->long_term_borrowings = $long_term_borrowings;
    }

    /**
     * @return mixed
     */
    public function getDefferedTaxLiabilities()
    {
        return $this->deffered_tax_liabilities;
    }

    /**
     * @param mixed $deffered_tax_liabilities
     */
    public function setDefferedTaxLiabilities($deffered_tax_liabilities)
    {
        $this->deffered_tax_liabilities = $deffered_tax_liabilities;
    }

    /**
     * @return mixed
     */
    public function getShortTermBorrowings()
    {
        return $this->short_term_borrowings;
    }

    /**
     * @param mixed $short_term_borrowings
     */
    public function setShortTermBorrowings($short_term_borrowings)
    {
        $this->short_term_borrowings = $short_term_borrowings;
    }

    /**
     * @return mixed
     */
    public function getTradeAndOtherPayab()
    {
        return $this->trade_and_other_payab;
    }

    /**
     * @param mixed $trade_and_other_payab
     */
    public function setTradeAndOtherPayab($trade_and_other_payab)
    {
        $this->trade_and_other_payab = $trade_and_other_payab;
    }

    /**
     * @return mixed
     */
    public function getCurrentTaxLiability()
    {
        return $this->current_tax_liability;
    }

    /**
     * @param mixed $current_tax_liability
     */
    public function setCurrentTaxLiability($current_tax_liability)
    {
        $this->current_tax_liability = $current_tax_liability;
    }

    /**
     * @return mixed
     */
    public function getTotalEquityAndLiabilities()
    {
        return $this->total_equity_and_liabilities;
    }

    /**
     * @param mixed $total_equity_and_liabilities
     */
    public function setTotalEquityAndLiabilities($total_equity_and_liabilities)
    {
        $this->total_equity_and_liabilities = $total_equity_and_liabilities;
    }

    /**
     * @return mixed
     */
    public function getCurrentLiabilities()
    {
        return $this->current_liabilities;
    }

    /**
     * @param mixed $current_liabilities
     */
    public function setCurrentLiabilities($current_liabilities)
    {
        $this->current_liabilities = $current_liabilities;
    }

    /**
     * @return mixed
     */
    public function getOtherLiabilities()
    {
        return $this->other_liabilities;
    }

    /**
     * @param mixed $other_liabilities
     */
    public function setOtherLiabilities($other_liabilities)
    {
        $this->other_liabilities = $other_liabilities;
    }

    /**
     * @return mixed
     */
    public function getTotalLiabilities()
    {
        return $this->total_liabilities;
    }

    /**
     * @param mixed $total_liabilities
     */
    public function setTotalLiabilities($total_liabilities)
    {
        $this->total_liabilities = $total_liabilities;
    }

    /**
     * @return mixed
     */
    public function getNetAssetsles()
    {
        return $this->net_assetsles;
    }

    /**
     * @param mixed $net_assetsles
     */
    public function setNetAssetsles($net_assetsles)
    {
        $this->net_assetsles = $net_assetsles;
    }


}

?>