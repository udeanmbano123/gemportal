<?php 

class directors{

	var $director_id ; 
	var $user_id ; 
	var $title ; 
	var $forename ; 
	var $surname ; 
	var $age ; 
	var $citizenship ; 
	var $address ; 
    public function getdirector_id() {
        return $this->director_id;
    }

    public function setdirector_id( $director_id) {
        $this->director_id = $director_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function gettitle() {
        return $this->title;
    }

    public function settitle( $title) {
        $this->title = $title;
    }

    public function getforename() {
        return $this->forename;
    }

    public function setforename( $forename) {
        $this->forename = $forename;
    }

    public function getsurname() {
        return $this->surname;
    }

    public function setsurname( $surname) {
        $this->surname = $surname;
    }

    public function getage() {
        return $this->age;
    }

    public function setage( $age) {
        $this->age = $age;
    }

    public function getcitizenship() {
        return $this->citizenship;
    }

    public function setcitizenship( $citizenship) {
        $this->citizenship = $citizenship;
    }

    public function getaddress() {
        return $this->address;
    }

    public function setaddress( $address) {
        $this->address = $address;
    }


}
$u = new directors() ; 
$u->setuser_id("Tinashe Makaza") ; 
//echo  $u->getuser_id() ;


?>