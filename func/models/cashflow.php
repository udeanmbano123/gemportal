<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 19/4/2018
 * Time: 12:00 PM
 */

class cashflow{
    var $cash_flow_id ;
    var $user_id ;
    var $year ;
    var $is_audited ;
    var $operating_activities ;
    var $profit_or_loss_before_taxation_from_discontinued_operations ;
    var $profit_or_loss_before_taxation_from_continued_operations ;
    var $profit_or_loss_before_tax ;
    var $adjusted_for ;
    var $depreciation ;
    var $impairments ;
    var $ammorisations ;
    var $profit_or_loss_on_disposal_of_assets ;
    var $finance_income ;
    var $income_tax_paid ;
    var $working_capital_adjustments ;
    var $increase_decrease_in_inventories ;
    var $increase_decrease_in_debtors ;
    var $increase_decrease_in_creditors ;
    var $net_cashflows_from_operating_activities ;
    var $purchase_of_fixed_assets_on_maintaning_operations ;
    var $initial_capital_paid_in ;
    var $purchase_sale_long_term_financial_securities ;
    var $purchase_of_land ;
    var $increase_decrease_in_loans_and_investments ;
    var $proceeds_from_disposal_from_property_and_equipment ;
    var $interest_recieved_or_paid ;
    var $net_cashflows_from_investing_activities ;
    var $net_increase_in_cash_and_cash_equivalents ;
    var $cash_and_cash_equivalents_at_the_beginning_of_the_year ;
    var $cash_and_cash_equivalents_at_the_end_of_the_period ;

    /**
     * @return mixed
     */
    public function getCashFlowId()
    {
        return $this->cash_flow_id;
    }

    /**
     * @param mixed $cash_flow_id
     */
    public function setCashFlowId($cash_flow_id)
    {
        $this->cash_flow_id = $cash_flow_id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getisAudited()
    {
        return $this->is_audited;
    }

    /**
     * @param mixed $is_audited
     */
    public function setIsAudited($is_audited)
    {
        $this->is_audited = $is_audited;
    }

    /**
     * @return mixed
     */
    public function getOperatingActivities()
    {
        return $this->operating_activities;
    }

    /**
     * @param mixed $operating_activities
     */
    public function setOperatingActivities($operating_activities)
    {
        $this->operating_activities = $operating_activities;
    }

    /**
     * @return mixed
     */
    public function getProfitOrLossBeforeTaxationFromDiscontinuedOperations()
    {
        return $this->profit_or_loss_before_taxation_from_discontinued_operations;
    }

    /**
     * @param mixed $profit_or_loss_before_taxation_from_discontinued_operations
     */
    public function setProfitOrLossBeforeTaxationFromDiscontinuedOperations($profit_or_loss_before_taxation_from_discontinued_operations)
    {
        $this->profit_or_loss_before_taxation_from_discontinued_operations = $profit_or_loss_before_taxation_from_discontinued_operations;
    }

    /**
     * @return mixed
     */
    public function getProfitOrLossBeforeTaxationFromContinuedOperations()
    {
        return $this->profit_or_loss_before_taxation_from_continued_operations;
    }

    /**
     * @param mixed $profit_or_loss_before_taxation_from_continued_operations
     */
    public function setProfitOrLossBeforeTaxationFromContinuedOperations($profit_or_loss_before_taxation_from_continued_operations)
    {
        $this->profit_or_loss_before_taxation_from_continued_operations = $profit_or_loss_before_taxation_from_continued_operations;
    }

    /**
     * @return mixed
     */
    public function getProfitOrLossBeforeTax()
    {
        return $this->profit_or_loss_before_tax;
    }

    /**
     * @param mixed $profit_or_loss_before_tax
     */
    public function setProfitOrLossBeforeTax($profit_or_loss_before_tax)
    {
        $this->profit_or_loss_before_tax = $profit_or_loss_before_tax;
    }

    /**
     * @return mixed
     */
    public function getAdjustedFor()
    {
        return $this->adjusted_for;
    }

    /**
     * @param mixed $adjusted_for
     */
    public function setAdjustedFor($adjusted_for)
    {
        $this->adjusted_for = $adjusted_for;
    }

    /**
     * @return mixed
     */
    public function getDepreciation()
    {
        return $this->depreciation;
    }

    /**
     * @param mixed $depreciation
     */
    public function setDepreciation($depreciation)
    {
        $this->depreciation = $depreciation;
    }

    /**
     * @return mixed
     */
    public function getImpairments()
    {
        return $this->impairments;
    }

    /**
     * @param mixed $impairments
     */
    public function setImpairments($impairments)
    {
        $this->impairments = $impairments;
    }

    /**
     * @return mixed
     */
    public function getAmmorisations()
    {
        return $this->ammorisations;
    }

    /**
     * @param mixed $ammorisations
     */
    public function setAmmorisations($ammorisations)
    {
        $this->ammorisations = $ammorisations;
    }

    /**
     * @return mixed
     */
    public function getProfitOrLossOnDisposalOfAssets()
    {
        return $this->profit_or_loss_on_disposal_of_assets;
    }

    /**
     * @param mixed $profit_or_loss_on_disposal_of_assets
     */
    public function setProfitOrLossOnDisposalOfAssets($profit_or_loss_on_disposal_of_assets)
    {
        $this->profit_or_loss_on_disposal_of_assets = $profit_or_loss_on_disposal_of_assets;
    }

    /**
     * @return mixed
     */
    public function getFinanceIncome()
    {
        return $this->finance_income;
    }

    /**
     * @param mixed $finance_income
     */
    public function setFinanceIncome($finance_income)
    {
        $this->finance_income = $finance_income;
    }

    /**
     * @return mixed
     */
    public function getIncomeTaxPaid()
    {
        return $this->income_tax_paid;
    }

    /**
     * @param mixed $income_tax_paid
     */
    public function setIncomeTaxPaid($income_tax_paid)
    {
        $this->income_tax_paid = $income_tax_paid;
    }

    /**
     * @return mixed
     */
    public function getWorkingCapitalAdjustments()
    {
        return $this->working_capital_adjustments;
    }

    /**
     * @param mixed $working_capital_adjustments
     */
    public function setWorkingCapitalAdjustments($working_capital_adjustments)
    {
        $this->working_capital_adjustments = $working_capital_adjustments;
    }

    /**
     * @return mixed
     */
    public function getIncreaseDecreaseInInventories()
    {
        return $this->increase_decrease_in_inventories;
    }

    /**
     * @param mixed $increase_decrease_in_inventories
     */
    public function setIncreaseDecreaseInInventories($increase_decrease_in_inventories)
    {
        $this->increase_decrease_in_inventories = $increase_decrease_in_inventories;
    }

    /**
     * @return mixed
     */
    public function getIncreaseDecreaseInDebtors()
    {
        return $this->increase_decrease_in_debtors;
    }

    /**
     * @param mixed $increase_decrease_in_debtors
     */
    public function setIncreaseDecreaseInDebtors($increase_decrease_in_debtors)
    {
        $this->increase_decrease_in_debtors = $increase_decrease_in_debtors;
    }

    /**
     * @return mixed
     */
    public function getIncreaseDecreaseInCreditors()
    {
        return $this->increase_decrease_in_creditors;
    }

    /**
     * @param mixed $increase_decrease_in_creditors
     */
    public function setIncreaseDecreaseInCreditors($increase_decrease_in_creditors)
    {
        $this->increase_decrease_in_creditors = $increase_decrease_in_creditors;
    }

    /**
     * @return mixed
     */
    public function getNetCashflowsFromOperatingActivities()
    {
        return $this->net_cashflows_from_operating_activities;
    }

    /**
     * @param mixed $net_cashflows_from_operating_activities
     */
    public function setNetCashflowsFromOperatingActivities($net_cashflows_from_operating_activities)
    {
        $this->net_cashflows_from_operating_activities = $net_cashflows_from_operating_activities;
    }

    /**
     * @return mixed
     */
    public function getPurchaseOfFixedAssetsOnMaintaningOperations()
    {
        return $this->purchase_of_fixed_assets_on_maintaning_operations;
    }

    /**
     * @param mixed $purchase_of_fixed_assets_on_maintaning_operations
     */
    public function setPurchaseOfFixedAssetsOnMaintaningOperations($purchase_of_fixed_assets_on_maintaning_operations)
    {
        $this->purchase_of_fixed_assets_on_maintaning_operations = $purchase_of_fixed_assets_on_maintaning_operations;
    }

    /**
     * @return mixed
     */
    public function getInitialCapitalPaidIn()
    {
        return $this->initial_capital_paid_in;
    }

    /**
     * @param mixed $initial_capital_paid_in
     */
    public function setInitialCapitalPaidIn($initial_capital_paid_in)
    {
        $this->initial_capital_paid_in = $initial_capital_paid_in;
    }

    /**
     * @return mixed
     */
    public function getPurchaseSaleLongTermFinancialSecurities()
    {
        return $this->purchase_sale_long_term_financial_securities;
    }

    /**
     * @param mixed $purchase_sale_long_term_financial_securities
     */
    public function setPurchaseSaleLongTermFinancialSecurities($purchase_sale_long_term_financial_securities)
    {
        $this->purchase_sale_long_term_financial_securities = $purchase_sale_long_term_financial_securities;
    }

    /**
     * @return mixed
     */
    public function getPurchaseOfLand()
    {
        return $this->purchase_of_land;
    }

    /**
     * @param mixed $purchase_of_land
     */
    public function setPurchaseOfLand($purchase_of_land)
    {
        $this->purchase_of_land = $purchase_of_land;
    }

    /**
     * @return mixed
     */
    public function getIncreaseDecreaseInLoansAndInvestments()
    {
        return $this->increase_decrease_in_loans_and_investments;
    }

    /**
     * @param mixed $increase_decrease_in_loans_and_investments
     */
    public function setIncreaseDecreaseInLoansAndInvestments($increase_decrease_in_loans_and_investments)
    {
        $this->increase_decrease_in_loans_and_investments = $increase_decrease_in_loans_and_investments;
    }

    /**
     * @return mixed
     */
    public function getProceedsFromDisposalFromPropertyAndEquipment()
    {
        return $this->proceeds_from_disposal_from_property_and_equipment;
    }

    /**
     * @param mixed $proceeds_from_disposal_from_property_and_equipment
     */
    public function setProceedsFromDisposalFromPropertyAndEquipment($proceeds_from_disposal_from_property_and_equipment)
    {
        $this->proceeds_from_disposal_from_property_and_equipment = $proceeds_from_disposal_from_property_and_equipment;
    }

    /**
     * @return mixed
     */
    public function getInterestRecievedOrPaid()
    {
        return $this->interest_recieved_or_paid;
    }

    /**
     * @param mixed $interest_recieved_or_paid
     */
    public function setInterestRecievedOrPaid($interest_recieved_or_paid)
    {
        $this->interest_recieved_or_paid = $interest_recieved_or_paid;
    }

    /**
     * @return mixed
     */
    public function getNetCashflowsFromInvestingActivities()
    {
        return $this->net_cashflows_from_investing_activities;
    }

    /**
     * @param mixed $net_cashflows_from_investing_activities
     */
    public function setNetCashflowsFromInvestingActivities($net_cashflows_from_investing_activities)
    {
        $this->net_cashflows_from_investing_activities = $net_cashflows_from_investing_activities;
    }

    /**
     * @return mixed
     */
    public function getNetIncreaseInCashAndCashEquivalents()
    {
        return $this->net_increase_in_cash_and_cash_equivalents;
    }

    /**
     * @param mixed $net_increase_in_cash_and_cash_equivalents
     */
    public function setNetIncreaseInCashAndCashEquivalents($net_increase_in_cash_and_cash_equivalents)
    {
        $this->net_increase_in_cash_and_cash_equivalents = $net_increase_in_cash_and_cash_equivalents;
    }

    /**
     * @return mixed
     */
    public function getCashAndCashEquivalentsAtTheBeginningOfTheYear()
    {
        return $this->cash_and_cash_equivalents_at_the_beginning_of_the_year;
    }

    /**
     * @param mixed $cash_and_cash_equivalents_at_the_beginning_of_the_year
     */
    public function setCashAndCashEquivalentsAtTheBeginningOfTheYear($cash_and_cash_equivalents_at_the_beginning_of_the_year)
    {
        $this->cash_and_cash_equivalents_at_the_beginning_of_the_year = $cash_and_cash_equivalents_at_the_beginning_of_the_year;
    }

    /**
     * @return mixed
     */
    public function getCashAndCashEquivalentsAtTheEndOfThePeriod()
    {
        return $this->cash_and_cash_equivalents_at_the_end_of_the_period;
    }

    /**
     * @param mixed $cash_and_cash_equivalents_at_the_end_of_the_period
     */
    public function setCashAndCashEquivalentsAtTheEndOfThePeriod($cash_and_cash_equivalents_at_the_end_of_the_period)
    {
        $this->cash_and_cash_equivalents_at_the_end_of_the_period = $cash_and_cash_equivalents_at_the_end_of_the_period;
    }


}