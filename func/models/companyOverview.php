<?php
class companyOverview{
	var $company_overview_id ;
	var $user_id ;
	var $company_overview_history ;
	var $company_overview_rights	;

    public function getcompany_overview_id() {
        return $this->company_overview_id;
    }

    public function setcompany_overview_id( $company_overview_id) {
        $this->company_overview_id = $company_overview_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getcompany_overview_history() {
        return $this->company_overview_history;
    }

    public function setcompany_overview_history( $company_overview_history) {
        $this->company_overview_history = $company_overview_history;
    }

    public function getcompany_overview_rights() {
        return $this->company_overview_rights;
    }

    public function setcompany_overview_rights( $company_overview_rights) {
        $this->company_overview_rights = $company_overview_rights;
    }


}




?>