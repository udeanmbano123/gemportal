<?php 

class sectors{

    
    var $name ; 
    var $user_id;
    var $role ; 
    var $company_id ; 
   
    public function getname() {
        return $this->name;
    }

    public function setname( $name) {
        $this->name = $name;
    }
    public function getcompany_id() {
        return $this->company_id;
    }

    public function setcompany_id( $company_id) {
        $this->company_id = $company_id;
    }
     public function getrole() {
        return $this->role;
    }

    public function setrole( $role) {
        $this->role= $role;
    }
    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }
}
?>