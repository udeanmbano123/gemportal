<?php 

class enterpriseSizeCategorisation{

	var $enterprise_size_categorisation_id ; 
	var $user_id ; 
	var $staff_levels ; 
	var $annual_turnover ; 
	var $gross_value_of_assets ; 
	
    public function getenterprise_size_categorisation_id() {
        return $this->enterprise_size_categorisation_id;
    }

    public function setenterprise_size_categorisation_id( $enterprise_size_categorisation_id) {
        $this->enterprise_size_categorisation_id = $enterprise_size_categorisation_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getstaff_levels() {
        return $this->staff_levels;
    }

    public function setstaff_levels( $staff_levels) {
        $this->staff_levels = $staff_levels;
    }

    public function getannual_turnover() {
        return $this->annual_turnover;
    }

    public function setannual_turnover( $annual_turnover) {
        $this->annual_turnover = $annual_turnover;
    }

    public function getgross_value_of_assets() {
        return $this->gross_value_of_assets;
    }

    public function setgross_value_of_assets( $gross_value_of_assets) {
        $this->gross_value_of_assets = $gross_value_of_assets;
    }
}
$u = new enterpriseSizeCategorisation() ; 
$u->setuser_id("Tinashe Makaza") ; 
//echo  $u->getuser_id() ;


?>