<?php

class Users{
    var $users_id;
	var $users_type;
	var $users_fullname;
	var $users_username;
	var $users_password;
	var $users_email;
	var $users_phonenumber;
	var $users_position;
	var $users_status;
	var $users_created_date;
	var $users_company_name;
	var $users_company_regnum;
	var $users_company_dateofincorp;
	var $users_company_country;
	var $users_form_name;
    var $stage;

    public function setusers_id( $users_id) {
        $this->users_id = $users_id;
    }
    public function  getusers_id() {
        return $this->users_id;
    }
     public function setstage( $stage) {
        $this->stage = $stage;
    }
    public function  getstage() {
        return $this->stage;
    }

    public function getusers_fullname() {
        return $this->users_fullname;
    }

    public function  setusers_fullname( $users_fullname) {
        $this->users_fullname = $users_fullname;
    }

    public function  getusers_username() {
        return $this->users_username;
    }

    public function  setusers_username( $users_username) {
        $this->users_username = $users_username;
    }

    public function  getusers_password() {
        return $this->users_password;
    }

    public function  setusers_password( $users_password) {
        $this->users_password = $users_password;
    }

    public function  getusers_email() {
        return $this->users_email;
    }

    public function  setusers_email( $users_email) {
        $this->users_email = $users_email;
    }

    public function  getusers_phonenumber() {
        return $this->users_phonenumber;
    }

    public function  setusers_phonenumber( $users_phonenumber) {
        $this->users_phonenumber = $users_phonenumber;
    }

    public function  getusers_position() {
        return $this->users_position;
    }

    public function  setusers_position( $users_position) {
        $this->users_position = $users_position;
    }

    public function  getusers_status() {
        return $this->users_status;
    }

    public function  setusers_status( $users_status) {
        $this->users_status = $users_status;
    }

    public function  getusers_created_date() {
        return $this->users_created_date;
    }

    public function  setusers_created_date( $users_created_date) {
        $this->users_created_date = $users_created_date;
    }

    public function  getusers_company_name() {
        return $this->users_company_name;
    }

    public function  setusers_company_name( $users_company_name) {
        $this->users_company_name = $users_company_name;
    }

    public function  getusers_company_regnum() {
        return $this->users_company_regnum;
    }

    public function  setusers_company_regnum( $users_company_regnum) {
        $this->users_company_regnum = $users_company_regnum;
    }

    public function  getusers_company_dateofincorp() {
        return $this->users_company_dateofincorp;
    }

    public function  setusers_company_dateofincorp( $users_company_dateofincorp) {
        $this->users_company_dateofincorp = $users_company_dateofincorp;
    }

    public function  getusers_company_country() {
        return $this->users_company_country;
    }

    public function  setusers_company_country( $users_company_country) {
        $this->users_company_country = $users_company_country;
    }

    public function  getusers_form_name() {
        return $this->users_form_name;
    }

    public function  setusers_form_name( $users_form_name) {
        $this->users_form_name = $users_form_name;
    }
    public function  setusers_type( $user_type) {
        $this->users_type = $user_type;
    }
    
    public function  getusers_type() {
        return $this->users_type;
    }



}


// $u  = new Users();
// $u->setusers_id("Tinashe Makaza") ; 

// echo $u->getusers_id() ; 


?>