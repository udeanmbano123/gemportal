<?php 

class companyOverviewRawMaterials{
	var $company_overview_raw_materials_id ;
	var $user_id ;
	var $company_overview_id ;
	var $name ;
	var $desc ;
	var $volume ;
    public function getcompany_overview_raw_materials_id() {
        return $this->company_overview_raw_materials_id;
    }

    public function setcompany_overview_raw_materials_id( $company_overview_raw_materials_id) {
        $this->company_overview_raw_materials_id = $company_overview_raw_materials_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getcompany_overview_id() {
        return $this->company_overview_id;
    }

    public function setcompany_overview_id( $company_overview_id) {
        $this->company_overview_id = $company_overview_id;
    }

    public function getname() {
        return $this->name;
    }

    public function setname( $name) {
        $this->name = $name;
    }

    public function getdesc() {
        return $this->desc;
    }

    public function setdesc( $desc) {
        $this->desc = $desc;
    }

    public function getvolume() {
        return $this->volume;
    }

    public function setvolume( $volume) {
        $this->volume = $volume;
    }
}
$u = new companyOverviewRawMaterials() ; 
$u->setuser_id("Tinashe Makaza") ; 
//echo  $u->getuser_id() ;


?>