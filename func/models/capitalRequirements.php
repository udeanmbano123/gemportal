<?php

class capitalRequirements{

	var $capital_req_id ; 
	var $user_id ; 
	var $company_name ; 
	var $company_registration_number ; 
	var $date_of_incorporation ; 
	var $country_of_incorporation ; 
	var $region_of_incorporation ;
	var $type_of_entity ;
	var $business_sector ; 
	var $nature_of_business ; 
	var $business_nature ; 
	var $telephone ; 
	var $fax_number ; 
	var $registered_office_physical_address ; 
	var $postal_address ; 
	var $principal_place_of_business ; 
	var $raised_equity ; 
	var $raised_equity_capex ;
	var $raised_equity_bd ;
	var $raised_equity_gwc ;
	var $raised_debt ;
	var $raised_debt_capex ;
	var $raised_debt_bd ;
	var $raised_debt_gwc ;
	var $raised_other ;
	var $purpose_of_funds ;
    var $share_on_offer ;  

    public function getcapital_req_id() {
        return $this->capital_req_id;
    }

    public function setcapital_req_id( $capital_req_id) {
        $this->capital_req_id = $capital_req_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }
    public function getshare_on_offer() {
        return $this->share_on_offer;
    }

    public function setshare_on_offer( $share_on_offer) {
        $this->share_on_offer = $share_on_offer;
    }

    public function getcompany_name() {
        return $this->company_name;
    }

    public function setcompany_name( $company_name) {
        $this->company_name = $company_name;
    }

    public function getcompany_registration_number() {
        return $this->company_registration_number;
    }

    public function setcompany_registration_number( $company_registration_number) {
        $this->company_registration_number = $company_registration_number;
    }

    public function getdate_of_incorporation() {
        return $this->date_of_incorporation;
    }

    public function setdate_of_incorporation( $date_of_incorporation) {
        $this->date_of_incorporation = $date_of_incorporation;
    }

    public function getcountry_of_incorporation() {
        return $this->country_of_incorporation;
    }

    public function setcountry_of_incorporation( $country_of_incorporation) {
        $this->country_of_incorporation = $country_of_incorporation;
    }
    public function getregion_of_incorporation() {
        return $this->region_of_incorporation;
    }

    public function setregion_of_incorporation( $region_of_incorporation) {
        $this->region_of_incorporation = $region_of_incorporation;
    }

    public function gettype_of_entity() {
        return $this->type_of_entity;
    }

    public function settype_of_entity( $type_of_entity) {
        $this->type_of_entity = $type_of_entity;
    }

    public function getbusiness_sector() {
        return $this->business_sector;
    }

    public function setbusiness_sector( $business_sector) {
        $this->business_sector = $business_sector;
    }

    public function getnature_of_business() {
        return $this->nature_of_business;
    }

    public function setnature_of_business( $nature_of_business) {
        $this->nature_of_business = $nature_of_business;
    }

    public function getbusiness_nature() {
        return $this->business_nature;
    }

    public function setbusiness_nature( $business_nature) {
        $this->business_nature = $business_nature;
    }

    public function gettelephone() {
        return $this->telephone;
    }

    public function settelephone( $telephone) {
        $this->telephone = $telephone;
    }

    public function getfax_number() {
        return $this->fax_number;
    }

    public function setfax_number( $fax_number) {
        $this->fax_number = $fax_number;
    }

    public function getregistered_office_physical_address() {
        return $this->registered_office_physical_address;
    }

    public function setregistered_office_physical_address( $registered_office_physical_address) {
        $this->registered_office_physical_address = $registered_office_physical_address;
    }

    public function getpostal_address() {
        return $this->postal_address;
    }

    public function setpostal_address( $postal_address) {
        $this->postal_address = $postal_address;
    }

    public function getprincipal_place_of_business() {
        return $this->principal_place_of_business;
    }

    public function setprincipal_place_of_business( $principal_place_of_business) {
        $this->principal_place_of_business = $principal_place_of_business;
    }

    public function getraised_equity() {
        return $this->raised_equity;
    }

    public function setraised_equity( $raised_equity) {
        $this->raised_equity = $raised_equity;
    }
    public function getraised_equity_capex() {
        return $this->raised_equity_capex;
    }

    public function setraised_equity_capex( $raised_equity_capex) {
        $this->raised_equity_capex = $raised_equity_capex;
    }
    public function getraised_equity_bd() {
        return $this->raised_equity_bd;
    }

    public function setraised_equity_bd( $raised_equity_bd) {
        $this->raised_equity_bd = $raised_equity_bd;
    }
    public function getraised_equity_gwc() {
        return $this->raised_equity_gwc;
    }

    public function setraised_equity_gwc( $raised_equity_gwc) {
        $this->raised_equity_gwc = $raised_equity_gwc;
    }

    public function getraised_debt() {
        return $this->raised_debt;
    }

    public function setraised_debt( $raised_debt) {
        $this->raised_debt = $raised_debt;
    }

    public function getraised_debt_capex() {
        return $this->raised_debt_capex;
    }

    public function setraised_debt_capex( $raised_debt_capex) {
        $this->raised_debt_capex = $raised_debt_capex;
    }
    public function getraised_debt_bd() {
        return $this->raised_debt_bd;
    }

    public function setraised_debt_bd( $raised_debt_bd) {
        $this->raised_debt_bd = $raised_debt_bd;
    }
    public function getraised_debt_gwc() {
        return $this->raised_debt_gwc;
    }

    public function setraised_debt_gwc( $raised_debt_gwc) {
        $this->raised_debt_gwc = $raised_debt_gwc;
    }

    public function getraised_other() {
        return $this->raised_other;
    }

    public function setraised_other( $raised_other) {
        $this->raised_other = $raised_other;
    }

    public function getpurpose_of_funds() {
        return $this->purpose_of_funds;
    }

    public function setpurpose_of_funds( $purpose_of_funds) {
        $this->purpose_of_funds = $purpose_of_funds;
    }

}

$u = new capitalRequirements();
$u->setpurpose_of_funds("Tinashe Makaza") ; 
//echo $u->getpurpose_of_funds() ;



?>