<?php 

class committees{

	var $committees_id ; 
	var $user_id ; 
	var $committee_name ; 
	var $chairman ; 
	var $attendees ; 
	var $quoraum ; 
	var $meeting_frequency ; 
	var $committee_responsibilities ; 

    public function getcommittees_id() {
       return $this->committees_id;
    }

    public function setcommittees_id( $committees_id) {
        $this->committees_id = $committees_id;
    }

    public function getuser_id() {
       return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getcommittee_name() {
       return $this->committee_name;
    }

    public function setcommittee_name( $committee_name) {
        $this->committee_name = $committee_name;
    }

    public function getchairman() {
       return $this->chairman;
    }

    public function setchairman( $chairman) {
        $this->chairman = $chairman;
    }

    public function getattendees() {
       return $this->attendees;
    }

    public function setattendees( $attendees) {
        $this->attendees = $attendees;
    }

    public function getquoraum() {
       return $this->quoraum;
    }

    public function setquoraum( $quoraum) {
        $this->quoraum = $quoraum;
    }

    public function getmeeting_frequency() {
       return $this->meeting_frequency;
    }

    public function setmeeting_frequency( $meeting_frequency) {
        $this->meeting_frequency = $meeting_frequency;
    }

    public function getcommittee_responsibilities() {
       return $this->committee_responsibilities;
    }

    public function setcommittee_responsibilities( $committee_responsibilities) {
        $this->committee_responsibilities = $committee_responsibilities;
    }

}

$u  = new committees() ; 
$u->setquoraum("Tinashe Makaza") ; 
//echo $u->getquoraum() ;

?>