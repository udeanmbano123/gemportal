<?php 

class directorsShareholding{

	var $directors_shareholding_id ; 
	var $user_id ; 
	var $directors_name ; 
	var $direct_indirect_equity_interest ; 
	var $shareholding ; 
	
    public function getdirectors_shareholding_id() {
        return $this->directors_shareholding_id;
    }

    public function setdirectors_shareholding_id( $directors_shareholding_id) {
        $this->directors_shareholding_id = $directors_shareholding_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getdirectors_name() {
        return $this->directors_name;
    }

    public function setdirectors_name( $directors_name) {
        $this->directors_name = $directors_name;
    }

    public function getdirect_indirect_equity_interest() {
        return $this->direct_indirect_equity_interest;
    }

    public function setdirect_indirect_equity_interest( $direct_indirect_equity_interest) {
        $this->direct_indirect_equity_interest = $direct_indirect_equity_interest;
    }

    public function getshareholding() {
        return $this->shareholding;
    }

    public function setshareholding( $shareholding) {
        $this->shareholding = $shareholding;
    }

}

$u = new directorsShareholding() ; 
$u->setuser_id("Tinashe Makaza") ; 
//echo  $u->getuser_id() ;


?>