<?php


class personalFinancial{

	var $personal_financial_id;
	var $user_id;
	var $guarantor_name;
	var $financial_institution;
	var $facility_type;
	var $facility_amount;
	var $monthly_installment;
	var $balance;
	var $tenure;
	var $start_date	;

    public function getPersonal_financial_id() {
        return $this->personal_financial_id;
    }

    public function setPersonal_financial_id( $personal_financial_id) {
        $this->personal_financial_id = $personal_financial_id;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function setUser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getGuarantor_name() {
        return $this->guarantor_name;
    }

    public function setGuarantor_name( $guarantor_name) {
        $this->guarantor_name = $guarantor_name;
    }

    public function getFinancial_institution() {
        return $this->financial_institution;
    }

    public function setFinancial_institution( $financial_institution) {
        $this->financial_institution = $financial_institution;
    }

    public function getFacility_type() {
        return $this->facility_type;
    }

    public function setFacility_type( $facility_type) {
        $this->facility_type = $facility_type;
    }

    public function getFacility_amount() {
        return $this->facility_amount;
    }

    public function setFacility_amount( $facility_amount) {
        $this->facility_amount = $facility_amount;
    }

    public function getMonthly_installment() {
        return $this->monthly_installment;
    }

    public function setMonthly_installment( $monthly_installment) {
        $this->monthly_installment = $monthly_installment;
    }

    public function getBalance() {
        return $this->balance;
    }

    public function setBalance( $balance) {
        $this->balance = $balance;
    }

    public function getTenure() {
        return $this->tenure;
    }

    public function setTenure( $tenure) {
        $this->tenure = $tenure;
    }

    public function getStart_date() {
        return $this->start_date;
    }

    public function setStart_date( $start_date) {
        $this->start_date = $start_date;
    }

}

$u  = new personalFinancial() ; 
$u->setTenure("Tinashe makaza") ; 
//echo  $u->getTenure() ;

?>