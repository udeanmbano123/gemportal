<?php
/**
 * Created by PhpStorm.
 * User: tinah
 * Date: 20/8/2018
 * Time: 1:21 PM
 */

class fin_profit_loss_upload{
    /**
     * @return mixed
     */
    public function getDocUrl()
    {
        return $this->doc_url;
    }

    /**
     * @param mixed $doc_url
     */
    public function setDocUrl($doc_url)
    {
        $this->doc_url = $doc_url;
    }

    /**
     * @return mixed
     */
    public function getisAudited()
    {
        return $this->is_audited;
    }

    /**
     * @param mixed $is_audited
     */
    public function setIsAudited($is_audited)
    {
        $this->is_audited = $is_audited;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}