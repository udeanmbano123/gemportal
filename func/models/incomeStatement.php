<?php 

class incomeStatement{
    var $income_statement_id ;
    var $user_id ;
    var $is_audited ;
    var $revenue_or_turnover ;
    var $less_operating_costs ;
    var $year ;
    var $EBIT ;
    var $finance_costs ;
    var $cost_of_sales ;
    var $gross_profit ;
    var $earnings_before_taxation ;
    var $profit_or_loss_after_taxation ;
    var $retained_profit ;
    var $profit_or_loss_carried_fwd ;

    /**
     * @return mixed
     */
    public function getIncomeStatementId()
    {
        return $this->income_statement_id;
    }

    /**
     * @param mixed $income_statement_id
     */
    public function setIncomeStatementId($income_statement_id)
    {
        $this->income_statement_id = $income_statement_id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getisAudited()
    {
        return $this->is_audited;
    }

    /**
     * @param mixed $is_audited
     */
    public function setIsAudited($is_audited)
    {
        $this->is_audited = $is_audited;
    }

    /**
     * @return mixed
     */
    public function getRevenueOrTurnover()
    {
        return $this->revenue_or_turnover;
    }

    /**
     * @param mixed $revenue_or_turnover
     */
    public function setRevenueOrTurnover($revenue_or_turnover)
    {
        $this->revenue_or_turnover = $revenue_or_turnover;
    }

    /**
     * @return mixed
     */
    public function getLessOperatingCosts()
    {
        return $this->less_operating_costs;
    }

    /**
     * @param mixed $less_operating_costs
     */
    public function setLessOperatingCosts($less_operating_costs)
    {
        $this->less_operating_costs = $less_operating_costs;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getEBIT()
    {
        return $this->EBIT;
    }

    /**
     * @param mixed $EBIT
     */
    public function setEBIT($EBIT)
    {
        $this->EBIT = $EBIT;
    }

    /**
     * @return mixed
     */
    public function getFinanceCosts()
    {
        return $this->finance_costs;
    }

    /**
     * @param mixed $finance_costs
     */
    public function setFinanceCosts($finance_costs)
    {
        $this->finance_costs = $finance_costs;
    }

    /**
     * @return mixed
     */
    public function getCostOfSales()
    {
        return $this->cost_of_sales;
    }

    /**
     * @param mixed $cost_of_sales
     */
    public function setCostOfSales($cost_of_sales)
    {
        $this->cost_of_sales = $cost_of_sales;
    }

    /**
     * @return mixed
     */
    public function getGrossProfit()
    {
        return $this->gross_profit;
    }

    /**
     * @param mixed $gross_profit
     */
    public function setGrossProfit($gross_profit)
    {
        $this->gross_profit = $gross_profit;
    }

    /**
     * @return mixed
     */
    public function getEarningsBeforeTaxation()
    {
        return $this->earnings_before_taxation;
    }

    /**
     * @param mixed $earnings_before_taxation
     */
    public function setEarningsBeforeTaxation($earnings_before_taxation)
    {
        $this->earnings_before_taxation = $earnings_before_taxation;
    }

    /**
     * @return mixed
     */
    public function getProfitOrLossAfterTaxation()
    {
        return $this->profit_or_loss_after_taxation;
    }

    /**
     * @param mixed $profit_or_loss_after_taxation
     */
    public function setProfitOrLossAfterTaxation($profit_or_loss_after_taxation)
    {
        $this->profit_or_loss_after_taxation = $profit_or_loss_after_taxation;
    }

    /**
     * @return mixed
     */
    public function getRetainedProfit()
    {
        return $this->retained_profit;
    }

    /**
     * @param mixed $retained_profit
     */
    public function setRetainedProfit($retained_profit)
    {
        $this->retained_profit = $retained_profit;
    }

    /**
     * @return mixed
     */
    public function getProfitOrLossCarriedFwd()
    {
        return $this->profit_or_loss_carried_fwd;
    }

    /**
     * @param mixed $profit_or_loss_carried_fwd
     */
    public function setProfitOrLossCarriedFwd($profit_or_loss_carried_fwd)
    {
        $this->profit_or_loss_carried_fwd = $profit_or_loss_carried_fwd;
    }


}
?>