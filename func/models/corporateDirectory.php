<?php 

class corporateDirectory{
	var $corporate_directory_id ; 
	var $user_id ; 
	var $duration_in_stated_business_line ; 
	var $customer_basecorporates ; 
	var $customer_baseindividuals ; 
	var $business_year_end ; 
	var $business_premises_are ; 
	var $name_of_holding_company ; 
	var $contact_person_name ; 
	var $contact_person_position ; 
	var $contact_person_cell ; 
	var $contact_person_telephone ; 
	var $contact_person_email ; 
	var $company_secretary_address ; 
	var $attorney_office_address ; 
	var $accountant_office_address ; 
	var $auditors_office_address ; 
	var $principal_bankers_address ; 
	var $authorised_share_capital ; 
	var $issued_share_capital ; 

    public function getcorporate_directory_id() {
        return $this->corporate_directory_id;
    }

    public function setcorporate_directory_id( $corporate_directory_id) {
        $this->corporate_directory_id = $corporate_directory_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getduration_in_stated_business_line() {
        return $this->duration_in_stated_business_line;
    }

    public function setduration_in_stated_business_line( $duration_in_stated_business_line) {
        $this->duration_in_stated_business_line = $duration_in_stated_business_line;
    }

    public function getcustomer_basecorporates() {
        return $this->customer_basecorporates;
    }

    public function setcustomer_basecorporates( $customer_basecorporates) {
        $this->customer_basecorporates = $customer_basecorporates;
    }

    public function getcustomer_baseindividuals() {
        return $this->customer_baseindividuals;
    }

    public function setcustomer_baseindividuals( $customer_baseindividuals) {
        $this->customer_baseindividuals = $customer_baseindividuals;
    }

    public function getbusiness_year_end() {
        return $this->business_year_end;
    }

    public function setbusiness_year_end( $business_year_end) {
        $this->business_year_end = $business_year_end;
    }

    public function getbusiness_premises_are() {
        return $this->business_premises_are;
    }

    public function setbusiness_premises_are( $business_premises_are) {
        $this->business_premises_are = $business_premises_are;
    }

    public function getname_of_holding_company() {
        return $this->name_of_holding_company;
    }

    public function setname_of_holding_company( $name_of_holding_company) {
        $this->name_of_holding_company = $name_of_holding_company;
    }

    public function getcontact_person_name() {
        return $this->contact_person_name;
    }

    public function setcontact_person_name( $contact_person_name) {
        $this->contact_person_name = $contact_person_name;
    }

    public function getcontact_person_position() {
        return $this->contact_person_position;
    }

    public function setcontact_person_position( $contact_person_position) {
        $this->contact_person_position = $contact_person_position;
    }

    public function getcontact_person_cell() {
        return $this->contact_person_cell;
    }

    public function setcontact_person_cell( $contact_person_cell) {
        $this->contact_person_cell = $contact_person_cell;
    }

    public function getcontact_person_telephone() {
        return $this->contact_person_telephone;
    }

    public function setcontact_person_telephone( $contact_person_telephone) {
        $this->contact_person_telephone = $contact_person_telephone;
    }

    public function getcontact_person_email() {
        return $this->contact_person_email;
    }

    public function setcontact_person_email( $contact_person_email) {
        $this->contact_person_email = $contact_person_email;
    }

    public function getcompany_secretary_address() {
        return $this->company_secretary_address;
    }

    public function setcompany_secretary_address( $company_secretary_address) {
        $this->company_secretary_address = $company_secretary_address;
    }

    public function getattorney_office_address() {
        return $this->attorney_office_address;
    }

    public function setattorney_office_address( $attorney_office_address) {
        $this->attorney_office_address = $attorney_office_address;
    }

    public function getaccountant_office_address() {
        return $this->accountant_office_address;
    }

    public function setaccountant_office_address( $accountant_office_address) {
        $this->accountant_office_address = $accountant_office_address;
    }

    public function getauditors_office_address() {
        return $this->auditors_office_address;
    }

    public function setauditors_office_address( $auditors_office_address) {
        $this->auditors_office_address = $auditors_office_address;
    }

    public function getprincipal_bankers_address() {
        return $this->principal_bankers_address;
    }

    public function setprincipal_bankers_address( $principal_bankers_address) {
        $this->principal_bankers_address = $principal_bankers_address;
    }

    public function getauthorised_share_capital() {
        return $this->authorised_share_capital;
    }

    public function setauthorised_share_capital( $authorised_share_capital) {
        $this->authorised_share_capital = $authorised_share_capital;
    }

    public function getissued_share_capital() {
        return $this->issued_share_capital;
    }

    public function setissued_share_capital( $issued_share_capital) {
        $this->issued_share_capital = $issued_share_capital;
    }

}
$u = new corporateDirectory() ; 
$u->setuser_id("Tinashe Makaza") ; 
//echo  $u->getuser_id() ;


?>