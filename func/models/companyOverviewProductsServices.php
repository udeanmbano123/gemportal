<?php 

class companyOverviewProductsServices{

	var $company_overview_products_services_id ; 
	var $user_id ; 
	var $company_overview_id ; 
	var $product_service_name ; 
	var $description_of_product ; 
	var $sales_volume ; 

    public function getcompany_overview_products_services_id() {
        return $this->company_overview_products_services_id;
    }

    public function setcompany_overview_products_services_id( $company_overview_products_services_id) {
        $this->company_overview_products_services_id = $company_overview_products_services_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getcompany_overview_id() {
        return $this->company_overview_id;
    }

    public function setcompany_overview_id( $company_overview_id) {
        $this->company_overview_id = $company_overview_id;
    }

    public function getproduct_service_name() {
        return $this->product_service_name;
    }

    public function setproduct_service_name( $product_service_name) {
        $this->product_service_name = $product_service_name;
    }

    public function getdescription_of_product() {
        return $this->description_of_product;
    }

    public function setdescription_of_product( $description_of_product) {
        $this->description_of_product = $description_of_product;
    }

    public function getsales_volume() {
        return $this->sales_volume;
    }

    public function setsales_volume( $sales_volume) {
        $this->sales_volume = $sales_volume;
    }
}
$u = new companyOverviewProductsServices() ; 
$u->setuser_id("Tinashe Makaza") ; 
//echo  $u->getuser_id() ;


?>