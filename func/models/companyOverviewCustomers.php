<?php 

class companyOverviewCustomers{
	var $company_overview_customers_id ;
	var $user_id ;
	var $company_overview_id ;
	var $customer_name ;
	var $customer_location ;
	var $customer_years_rel ;
	var $customer_sales ;
	var $customer_trade_terms ;
	var $customer_distr_channel ;
    public function getcompany_overview_customers_id() {
        return $this->company_overview_customers_id;
    }

    public function setcompany_overview_customers_id( $company_overview_customers_id) {
        $this->company_overview_customers_id = $company_overview_customers_id;
    }

    public function getuser_id() {
        return $this->user_id;
    }

    public function setuser_id( $user_id) {
        $this->user_id = $user_id;
    }

    public function getcompany_overview_id() {
        return $this->company_overview_id;
    }

    public function setcompany_overview_id( $company_overview_id) {
        $this->company_overview_id = $company_overview_id;
    }

    public function getcustomer_name() {
        return $this->customer_name;
    }

    public function setcustomer_name( $customer_name) {
        $this->customer_name = $customer_name;
    }

    public function getcustomer_location() {
        return $this->customer_location;
    }

    public function setcustomer_location( $customer_location) {
        $this->customer_location = $customer_location;
    }

    public function getcustomer_years_rel() {
        return $this->customer_years_rel;
    }

    public function setcustomer_years_rel( $customer_years_rel) {
        $this->customer_years_rel = $customer_years_rel;
    }

    public function getcustomer_sales() {
        return $this->customer_sales;
    }

    public function setcustomer_sales( $customer_sales) {
        $this->customer_sales = $customer_sales;
    }

    public function getcustomer_trade_terms() {
        return $this->customer_trade_terms;
    }

    public function setcustomer_trade_terms( $customer_trade_terms) {
        $this->customer_trade_terms = $customer_trade_terms;
    }

    public function getcustomer_distr_channel() {
        return $this->customer_distr_channel;
    }

    public function setcustomer_distr_channel( $customer_distr_channel) {
        $this->customer_distr_channel = $customer_distr_channel;
    }
}
$u = new companyOverviewCustomers() ; 
$u->setuser_id("Tinashe Makaza") ; 
//echo  $u->getuser_id() ;


?>