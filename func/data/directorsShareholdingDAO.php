<?php
require_once 'DAO.php';
class directorsShareholdingDAO extends DAO {
    function selectOnedirectorsShareholdingByEmail($email){
        $sql = "SELECT *  FROM directors_shareholding where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectdirectorsShareholdingByEmail($email){
        $sql = "SELECT *  FROM directors_shareholding where user_id = '".$email."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectdirectorsShareholdingById($tab_id){
        $sql = "SELECT *  FROM directors_shareholding where directors_shareholding_id = '".$tab_id."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createdirectorsShareholding(directorsShareholding $capitalreq){
        $sql = "INSERT INTO directors_shareholding
                            (directors_shareholding_id,
                            user_id,
                            directors_name,
                            direct_indirect_equity_interest,
                            shareholding)
                            VALUES
                            (NULL,
                            '".$capitalreq->getuser_id()."',
                            '".$capitalreq->getdirectors_name()."',
                            '".$capitalreq->getdirect_indirect_equity_interest()."',
                            '".$capitalreq->getshareholding()."');" ;
        return $this->conn->query($sql) ;

    }

    function updatedirectorsShareholding(directorsShareholding $capitalreq){
        $sql = "UPDATE directors_shareholding
                        SET
                        
                        directors_name = '".$capitalreq->getdirectors_name()."',
                        direct_indirect_equity_interest = '".$capitalreq->getdirect_indirect_equity_interest()."',
                        shareholding = '".$capitalreq->getshareholding()."'
                        WHERE directors_shareholding_id = '".$capitalreq->getdirectors_shareholding_id()."';" ;
        //echo  $sql ;
        return $this->conn->query($sql) ;

    }
    function deleteDirectors(directorsShareholding $capitalreq){
        $sql = "DELETE FROM directors_shareholding   WHERE  directors_shareholding_id ='".$capitalreq->getdirectors_shareholding_id()."';" ;
        
       return $this->conn->query($sql) ;

    }
}
?>