<?php
require_once 'DAO.php';
class finRatiosDAO  extends DAO {

    function getCurrentRatio($Email){
        $sql = "SELECT 
                ((current_assets_Biologicalassets
                                + current_assets_Inventories 
                                + current_assets_Tradeotherreceivables 
                                + current_assets_Prepayments
                                + current_assets_Bank
                                +current_assets_Cash
                                + current_assets_Other)/ 
                (current_liabilities_Shorttermborrowings +
                current_liabilities_Tradeotherpayables + 
                current_liabilities_Currenttaxliability + 
                current_liabilities_Other ) ) as current_ratio FROM fin_balance_sheet WHERE user_id = '$Email' ";
        // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['current_ratio'], 4);
         }
         return $return;

    }
    function getQuickRatio($Email){
        $sql = "SELECT 
                ( ( (current_assets_Biologicalassets
                                + current_assets_Inventories 
                                + current_assets_Tradeotherreceivables 
                                + current_assets_Prepayments
                                + current_assets_Bank
                                +current_assets_Cash
                                + current_assets_Other) - current_assets_Inventories ) / 
                (current_liabilities_Shorttermborrowings +
                current_liabilities_Tradeotherpayables + 
                current_liabilities_Currenttaxliability + 
                current_liabilities_Other ) ) as quick_ratio FROM fin_balance_sheet  WHERE user_id = '$Email' ";
        // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['quick_ratio'], 4);
         }
         return $return;

    }
   function getTotalDebtRatioRatio($Email){
        $sql = "SELECT (
                (non_current_liabilities_Longtermborrowings + 
                non_current_liabilities_Defferredtaxliabilities + 
                non_current_liabilities_Other + 
                current_liabilities_Shorttermborrowings + 
                current_liabilities_Tradeotherpayables + 
                current_liabilities_Currenttaxliability + 
                current_liabilities_Other)
                /
                (non_current_assests_Propertyplantandequipment + 
                non_current_assests_Investmentproperties + 
                non_current_assests_Intangibleassets + 
                non_current_assests_Other + 
                current_assets_Biologicalassets + 
                current_assets_Inventories + 
                current_assets_Tradeotherreceivables + 
                current_assets_Prepayments + 
                current_assets_Bank + 
                current_assets_Cash + 
                current_assets_Other)
                ) as current_ratio FROM fin_balance_sheet  WHERE user_id = '$Email' ";
        // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['current_ratio'], 4);
         }
         return $return;

    }
   function getDebtEquityRatio($Email){
        $sql = "SELECT (
                (non_current_liabilities_Longtermborrowings + 
                non_current_liabilities_Defferredtaxliabilities + 
                non_current_liabilities_Other + 
                current_liabilities_Shorttermborrowings + 
                current_liabilities_Tradeotherpayables + 
                current_liabilities_Currenttaxliability + 
                current_liabilities_Other)
                /
                (equity_attributed_Issuedsharecapital +
                equity_attributed_Sharepremium +
                equity_attributed_Revenuereserves +
                equity_attributed_Capitalreserves +
                equity_attributed_Other +
                non_controlling_interest)
                ) as current_ratio FROM fin_balance_sheet  WHERE user_id = '$Email' ";
        // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['current_ratio'], 4);
         }
         return $return;

    }
   function getLongTermDebtRatio($Email){
        $sql = "SELECT (
                (non_current_liabilities_Longtermborrowings + 
				non_current_liabilities_Defferredtaxliabilities + 
				non_current_liabilities_Other)
                /
                (non_current_assests_Propertyplantandequipment + 
                non_current_assests_Investmentproperties + 
                non_current_assests_Intangibleassets + 
                non_current_assests_Other + 
                current_assets_Biologicalassets + 
                current_assets_Inventories + 
                current_assets_Tradeotherreceivables + 
                current_assets_Prepayments + 
                current_assets_Bank + 
                current_assets_Cash + 
                current_assets_Other)
                ) as current_ratio FROM fin_balance_sheet  WHERE user_id = '$Email' ";
        // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['current_ratio'], 4);
         }
         return $return;

    }
   function getEquityRatio($Email){
        $sql = "SELECT (
                (equity_attributed_Issuedsharecapital +
                equity_attributed_Sharepremium +
                equity_attributed_Revenuereserves +
                equity_attributed_Capitalreserves +
                equity_attributed_Other +
                non_controlling_interest)
                /
                (non_current_assests_Propertyplantandequipment + 
                non_current_assests_Investmentproperties + 
                non_current_assests_Intangibleassets + 
                non_current_assests_Other + 
                current_assets_Biologicalassets + 
                current_assets_Inventories + 
                current_assets_Tradeotherreceivables + 
                current_assets_Prepayments + 
                current_assets_Bank + 
                current_assets_Cash + 
                current_assets_Other)
                ) as current_ratio FROM fin_balance_sheet  WHERE user_id = '$Email' ";
        // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['current_ratio'], 4);
         }
         return $return;

    }
    function getCashRatio($Email){
        $sql = "SELECT 
                ( (current_assets_Cash) / 
                (current_liabilities_Shorttermborrowings +
                current_liabilities_Tradeotherpayables + 
                current_liabilities_Currenttaxliability + 
                current_liabilities_Other ) ) as cash_ratio FROM fin_balance_sheet  WHERE user_id = '$Email' ";
        // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['cash_ratio'], 4);
         }
         return $return;

    }
    function getPerNumber($Email){
		 
      /*  $sql = "SELECT 
                ( ( (current_assets_Cash) / 
                (current_liabilities_Shorttermborrowings +
                current_liabilities_Tradeotherpayables + 
                current_liabilities_Currenttaxliability + 
                current_liabilities_Other ) ) * 100 ) as cash_ratio FROM fin_balance_sheet  WHERE user_id = '$Email' ";
  */ $sql = "SELECT 
                ( ( (current_assets_Cash) / 
                 (current_assets_Biologicalassets + 
                current_assets_Inventories + 
                current_assets_Tradeotherreceivables + 
                current_assets_Prepayments + 
                current_assets_Bank + 
                current_assets_Cash + 
                current_assets_Other)) * 100 ) as cash_ratio , year FROM fin_balance_sheet  WHERE user_id ='$Email' group by year";
              
	   // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['cash_ratio'], 4);
         }
         return $return;

    }
    function getAverageCollectionPeriod($Email){
       /* $sql = "SELECT  t.current_assets_Tradeotherreceivables / 
( (ut.profit_loss_Saleofgoods + ut.profit_loss_Renderingofservices + ut.profit_loss_OtherRevenues )/360) as AverageCollectionPeriod 
FROM fin_balance_sheet t , fin_profit_loss ut 
WHERE t.user_id = ut.user_id  and t.user_id =  '$Email'  Group by fin_profit_loss_id ";
       */
        $sql = "SELECT  
    (t.current_assets_Tradeotherreceivables)/(
(ut.profit_loss_Saleofgoods + ut.profit_loss_Renderingofservices + ut.profit_loss_OtherRevenues )/360) as AverageCollectionPeriod  , t.year
FROM fin_balance_sheet t , fin_profit_loss ut 
WHERE t.user_id = ut.user_id and t.year=ut.year and t.user_id = '$Email' Group by t.year";
         
	   //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['AverageCollectionPeriod'], 4);
         }
         return $return;

    }
    function getInventoryTurnoverRatios($Email){
        $sql = "SELECT  
    (ut.profit_loss_Saleofgoods + ut.profit_loss_Renderingofservices + ut.profit_loss_OtherRevenues )/(t.current_assets_Inventories) as AverageCollectionPeriod  , t.year
FROM fin_balance_sheet t , fin_profit_loss ut 
WHERE t.user_id = ut.user_id and t.year=ut.year and t.user_id = '$Email' Group by t.year ";
         //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['AverageCollectionPeriod'], 4);
         }
         return $return;

    }
    function getReceivableTurnoverRatios($Email){
        $sql = "SELECT (ut.profit_loss_Saleofgoods + ut.profit_loss_Renderingofservices + ut.profit_loss_OtherRevenues )/(t.current_assets_Tradeotherreceivables) as AverageCollectionPeriod  , t.year
FROM fin_balance_sheet t , fin_profit_loss ut 
WHERE t.user_id = ut.user_id and t.year=ut.year and t.user_id = '$Email' Group by t.year ";
         //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['AverageCollectionPeriod'], 4);
         }
         return $return;

    }

    function getTotalAssetTurnoverRatios($Email){
        $sql = "SELECT  
    
(ut.profit_loss_Saleofgoods + ut.profit_loss_Renderingofservices + ut.profit_loss_OtherRevenues )/(non_current_assests_Propertyplantandequipment 
+ non_current_assests_Investmentproperties 
+ non_current_assests_Intangibleassets 
+ non_current_assests_Other 
+ current_assets_Biologicalassets 
+ current_assets_Inventories 
+ current_assets_Tradeotherreceivables 
+ current_assets_Prepayments 
+ current_assets_Bank 
+ current_assets_Cash 
+ current_assets_Other) as AverageCollectionPeriod  , t.year
FROM fin_balance_sheet t , fin_profit_loss ut 
WHERE t.user_id = ut.user_id and t.year=ut.year and t.user_id = '$Email' Group by t.year";
         //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['AverageCollectionPeriod'], 4);
         }
         return $return;

    }
   function getFixedAssetTurnoverRatios($Email){
        $sql = "SELECT (ut.profit_loss_Saleofgoods + ut.profit_loss_Renderingofservices + ut.profit_loss_OtherRevenues )/(t.non_current_assests_Propertyplantandequipment) as AverageCollectionPeriod  , t.year
FROM fin_balance_sheet t , fin_profit_loss ut 
WHERE t.user_id = ut.user_id and t.year=ut.year and t.user_id = '$Email' Group by t.year ";
         //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['AverageCollectionPeriod'], 4);
         }
         return $return;

    }
   function getEarningspershareRatios($Email){
        $sql = "SELECT ( 
                (ut.profit_loss_AttributabletoEquityholdersoftheparent ) / 
                t.OtherTotal_Ordinary_shares ) as AverageCollectionPeriod 
                FROM fin_balance_sheet t , fin_profit_loss ut 
                WHERE t.user_id = ut.user_id and t.user_id = '$Email'  Group by fin_profit_loss_id  ";
         //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['AverageCollectionPeriod'], 4);
         }
         return $return;

    }
   function getPriceEarningspershareRatios($Email){
        $sql = "SELECT (( 
                (ut.profit_loss_AttributabletoEquityholdersoftheparent ) / 
                t.OtherTotal_Ordinary_shares ) / t.OtherMarket_price_per_share) as AverageCollectionPeriod 
                FROM fin_balance_sheet t , fin_profit_loss ut 
                WHERE t.user_id = ut.user_id and t.user_id = '$Email' Group by fin_profit_loss_id ";
         //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['AverageCollectionPeriod'], 4);
         }
         return $return;

    }
   function getPriceEarningsRatios($Email){
        $sql = "SELECT ( 
                (ut.profit_loss_AttributabletoEquityholdersoftheparent ) / 
                t.OtherTotal_Ordinary_shares ) as AverageCollectionPeriod 
                FROM fin_balance_sheet t , fin_profit_loss ut 
                WHERE t.user_id = ut.user_id and t.user_id = '$Email'  Group by fin_profit_loss_id  ";
         //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['AverageCollectionPeriod'], 4);
         }
         return $return;

    }
   function getGrossProfitMarginRatios($Email){
        $sql = "SELECT (((profit_loss_Saleofgoods +
                profit_loss_Renderingofservices +
                profit_loss_OtherRevenues)-(profit_loss_CostofSales)) / 
                (profit_loss_Saleofgoods +
                profit_loss_Renderingofservices +
                profit_loss_OtherRevenues))*100 as theratio ,year FROM fin_profit_loss WHERE user_id ='$Email' group by year";
         //echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['theratio'], 4);
         }
         return $return;

    }
   function getNetProfitMarginRatios($Email){
        $sql = "SELECT (((((profit_loss_Saleofgoods +
                profit_loss_Renderingofservices +
                profit_loss_OtherRevenues)-profit_loss_CostofSales+`profit_loss_Otheroperatingincome`-`profit_loss_Otheroperatingexpenses`-`profit_loss_Staffcosts`-`profit_loss_Depreciationarmotisation`)+`profit_loss_Increaseinfairvalueadjustmentsimpairments`-`profit_loss_Decreaseinfairvalueadjustmentsimpairments`-`profit_loss_Financecosts`+`profit_loss_Financeincome`)-`profit_loss_Incometaxexpense`)/(profit_loss_Saleofgoods +
                profit_loss_Renderingofservices +
                profit_loss_OtherRevenues) )*100    
                as theratio ,fin_profit_loss.year FROM fin_profit_loss  WHERE user_id = '$Email' group by year";
//         echo $sql ;'$Email'
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['theratio'], 4);
         }
         return $return;

    }
   function getROARatios($Email){
        $sql = "SELECT (((((profit_loss_Saleofgoods +
                profit_loss_Renderingofservices +
                profit_loss_OtherRevenues)-profit_loss_CostofSales+`profit_loss_Otheroperatingincome`-`profit_loss_Otheroperatingexpenses`-`profit_loss_Staffcosts`-`profit_loss_Depreciationarmotisation`)+`profit_loss_Increaseinfairvalueadjustmentsimpairments`-`profit_loss_Decreaseinfairvalueadjustmentsimpairments`-`profit_loss_Financecosts`+`profit_loss_Financeincome`)-`profit_loss_Incometaxexpense`)/(non_current_assests_Propertyplantandequipment 
+ non_current_assests_Investmentproperties 
+ non_current_assests_Intangibleassets 
+ non_current_assests_Other 
+ current_assets_Biologicalassets 
+ current_assets_Inventories 
+ current_assets_Tradeotherreceivables 
+ current_assets_Prepayments 
+ current_assets_Bank 
+ current_assets_Cash 
+ current_assets_Other) )*100    
                as theratio ,fin_profit_loss.year FROM fin_profit_loss,fin_balance_sheet  WHERE fin_profit_loss.user_id = '$Email' and fin_balance_sheet.user_id= '$Email' group by year ";
//         echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['theratio'], 4);
         }
         return $return;

    }
   function getROERatios($Email){
        $sql = "SELECT (((((profit_loss_Saleofgoods +
                profit_loss_Renderingofservices +
                profit_loss_OtherRevenues)-profit_loss_CostofSales+`profit_loss_Otheroperatingincome`-`profit_loss_Otheroperatingexpenses`-`profit_loss_Staffcosts`-`profit_loss_Depreciationarmotisation`)+`profit_loss_Increaseinfairvalueadjustmentsimpairments`-`profit_loss_Decreaseinfairvalueadjustmentsimpairments`-`profit_loss_Financecosts`+`profit_loss_Financeincome`)-`profit_loss_Incometaxexpense`)/((`equity_attributed_Issuedsharecapital`+`equity_attributed_Sharepremium`+`equity_attributed_Revenuereserves`+`equity_attributed_Capitalreserves`+`equity_attributed_Other`)+`non_controlling_interest`) )*100    
                as theratio ,fin_profit_loss.year FROM fin_profit_loss,fin_balance_sheet  WHERE fin_profit_loss.user_id = '$Email' and fin_balance_sheet.user_id= '$Email' group by year";
//         echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['theratio'], 4);
         }
         return $return;

    }
    function getNetWorkingCapitalCurrentLiabilities($Email){
       $sql = "SELECT ((current_assets_Biologicalassets + current_assets_Inventories + current_assets_Tradeotherreceivables + current_assets_Prepayments + current_assets_Bank + current_assets_Cash + current_assets_Other) - (current_liabilities_Shorttermborrowings + current_liabilities_Tradeotherpayables + current_liabilities_Currenttaxliability + current_liabilities_Other ) ) / (current_liabilities_Shorttermborrowings + current_liabilities_Tradeotherpayables + current_liabilities_Currenttaxliability + current_liabilities_Other ) as current_ratio ,year FROM fin_balance_sheet WHERE user_id='$Email' group by year";
        // echo $sql ;
         $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
           $return[] = round($row['current_ratio'], 4);
         }
         return $return;

    }
    function getCurrentRatioValue($getValue){
        switch ($getValue){
            case $getValue < 1 :
                return "WEAK" ;
                break ;
            case $getValue >= 1 && $getValue <= 1.5 :
                return "FAIR" ;
                break ;
            case $getValue > 1.5 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
   function getQuickRatioValue($getValue){
        switch ($getValue){
            case $getValue < 1.2 :
                return "WEAK" ;
                break ;
            case $getValue >= 1.2 && $getValue <= 1.5 :
                return "FAIR" ;
                break ;
            case $getValue > 1.5 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
   function getCashRatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.6 :
                return "WEAK" ;
                break ;
            case $getValue >= 0.6 && $getValue <= 0.75 :
                return "FAIR" ;
                break ;
            case $getValue > 0.75 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
    function getWCCLRatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.1:
                return "WEAK" ;
                break ;
            case $getValue >= 0.1 && $getValue <= 0.25 :
                return "FAIR" ;
                break ;
            case $getValue > 0.25 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
    function getAverageCollectionPeriodRatioValue($getValue){
        switch ($getValue){
            case $getValue < 40:
                return "WEAK" ;
                break ;
            case $getValue >= 40 && $getValue <= 55 :
                return "FAIR" ;
                break ;
            case $getValue > 55 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
    function getInventoryTurnoverRatioValue($getValue){
        switch ($getValue){
            case $getValue < 1:
                return "WEAK" ;
                break ;
            case $getValue >= 1 && $getValue <= 1.5 :
                return "FAIR" ;
                break ;
            case $getValue > 1.5 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
    function getReceivableTurnoverRatioValue($getValue){
        switch ($getValue){
            case $getValue < 2:
                return "WEAK" ;
                break ;
            case $getValue >= 2 && $getValue <= 3 :
                return "FAIR" ;
                break ;
            case $getValue > 3 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
    function getFixedAssetTurnoverRatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.2:
                return "WEAK" ;
                break ;
            case $getValue >= 0.2 && $getValue <= 0.29 :
                return "FAIR" ;
                break ;
            case $getValue >= 0.3 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
   function getTotalAssetTurnoverRatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.2:
                return "WEAK" ;
                break ;
            case $getValue >= 0.2 && $getValue <= 0.29 :
                return "FAIR" ;
                break ;
            case $getValue >= 0.3 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
  function getTotalDebtRatioRatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.30:
                return "STRONG" ;
                break ;
            case $getValue >= 0.30 && $getValue <= 0.55 :
                return "FAIR" ;
                break ;
            case $getValue >= 0.55 :
                return "WEAK" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
  function getDebtEquityRatioValue($getValue)
  {
      switch ($getValue) {
          case $getValue < 0.42:
              return "WEAK";
              break;
          case $getValue >= 0.42 && $getValue <= 1.22 :
              return "FAIR";
              break;
          case $getValue >= 1.22 :
              return "STRONG";
              break;
          default:
              return "UNKNOWN";
              break;
      }
  }

  function getEquityRatioValue($getValue){
      switch ($getValue){
          case $getValue < 0.70:
              return "WEAK" ;
              break ;
          case $getValue >= 0.70 && $getValue <= 0.90 :
              return "FAIR" ;
              break ;
          case $getValue >= 0.90 :
              return "STRONG" ;
              break ;
          default:
              return "UNKNOWN" ;
              break ;
      }
    }
  function getLongTermDebtRatioValue($getValue){
      switch ($getValue){
          case $getValue < 0.20:
              return "STRONG" ;
              break ;
          case $getValue >= 0.20 && $getValue <= 0.30 :
              return "FAIR" ;
              break ;
          case $getValue >= 0.30 :
              return "WEAK" ;
              break ;
          default:
              return "UNKNOWN" ;
              break ;
      }
    }
    function getGrossProfitMarginRatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.70:
                return "WEAK" ;
                break ;
            case $getValue >= 0.70 && $getValue <= 0.90 :
                return "FAIR" ;
                break ;
            case $getValue >= 0.90 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
    function getNetProfitMarginRatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.70:
                return "WEAK" ;
                break ;
            case $getValue >= 0.70 && $getValue < 0.90 :
                return "FAIR" ;
                break ;
            case $getValue >= 0.90 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
    function getROARatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.025:
                return "WEAK" ;
                break ;
            case $getValue >= 0.025 && $getValue < 0.05 :
                return "FAIR" ;
                break ;
            case $getValue >= 0.5 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }
    function getROERatioValue($getValue){
        switch ($getValue){
            case $getValue < 0.025:
                return "WEAK" ;
                break ;
            case $getValue >= 0.025 && $getValue < 0.05 :
                return "FAIR" ;
                break ;
            case $getValue >= 0.5 :
                return "STRONG" ;
                break ;
            default:
                return "UNKNOWN" ;
                break ;
        }
    }


}
