<?php
require_once 'DAO.php';
class Last2yrsDAO extends DAO {
    
    function selectBalanceSheetLast2Years($email){
    $last2yrs= date("Y",strtotime("-2 year"));
    $sql = "SELECT *  FROM balance_sheet where user_id = '".$email."'  and  year= '". $last2yrs."'";
        
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;

        }
        
         return $return;
    }
    function selectCashflows($email){
        $sql = "SELECT *  FROM fin_cash_flow where user_id = '".$email."' ";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectProfitLoss($email){
        $sql = "SELECT *  FROM fin_profit_loss where user_id = '".$email."' ";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectBalanceSheet($email){
        $sql = "SELECT *  FROM fin_balance_sheet where user_id = '".$email."' ";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function PrTotalRevenue($email){
        $sql = "SELECT sum(`profit_loss_Saleofgoods`+`profit_loss_Renderingofservices`+`profit_loss_OtherRevenues`) as 'Total',year FROM `fin_profit_loss` where user_id='".$email."' group by year";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
//
    function GrossProfit($email){
        $sql = "SELECT sum((`profit_loss_Saleofgoods`+`profit_loss_Renderingofservices`+`profit_loss_OtherRevenues`))-(`profit_loss_CostofSales`) as 'Total',year FROM `fin_profit_loss` where user_id='".$email."' group by year";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function OperatingProfit($email){
        $sql = "SELECT sum((`profit_loss_Saleofgoods`+`profit_loss_Renderingofservices`+`profit_loss_OtherRevenues`+`profit_loss_Otheroperatingincome`)-`profit_loss_Otheroperatingexpenses`-`profit_loss_Staffcosts`-`profit_loss_Depreciationarmotisation`) as 'Total',year FROM `fin_profit_loss` where user_id='".$email."' group by year";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function PrBeforeTax($email){
        $sql = "SELECT sum((`profit_loss_Increaseinfairvalueadjustmentsimpairments`-`profit_loss_Decreaseinfairvalueadjustmentsimpairments`)+(`profit_loss_Financeincome`)) as 'Total',year FROM `fin_profit_loss` where user_id='".$email."' group by year";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    //
    function PrLossYear($email){
        $sql = "SELECT sum((((`profit_loss_Increaseinfairvalueadjustmentsimpairments`-`profit_loss_Decreaseinfairvalueadjustmentsimpairments`)+(`profit_loss_Financeincome`))-`profit_loss_Incometaxexpense`)) as 'Total',year FROM `fin_profit_loss` where user_id='".$email."' group by year";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function TotalYear($email){
        $sql = "SELECT sum(`profit_loss_AttributabletoEquityholdersoftheparent`+`profit_loss_AttributabletoNoncontrollingintrests`) as 'Total',year FROM `fin_profit_loss` where user_id='".$email."' group by year";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function NetCash($email){
        $sql = "SELECT sum(`cash_flow_LossProfitbeforetax`+`cash_flow_Adjustments`)as 'Total',year FROM `fin_cash_flow` where user_id='".$email."' group by year";
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
}

?>