<?php
require_once 'DAO.php';
class corporateDirectoryDAO extends DAO {


    function selectOnecorporateDirectoryByEmail($email){
        $sql = "SELECT *  FROM corporate_directory where user_id = '".$email."' LIMIT 1 ";
//        echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }


    function createCorporateDirectory(corporateDirectory $cd){
        $sql = "INSERT INTO corporate_directory
                            (corporate_directory_id,
                            user_id,
                            duration_in_stated_business_line,
                            customer_basecorporates,
                            customer_baseindividuals,
                            business_year_end,
                            business_premises_are,
                            name_of_holding_company,
                            contact_person_name,
                            contact_person_position,
                            contact_person_cell,
                            contact_person_telephone,
                            contact_person_email,
                            company_secretary_address,
                            attorney_office_address,
                            accountant_office_address,
                            auditors_office_address,
                            principal_bankers_address,
                            authorised_share_capital,
                            issued_share_capital)
                            VALUES
                            (NULL,
                            '".$cd->getuser_id()."',
                            '".$cd->getduration_in_stated_business_line()."',
                            '".$cd->getcustomer_basecorporates()."',
                            '".$cd->getcustomer_baseindividuals()."',
                            '".$cd->getbusiness_year_end()."',
                            '".$cd->getbusiness_premises_are()."',
                            '".$cd->getname_of_holding_company()."',
                            '".$cd->getcontact_person_name()."',
                            '".$cd->getcontact_person_position()."',
                            '".$cd->getcontact_person_cell()."',
                            '".$cd->getcontact_person_telephone()."',
                            '".$cd->getcontact_person_email()."',
                            '".$cd->getcompany_secretary_address()."',
                            '".$cd->getattorney_office_address()."',
                            '".$cd->getaccountant_office_address()."',
                            '".$cd->getauditors_office_address()."',
                            '".$cd->getprincipal_bankers_address()."',
                            '".$cd->getauthorised_share_capital()."',
                            '".$cd->getissued_share_capital()."');

                " ;
        //echo  $sql ;
        return $this->conn->query($sql) ;

    }
    function updateCorporateDirectory(corporateDirectory $cd){
        $sql = "UPDATE corporate_directory
                        SET
                        corporate_directory_id = '".$cd->getcorporate_directory_id()."',
                        user_id = '".$cd->getuser_id()."',
                        duration_in_stated_business_line = '".$cd->getduration_in_stated_business_line()."',
                        customer_basecorporates = '".$cd->getcustomer_basecorporates()."',
                        customer_baseindividuals = '".$cd->getcustomer_baseindividuals()."',
                        business_year_end = '".$cd->getbusiness_year_end()."',
                        business_premises_are = '".$cd->getbusiness_premises_are()."',
                        name_of_holding_company = '".$cd->getname_of_holding_company()."',
                        contact_person_name = '".$cd->getcontact_person_name()."',
                        contact_person_position = '".$cd->getcontact_person_position()."',
                        contact_person_cell = '".$cd->getcontact_person_cell()."',
                        contact_person_telephone = '".$cd->getcontact_person_telephone()."',
                        contact_person_email = '".$cd->getcontact_person_email()."',
                        company_secretary_address = '".$cd->getcompany_secretary_address()."',
                        attorney_office_address = '".$cd->getattorney_office_address()."',
                        accountant_office_address = '".$cd->getaccountant_office_address()."',
                        auditors_office_address = '".$cd->getauditors_office_address()."',
                        principal_bankers_address = '".$cd->getprincipal_bankers_address()."',
                        authorised_share_capital = '".$cd->getauthorised_share_capital()."',
                        issued_share_capital = '".$cd->getissued_share_capital()."'
                        WHERE corporate_directory_id = '".$cd->getcorporate_directory_id()."';

                " ;
        //echo  $sql ;
        return $this->conn->query($sql) ;

    }
}

?>