<?php
require_once 'DAO.php';
class companyOverviewRawMaterialsDAO extends DAO {
    function selectOnecompanyOverviewRawMaterialsByEmail($email){
        $sql = "SELECT *  FROM company_overview_raw_materials where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function selectcompanyOverviewRawMaterialsByEmail($email){
        $sql = "SELECT *  FROM company_overview_raw_materials where user_id = '".$email."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createcompanyOverviewRawMaterials(companyOverviewRawMaterials $capitalreq){
        $sql = " INSERT INTO company_overview_raw_materials
                (company_overview_raw_materials_id,
                user_id,
                company_overview_id,
                `name`,
                `desc`,
                volume)
                VALUES
                (NULL,
                '".$capitalreq->getuser_id()."',
                '".$capitalreq->getcompany_overview_id()."',
                '".$capitalreq->getname()."',
                '".$capitalreq->getdesc()."',
                '".$capitalreq->getvolume()."');" ;
                
        return $this->conn->query($sql) ;

    }

    function updatecompanyOverviewRawMaterials(companyOverviewRawMaterials $capitalreq){
        $sql = "UPDATE company_overview_raw_materials
                    SET
                    company_overview_id = '".$capitalreq->getcompany_overview_id()."',
                    `name` = '".$capitalreq->getname()."',
                    `desc` = '".$capitalreq->getdesc()."',
                    volume = '".$capitalreq->getvolume()."'
                    WHERE company_overview_raw_materials_id = '".$capitalreq->getcompany_overview_raw_materials_id()."';" ;
        
        return $this->conn->query($sql) ;

    }
     function deletecompanyOverviewRawMaterials(companyOverviewRawMaterials $capitalreq){
        $sql = "DELETE FROM  company_overview_raw_materials WHERE company_overview_raw_materials_id = '".$capitalreq->getcompany_overview_raw_materials_id()."';" ;
        
        return $this->conn->query($sql) ;

    }
}

?>