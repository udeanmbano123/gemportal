<?php
require_once 'DAO.php';

class uploadFinancialsDAO extends DAO {


    //SELECT id, is_audited, doc_url, user_id FROM capitalonline.fin_balance_sheet_upload;
    function selectOneuploadFinancialsByEmailBalanceSheet($email){
        $sql = "SELECT *  FROM fin_balance_sheet_upload where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

   //SELECT id, is_audited, doc_url, user_id FROM capitalonline.fin_balance_sheet_upload;
    function selectOneuploadFinancialsByEmailProfitLoss($email){
        $sql = "SELECT *  FROM fin_profit_loss_upload where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

   //SELECT id, is_audited, doc_url, user_id FROM capitalonline.fin_balance_sheet_upload;
    function selectOneuploadFinancialsByEmailCashFlow($email){
        $sql = "SELECT *  FROM fin_cash_flow_upload where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createuploadFinancialsBalanceSheet(fin_balance_sheet_upload $capitalreq){
        $sql = "INSERT INTO fin_balance_sheet_upload
                            (id,
                            user_id,
                            is_audited,
                            doc_url)
                            VALUES
                            (NULL,
                            '".$capitalreq->getUserId()."',
                            '".$capitalreq->getisAudited()."',
                            '".$capitalreq->getDocUrl()."');
                            " ;
        return $this->conn->query($sql) ;

    }

    function createuploadFinancialsCashFlow(fin_cash_flow_upload $capitalreq){
        $sql = "INSERT INTO fin_cash_flow_upload
                            (id,
                            user_id,
                            is_audited,
                            doc_url)
                            VALUES
                            (NULL,
                            '".$capitalreq->getUserId()."',
                            '".$capitalreq->getisAudited()."',
                            '".$capitalreq->getDocUrl()."');
                            " ;
        return $this->conn->query($sql) ;

    }

    function createuploadFinancialsProfitLoss(fin_profit_loss_upload $capitalreq){
        $sql = "INSERT INTO fin_profit_loss_upload
                            (id,
                            user_id,
                            is_audited,
                            doc_url)
                            VALUES
                            (NULL,
                            '".$capitalreq->getUserId()."',
                            '".$capitalreq->getisAudited()."',
                            '".$capitalreq->getDocUrl()."');
                            " ;
        return $this->conn->query($sql) ;

    }

    function updateuploadFinancialsBalanceSheet(fin_balance_sheet_upload $capitalreq){
        $sql = "UPDATE fin_balance_sheet_upload
                        SET
                        user_id = '".$capitalreq->getUserId()."',
                        is_audited = '".$capitalreq->getisAudited()."' " ;
                        if($capitalreq->getDocUrl() == "func/controller/"){
                            $sql .= " WHERE id = '".$capitalreq->getId()."';
                                         " ;
                        }else{
                            $sql .= " , doc_url = '".$capitalreq->getDocUrl()."'
                                        WHERE id = '".$capitalreq->getId()."';
                                         " ;
                        }

       // echo  $sql ;
        return $this->conn->query($sql) ;

    }

    function updateuploadFinancialsProfitLoss(fin_profit_loss_upload $capitalreq){
        $sql = "UPDATE fin_profit_loss_upload
                        SET
                        user_id = '".$capitalreq->getUserId()."',
                        is_audited = '".$capitalreq->getisAudited()."' " ;
                        if($capitalreq->getDocUrl() == "func/controller/"){
                            $sql .= " WHERE id = '".$capitalreq->getId()."';
                                         " ;
                        }else{
                            $sql .= " , doc_url = '".$capitalreq->getDocUrl()."'
                                        WHERE id = '".$capitalreq->getId()."';
                                         " ;
                        }

       // echo  $sql ;
        return $this->conn->query($sql) ;

    }

    function updateuploadFinancialsCashFlow(fin_cash_flow_upload $capitalreq){
        $sql = "UPDATE fin_cash_flow_upload
                        SET
                        user_id = '".$capitalreq->getUserId()."',
                        is_audited = '".$capitalreq->getisAudited()."' " ;
                        if($capitalreq->getDocUrl() == "func/controller/"){
                            $sql .= " WHERE id = '".$capitalreq->getId()."';
                                         " ;
                        }else{
                            $sql .= " , doc_url = '".$capitalreq->getDocUrl()."'
                                        WHERE id = '".$capitalreq->getId()."';
                                         " ;
                        }

       // echo  $sql ;
        return $this->conn->query($sql) ;

    }




}


?>