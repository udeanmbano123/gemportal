<?php
require_once 'DAO.php';
class enterpriseSizeCategorisationDAO extends DAO {
    function selectOneEnterpriseSizeCategorisationBYEmail($email){
        $sql = "SELECT *  FROM enterprise_size_categorisation where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createEnterpriseSizeCategorisation(enterpriseSizeCategorisation $es_cate){
        $sql = "INSERT INTO enterprise_size_categorisation
                            (enterprise_size_categorisation_id,
                            user_id,
                            staff_levels,
                            annual_turnover,
                            gross_value_of_assets)
                            VALUES
                            (NULL,
                            '".$es_cate->getuser_id()."',
                            '".$es_cate->getstaff_levels()."',
                            '".$es_cate->getannual_turnover()."',
                            '".$es_cate->getgross_value_of_assets()."');
                            " ;
        return $this->conn->query($sql) ;

    }

    function updateEnterpriseSizeCategorisation(enterpriseSizeCategorisation $es_cate){
        $sql = "UPDATE enterprise_size_categorisation
                        SET
                        user_id = '".$es_cate->getuser_id()."',
                        staff_levels = '".$es_cate->getstaff_levels()."',
                        annual_turnover = '".$es_cate->getannual_turnover()."',
                        gross_value_of_assets = '".$es_cate->getgross_value_of_assets()."'
                        WHERE enterprise_size_categorisation_id = '".$es_cate->getenterprise_size_categorisation_id()."';
                        " ;
        //echo  $sql ;
        return $this->conn->query($sql) ;

    }


}

?>