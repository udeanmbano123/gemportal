<?php
require_once 'DAO.php';
class balanceSheetDAO extends DAO {
    function selectOneBalanceSheetByEmail($email){
        $sql = "SELECT *  FROM balance_sheet where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectBalanceSheetByEmail($email , $typeBal){
        $sql = "SELECT *  FROM balance_sheet where user_id = '".$email."' and balance_sheet_type = '".$typeBal."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectBalanceSheetById($tab_id){
        $sql = "SELECT *  FROM balance_sheet where balance_sheet_id = '".$tab_id."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createBalanceSheet(balanceSheet $capitalreq){
        $sql = "INSERT INTO balance_sheet
                            (balance_sheet_id,
                            user_id,
                            balance_sheet_type,
                            `year`,
                            fixed_assets,
                            current_assets,
                            total_assets,
                            current_liabilities,
                            other_liabilities,
                            total_liabilities,
                            net_assets)
                            VALUES
                            (NULL,
                            '".$capitalreq->getuser_id()."',
                            '".$capitalreq->getbalance_sheet_type()."',
                            '".$capitalreq->getyear()."',
                            '".$capitalreq->getfixed_assets()."',
                            '".$capitalreq->getcurrent_assets()."',
                            '".$capitalreq->gettotal_assets()."',
                            '".$capitalreq->getcurrent_liabilities()."',
                            '".$capitalreq->getother_liabilities()."',
                            '".$capitalreq->gettotal_liabilities()."',
                            '".$capitalreq->getnet_assets()."');
                            " ;
                            echo $sql;
        return $this->conn->query($sql) ;

    }

    function updateBalanceSheet(balanceSheet $capitalreq){
        $sql = "UPDATE balance_sheet
                        SET
                        
                        balance_sheet_type = '".$capitalreq->getbalance_sheet_type()."',
                        year = '".$capitalreq->getyear()."',
                        fixed_assets = '".$capitalreq->getfixed_assets()."',
                        current_assets = '".$capitalreq->getcurrent_assets()."',
                        total_assets = '".$capitalreq->gettotal_assets()."',
                        current_liabilities = '".$capitalreq->getcurrent_liabilities()."',
                        other_liabilities = '".$capitalreq->getother_liabilities()."',
                        total_liabilities = '".$capitalreq->gettotal_liabilities()."',
                        net_assets = '".$capitalreq->getnet_assets()."'
                        WHERE balance_sheet_id = '".$capitalreq->getbalance_sheet_id()."';
                        " ;
        //echo  $sql ;
        return $this->conn->query($sql) ;

    }
    function deleteBalanceSheet(balanceSheet $capitalreq){
        $sql = "DELETE FROM     balance_sheet WHERE  balance_sheet_id = '".$capitalreq->getbalance_sheet_id()."';" ;
        
       return $this->conn->query($sql) ;

    }
}
?>