<?php
require_once 'DAO.php';
class incomeStatementDAO extends DAO {
    function selectOneincomeStatementByEmail($email){
        $sql = "SELECT *  FROM income_statement where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectincomeStatementByEmail($email , $typeInc){
        $sql = "SELECT *  FROM income_statement where user_id = '".$email."' and income_statement_type = '".$typeInc."'  ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectincomeStatementByID($tab_id){
        $sql = "SELECT *  FROM income_statement where income_statement_id = '".$tab_id."'   ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createincomeStatement(incomeStatement $capitalreq){
        $sql = "INSERT INTO income_statement
(income_statement_id,
user_id,
income_statement_type,
year,
gross_income_per,
gross_income,
cost_of_sales,
gross_profit,
net_profit)
VALUES
(NULL,
'".$capitalreq->getuser_id()."',
'".$capitalreq->getincome_statement_type()."',
'".$capitalreq->getyear()."',
'".$capitalreq->getgross_income_per()."',
'".$capitalreq->getgross_income()."',
'".$capitalreq->getcost_of_sales()."',
'".$capitalreq->getgross_profit()."',
'".$capitalreq->getnet_profit()."');" ;

        return $this->conn->query($sql) ;

    }

    function updateincomeStatement(incomeStatement $capitalreq){
        $sql = "UPDATE income_statement
SET

income_statement_type = '".$capitalreq->getincome_statement_type()."',
year = '".$capitalreq->getyear()."',
gross_income_per = '".$capitalreq->getgross_income_per()."',
gross_income = '".$capitalreq->getgross_income()."',
cost_of_sales = '".$capitalreq->getcost_of_sales()."',
gross_profit = '".$capitalreq->getgross_profit()."',
net_profit = '".$capitalreq->getnet_profit()."'
WHERE income_statement_id = '".$capitalreq->getincome_statement_id()."';" ;
        //echo  $sql ;
        return $this->conn->query($sql) ;

    }
    function deleteIncomeStatement(incomeStatement $capitalreq){
        $sql = "DELETE FROM    income_statement WHERE income_statement_id = '".$capitalreq->getincome_statement_id()."';" ;
        
       return $this->conn->query($sql) ;

    }
}

?>