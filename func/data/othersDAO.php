<?php
require_once 'DAO.php';
class othersDAO extends DAO{

    function HistoricalYears(){
        $lastyr=date("Y",strtotime("1 year"));
        $last2yrs= date("Y",strtotime("-2 year"));
        $last3yrs= date("Y",strtotime("-3 year"));
        $historical_years=array($lastyr,$last2yrs,$last3yrs);
        return $historical_years;
    }


    function ProjectedYears()
    {
        $nextyear=date("Y",strtotime("1 year"));
        $next2yrs=date("Y",strtotime("2 year"));
        $next3yrs=date("Y",strtotime("3 year"));
        $projected_years=array($nextyear,$next2yrs,$next3yrs);
        return $projected_years;

    }

    function getNumYears(){
        $sql = "SELECT * FROM financial_year;" ;
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;

        }
        return $return;
    }


    function FinancialAdvisors(){
        $sql = "SELECT * FROM financial_financier_companies ";
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;

        }
        return $return;
    }

    function getAllCountries(){
        $sql = "SELECT * FROM para_countries ";
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function getAllMessages($email){
        $sql = "SELECT * FROM messages WHERE messages_to = '$email' ORDER BY id DESC ";
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function sendMessage($email , $title , $message){
        $sqlss =  "INSERT INTO `messages`(id, messages_body, messages_subject, messages_isimportant,
 messages_isread, messages_docs, messages_to, messages_from) VALUES (NULL, '".$message."' , '".$title."' , '1',
 '1', '','".$email."' , 'Finsec Admin') ; " ;

        return $this->conn->query($sqlss) ;
    }
    function getAllRegion(){
        $sql = "SELECT * FROM para_regions ";
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function getAllBusinessNature(){
        $sql = "SELECT * FROM business_nature ";
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function getDirectors($email_id){
        $sql = "SELECT * FROM directors WHERE user_id = '".$email_id."'";
        $return[]=null;
        try {
            $result = mysqli_query($this->conn, $sql);
            while($row=mysqli_fetch_array($result)) {
                $return[] = $row;
            }
            return $return;
        }catch(Exception $e) {
            return '';
        }
    }

    function getAllScores($id){
        $sql = "SELECT user_scores_total FROM user_scores  WHERE user_scores_id = '$id' ";
        $result = mysqli_query($this->conn, $sql);
        $return  = "" ;
        while($row=mysqli_fetch_array($result)) {
            $return = $row['user_scores_total'];
        }
        return $return;
    }
    function getQualified(){
        $sql = "SELECT 
t.scores_total_user , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
GROUP BY t.scores_total_user 
" ;
        $result = mysqli_query($this->conn, $sql);
        $count = 0 ;
        while($row=mysqli_fetch_array($result)) {
            if($row['total_score'] > $this->getThreshHold()){
                @$user_app=  (new controlDAO())-> getUser()->getUserApplicationLevel($row['scores_total_user']) ;

                if(@$user_app == "BASIC" || @$user_app == "ADVANCED" || @$user_app == "INTERMEDIATE"){
                    //not done
                }else {
                    $count++;
                }

            }
        }
        return $count;

    }
    function getPendingApplications(){
        $sql = "SELECT 
SUM(ut.score) as total_score ,
qt.users_fullname , u.company_name, u.business_sector , u.region_of_incorporation , u.telephone ,
qt.users_created_date ,u.user_id as email, zb.send_to ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u, applicant_level zb
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
and t.scores_total_user = zb.user_id  and zb.send_to != 'Financier'
GROUP BY t.scores_total_user ORDER BY  total_score DESC 
" ;
        $result = mysqli_query($this->conn, $sql);
        $myarr = [] ;
        $i = 0 ;
        while($row=mysqli_fetch_array($result)) {
            if($row['total_score'] > $this->getThreshHold()){
                @$user_app=  (new controlDAO())-> getUser()->getUserApplicationLevel($row['email']) ;

                if(@$user_app == "BASIC" || @$user_app == "ADVANCED" || @$user_app == "INTERMEDIATE"){
                    //not done
                }else {
                    $myarr[$i] = $row ;
                    $i++;
                }


            }
        }
        return $myarr;

    }
    function getAllApplicationsLists(){
        $sql = "SELECT 
SUM(ut.score) as total_score ,
qt.users_fullname , u.company_name, u.business_sector , u.region_of_incorporation , u.telephone ,
qt.users_created_date ,u.user_id as email, zb.send_to ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u, applicant_level zb
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
and t.scores_total_user = zb.user_id 
GROUP BY t.scores_total_user ORDER BY  total_score DESC 
" ;
        $result = mysqli_query($this->conn, $sql);
        $myarr = [] ;
        $i = 0 ;
        while($row=mysqli_fetch_array($result)) {

            $myarr[$i] = $row ;
            $i++;

        }
        return $myarr;

    }
    function getFinancierRecomendedApplicationsRequestView(){
        $sql = "SELECT 
t.scores_total_user , vt.done_by ,  vt.approve_fin , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required ,
u.business_sector , u.region_of_incorporation ,u.telephone , u.company_name
FROM scores_total t , scores ut , users qt , capital_requirements u , approved_applications vt
WHERE t.scores_id = ut.id 
and vt.applicant_id = u.user_id
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
GROUP BY vt.id  " ;
        $result = mysqli_query($this->conn, $sql);
        $myarr = [] ;
        $i = 0 ;
        while($row=mysqli_fetch_array($result)) {
            $myarr[$i] = $row ;
            $i++;
        }
        return $myarr;

    }
    function getFinancierRecomendedApplications(){
        $sql = "SELECT 
SUM(ut.score) as total_score ,
qt.users_fullname , u.company_name, u.business_sector , u.region_of_incorporation , u.telephone ,
qt.users_created_date ,u.user_id as email, zb.send_to ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u, applicant_level zb
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
and t.scores_total_user = zb.user_id and zb.send_to = 'Financier'
GROUP BY t.scores_total_user ORDER BY  total_score DESC " ;
        $result = mysqli_query($this->conn, $sql);
        $myarr = [] ;
        $i = 0 ;
        while($row=mysqli_fetch_array($result)) {
            if($row['total_score'] > $this->getThreshHold()){
                $myarr[$i] = $row ;
                $i++;
            }
        }
        return $myarr;

    }
    function getFinancierAcceptedApplications(){
        $sql = "SELECT 
SUM(ut.score) as total_score ,
qt.users_fullname , u.company_name, u.business_sector , u.region_of_incorporation , u.telephone ,
qt.users_created_date ,u.user_id as email,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email
GROUP BY t.scores_total_user ORDER BY  total_score DESC 
" ;
        $result = mysqli_query($this->conn, $sql);
        $myarr = [] ;
        $i = 0 ;
        while($row=mysqli_fetch_array($result)) {
            if($row['total_score'] > $this->getThreshHold()){
                $myarr[$i] = $row ;
                $i++;
            }
        }
        return $myarr;

    }

    function getStageValue($stage){
        $sql = "SELECT 
t.scores_total_user , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required ,
zb.send_to
FROM scores_total t , applicant_level zb, scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
and t.scores_total_user = zb.user_id and zb.send_to ='$stage'
GROUP BY t.scores_total_user " ;

        $result = mysqli_query($this->conn, $sql);
        $count = 0 ;
        while($row=mysqli_fetch_array($result)) {
            $count += $row['capital_required'] ;
        }
        return $count;

    }

    function getStageValueAdvance($stage){
        $sql = "SELECT 
t.scores_total_user , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required ,
zb.send_to
FROM scores_total t , applicant_level zb, scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
and t.scores_total_user = zb.user_id and zb.send_to in $stage
GROUP BY t.scores_total_user " ;

        $result = mysqli_query($this->conn, $sql);
        $count = 0 ;
        while($row=mysqli_fetch_array($result)) {
            $count += $row['capital_required'] ;

        }
        return $count;

    }

    function getRegionValue($region){
        $sql = "SELECT 
t.scores_total_user , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date , u.company_name, u.region_of_incorporation,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email and u.region_of_incorporation = '$region'
GROUP BY t.scores_total_user " ;

        $result = mysqli_query($this->conn, $sql);
        $count = 0 ;
        while($row=mysqli_fetch_array($result)) {
            $count += $row['capital_required'] ;
        }
        return $count;

    }
    function getSectorValue($sector){
        $sql = "SELECT 
t.scores_total_user , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date , u.company_name, u.business_sector,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email and u.business_sector = '$sector'
GROUP BY t.scores_total_user " ;

        $result = mysqli_query($this->conn, $sql);
        $count = 0 ;
        while($row=mysqli_fetch_array($result)) {
            $count += $row['capital_required'] ;
        }
        return $count;

    }
    function getSectorCount($sector){
        $sql = "SELECT 
count(u.user_id) as 'Total'
FROM  capital_requirements u
WHERE 
u.business_sector = '$sector'
" ;

        $result = mysqli_query($this->conn, $sql);
        $return  = "" ;
        while($row=mysqli_fetch_array($result)) {
            $return = $row['Total'];
        }
        return $return;


    }
    function getRegionCount($region){
        $sql = "SELECT 
count(u.user_id) as 'Total'
FROM  capital_requirements u
WHERE 
u.region_of_incorporation = '$region'
" ;

        $result = mysqli_query($this->conn, $sql);
        $return  = "" ;
        while($row=mysqli_fetch_array($result)) {
            $return = $row['Total'];
        }
        return $return;


    }
    function getStageValueAll(){
        $sql = "SELECT 
t.scores_total_user , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required ,
zb.send_to
FROM scores_total t , applicant_level zb, scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
and t.scores_total_user = zb.user_id
GROUP BY t.scores_total_user " ;

        $result = mysqli_query($this->conn, $sql);
        $count = 0 ;
        while($row=mysqli_fetch_array($result)) {
            $count += $row['capital_required'] ;

        }
        return $count;

    }
    function getThreshHold(){
        $sql = "SELECT `value` FROM parameters WHERE data = 'threshold score';";
        $result = mysqli_query($this->conn, $sql);
        $return  = "" ;
        while($row=mysqli_fetch_array($result)) {
            $return = $row['value'];
        }
        return $return;
    }

    function getApplicationLevel($my_level){
        $sql = "SELECT `send_to` FROM applicant_level WHERE user_id = '$my_level';";
        $result = mysqli_query($this->conn, $sql);
        $return  = "" ;
        while($row=mysqli_fetch_array($result)) {
            $return = $row['send_to'];
        }
        return $return;
    }

    function checkapplicationInterest($my_level , $app_id){
        $sql = "SELECT * FROM approved_applications  WHERE done_by = '$my_level' and applicant_id = '$app_id' ;";
        $result = mysqli_query($this->conn, $sql);
        $count = 0 ;
        while($row=mysqli_fetch_array($result)) {
            $count++ ;
        }
        return $count;
    }
    function checkapplicationApproved($my_level , $app_id){
        $sql = "SELECT * FROM approved_applications  WHERE done_by = '$my_level' and applicant_id = '$app_id' and  approve_fin = 'yes' ;";
        $result = mysqli_query($this->conn, $sql);
        $count = 0 ;
        while($row=mysqli_fetch_array($result)) {
            $count++ ;
        }
        return $count;
    }

    function getRecomendedApplications(){
        $sql = "SELECT 
t.scores_total_user , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u , applicant_level zb
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
and t.scores_total_user = zb.user_id 
and zb.send_to = 'Financier'
GROUP BY t.scores_total_user
order by qt.users_created_date desc 
" ;
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;

    }

    function getRecentApplications(){
        $sql = "SELECT 
t.scores_total_user , 
SUM(ut.score) as total_score ,
qt.users_fullname , 
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email 
GROUP BY t.scores_total_user 
order by qt.users_created_date desc
" ;
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;

    }
    function getRecentApplicationsRankScores(){
        $sql = "SELECT 
SUM(ut.score) as total_score ,
qt.users_fullname , u.company_name,
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email
GROUP BY t.scores_total_user ORDER BY  total_score DESC " ;
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;

    }
    function getRecomendedApplicationsRankScores(){
        $sql = "SELECT 
SUM(ut.score) as total_score ,
qt.users_fullname , u.company_name,
qt.users_created_date ,
(u.raised_equity + u.raised_debt + u.raised_other) as capital_required 
FROM scores_total t , scores ut , users qt , capital_requirements u , applicant_level zb
WHERE t.scores_id = ut.id 
and t.scores_total_user = u.user_id 
and t.scores_total_user = qt.users_email and t.scores_total_user = zb.user_id 
and zb.send_to = 'Financier'
GROUP BY t.scores_total_user ORDER BY  total_score DESC" ;
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;

    }
    function setCompanyProfitLoss($id ,$year){
        $sql = " INSERT INTO fin_profit_loss
                    (`fin_profit_loss_id`,
                    `user_id`,
                    `year`,
                    `is_audited`,
                    `profit_loss_Saleofgoods`,
                    `profit_loss_Renderingofservices`,
                    `profit_loss_OtherRevenues`,
                    `profit_loss_CostofSales`,
                    `profit_loss_Otheroperatingincome`,
                    `profit_loss_Otheroperatingexpenses`,
                    `profit_loss_Staffcosts`,
                    `profit_loss_Depreciationarmotisation`,
                    `profit_loss_Increaseinfairvalueadjustmentsimpairments`,
                    `profit_loss_Decreaseinfairvalueadjustmentsimpairments`,
                    `profit_loss_Financecosts`,
                    `profit_loss_Financeincome`,
                    `profit_loss_Incometaxexpense`,
                    `profit_loss_AttributabletoEquityholdersoftheparent`,
                    `profit_loss_AttributabletoNoncontrollingintrests`)
                    VALUES
                    (NULL,
                    '$id',
                    '$year',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0');
                            
        
        ";
        return $this->conn->query($sql) ;
    }

    function setCompanyCashFlow($id ,$year){
        $sql = "INSERT INTO fin_cash_flow
                    (`fin_cash_flow_id`,
                    `user_id`,
                    `year`,
                    `is_audited`,
                    `cash_flow_LossProfitbeforetax`,
                    `cash_flow_Adjustments`,
                    `cash_flow_NetCashInvestingactivities`,
                    `cash_flow_Netfinancingactivities`,
                    `cash_flow_Cashequivalents`)
                    VALUES
                    (NULL,
                    '$id',
                    '$year',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0');
                    ";
        return $this->conn->query($sql) ;
    }

    function setCompanyBalanceSheet($id ,$year){
        $sql = "INSERT INTO fin_balance_sheet
                        (`fin_balance_sheet_id`,
                        `non_current_assests_Propertyplantandequipment`,
                        `non_current_assests_Investmentproperties`,
                        `non_current_assests_Intangibleassets`,
                        `non_current_assests_Other`,
                        `current_assets_Biologicalassets`,
                        `current_assets_Inventories`,
                        `current_assets_Tradeotherreceivables`,
                        `current_assets_Prepayments`,
                        `current_assets_Bank`,
                        `current_assets_Cash`,
                        `current_assets_Other`,
                        `equity_attributed_Issuedsharecapital`,
                        `equity_attributed_Sharepremium`,
                        `equity_attributed_Revenuereserves`,
                        `equity_attributed_Capitalreserves`,
                        `equity_attributed_Other`,
                        `non_controlling_interest`,
                        `non_current_liabilities_Longtermborrowings`,
                        `non_current_liabilities_Defferredtaxliabilities`,
                        `non_current_liabilities_Other`,
                        `current_liabilities_Shorttermborrowings`,
                        `current_liabilities_Tradeotherpayables`,
                        `current_liabilities_Currenttaxliability`,
                        `current_liabilities_Other`,
                        `OtherTotal_Ordinary_shares`,
                        `OtherMarket_price_per_share`,
                        `user_id`,
                        `year`,
                        `is_audited`)
                        VALUES
                        (NULL,
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '0',
                        '$id',
                        '$year',
                        '0');
                        ";
        return $this->conn->query($sql) ;
    }
    function setBusinessPlan($user_id){
        $sql = "INSERT INTO business_plan
                        (`id`,
                        `user_id`,
                        `Executive_Problem`,
                        `Executive_Solution`,
                        `Executive_PESTEL`,
                        `Opportunity_Problem_Statement`,
                        `Opportunity_Proposed_Solution`,
                        `Opportunity_Validation_of_Problem_Solution`,
                        `Opportunity_Future_Prospects_of_Issuer`,
                        `Market_Analysis_Market_Segmentation`,
                        `Market_Analysis_Target_Market_Segment_Strategy`,
                        `Market_Analysis_Market_Needs`,
                        `Market_Analysis_Market_Trends`,
                        `Market_Analysis_Market_Growth`,
                        `Market_Analysis_Key_Customers`,
                        `Market_Analysis_Future_Markets`,
                        `Market_Analysis_Competition`,
                        `Market_Analysis_Competitors_and_Alternatives`,
                        `Market_Analysis_Our_Advantages`,
                        `Execution_Marketing_Plan`,
                        `Execution_Sales_Plan`,
                        `Execution_Location_and_Facilities`,
                        `Execution_Technology`,
                        `Execution_Equipment_Tools`,
                        `Execution_Milestones`,
                        `Execution_Key_Metrics`,
                        `Company_Management_Corporate_Governance_Framework`,
                        `Company_Management_Management_Team`,
                        `Company_Management_Management_Team_Gaps`,
                        `Company_Management_Personnel_Plan`,
                        `Company_Management_Company_History_Ownership`)
                        VALUES
                        (NULL,
                        '".$user_id."',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        ''); ";

        return $this->conn->query($sql) ;
    }

    function setPrelisting($user_id){
        $sql = "INSERT INTO prelisting
                        (`id`,
                        `user_id`,
                        `pre_listing_Introduction`,
                        `pre_listing_General_Description_programme`,
                        `pre_listing_Statement_changes_Equity`,
                        `pre_listing_Borrowing_Powers`,
                        `pre_listing_Statement_adequacy_capital`,
                        `pre_listing_Members_board_prospects`)
                        VALUES
                        (NULL,
                            '".$user_id."',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '');
                         ";

        return $this->conn->query($sql) ;
    }

    function setScore($email , $id_set){
        $testVar = $this->searchScore($id_set , $email) ;

        if($testVar == "" ){
            $test_Var =  $this->getScoreInputID($id_set) ;
            $sql = "INSERT INTO scores_total (scores_total_user , scores_id) VALUES ('$email' , '$test_Var')  " ;

            return $this->conn->query($sql) ;
        }
        return true  ;
    }
    function deleteSetScore($email , $id_set){
        $testVar_ = $this->searchScore($id_set , $email) ;

        if($testVar_ != "" ){
            $sql = "DELETE FROM scores_total WHERE scores_total_id ='$testVar_'  " ;
            return $this->conn->query($sql) ;
        }
        return true  ;
    }
    function getScoreInputID($id_set){
        $sql = "SELECT id FROM scores WHERE form_input  = '$id_set'; ";
        $result = mysqli_query($this->conn, $sql);
        $return  = "" ;

        while($row=mysqli_fetch_array($result)) {
            $return = $row['id'];
        }
        return $return;
    }

    function searchCF($id) {
        $sql = " SELECT fin_cash_flow_id  FROM fin_cash_flow WHERE user_id ='$id' ";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_num_rows($result);
    }
    function searchBS($id) {
        $sql = " SELECT fin_balance_sheet_id  FROM fin_balance_sheet WHERE user_id ='$id' ";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_num_rows($result);
    }
    function searchPL($id) {
        $sql = " SELECT fin_profit_loss_id  FROM fin_profit_loss WHERE user_id ='$id' ";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_num_rows($result);
    }
    function searchBP($id) {
        $sql = " SELECT id  FROM business_plan WHERE user_id ='$id' ";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_num_rows($result);
    }
    function searchPS($id) {
        $sql = " SELECT id  FROM prelisting WHERE user_id ='$id' ";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_num_rows($result);
    }
    function searchScore($id , $email) {
        $sql = "SELECT scores_total_id FROM scores_total t , scores sc WHERE t.scores_total_user = '$email' 
              and t.scores_id = sc.id and sc.form_input ='$id' ";
        $result = mysqli_query($this->conn, $sql);
        $return  = "" ;

        while($row=mysqli_fetch_array($result)) {
            $return = $row['scores_total_id'];
        }
        return $return;
    }

    function getTotalScores($id){
        $sql = "SELECT SUM(ut.score)  as scores_total FROM scores_total t , scores ut WHERE 
t.scores_id  = ut.id and scores_total_user = '$id'";

        $result = mysqli_query($this->conn, $sql);
        $return  = "0" ;
        while($row=mysqli_fetch_array($result)) {
            $return = $row['scores_total'];
        }
        return $return;
    }

    function getAllSectors(){
        $sql = "SELECT * FROM business_sector ";
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function getUserfromPaymentsID($id_num){
        $sql = "SELECT EmailAddress FROM payments WHERE payments_ref = '".$id_num."' " ;
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return = $row['EmailAddress'];

        }
        return $return;
    }
    function getUserPemission($id_num){
        $sql = "SELECT users_type FROM users WHERE users_email = '".$id_num."' " ;
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return = $row['users_type'];
        }
        return $return;
    }
    function updateBusinessPlan($columnTEXT , $valueTEXT , $user_id){
        $sql = "UPDATE business_plan SET `".$columnTEXT."`='".$valueTEXT."' WHERE `user_id`='".$user_id."'; " ;

        return $this->conn->query($sql) ;
    }
    function updatePrelistingStatement($columnTEXT , $valueTEXT , $user_id){
        $sql = "UPDATE prelisting SET `".$columnTEXT."`='".$valueTEXT."' WHERE `user_id`='".$user_id."'; " ;

        return $this->conn->query($sql) ;
    }

    function update_payment($id_num , $reference, $amount, $status, $pollurl, $paynowreference){

        $sql = " UPDATE payments
                SET
                Price = '".$amount."',
                PaymentStatus = '".$status."',
                PaynowRef = '".$paynowreference."',
                PollUrl = '".$pollurl."'
                WHERE payments_ref = '".$id_num."' ;
        " ;
        if($status == "Cancelled"){
            $this->update_User_Pemission($id_num) ;
        }

        return $this->conn->query($sql) ;

    }

    function update_User_Pemission($id_num){
        $email_add = $this->getUserfromPaymentsID($id_num) ;
        $sql = " UPDATE Users
                SET
                users_advance_score = '1'
                WHERE users_email = '".$email_add."' ;
        " ;

        return $this->conn->query($sql) ;
    }

    function createPayment( $Company , $Email , $Pay_ref){
        $sql = "INSERT INTO payments
                    (Id,
                    EmailAddress,
                    Company,
                    Price,
                    PaymentStatus,
                    PostedStatus,
                    PaynowRef,
                    payments_ref,
                    PollUrl
                    )
                    VALUES
                    (NULL ,
                    '".$Email."',
                    '".$Company."',
                    '100',
                    'cancelled',
                    '',
                    '',
                    '".$Pay_ref."',
                    '');
                    " ;

        return $this->conn->query($sql) ;

    }
    function getCountries(){
        $sql = "SELECT *  FROM para_countries ";

        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;

        }
        // print_r($return) ;

        return $return;

    }
    function getThreshold(){
        $sql = "SELECT *  FROM parameters ";

        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;

        }
        // print_r($return) ;

        return $return;

    }
    function editThreshold(threshold $mycompany){
        //var_dump($myUser) ;
        $sql = "UPDATE parameters SET
                    
                    value='".$mycompany->getthreshold_value()."'
                    
                   
                   
                   WHERE parameter_id= '".$mycompany->getthreshold_id()."';
                    " ;





        return $this->conn->query($sql) ;
    }
    function createcompany(companies $mycompany){
        //var_dump($myUser) ;
        $sql = "INSERT INTO financial_financier_companies
                    (users_id,
                    names,
                    type
                    )
                    VALUES
                    ( 
                    '".$mycompany->getuser_id()."',
                    '".$mycompany->getname()."',
                    '".$mycompany->getrole()."' ); " ;


        return $this->conn->query($sql) ;
    }
    function editcompany(companies $mycompany){
        //var_dump($myUser) ;
        $sql = "UPDATE financial_financier_companies SET
                    
                    names='".$mycompany->getname()."',
                    
                   
                   type = '".$mycompany->getrole()."'
                    
                   WHERE financial_advisors_companies_id= '".$mycompany->getcompany_id()."';
                    " ;


        return $this->conn->query($sql) ;
    }
    function deletecompany(companies $mycompany){
        $sql = "DELETE FROM financial_financier_companies  WHERE  financial_advisors_companies_id ='".$mycompany->getcompany_id()."';" ;

        return $this->conn->query($sql) ;

    }
    function createcountry(countries $mycountry){
        //var_dump($myUser) ;
        $sql = "INSERT INTO para_countries
                    (users_id,
                    country_name
                    
                    )
                    VALUES
                    ( 
                    '".$mycountry->getuser_id()."',
                    
                    '".$mycountry->getname()."' ); " ;


        return $this->conn->query($sql) ;
    }
    function editcountry(countries $mycountry){
        //var_dump($myUser) ;
        $sql = "UPDATE para_countries SET
                    
                    country_name='".$mycountry->getname()."'
                    
                   
                   
                    
                   WHERE country_id= '".$mycountry->getcountry_id()."';
                    " ;





        return $this->conn->query($sql) ;
    }
    function getSectors(){
        $sql = "SELECT *  FROM business_sector ";
        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;

        }
        // print_r($return) ;

        return $return;

    }
    function createsector(sectors $mycountry){
        //var_dump($myUser) ;
        $sql = "INSERT INTO business_sector
                    (users_id,
                    sector_name
                    
                    )
                    VALUES
                    ( 
                    '".$mycountry->getuser_id()."',
                    
                    '".$mycountry->getname()."' ); " ;


        return $this->conn->query($sql) ;
    }
    function editsector(sectors $mycountry){
        //var_dump($myUser) ;
        $sql = "UPDATE business_sector SET
                    
                    sector_name='".$mycountry->getname()."'
                    
                   
                   
                    
                   WHERE business_sector_id= '".$mycountry->getcompany_id()."';
                    " ;





        return $this->conn->query($sql) ;
    }
    function getPermissions(){
        $sql = "SELECT * FROM permissions ";

        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;

        }
        // print_r($return) ;

        return $return;

    }
    function AllCompanies()
    {
        $sql = "SELECT * FROM financial_financier_companies ";

        $result = mysqli_query($this->conn, $sql);
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;

        }
        // print_r($return) ;

        return @$return;
    }

    function getPreList($email_id){
        $sql = "SELECT * FROM prelisting WHERE user_id = '".$email_id."'";
        $return[]=null;
        try {
            $result = mysqli_query($this->conn, $sql);
            while($row=mysqli_fetch_array($result)) {
                $return[] = $row;
            }
            return $return;
        }catch(Exception $e) {
            return '';
        }
    }
    function getBusinessPlan($email_id){
        $sql = "SELECT * FROM business_plan WHERE user_id = '".$email_id."'";
        $return[]=null;
        try {
            $result = mysqli_query($this->conn, $sql);
            while($row=mysqli_fetch_array($result)) {
                $return[] = $row;
            }
            return $return;
        }catch(Exception $e) {
            return '';
        }
    }
}


?>