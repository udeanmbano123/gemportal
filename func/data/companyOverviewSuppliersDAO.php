<?php
require_once 'DAO.php';
class companyOverviewSuppliersDAO extends DAO {
    function selectOnecompanyOverviewSuppliersByEmail($email){
        $sql = "SELECT *  FROM company_overview_suppliers where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectcompanyOverviewCustomersByEmail($email){
        $sql = "SELECT *  FROM company_overview_suppliers where user_id = '".$email."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createcompanyOverviewSuppliers(companyOverviewSuppliers $capitalreq){
        $sql = "INSERT INTO company_overview_suppliers
(company_overview_suppliers_id,
user_id,
company_overview_id,
customer_name,
customer_location,
customer_years_rel,
customer_sales,
customer_trade_terms,
customer_distr_channel)
VALUES
(NULL,
'".$capitalreq->getuser_id()."',
'".$capitalreq->getcompany_overview_id()."',
'".$capitalreq->getcustomer_name()."',
'".$capitalreq->getcustomer_location()."',
'".$capitalreq->getcustomer_years_rel()."',
'".$capitalreq->getcustomer_sales()."',
'".$capitalreq->getcustomer_trade_terms()."',
'".$capitalreq->getcustomer_distr_channel()."');" ;
        return $this->conn->query($sql) ;

    }

    function updatecompanyOverviewSuppliers(companyOverviewSuppliers $capitalreq){
        $sql = "UPDATE company_overview_suppliers
SET

company_overview_id = '".$capitalreq->getcompany_overview_id()."',
customer_name = '".$capitalreq->getcustomer_name()."',
customer_location = '".$capitalreq->getcustomer_location()."',
customer_years_rel = '".$capitalreq->getcustomer_years_rel()."',
customer_sales = '".$capitalreq->getcustomer_sales()."',
customer_trade_terms = '".$capitalreq->getcustomer_trade_terms()."',
customer_distr_channel = '".$capitalreq->getcustomer_distr_channel()."'
WHERE company_overview_suppliers_id = '".$capitalreq->getcompany_overview_suppliers_id()."';" ;
        
        return $this->conn->query($sql) ;

    }
    function deletecompanyOverviewSuppliers(companyOverviewSuppliers $capitalreq){
        $sql = "DELETE FROM  company_overview_suppliers WHERE company_overview_suppliers_id = '".$capitalreq->getcompany_overview_suppliers_id()."';" ;
        
        return $this->conn->query($sql) ;

    }
}

?>