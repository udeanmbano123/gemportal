<?php

require_once 'DAO.php';
class personalFinancialDAO extends DAO {
    function selectOnepersonalFinancialByEmail($email){
        $sql = "SELECT *  FROM personal_financial where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectpersonalFinancialByEmail($email){
        $sql = "SELECT *  FROM personal_financial where user_id = '".$email."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectpersonalFinancialById($tab_id){
        $sql = "SELECT *  FROM personal_financial where personal_financial_id  = '".$tab_id."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createpersonalFinancial(personalFinancial $capitalreq){
        $sql = "INSERT INTO personal_financial
                            (personal_financial_id,
                            user_id,
                            guarantor_name,
                            financial_institution,
                            facility_type,
                            facility_amount,
                            monthly_installment,
                            balance,
                            tenure,
                            start_date)
                            VALUES
                            (NULL,
                            '".$capitalreq->getuser_id()."',
                            '".$capitalreq->getguarantor_name()."',
                            '".$capitalreq->getfinancial_institution()."',
                            '".$capitalreq->getfacility_type()."',
                            '".$capitalreq->getfacility_amount()."',
                            '".$capitalreq->getmonthly_installment()."',
                            '".$capitalreq->getbalance()."',
                            '".$capitalreq->gettenure()."',
                            '".$capitalreq->getstart_date()."');
" ;
        return $this->conn->query($sql) ;

    }

    function updatepersonalFinancial(personalFinancial $capitalreq){
        $sql = "UPDATE personal_financial
                        SET
                        
                        guarantor_name = '".$capitalreq->getguarantor_name()."',
                        financial_institution = '".$capitalreq->getfinancial_institution()."',
                        facility_type = '".$capitalreq->getfacility_type()."',
                        facility_amount = '".$capitalreq->getfacility_amount()."',
                        monthly_installment = '".$capitalreq->getmonthly_installment()."',
                        balance = '".$capitalreq->getbalance()."',
                        tenure = '".$capitalreq->gettenure()."',
                        start_date = '".$capitalreq->getstart_date()."'
                        WHERE personal_financial_id = '".$capitalreq->getpersonal_financial_id()."';" ;
        //echo  $sql ;
        return $this->conn->query($sql) ;

    }
    function deleteFinancial(personalFinancial $capitalreq){
        $sql = "DELETE FROM  personal_financial WHERE  personal_financial_id = '".$capitalreq->getpersonal_financial_id()."';" ;
        
        return $this->conn->query($sql) ;

    }

}
?>