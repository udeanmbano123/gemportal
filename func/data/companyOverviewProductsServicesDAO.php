<?php
require_once 'DAO.php';
class companyOverviewProductsServicesDAO extends DAO {
    function selectOneCompanyOverviewProductsServicesByEmail($email){
        $sql = "SELECT *  FROM company_overview_products_services where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectCompanyOverviewProductsServicesByEmail($email){
        $sql = "SELECT *  FROM company_overview_products_services where user_id = '".$email."'  ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createcompanyOverviewProductsServices(companyOverviewProductsServices $capitalreq){
        $sql = "INSERT INTO company_overview_products_services
                            (company_overview_products_services_id,
                            user_id,
                            company_overview_id,
                            product_service_name,
                            description_of_product,
                            sales_volume)
                            VALUES
                            (NULL,
                            '".$capitalreq->getuser_id()."',
                            '".$capitalreq->getcompany_overview_id()."',
                            '".$capitalreq->getproduct_service_name()."',
                            '".$capitalreq->getdescription_of_product()."',
                            '".$capitalreq->getsales_volume()."');" ;
        return $this->conn->query($sql) ;

    }

    function updatecompanyOverviewProductsServices(companyOverviewProductsServices $capitalreq){
        $sql = "UPDATE company_overview_products_services
                        SET
                        user_id = '".$capitalreq->getuser_id()."',
                        company_overview_id = '".$capitalreq->getcompany_overview_id()."',
                        product_service_name = '".$capitalreq->getproduct_service_name()."',
                        description_of_product = '".$capitalreq->getdescription_of_product()."',
                        sales_volume = '".$capitalreq->getsales_volume()."'
                        WHERE company_overview_products_services_id = '".$capitalreq->getcompany_overview_products_services_id()."';" ;
        echo  $sql ;
        return $this->conn->query($sql) ;

    }
    function deletecompanyOverviewProductsServices(companyOverviewProductsServices $capitalreq){
        $sql = "DELETE FROM company_overview_products_services WHERE company_overview_products_services_id = '".$capitalreq->getcompany_overview_products_services_id()."';" ;
        echo  $sql ;
        return $this->conn->query($sql) ;

    }
}
?>