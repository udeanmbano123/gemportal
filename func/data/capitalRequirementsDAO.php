<?php
require_once 'DAO.php';
class capitalRequirementsDAO extends DAO {



    function selectOneCapitalReqByEmail($email){
            $sql = "SELECT *  FROM capital_requirements where user_id = '".$email."' LIMIT 1 ";
            //echo $sql ;
            $result = mysqli_query($this->conn, $sql);
            $return = array() ;
            while($row=mysqli_fetch_array($result)) {
                $return[] = $row;
            }
            return $return;
    }


    function selectRecommend($email){
            $sql = "SELECT *  FROM recommendations where applicant_id = '".$email."' LIMIT 1 ";
            //echo $sql ;
            $result = mysqli_query($this->conn, $sql);
            $return = "" ;
            while($row=mysqli_fetch_array($result)) {
                $return = $row['recommendation'];
            }
            return $return;
    }

    function createcapitalRequirements(capitalRequirements $capitalreq){
        $sql = "INSERT INTO capital_requirements
                    (capital_req_id,
                    user_id,
                    company_name,
                    company_registration_number,
                    date_of_incorporation,
                    country_of_incorporation,
                    region_of_incorporation,
                    type_of_entity,
                    business_sector,
                    nature_of_business,
                    business_nature,
                    telephone,
                    fax_number,
                    registered_office_physical_address,
                    postal_address,
                    principal_place_of_business,
                    raised_equity,
                    raised_equity_capex,
                    raised_equity_bd,
                    raised_equity_gwc,
                    raised_debt,
                    raised_debt_capex,
                    raised_debt_bd,
                    raised_debt_gwc,
                    raised_other,
                    purpose_of_funds,
                    share_on_offer)
                    VALUES
                    (NULL,
                    '".$capitalreq->getuser_id()."' ,
                    '".$capitalreq->getcompany_name()."' ,
                    '".$capitalreq->getcompany_registration_number()."' ,
                    '".$capitalreq->getdate_of_incorporation()."' ,
                    '".$capitalreq->getcountry_of_incorporation()."' ,
                    '".$capitalreq->getregion_of_incorporation()."' ,
                    '".$capitalreq->gettype_of_entity()."' ,
                    '".$capitalreq->getbusiness_sector()."' ,
                    '".$capitalreq->getnature_of_business()."' ,
                    '".$capitalreq->getbusiness_nature()."' ,
                    '".$capitalreq->gettelephone()."' ,
                    '".$capitalreq->getfax_number()."' ,
                    '".$capitalreq->getregistered_office_physical_address()."' ,
                    '".$capitalreq->getpostal_address()."' ,
                    '".$capitalreq->getprincipal_place_of_business()."' ,
                    '".$capitalreq->getraised_equity()."' ,
                    '".$capitalreq->getraised_equity_capex()."' ,
                    '".$capitalreq->getraised_equity_bd()."' ,
                    '".$capitalreq->getraised_equity_gwc()."' ,
                    '".$capitalreq->getraised_debt()."' ,
                    '".$capitalreq->getraised_debt_capex()."' ,
                    '".$capitalreq->getraised_debt_bd()."' ,
                    '".$capitalreq->getraised_debt_gwc()."' ,
                    '".$capitalreq->getraised_other()."' ,
                    '".$capitalreq->getpurpose_of_funds()."',
                    '".$capitalreq->getshare_on_offer()."');
                    " ;
        return $this->conn->query($sql) ;

    }

    function updateCapitalRequirements(capitalRequirements $capitalreq){
        $sql = "UPDATE capital_requirements
                        SET
                        user_id = '".$capitalreq->getuser_id()."',
                        company_name = '".$capitalreq->getcompany_name()."',
                        company_registration_number = '".$capitalreq->getcompany_registration_number()."',
                        date_of_incorporation = '".$capitalreq->getdate_of_incorporation()."',
                        country_of_incorporation = '".$capitalreq->getcountry_of_incorporation()."',
                        region_of_incorporation = '".$capitalreq->getregion_of_incorporation()."',
                        type_of_entity = '".$capitalreq->gettype_of_entity()."',
                        business_sector = '".$capitalreq->getbusiness_sector()."',
                        nature_of_business = '".$capitalreq->getnature_of_business()."',
                        business_nature = '".$capitalreq->getbusiness_nature()."',
                        telephone = '".$capitalreq->gettelephone()."',
                        fax_number = '".$capitalreq->getfax_number()."',
                        registered_office_physical_address = '".$capitalreq->getregistered_office_physical_address()."',
                        postal_address = '".$capitalreq->getpostal_address()."',
                        principal_place_of_business = '".$capitalreq->getprincipal_place_of_business()."',
                        raised_equity = '".$capitalreq->getraised_equity()."',
                        raised_equity_capex = '".$capitalreq->getraised_equity_capex()."',
                        raised_equity_bd = '".$capitalreq->getraised_equity_bd()."',
                        raised_equity_gwc = '".$capitalreq->getraised_equity_gwc()."',
                        raised_debt = '".$capitalreq->getraised_debt()."',
                        raised_debt_capex = '".$capitalreq->getraised_debt_capex()."',
                        raised_debt_bd = '".$capitalreq->getraised_debt_bd()."',
                        raised_debt_gwc = '".$capitalreq->getraised_debt_gwc()."',
                        raised_other = '".$capitalreq->getraised_other()."',
                        purpose_of_funds = '".$capitalreq->getpurpose_of_funds()."',
                        share_on_offer = '".$capitalreq->getshare_on_offer()."'
                        WHERE capital_req_id = '".$capitalreq->getcapital_req_id()."' ;
                " ;
        echo  $sql ;
        return $this->conn->query($sql) ;

    }

}
