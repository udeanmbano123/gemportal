<?php
require_once 'DAO.php';
class committeesDAO extends DAO {
    function selectOneCommitteesByEmail($email){
        $sql = "SELECT *  FROM committees where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectCommitteesByEmail($email){
        $sql = "SELECT *  FROM committees where user_id = '".$email."'  ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
     function selectcommitteeByDirectorId($tab_id){
        $sql = "SELECT *  FROM committees where committees_id = '".$tab_id."'  ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createCommittees(committees $capitalreq){
        $sql = "INSERT INTO committees
                            (committees_id,
                            user_id,
                            committee_name,
                            chairman,
                            attendees,
                            quoraum,
                            meeting_frequency,
                            committee_responsibilities)
                            VALUES
                            (NULL,
                            '".$capitalreq->getuser_id()."',
                            '".$capitalreq->getcommittee_name()."',
                            '".$capitalreq->getchairman()."',
                            '".$capitalreq->getattendees()."',
                            '".$capitalreq->getquoraum()."',
                            '".$capitalreq->getmeeting_frequency()."',
                            '".$capitalreq->getcommittee_responsibilities()."');
                            " ;
        return $this->conn->query($sql) ;

    }

    function updateCommittees(committees $capitalreq){
        $sql = "UPDATE committees
                    SET
                  
                    committee_name = '".$capitalreq->getcommittee_name()."',
                    chairman = '".$capitalreq->getchairman()."',
                    attendees = '".$capitalreq->getattendees()."',
                    quoraum = '".$capitalreq->getquoraum()."',
                    meeting_frequency = '".$capitalreq->getmeeting_frequency()."',
                    committee_responsibilities = '".$capitalreq->getcommittee_responsibilities()."'
                    WHERE committees_id = '".$capitalreq->getcommittees_id()."';
                    " ;
        //echo  $sql ;
        return $this->conn->query($sql) ;

    }
     function deleteCommittees(committees $capitalreq){
        $sql = "DELETE FROM  committees  WHERE  committees_id ='".$capitalreq->getcommittees_id()."';" ;
        
       return $this->conn->query($sql) ;

    }
}

?>