<?php
require_once 'DAO.php';
class directorsDAO extends DAO {
    function selectOnedirectorsByEmail($email){
        $sql = "SELECT *  FROM directors where user_id = '".$email."' LIMIT 1 ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectdirectorsByEmail($email){
        $sql = "SELECT *  FROM directors where user_id = '".$email."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }
    function selectdirectorsByDirectorId($tab_id){
        $sql = "SELECT *  FROM directors where director_id = '".$tab_id."' ";
        //echo $sql ;
        $result = mysqli_query($this->conn, $sql);
        $return = array() ;
        while($row=mysqli_fetch_array($result)) {
            $return[] = $row;
        }
        return $return;
    }

    function createdirectors(directors $capitalreq){
        $sql = "INSERT INTO directors
                            (director_id,
                            user_id,
                            title,
                            forename,
                            surname,
                            age,
                            citizenship,
                            address)
                            VALUES
                            (NULL,
                            '".$capitalreq->getuser_id()."',
                            '".$capitalreq->gettitle()."',
                            '".$capitalreq->getforename()."',
                            '".$capitalreq->getsurname()."',
                            '".$capitalreq->getage()."',
                            '".$capitalreq->getcitizenship()."',
                            '".$capitalreq->getaddress()."');" ;
        return $this->conn->query($sql) ;

    }

    function updatedirectors(directors $capitalreq){
        $sql = "UPDATE directors
                SET
               
                title = '".$capitalreq->gettitle()."',
                forename = '".$capitalreq->getforename()."',
                surname = '".$capitalreq->getsurname()."',
                age = '".$capitalreq->getage()."',
                citizenship = '".$capitalreq->getcitizenship()."',
                address = '".$capitalreq->getaddress()."'
                WHERE director_id = '".$capitalreq->getdirector_id()."';" ;
        
        return $this->conn->query($sql) ;

    }
     function deletedirectors(directors $capitalreq){
        $sql = "DELETE FROM   directors WHERE director_id = '".$capitalreq->getdirector_id()."';" ;
        
       return $this->conn->query($sql) ;

    }
}

?>