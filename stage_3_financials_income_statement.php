<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ; 
$extra_css = "" ; 
$page_number = "10" ; 
$extra_js = "" ;
$thy_stage = "3" ;
$thy_qsn = "471/472" ;
$thy_qp = round(471/472 * 100, 2);
$my_title = "Income Statement  6 - 7" ;
@$msg=$_GET['msg'];
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg']; 
require_once("require/header.php") ;
require_once 'func/controlDAO.php' ;

?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;padding:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_financials_cashflow.php"> <strong>Cash Flow </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_financials_balancesheet.php"> <strong>Balance Sheet  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <form action="func/controller/IncomeStatementController.php"  class="login-form" method="post">
                          <input type="hidden" value="<?php echo $_SESSION['email']?>"  name="user_id">

                        <table class="table table-bordered responsive">


                                <thead class="text-uppercase">
                                    <th class="c-gray">OPTION</th>
                                    <th colspan="3" class="c-gray">HISTORICAL</th>
                                    <th colspan="3" class="c-gray">PROJECTED</th>
                                </thead>
                            <tbody>
                            <colgroup>
                                <col  style="background-color:#f5f5f5;">
                                <col span="3" style="background-color:yellow">
                                <col span="3" style="background-color:pink">
                            </colgroup>

                            <tr>
                                <td>Years   </td>
                                <td> <input type = "text" value="2015" name="last_3_years" readonly /></td>
                                <td> <input type = "text" value="2016" name="last_2_years" readonly /></td>
                                <td> <input type = "text" value="2017" name="last_year" readonly /></td>
                                <td> <input type = "text" value="2018" name="this_year"    readonly/> </td>
                                <td> <input type = "text" value="2019" name="next_year"   readonly /> </td>
                                <td> <input type = "text" value="2020" name="next_2_years"  readonly /> </td>
                            </tr>
                            <tr>
                                <td> Is Audited  </td>
                                <td> <input type="checkbox"  value="yes" name="last_3_years_audited"  /></td>
                                <td>  <input type="checkbox"  value="yes" name="last_2_years_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="last_year_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="this_year_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="next_year_audited"   /></td>
                                <td>  <input type="checkbox"  value="yes" name="next_2_years_audited" /></td>

                            </tr>
                            <tr>
                                <td> Upload File  </td>
                                <td> <input type="file"  value="yes" name="file_"  /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_"   /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>

                            </tr> 
                            <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >REVENUE </font></b></td>
                            </tr>                           
                            <tr>
                                <td>Cost of Goods </td>
                                <td> <input type = "text"  name="last_3_years_cost_of_goods"  /></td>
                                <td> <input type = "text"  name="last_2_years_cost_of_goods"  /></td>
                                <td> <input type = "text"  name="last_year_cost_of_goods"   /></td>
                                <td> <input type = "text"  name="this_year_cost_of_goods"  /> </td>
                                <td> <input type = "text"  name="next_year_cost_of_goods"  /> </td>
                                <td> <input type = "text"  name="next_2_years_cost_of_goods"  /> </td>
                            </tr>
                           
                            </tr>
                            <tr>
                                <td>Rendering Services </td>
                                <td> <input type = "text"  name="last_3_years_rendering_of_services"  /></td>
                                <td> <input type = "text"  name="last_2_years_rendering_of_services"  /></td>
                                <td> <input type = "text"  name="last_year_rendering_of_services"   /></td>
                                <td> <input type = "text"  name="this_year_rendering_of_services"  /> </td>
                                <td> <input type = "text"  name="next_year_rendering_of_services"  /> </td>
                                <td> <input type = "text"  name="next_2_years_rendering_of_services"  /> </td>
                            </tr>
                            <tr>
                                <td>Other </td>
                                <td> <input type = "text"  name="last_3_years_other_revenues"  /></td>
                                <td> <input type = "text"  name="last_2_years_other_revenues"  /></td>
                                <td> <input type = "text"  name="last_year_other_revenues"   /></td>
                                <td> <input type = "text"  name="this_year_other_revenues"  /> </td>
                                <td> <input type = "text"  name="next_year_other_revenues"   /> </td>
                                <td> <input type = "text"  name="next_2_years_other_revenues"  /> </td>
                            </tr>
                            
                            <tr>
                                <td>Cost of Sales </td>
                                <td> <input type = "text"  name="last_3_years_cost_of_sales"  /></td>
                                <td> <input type = "text"  name="last_2_years_cost_of_sales""  /></td>
                                <td> <input type = "text"  name="last_year_cost_of_sales"   /></td>
                                <td> <input type = "text"  name="this_year_cost_of_sales"  /> </td>
                                <td> <input type = "text"  name="next_year_cost_of_sales"   /> </td>
                                <td> <input type = "text"  name="next_2_years_cost_of_sales"  /> </td>
                            </tr>
                            
                            <tr>
                                <td> Operating Income</td>
                                <td> <input type = "text"  name="last_3_years_other_operating_income" /></td>
                                <td> <input type = "text"  name="last_2_years_other_operating_income"  /></td>
                                <td> <input type = "text"  name="last_year_other_operating_income"  /></td>
                                <td> <input type = "text"  name="this_year_other_operating_income"   /> </td>
                                <td> <input type = "text"  name="next_year_other_operating_income"  /> </td>
                                <td> <input type = "text"  name="last_3_years_other_operating_income" /> </td>
                            </tr>
                            <tr>
                                <td>Other Operating Expenses</td>
                                <td> <input type = "text"  name="last_3_years_other_operating_expenses" /></td>
                                <td> <input type = "text"  name="last_2_years_other_operating_expenses"  /></td>
                                <td> <input type = "text"  name="last_year_other_operating_expenses"  /></td>
                                <td> <input type = "text"  name="this_year_other_operating_expenses"   /> </td>
                                <td> <input type = "text"  name="next_year_other_operating_expenses"  /> </td>
                                <td> <input type = "text"  name="next_2_years_other_operating_expenses" /> </td>
                            </tr>
                            
                            <tr>
                                <td>Staff Costs </td>
                               <td> <input type = "text"  name="last_3_years_staff_costs" /></td>
                                <td> <input type = "text"  name="last_2_years_staff_costs"  /></td>
                                <td> <input type = "text"  name="last_year_staff_costs"  /></td>
                                <td> <input type = "text"  name="this_year_staff_costs"/> </td>
                                <td> <input type = "text"  name="next_year_staff_costs"/> </td>
                                <td> <input type = "text"  name="next_2_years_staff_costs"/> </td>
                            </tr>
                            <tr>
                                <td>Depreciation and Ammortisation </td>
                               <td> <input type = "text"  name="last_3_years_depreciation" /></td>
                                <td> <input type = "text"  name="last_3_years_depreciation"  /></td>
                                <td> <input type = "text"  name="last_year_depreciation"  /></td>
                                <td> <input type = "text"  name="this_year_depreciation"   /> </td>
                                <td> <input type = "text"  name="next_year_depreciation"   /> </td>
                                <td> <input type = "text"  name="next_2_years_depreciation"   /> </td>
                            </tr>
                            <tr>
                                <td>Increase in fair value adjustments impairments   </td>
                                <td> <input type = "text"  name="last_3_years_increase_in_fair_value_adjustments_impairments" /></td>
                                <td> <input type = "text"  name="last_2_years_increase_in_fair_value_adjustments_impairments"  /></td>
                                <td> <input type = "text"  name="last_year_increase_in_fair_value_adjustments_impairments"  /></td>
                                <td> <input type = "text"  name="this_year_increase_in_fair_value_adjustments_impairments"   /> </td>
                                <td> <input type = "text"  name="next_year_increase_in_fair_value_adjustments_impairments"/> </td>
                                <td> <input type = "text"  name="next_2_years_increase_in_fair_value_adjustments_impairments"   /> </td>
                            </tr>
                            <tr>
                                <td>Decrease in fair value adjustments and impairments</td>
                                <td> <input type = "text"  name="last_3_years_decrease_in_fair_value_adjustments_impairments" /></td>
                                <td> <input type = "text"  name="last_2_years_decrease_in_fair_value_adjustments_impairments"  /></td>
                                <td> <input type = "text"  name="last_year_decrease_in_fair_value_adjustments_impairments"  /></td>
                                <td> <input type = "text"  name="this_year_decrease_in_fair_value_adjustments_impairments"    /> </td>
                                <td> <input type = "text"  name="next_year_decrease_in_fair_value_adjustments_impairments"    /> </td>
                                <td> <input type = "text"  name="last_2_years_decrease_in_fair_value_adjustments_impairments"     /> </td>
                            </tr>
                            <tr>
                                <td>Finance costs </td>
                                <td> <input type = "text"  name="last_3_years_finance_costs" /></td>
                                <td> <input type = "text"  name="last_2_years_finance_costs"  /></td>
                                <td> <input type = "text"  name="last_year_finance_costs"  /></td>
                                <td> <input type = "text"  name="this_year_finance_costs"    /> </td>
                                <td> <input type = "text"  name="next_year_finance_costs"   /> </td>
                                <td> <input type = "text"  name="next_2_years_finance_costs"    /> </td>
                            </tr>
                            <tr>
                                <td>Finance income </td>
                                <td> <input type = "text"  name="last_3_years_finance_income"  requyired/></td>
                                <td> <input type = "text"  name="last_2_years_finance_income"  /></td>
                                <td> <input type = "text"  name="last_year_finance_income"  /></td>
                                <td> <input type = "text"  name="this_year_fiance_income"    /> </td>
                                <td> <input type = "text"  name="next_year_finance_income"   /> </td>
                                <td> <input type = "text"  name="next_2_years_finance_income"  /> </td>
                            </tr>
                            <tr>
                                <td>Income tax expense </td>
                                <td> <input type = "text"  name="last_3_years_income_tax_expense"  requyired/></td>
                                <td> <input type = "text"  name="last_2_years_income_tax_expense"  /></td>
                                <td> <input type = "text"  name="last_year_income_tax_expense"  /></td>
                                <td> <input type = "text"  name="this_year_income_tax_expense"    /> </td>
                                <td> <input type = "text"  name="next_year_income_tax_expense"   /> </td>
                                <td> <input type = "text"  name="next_2_years_income_tax_expense"  /> </td>
                            </tr>
                            <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >ATTRIBUTED TO </font></b></td>
                            </tr> 
                             <tr>
                                <td>Equity holders of parent </td>
                                <td> <input type = "text"  name="last_3_years_equity_holders_of_parent"  requyired/></td>
                                <td> <input type = "text"  name="last_2_years_equity_holders_of_parent"  /></td>
                                <td> <input type = "text"  name="last_year_equity_holders_of_parent"  /></td>
                                <td> <input type = "text"  name="this_year_equity_holders_of_parent"    /> </td>
                                <td> <input type = "text"  name="next_year_equity_holders_of_parent"   /> </td>
                                <td> <input type = "text"  name="next_2_years_equity_holders_of_parent"  /> </td>
                            </tr>
                            <tr>
                                <td>Non controlling interests </td>
                                <td> <input type = "text"  name="last_3_years_non_controlling_interests"  requyired/></td>
                                <td> <input type = "text"  name="last_2_years_non_controlling_interests"  /></td>
                                <td> <input type = "text"  name="last_year_non_controlling_interests"  /></td>
                                <td> <input type = "text"  name="this_year_non_controlling_interests"    /> </td>
                                <td> <input type = "text"  name="next_year_non_controlling_interests"   /> </td>
                                <td> <input type = "text"  name="next_2_years_non_controlling_interests"  /> </td>
                            </tr>
                            </tbody>
                        </table>

                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_3_financials_cashflow.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                
                                <button type="submit" class="btn green uppercase btn-outline pull-right">[Next>>]</button>
                            </div>
                        </div>
                    </div>

                        </form>


                    </div>


                    <!--  First form of the dialog
                          Income Statement  -->
                    
                    </div>                    




                </div>
<?php

require_once("require/footer.php") ;

?>