<?php
//comment on the admin company
require_once 'func/controlDAO.php' ;
?>



<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | Capital Online</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

      	<?php 
      		include("req/header_admin.php") ;
          $extra_js = "";
          @$msg=$_GET['msg'];
      	?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Finsec Admin </h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li class="active"><a href="#">Admin </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-2  inbox-panel">
                                    <div>
                                        <div class="list-group mail-list "> 
                                          <a href="admin_home.php" class="list-group-item ">Home </a>
                                          <a href="all_users.php" class="list-group-item ">Users </a>
                                          <a href="scoring_parameters.php" class="list-group-item">Scores </a>
                                           
                                          <a href="admin_countries.php" class="list-group-item">Countries </a>
                                          
                                          <a href="verification_scoring_parameters.php" class="list-group-item">Verification Scoring </a>
                                             
                                             <a href="admin_parameters.php" class="list-group-item ">Parameters </a>
                                          
                                          <a href="admin_companies.php" class="list-group-item  active">Companies </a>
                                          <a href="admin_sector.php" class="list-group-item  ">Sectors </a>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-10 mail_listing">
                                   <h5>Financier Companies :  <p><?php echo "<br><font color='green'  >".@$msg. " </font>" ;?></p></h5>
                                      <a href="javascript:void()" class="new_product">[ Add New ]</a>
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                         <th>Id</th>
                                         <th>Financier Name</th>
                                         <th>Email</th>
                                         <th>Action</th>
                                         <!-- <th>Password</th> -->
                                      </tr>
                                      </thead>
                                      <tbody>
                                       <?php
                                      $ifPopulated =  (new controlDAO())-> getOthers()->AllCompanies() ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="3" style="text-align:center">No Financier Companies found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# directors_shareholding_id, user_id, , ,
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         
                                                         <td id = 'idnum".@$ifPopulated[$i]['financial_advisors_companies_id']."'>".@$ifPopulated[$i]['financial_advisors_companies_id']."</td>
                                                         <td id = 'name".@$ifPopulated[$i]['financial_advisors_companies_id']."'>".@$ifPopulated[$i]['names']."</td>
                                                         <td id = 'type".@$ifPopulated[$i]['financial_advisors_companies_id']."'' >".@$ifPopulated[$i]['type']."</td>
                                                          <td>| <a href='#' id='editprod_".$ifPopulated[$i]['financial_advisors_companies_id']."' class='edit_product'>[Edit]</a>| <a href='func/controller/applicationsController.php?accept=".@$ifPopulated[$i]['email']."' >[delete]</a>| <a href ='func/controller/applicationsController.php?reject=".@$ifPopulated[$i]['email']."'>[Reject]</a> </td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>

                                      </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="exampleModalLabel1">The Companies are:</h4> </div>
                                <form action ="func/controller/othersController.php" method ="POST" name ="product_service">
                                    <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                                    <input type ='hidden' value = 'true' name ='create_prod' id="optn" />
                                    <input type ='hidden' name ='' id="cops_id" />
                                    <input type ='hidden' name ='financial_advisors_companies_id' id="financial_advisors_companies_id" />
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Company:</label>
                                            <input type="text" name ="company" class="form-control" id="ecompany"> </div>
                                        <div class="form-group">
                                            <label for="message-text1" class="control-label">Role:</label>
                                            <input type="text" name ="role" class="form-control" id="erole">
                                        </div>
                                        
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                     <?php
$extra_js = "
<script>
$(document).ready(function(){
    
 
  $(document).on('click', '.new_product', function(){
    $('input[name=edit_prod]').attr('name', 'create_prod');
    $('#exampleModal').modal('show');
    $('#ecompany').val('');
    $('#erole').val('');
    $('#esales_volume').val('');
  });
  $(document).on('click', '.edit_product', function(){
     $('#ecompany').val('');
    $('#erole').val('');
    $('#esales_volume').val('');      
    var id=$(this).attr('id').split('_')[1];
    $('input[name=create_prod]').attr('name', 'edit_prod');
    var first=$('#idnum'+id).text();
    var last=$('#name'+id).text();
    var address=$('#type'+id).text();
        //alert(id+first+last+address) ; 
    $('#exampleModal').modal('show');
    $('#ecompany').val(last.trim());
    $('#erole').val(address.trim());
    $('#cops_id').val(first.trim());
    $('#financial_advisors_companies_id').val(id.trim());
    
  });

  
});
</script>
" ;
?>

            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <?php echo $extra_js;  ?>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>