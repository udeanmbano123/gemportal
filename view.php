 <?php
 ?><div class="login-content" style ="margin-top:0px;">
                         <h1> <?php echo  $my_title ; ?> </h1>
                        <form action="func/controller/companyOverviewController.php" enctype="multipart/form-data" class="login-form" method="post">
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                            <div class="row">
                                <div class="col-xs-12">
                                    Introduction, History and Major Milestones:
                                     <textarea class="form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50" name="company_overview_history"><?=@$company_overview_history;?></textarea>
                                </div>
                            </div>  
                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>The Company’s Products/Services :  <p><?php echo "<br><font color='green'  >".@$msg. " </font>" ;?></p>
                                      <a href="javascript:void()" class="new_product">[ Add New ]</a>
                                    </h5>
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                         <th>Product / Service Name</th>
                                         <th>Description of Product</th>
                                         <th>Sales Volume (% of Total Sales)</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                          $ifPopulated =  (new controlDAO())->getcompanyOverviewProductsServices()->selectCompanyOverviewProductsServicesByEmail($_SESSION['email']) ;
                                          if(!$ifPopulated){
                                          //echo "There is no data " ;

                                            echo '<tr>
                                                     <td colspan="5" style="text-align:center">No Products found</td>
                                                  </tr>' ;
                                          }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //company_overview_products_services_id, user_id, company_overview_id, product_service_name, description_of_product, sales_volume
                                                extract($ifPopulated[$i]);
                                                echo "<tr>
                                                         <td>
                                                            <span id='product_service_name".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                            ".@$ifPopulated[$i]['product_service_name']."
                                                            </span>
                                                         </td>
                                                         <td>
                                                             <span id='description_of_product".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                             ".@$ifPopulated[$i]['description_of_product']."
                                                             </span>
                                                         </td>
                                                         <td>
                                                             <span id='sales_volume".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                             ".@$ifPopulated[$i]['sales_volume']."
                                                             </span>
                                                         </td>
                                                         <td><a href='#' id='editprod_".$ifPopulated[$i]['company_overview_products_services_id']."' class='edit_product'>[Edit]</a> | <a href='#' id='deleteprod_".$ifPopulated[$i]['company_overview_products_services_id']."' class='delete_product'>[Delete]</a> </td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                          }
                                      ?>

                                      </tbody>
                                    </table>
                                </div>

                            </div>

                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>The Company’s Major Raw Materials (In Respect to Manufacturers Only) :
<!--                                      <a href="javascript:void()" data-toggle="modal" data-target="#exampleModalone" data-whatever="@mdo">[ Add New ]</a>-->
                                        <a href="javascript:void()" class="new_raw">[ Add New ] <p><?php echo "<br><font color='green'  >".@$raw_msg. " </font>" ;?></p></a>
                                    </h5> 
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                         <th>Raw Material Name</th>
                                         <th>Description of Raw Material</th>
                                         <th>Purchase Volume (% of Total Purchases)</th>
                                      </tr>
                                      </thead>
                                      <tbody>

                                      <?php
                                      $ifPopulated =  (new controlDAO())->getcompanyOverviewRawMaterials()->selectcompanyOverviewRawMaterialsByEmail($_SESSION['email']) ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="3" style="text-align:center">No Materials found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //company_overview_products_services_id, user_id, company_overview_id, product_service_name, description_of_product, sales_volume
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>
                                                             <span id='name".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                             ".@$ifPopulated[$i]['name']."
                                                             </span>
                                                         </td>
                                                         <td>
                                                             <span id='desc".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                             ".@$ifPopulated[$i]['desc']."
                                                             </span>
                                                         </td>
                                                         <td>
                                                             <span id='volume".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                             ".@$ifPopulated[$i]['volume']."
                                                             </span>
                                                         </td>
                                                         <td><a href='javascript:void();' id='editprod_".$ifPopulated[$i]['company_overview_raw_materials_id']."' class='edit_raw'>[Edit]</a> | <a href='#' id='deleteprod_".$ifPopulated[$i]['company_overview_raw_materials_id']."' class='delete_raw'>[Delete]</a> </td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>

                                      </tbody>
                                    </table>
                                </div>

                            </div>
                            
                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>Major Customers and Distribution Channels :
                                        <a href="javascript:void()" class="new_customer">[ Add New ] <p><?php echo "<br><font color='green'  >".@$customer_msg. " </font>" ;?></p></a>
                                    </h5> 
                                    <table class="table table-bordered">
                                      <thead>

                                      <tr>
                                         <th>Customer (Buyer)’s Name</th>
                                         <th>Location</th>
                                         <th>Years of Relationship</th>
                                         <th>% of Sales</th>
                                         <th>Trade Terms</th>
                                         <th>Distribution Channel</th>
                                      </tr>
                                      </thead>
                                      <tbody>

                                      <?php
                                      $ifPopulated =  (new controlDAO())->getcompanyOverviewCustomers()->selectcompanyOverviewCustomersByEmail($_SESSION['email']) ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="6" style="text-align:center">No Customers  found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>
                                                         <span id='customer_name".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_name']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='customer_location".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_location']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='customer_years_rel".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_years_rel']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='customer_sales".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_sales']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='customer_trade_terms".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_trade_terms']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='customer_distr_channel".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_distr_channel']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <a href='javascript:void();' id='editprod_".$ifPopulated[$i]['company_overview_customers_id']."' class='edit_customer'>[Edit]</a> 
                                                         | 
                                                         <a href='#' id='deleteprod_".$ifPopulated[$i]['company_overview_customers_id']."' class='delete_customer'>[Delete]</a> </td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>

                                      </tbody>
                                    </table>
                                </div>

                            </div>

                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>Major Suppliers and Supply Channels :
                                        <a href="javascript:void()" class="new_supply">[ Add New ] </a><p><?php echo "<br><font color='green'  >".@$supplier_msg. " </font>" ;?></p>
<!--                                      <a href="javascript:void()" data-toggle="modal" data-target="#exampleModalthree" data-whatever="@mdo">[ Add New ]</a>-->
                                    </h5> 
                                    <table class="table table-bordered">
                                      <thead>

                                      <tr>
                                         <th>Customer (Buyer)’s Name</th>
                                         <th>Location</th>
                                         <th>Years of Relationship</th>
                                         <th>% of Sales</th>
                                         <th>Trade Terms</th>
                                         <th>Distribution Channel</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                      $ifPopulated =  (new controlDAO())->getcompanyOverviewSuppliers()->selectcompanyOverviewCustomersByEmail($_SESSION['email']) ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="6" style="text-align:center">No Suppliers found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //company_overview_suppliers_id, user_id, company_overview_id, , , , , ,
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>
                                                         <span id='de_customer_name".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_name']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='de_customer_location".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_location']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='de_customer_years_rel".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_years_rel']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='de_customer_sales".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_sales']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='de_customer_trade_terms".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_trade_terms']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <span id='de_customer_distr_channel".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                         ".@$ifPopulated[$i]['customer_distr_channel']."
                                                         </span>
                                                         </td>
                                                         <td>
                                                         <a href='javascript:void();' id='editprod_".$ifPopulated[$i]['company_overview_suppliers_id']."' class='edit_supply'>[Edit]</a> 
                                                         | 
                                                         <a href='#' id='deleteprod_".$ifPopulated[$i]['company_overview_suppliers_id']."' class='delete_supply'>[Delete]</a> 
                                                         </td>
                                                                                                                  
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>
                                      </tbody>
                                    </table>
                                </div>

                            </div>



                            <div class="row">  
                                <div class="col-xs-12">
                                    The Company’s Protected Rights
                                    <br>
                                    1. <a href="<?=$company_overview_rights;?>"> <?=@$company_overview_rights;?></a>
                                    <br>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="file" muiltiple autocomplete="off" placeholder="Business Nature" name="company_overview_rights"/>
                                </div>

                            </div>




                    </div>