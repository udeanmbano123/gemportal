<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "    <!-- summernotes CSS -->
    <link href='plugins/bower_components/summernote/dist/summernote.css' rel='stylesheet' />" ;
$my_title = "CORPORATE STRUCTURE  4 - 6" ;
$page_number = "8" ;
$thy_stage = "2" ;
$thy_qsn = "47/472" ;
$thy_qp = round(47/472 * 100, 2);
$extra_js = "    
    <script src='plugins/bower_components/summernote/dist/summernote.min.js'></script>
    <script>
    jQuery(document).ready(function() {
        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });
        $('.inline-editor').summernote({
            airMode: true
        });
    });
    window.edit = function() {
        $('.click2edit').summernote()
    }, window.save = function() {
        $('.click2edit').destroy()
    }
    </script>" ;
$main_page_title = "Intermediate Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
require("func/data/connect.php");
$user_email=$_SESSION['email'];
$sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
$result2=mysqli_query($con,$sql2);
while($row2=mysqli_fetch_array($result2)){
    $usertype=$row2['users_type'];
}
if($usertype=='applicant'){

  $ifPopulated =  (new controlDAO())->getcorporateStructure()->selectOnecorporateStructureByEmail($_SESSION['email']) ;
   if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

   }else{
     $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
      foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
      }
       $setOption .= "<input type ='hidden' value = '".@$corporate_structure_id."' name ='corporate_structure_id'/>" ;
    //echo $user_id ;
   }
}
elseif($usertype=='Administrator'){

  $ifPopulated =  (new controlDAO())->getcorporateStructure()->selectOnecorporateStructureByEmail($_GET['email']) ;
   if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

   }else{
     $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
      foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
      }
       $setOption .= "<input type ='hidden' value = '".@$corporate_structure_id."' name ='corporate_structure_id'/>" ;
    //echo $user_id ;
   }
}    
?>

                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5>
                            </div>
                            <?php if($usertype=='applicant'){?>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_2_board_committees.php"> <strong>Board Committees </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_directors_shareholding.php"> <strong>DIRECTORS' SHAREHOLDING </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_proprietors_partners_principapls_directors_profiles.php"> <strong>PROPRIETORS, PARTNERS, PRINCIPALS AND DIRECTORS PROFILES </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_1_review.php"> <strong>STAGE 1 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <?php } ?>
                            <!-- /.col-lg-12 -->
                        </div>
                        <form action="func/controller/corporateStructureController.php"  class="login-form" method="post">
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                            <div class="white-box-main">
                            <div class="row">
                                <div class="col-xs-12">
                                    Details of ownership structure where company is a subsidiary or part of other companies/corporate bodies.:
                                     <textarea  name = "desc" class="summernote form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"><?=@$desc;?></textarea>
                                </div>
                            </div>
                            </div>



                    </div>


                    <div class="login-footer">
                        <?php if($usertype=='applicant'){
                            ?>
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_2_board_committees.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
<!--                                <a href="stage_2_human_resource_organogram.php" class="btn green btn-outline pull-right" >[ Next >>] </a>-->
                                <button type="submit" class="btn green uppercase btn-outline pull-right">[Next >>]</button>
                            </div>
                        </div>
                        <?php } elseif ($usertype=='Administrator') {
                              ?>
                             <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <a href="applicantAlldetails.php?email=<?php echo $_GET['email'] ?>" class="btn green uppercase btn-outline pull-right">[<< Back]</a>
                            </div>
                            <?php } ?>
                    </div>

                    </form>
                </div>


<?php

require_once("require/footer.php") ;

?>