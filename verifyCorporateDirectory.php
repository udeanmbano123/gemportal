





<?php
require("func/data/connect.php");
require_once 'func/controlDAO.php' ;
$ifPopulated =  (new controlDAO())->getCorporateDirectory()->selectOnecorporateDirectoryByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>



<!DOCTYPE html>



<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

        <?php 
          include("req/header_main.php") ;
          

        ?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Finsec Analyst [Verification]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">

                                <div class="col-md-12 mail_listing">
                                    
                                         <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                        <div class="login-content" style ="margin-top:0px;">
                         <h4 class="text-uppercase">CORPORATE DIRECTORY</h4>
                            <form action="func/controller/verifyCorporateDirectoryController.php" class="login-form" method="post">
                            <input type ="hidden" value="signup" name="signup"/>
                            <input type ="hidden" value= "<?=@$_GET['email'] ?>"  name ="email"/>
                            <input type ="hidden" value= "corporatedirectory"  name ="form"/>
                            

                            
                            
                                
                             <div class="row">
                                <div class="col-xs-2">
                                    <b>Duration in stated business line:</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Duration" name="duration_in_stated_business_line" value="<?=@$duration_in_stated_business_line;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                   <input   name="pem[]" type="checkbox" 
                                     value="18" ##/>
                                   </div>
                                
                                <div class="col-xs-2">
                                            <b>Corporates</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Corporates" name="customer_basecorporates"  value="<?=@$customer_basecorporates;?>" readonly/>
                                        </div>
                                    <div class="col-xs-2">
                                  <input   name="pem[]" type="checkbox"   value="19" ##/> 
                                </div>
                              </div>


                                
                                    
                                    <div class="row">
                                        
                                       <div class="col-xs-2">
                                            <b>Induviduals</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Induviduals" name="customer_baseindividuals"  value="<?=@$customer_baseindividuals;?>" readonly/>
                                        </div>
                                         <div class="col-xs-2">
                                  <input  name="pem[]" type="checkbox"   value="20" ## />
                                </div>
                              
                              
                                <div class="col-xs-2">
                                    <b>Principal Bankers (Address)</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="email address" name="contact_person_email"  value="<?=@$principal_bankers_address ;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                               <input  type="checkbox"  name="pem[]"    value="21"    ## />
                                </div>


                                    </div>

                                
                            
                            <div class="row">  
                                <div class="col-xs-2">
                                    <b>Business Year End:</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Year" name="business_year_end" value="<?=@$business_year_end;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                  <input  name="pem[]"  type="checkbox"    value="22" ##/> 
                                </div>
                              
                                <div class="col-xs-2">
                                    <b>Business Premises are</b>
                                  
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Year" name="business_year_end" value="<?=@$business_premises_are;?>" readonly/>
                                
                                </div> 
                                <div class="col-xs-2">
                                 <input  type="checkbox"  name="pem[]"   
                                      
                                      value="23"  ## />

                                </div>
                                </div>
                                   <div class="row">                             
                                <div class="col-xs-2">
                                    <b>Name of holding company</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Holding company" name="name_of_holding_company" value="<?=@$name_of_holding_company;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                  <input  type="checkbox"  name="pem[]"    value="24" ##/>
                                </div>

                            

                            
                                <div class="col-xs-2">
                                    
                                        
                                            <b> Contact person Name</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="name" name="contact_person_name"  value="<?=@$contact_person_name;?>" readonly/>
                                        </div>
                                        <div class="col-xs-2">
                                <input  type="checkbox"  name="pem[]"    value="25"  ##/>
                                </div>
                              </div>
                                       <div class="row">
                                       <div class="col-xs-2">
                                            <b> Contact person Position</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="position" name="contact_person_position"  value="<?=@$contact_person_position;?>" readonly/>
                                        </div>
                                        <div class="col-xs-2">
                                <input  type="checkbox"  name="pem[]"    value="26"  ## />
                                </div>
                                       <div class="col-xs-2">
                                            <b> Contact person Cell</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="cellphone" name="contact_person_cell"  value="<?=@$contact_person_cell;?>" readonly/>
                                        </div>
                                  <div class="col-xs-2">
                                <input  type="checkbox"  name="pem[]"    value="27"  ##/>
                                </div>
                              </div>
                                       <div class="row">
                                        <div class="col-xs-2">
                                            <b>Tel</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="telephone" name="contact_person_telephone"  value="<?=@$contact_person_telephone;?>" readonly/>
                                        </div>
                                  <div class="col-xs-2">
                               <input  type="checkbox"  name="pem[]"   value="28" ##/>
                                </div>

                                         <div class="col-xs-2">
                                            <b>Email</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="email address" name="contact_person_email"  value="<?=@$contact_person_email;?>" readonly/>
                                        </div>
                                        <div class="col-xs-2">
                                <input  type="checkbox"  name="pem[]"   value="29"  ##/>
                                </div>
                                      </div>
                                       
                                    
                                
                            
                             <div class="row"> 
                                <div class="col-xs-2">
                                    <b>Company Secretary Address</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="email address" name="contact_person_email"  value="<?=@$company_secretary_address;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                <input  type="checkbox"  name="pem[]"    value="30"  ##/>
                                </div>
                                <div class="col-xs-2">
                                    <b>Attorney & Office Address</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="email address" name="contact_person_email"  value="<?=@$attorney_office_address ;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                <input  type="checkbox"  name="pem[]"    value="31"  ##/>
                                </div>

                              </div>
                              <div class="row">
                                <div class="col-xs-2">
                                    <b>Auditors & Office (Address)</b>
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="email address" name="contact_person_email"  value="<?=@$attorney_office_address ;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                               <input  type="checkbox"  name="pem[]"   
                                      
                                       value="32" ## />
                                </div>



                                <div class="col-xs-2">
                                    <b>Accountant  & Office (Address)</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="email address" name="contact_person_email"  value="<?=@$auditors_office_address ;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                <input  type="checkbox"  name="pem[]"    value="33"  ##/>
                                </div>
                              </div>
                               

                            <div class="row">  
                                <div class="col-xs-2">
                                    <b>Authorised share capital:</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="authorised" name="authorised_share_capital" value="<?=@$authorised_share_capital;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                               <input  type="checkbox"  name="pem[]"    value="34" ##/>
                                </div>
                                <div class="col-xs-2">
                                    <b>Issued share capital :</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="issued" name="issued_share_capital" value="<?=@$issued_share_capital;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                             <input  type="checkbox"  name="pem[]"    value="35" ##/>
                                </div>
                              </div>
                                                             
                            <div >
                                 &nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary">Next</button>
                               </div>
                          </form>

                            </div>

                            





                    </div>

<!--                                         <tr>
                                           <td colspan="4" style="text-align:center">No Appliacction found</td>
                                        </tr>  -->
                                        
                                    
                                        
                                     
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>