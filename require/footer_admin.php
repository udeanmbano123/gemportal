

</div>
</div>
<!-- END : LOGIN PAGE 5-1 -->
<!--[if lt IE 9]>
<script src="public/assets/global/plugins/respond.min.js"></script>
<script src="public/assets/global/plugins/excanvas.min.js"></script>
<script src="public/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="public/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="public/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<?php
echo $extra_js ;
?>

<?php if(@$cashflow == "true" ){ ?>

    <script type="text/javascript" language="javascript" >
        $(document).ready(function(){

            $('#cashflow_table').editable({
                container: 'body',
                selector: 'td.name',
                url: "update_cashflow.php",
                title: 'Enter Value',
                type: "POST",
                //dataType: 'json',
                validate: function(value){
                    if($.trim(value) == '')
                    {
                        return 'This field is required';
                    }
                    var regex = /((\d+)((\.\d{1,4})?))$/;
                    if(! regex.test(value))
                    {
                        return 'Enter Valid Numbers only!';
                    }
                }
            });




        });
    </script>
<?php }?>
<?php if(@$profitloss == "true" ){ ?>

    <script type="text/javascript" language="javascript" >
        $(document).ready(function(){

            $('#profitloss_table').editable({
                container: 'body',
                selector: 'td.name',
                url: "update_profitloss.php",
                title: 'Enter Value',
                type: "POST",
                //dataType: 'json',
                validate: function(value){
                    if($.trim(value) == '')
                    {
                        return 'This field is required';
                    }
                    var regex = /((\d+)((\.\d{1,4})?))$/;
                    if(! regex.test(value))
                    {
                        return 'Enter Valid Numbers only!';
                    }
                }
            });




        });
    </script>
<?php }?>
<?php if(@$balance_sheet == "true" ){ ?>

    <script type="text/javascript" language="javascript" >
        $(document).ready(function(){
            var result = [];
            var result2 = [];
            var result3 = [];
            var result4 = [];
            var result5 = [];
            var result6 = [];
            var result7 = [];
            var result8 = [];
            var result9 = [];
            var result10 = [];

            $('#div_t2 table tr:not(:first-child) ').each(function(){
                $('td', this).each(function(index, val){
                    if(index != 0 ){
                        if(!result[index])
                            result[index] = 0;
                        result[index] += parseInt($(val).text());
                    }else{
                        result[index] = "Total" ;
                    }

                });
            });


            //Fixed Assets
            $('#div_t1 table tr:not(:first-child) ').slice(0,-36).each(function(){
                $('td', this).each(function(index1, val1){
                    var ttl = 0 ;
                    if(index1 != 0 ){
                        if(!result2[index1])
                            result2[index1] = 0;
                        result2[index1] += parseInt($(val1).text());
                        if(index1 == 8){
                            ttl += parseInt($(val1).text());
                            // alert("FIXED Index-"+index1+"Value-"+$(val1).text() + "\n-ttl  =" + ttl) ;
                        }

                    }else{
                        // alert("FIXED Index-"+index1+"\nValue-"+$(val1).text()) ;
                        result2[index1] = "TOTAL NON CURRENT ASSETS" ;
                    }

                });
            });

            //Current Assets
            $('#div_t1 table tr:not(:first-child) ').slice(8, -24).each(function(){
                $('td', this).each(function(index2, val2){
                    if(index2 != 0 ){
                        if(!result3[index2])
                            result3[index2] = 0;
                        result3[index2] += parseInt($(val2).text());
                    }else{
                        // alert("CURRENT Index-"+index2+"Value-"+$(val2).text()) ;
                        result3[index2] = "TOTAL CURRENT ASSETS" ;
                    }

                });
            });
            //Total Equity
            $('#div_t1 table tr:not(:first-child) ').slice(0,-25).each(function(){
                $('td', this).each(function(index2, val2){
                    if(index2 != 0 ){
                        if(!result8[index2])
                            result8[index2] = 0;
                        result8[index2] += parseInt($(val2).text());
                    }else{
                        // alert("CURRENT Index-"+index2+"Value-"+$(val2).text()) ;
                        result8[index2] = "TOTAL ASSETS" ;
                    }

                });
            });
            //NON - CURRENT LIABILITIES
            $('#div_t1 table tr:not(:first-child) ').slice(26, -12).each(function(){
                $('td', this).each(function(index2, val2){
                    if(index2 != 0 ){
                        if(!result4[index2])
                            result4[index2] = 0;
                        result4[index2] += parseInt($(val2).text());
                    }else{
                        // alert("NON - CURRENT LIABILITIES Index-"+index2+" Value-"+$(val2).text()) ;
                        result4[index2] = "TOTAL NON CURRENT LIABILITIES" ;
                    }

                });
            });

            //CURRENT LIABILITIES
            $('#div_t1 table tr:not(:first-child) ').slice(31,-5).each(function(){
                $('td', this).each(function(index2, val2){
                    if(index2 != 0 ){
                        if(!result5[index2])
                            result5[index2] = 0;
                        result5[index2] += parseInt($(val2).text());
                    }else{
                        // alert("CURRENT LIABILITIES Index-"+index2+" Value-"+$(val2).text()) ;
                        result5[index2] = "TOTAL CURRENT LIABILITIES" ;
                    }

                });
            });

            //total LIABILITIES
            $('#div_t1 table tr:not(:first-child) ').slice(26, -5).each(function(){
                $('td', this).each(function(index2, val2){
                    if(index2 != 0 ){
                        if(!result6[index2])
                            result6[index2] = 0;
                        result6[index2] += parseInt($(val2).text());
                    }else{
                        // alert("CURRENT LIABILITIES Index-"+index2+" Value-"+$(val2).text()) ;
                        result6[index2] = "TOTAL LIABILITIES" ;
                    }

                });
            });

            //total equity and liabilities
            $('#div_t1 table tr:not(:first-child) ').slice(14, -5).each(function(){
                $('td', this).each(function(index2, val2){
                    if(index2 != 0 ){
                        if(!result7[index2])
                            result7[index2] = 0;
                        result7[index2] += parseInt($(val2).text());
                    }else{
                        // alert("CURRENT LIABILITIES Index-"+index2+" Value-"+$(val2).text()) ;
                        result7[index2] = "TOTAL EQUITY AND LIABILITIES" ;
                    }

                });
            });
            $('#div_t1 table tr:not(:first-child) ').slice(17, -18).each(function(){
                $('td', this).each(function(index2, val2){

                    if(index2 != 0 ){
                        if(!result9[index2])
                            result9[index2] = 0;
                        result9[index2] += parseInt($(val2).text());
                    }else {
                        // alert("CURRENT LIABILITIES Index-"+index2+" Value-"+$(val2).text()) ;
                        result9[index2] = "EQUITY ATTRIBUTED TO OWNERS OF THE PARENT (SUM)";
                    }

                });
            });
            $('#div_t1 table tr:not(:first-child) ').slice(18, -16).each(function(){
                $('td', this).each(function(index2, val2){

                    if(index2 != 0 ){
                        if(!result10[index2])
                            result10[index2] = 0;
                        result10[index2] += parseInt($(val2).text());
                    }else {
                        // alert("CURRENT LIABILITIES Index-"+index2+" Value-"+$(val2).text()) ;
                        result10[index2] = "TOTAL EQUITY";
                    }

                });
            });

            //$('#div_t2 table').append('<tr style="background-color: #000;color: #fff;"></tr>');
            $(result).each(function(){
                $('#div_t2 table tr').last().append('<td>'+this+'</td>')
            });

            $(result2).each(function(){
                $('.non_curr_ass_ttl').append('<td>'+this+'</td>');
            });

            $(result3).each(function(){
                $('.curr_ass_ttl').append('<td>'+this+'</td>');
            });

            $(result4).each(function(){
                $('.non_curr_lia_ttl').append('<td>'+this+'</td>');
            });
            $(result5).each(function(){
                $('.curr_lia_ttl').append('<td>'+this+'</td>');
            });
            $(result6).each(function(){
                $('.lia_ttl').append('<td>'+this+'</td>');
            });
            $(result8).each(function(){
                $('.ttl_equ').append('<td>'+this+'</td>');
            });
            $(result7).each(function(){
                $('.ttl_equlia').append('<td>'+this+'</td>');
            });
            $(result9).each(function(){
                $('.ttl_equattr').append('<td>'+this+'</td>');
            });
            $(result10).each(function(){
                $('.ttl_equity').append('<td>'+this+'</td>');
            });
        });
        $(document).ready(function(){

            $('#balancesheet_table').editable({
                container: 'body',
                selector: 'td.name',
                url: "update_balancesheet.php",
                title: 'Enter Value',
                type: "POST",
                //dataType: 'json',
                validate: function(value){
                    if($.trim(value) == '')
                    {
                        return 'This field is required';
                    }
                    var regex = /((\d+)((\.\d{1,4})?))$/;
                    if(! regex.test(value))
                    {
                        return 'Enter Valid Values only!';
                    }
                }
            });
            $('#balancesheet_table_other').editable({
                container: 'body',
                selector: 'td.name',
                url: "update_balancesheet.php",
                title: 'Enter Value',
                type: "POST",
                //dataType: 'json',
                validate: function(value){
                    if($.trim(value) == '')
                    {
                        return 'This field is required';
                    }
                    var regex = /((\d+)((\.\d{1,4})?))$/;
                    if(! regex.test(value))
                    {
                        return 'Enter Valid Values only!';
                    }
                }
            });




        });
    </script>
<?php }?>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="public/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="public/assets/pages/scripts/login-5.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->

<script>
    $(document).ready(function()
    {
        $('#clickmewow').click(function()
        {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>

</html>