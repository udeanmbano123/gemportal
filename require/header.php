<?php
session_start() ;
if(!isset($_SESSION['name'])){
    header("Location: index.php?sms=Please login first") ;
}
$thy_totalScore = $others->getTotalScores($_SESSION['email']) ;

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
<head>
        <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
        <title><?php echo $page_title ; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="SMEPortal SME Portal" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="public/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <?php echo $extra_css ; ?>
        <link rel="shortcut icon" href="favicon.ico" />
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5b1aeb2c10b99c7b36d4c354/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN : LOGIN PAGE 5-1 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-5 login-container bs-reset mt-login-5-bsfix">
                    <div class="login-content">
                        <img class="login-logo" src="img/finlogo.png" width = "90%"/>
                        <h1 style ="padding-top: 36px;"><?php echo @$main_page_title ; ?></h1>
                        <span><?php echo @$_GET['sms'] ;  ?></span>
                        <p class="white-box-main-text"><?php echo $message ; ?></p>

                        <div class="row" style="margin-right: -7.5px; margin-left: -7.5px;">
                            <div class="col-lg-6 col-sm-6 col-xs-12" style="padding-left: 7.5px;padding-right: 7.5px;">
                                <div class="white-box" >
                                    <h3 class="box-title">Questions </h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-book-open text-info"></i></li>
                                        <li class="text-right"><span class="counter"><?=@$thy_qsn;?></span></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-lg-6 col-sm-6 col-xs-12" style="padding-left: 7.5px;padding-right: 7.5px;">
                                <div class="white-box">
                                    <h3 class="box-title">Completion</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class=" icon-briefcase  text-danger"></i></li>
                                        <li class="text-right"><span class="counter"><?=@$thy_qp;?>%</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12" style="padding-left: 7.5px;padding-right: 7.5px;">
                                <div class="white-box">
                                    <h3 class="box-title">Scores</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-folder-alt text-purple"></i></li>
                                        <li class="text-right"><span class="counter"><?=$thy_totalScore;?></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12"  style="padding-left: 7.5px;padding-right: 7.5px;">
                                <div class="white-box">
                                    <h3 class="box-title">Stage</h3>
                                    <ul class="list-inline two-part">
                                        <li><i class="icon-layers text-success"></i></li>
                                        <li class="text-right"><span class="counter"><?php echo $thy_stage ;?></span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-xs-12"  style="padding-left: 7.5px;padding-right: 7.5px;">
                                <div class="white-box">
                                    <h3 class="box-title">Profile  </h3>
                                    <div class="message-center">
                                        <div style="text-align: right;">
                                            <div class="mail-contnet">
                                                <h5> <?php echo $_SESSION['name'] ; ?> </h5>
                                                <span class="time"><?=$_SESSION['email'];?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>


                </div>