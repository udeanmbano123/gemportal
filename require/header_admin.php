<?php
session_start() ;
if(!isset($_SESSION['name'])){
    header("Location: index.php?sms=Please login first") ;
}
$thy_totalScore = $others->getTotalScores($_SESSION['email']) ;

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
<head>
        <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
        <title><?php echo $page_title ; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="SMEPortal SME Portal" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="public/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <?php echo $extra_css ; ?>
        <style type="text/css">
            .myprint {
                text-align: center;
                text-transform: uppercase;
                border: 1px solid tomato;
                padding: 20px;
                margin: 50px auto;
                width: 250px;
                background-color: #f6f6f6;
            }
            .divredureview{

                border: 1px solid #17ebff;
                padding: 20px;
                background-color: #ffffff;
            }

            @media print {
                .content {
                    display: none;
                }
            }
        </style>
        <link rel="shortcut icon" href="favicon.ico" />

    </head>
    <!-- END HEAD -->

    <body class=" login">
    <?php
    require_once 'func/controlDAO.php' ;
    $others = (new controlDAO())->getOthersNew() ;
    $useremail=$_GET['email'];
    $extra_js = $others->getFinancierApprovedStatus($useremail);
    //echo $extra_js;
    ?>

    <!-- BEGIN : LOGIN PAGE 5-1 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                
                <div class="col-md-4 login-container bs-reset mt-login-5-bsfix">
                    <div class="login-content">
                        <img class="login-logo" src="img/finlogo.png" width = "90%"/>
                        <h1 style ="padding-top: 36px;"><?php echo @$main_page_title ; ?></h1>
                        <span><?php echo @$_GET['sms'] ;  ?></span>
                        <div class="white-box-main-text">

                            <table class="table table-striped table-hover">
                                <thead>
                                <tbody>
                                <tr>
                                    <td colspan="2" style="text-align: center"><h4>Stage One</h4></td>
                                </tr>
                                <tr class="success">
                                    <td>1</td>
                                    <td>CAPITAL REQUIREMENTS</td>
                                    <td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_1_capital_requirements.php?email=".@$_GET['email']."'>[View Details]</a>";

                                    }


                                    ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td>2</td>
                                    <td>CORPORATE DIRECTORY</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_1_corporate_directory.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>

                                    </td>

                                </tr>
                                <tr class="success">
                                    <td>3</td>
                                    <td>ENTERPRISE SIZE CATEGORISATION</td>
                                    <td><a href='view_stage_1_entreprise_size_categorisation.php?email=<?php echo @$_GET['email'] ; ?>'>[View Details]</a></td>

                                </tr>
                                <tr class="success">
                                    <td>4</td>
                                    <td>COMPANY OVERVIEW</td>
                                    <td><a href='view_stage_1_overview_of_company.php?email=<?php echo @$_GET['email'] ; ?>'>[View Details]</a></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center"><h4>Stage Two</h4></td>
                                </tr>

                                <tr class="success">
                                    <td>1</td>
                                    <td>PROPRIETORS, PARTNERS, PRINCIPALS & DIRECTORS PROFILES</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_2_proprietors_partners_principapls_directors_profiles.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>
                                    </td>
                                    </tr>
                                <tr class="success">
                                    <td>2</td>
                                    <td>DIRECTORS' SHAREHOLDING</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_2_directors_shareholding.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>
                                    </td>


                                </tr>
                                <tr class="success">
                                    <td>3</td>
                                    <td>BOARD COMMITTEES</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_2_board_committees.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>
                                    </td>

                                     </tr>
                                <tr class="success">
                                    <td>4</td>
                                    <td>CORPORATE STRUCTURE</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_2_corporate_structure.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>
                                    </td>

                                </tr>
                                <tr class="success">
                                    <td>5</td>
                                    <td>HUMAN RESOURCE ORGANOGRAM</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_2_human_resource_organogram.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>
                                    </td>

                                </tr>
                                <tr class="success">
                                    <td>6</td>
                                    <td>COMPANY SHAREHOLDERS</td>
                                    <td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_2_shareholders.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>
                                    </td>

                                </tr>

                                <tr>
                                    <td colspan="2" style="text-align: center"><h4>Stage Three</h4></td>
                                </tr>

                                <tr class="success">
                                    <td>1</td>
                                    <td>MATERIAL AND OTHER THIRD PARTY CONTRACTS </td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_3_material_contracts.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>


                                    </td>
                                </tr>
                                <tr class="success">
                                    <td>2</td>
                                    <td>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_3_material_asset_transactions.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>

                                    </td>

                                </tr>
                                <tr class="success">
                                    <td>3</td>
                                    <td>MATERIAL LITIGATION AND CLAIMS</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_3_material_litigation_claims.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>

                                    </td>

                                </tr>
                                <tr class="success">
                                    <td>4</td>
                                    <td>BALANCE SHEET</td>
                                    <td><a href='view_stage_3_fin_balancesheet.php?email=<?php echo @$_GET['email'] ; ?>'>[View Details]</a></td>
                                </tr>
                                <tr class="success">
                                    <td>5</td>
                                    <td>PROFIT AND LOSS</td>
                                    <td><a href='view_stage_3_fin_profitandloss.php?email=<?php echo @$_GET['email'] ; ?>'>[View Details]</a></td>
                                </tr>
                                <tr class="success">
                                    <td>6</td>
                                    <td>CASH FLOW</td>
                                    <td><a href='view_stage_3_fin_cashflow.php?email=<?php echo @$_GET['email'] ; ?>'>[View Details]</a></td>
                                </tr>
                                <tr class="success">
                                    <td>7</td>
                                    <td>PERSONAL FINANCIAL COMMITMENTS</td>


                         <td><a href='view_stage_3_personal_financial_commitments.php?email=<?php echo @$_GET['email'] ; ?>'>[View Details]</a></td>;



                                </tr>
                                <tr class="success">
                                    <td>8</td>
                                    <td>PRE-LISTING STATEMENT</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_3_prelisting.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>
                                    </td>

                                </tr>
                                <tr class="success">
                                    <td>9</td>
                                    <td>BUSINESS PLAN</td><td>
                                    <?php
                                    if($extra_js=="yes"){

                                        echo "<a href='view_stage_3_business_plan.php?email=".$_GET['email']."'>[View Details]</a>";

                                    }

                                    ?>

                                    </td>
                                </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>


                </div>