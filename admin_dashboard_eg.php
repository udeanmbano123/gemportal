<?php
require_once 'func/controlDAO.php';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Finsec | SME Portal</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">


    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
</head>
<body class="fix-sidebar fix-header">
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">

    <?php
    include("req/header_main.php");
    $extra_js = "    <!-- jQuery -->
    <script src='plugins/bower_components/jquery/dist/jquery.min.js'></script>
    <!-- Bootstrap Core JavaScript -->
    <script src='bootstrap/dist/js/bootstrap.min.js'></script>
    <!-- Menu Plugin JavaScript -->
    <script src='plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js'></script>
    <!--slimscroll JavaScript -->
    <script src='js/jquery.slimscroll.js'></script>
    <!--Wave Effects -->
    <script src='js/waves.js'></script>
    <!--weather icon -->
    <script src='plugins/bower_components/skycons/skycons.js'></script>
    <!--Morris JavaScript -->
    <script src='plugins/bower_components/raphael/raphael-min.js'></script>
    <script src='plugins/bower_components/morrisjs/morris.js'></script>
    <!-- jQuery for carousel -->
    <script src='plugins/bower_components/owl.carousel/owl.carousel.min.js'></script>
    <script src='plugins/bower_components/owl.carousel/owl.custom.js'></script>
    <!-- Sparkline chart JavaScript -->
    <script src='plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js'></script>
    <script src='plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js'></script>
    <!--Counter js -->
    <script src='plugins/bower_components/waypoints/lib/jquery.waypoints.js'></script>
    <script src='plugins/bower_components/counterup/jquery.counterup.min.js'></script>
    <!-- Custom Theme JavaScript -->
    <script src='js/custom.min.js'></script>
    <script src='js/widget.js'></script>
    <!--Style Switcher -->
    <script src='plugins/bower_components/styleswitcher/jQuery.style.switcher.js'></script>";
    ?>


    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Finsec Admin </h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li class="active"><a href="#">Admin </a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <!-- .row -->
            <div class="row">
                <div class="col-sm-12">

                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-2  inbox-panel">
                                <div>
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-xs-12">
                                                <div class="white-box">
                                                    <h3 class="box-title">Sector Analysis </h3>
                                                    <ul class="basic-list">
                                                        <li>Business <span class="pull-right label-danger label">21.8%</span></li>
                                                        <li>Agriculture <span class="pull-right label-purple label">21.8%</span></li>
                                                        <li>Mining <span class="pull-right label-success label">21.8%</span></li>
                                                        <li>Finance <span class="pull-right label-info label">21.8%</span></li>
                                                        <li>Education <span class="pull-right label-danger label">21.8%</span></li>
                                                        <li>Utilities <span class="pull-right label-info label">21.8%</span></li>
                                                        <li>Retail <span class="pull-right label-success label">21.8%</span></li>
                                                        <li>Tobacco <span class="pull-right label-info label">21.8%</span></li>
                                                        <li>Warehousing <span class="pull-right label-warning label">21.8%</span></li>
                                                        <li>Construction <span class="pull-right label-purple label">21.8%</span></li>
                                                        <li>Textile <span class="pull-right label-info label">21.8%</span></li>
                                                        <li>IT <span class="pull-right label-purple label">21.8%</span></li>
                                                        <li>Others <span class="pull-right label-info label">21.8%</span></li>
                                                        <li>Transport <span class="pull-right label-warning label">21.8%</span></li>
                                                        <li>Food & Hospitality<span class="pull-right label-purple label">21.8%</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 ">
                                <!-- DASHBOARD -->
                                <div class=" col-lg-12 col-sm-6">

                                    <!-- .row -->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small> Basic Registrations</h3>
                                                <div class="stats-row">
                                                    <div class="stat-item">
                                                        <h6>Count</h6> <b>80</b></div>
                                                    <div class="stat-item">
                                                        <h6>Percentage</h6> <b>15.40%</b></div>
                                                    <div class="stat-item">
                                                        <h6>Value</h6> <b>$1,000</b></div>
                                                </div>
                                                <div id="sparkline8"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title"><small class="pull-right m-t-10 text-danger"><i class="fa fa-sort-desc"></i> 18% High then last month</small>Intermediate Registrations</h3>
                                                <div class="stats-row">
                                                    <div class="stat-item">
                                                        <h6>Count</h6> <b>20</b></div>
                                                    <div class="stat-item">
                                                        <h6>Percentage</h6> <b>15.40%</b></div>
                                                    <div class="stat-item">
                                                        <h6>Value </h6> <b>$5,745</b></div>
                                                </div>
                                                <div id="sparkline9"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small>Advanced Registrations </h3>
                                                <div class="stats-row">
                                                    <div class="stat-item">
                                                        <h6>Count </h6> <b>40</b></div>
                                                    <div class="stat-item">
                                                        <h6>Percentage</h6> <b>15.40%</b></div>
                                                    <div class="stat-item">
                                                        <h6>Value </h6> <b>$5,150</b></div>
                                                </div>
                                                <div id="sparkline10"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="white-box text-center bg-purple">
                                                <h1 class="text-white counter">165</h1>
                                                <p class="text-white">Total Number</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="white-box text-center bg-info">
                                                <h1 class="text-white counter">2065</h1>
                                                <p class="text-white">Basic Stage </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="white-box text-center bg-theme-dark">
                                                <h1 class="counter text-white">465</h1>
                                                <p class="text-white">Intermediate stage</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="white-box text-center bg-success">
                                                <h1 class="text-white counter">6555</h1>
                                                <p class="text-white">Advanced stage</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->

                                    <!-- .row -->
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title">Total Value</h3>
                                                <div class="text-right"> <span class="text-muted"></span>
                                                    <h1> $12,000</h1> </div><!--  <span class="text-success">20%</span> -->
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title">Basic Value </h3>
                                                <div class="text-right"> <span class="text-muted"></span>
                                                    <h1> $5,000</h1> </div> <!-- <span class="text-danger">30%</span> -->
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title">Intermediate Value</h3>
                                                <div class="text-right"> <span class="text-muted"></span>
                                                    <h1> $10,000</h1> </div> <!-- <span class="text-info">60%</span> -->
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title">Advanced  Value</h3>
                                                <div class="text-right"> <span class="text-muted"></span>
                                                    <h1> $9,000</h1> </div> <!-- <span class="text-inverse">80%</span> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->


                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="white-box">
                                                <div class="row row-in">
                                                    <div class="col-lg-3 col-sm-6 row-in-br">
                                                        <div class="col-in row">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
                                                                <h5 class="text-muted vb">Total recieved </h5> </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                <h3 class="counter text-right m-t-15 text-danger">2093</h3> </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                                        <div class="col-in row">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
                                                                <h5 class="text-muted vb">Approved</h5> </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                <h3 class="counter text-right m-t-15 text-info">169</h3> </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6 row-in-br">
                                                        <div class="col-in row">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-mouse-alt"></i>
                                                                <h5 class="text-muted vb">Rejected</h5> </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                <h3 class="counter text-right m-t-15 text-success">431</h3> </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6  b-0">
                                                        <div class="col-in row">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-receipt"></i>
                                                                <h5 class="text-muted vb">Pending</h5> </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                <h3 class="counter text-right m-t-15 text-warning">157</h3> </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 70%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--row -->

                                    <!-- .row -->
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                                    <div class="white-box" style="border: 1px solid;">
                                                        <h3 class="box-title">Monthly registration </h3>
                                                        <ul class="list-inline text-right">
                                                            <li>
                                                                <h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>Basic</h5> </li>
                                                            <li>
                                                                <h5><i class="fa fa-circle m-r-5" style="color: #fdc006;"></i>Intermediate</h5> </li>
                                                            <li>
                                                                <h5><i class="fa fa-circle m-r-5" style="color: #9675ce;"></i>Advanced</h5> </li>
                                                        </ul>
                                                        <div id="morris-area-chart" style="height: 340px;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-12">
                                            <div class="white-box" style="border: 1px solid;">
                                                <h3 class="box-title">Recent Applications
                                                  <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                                                    <select class="form-control pull-right row b-none">
                                                      <option>March</option>
                                                      <option>April</option>
                                                      <option>May</option>
                                                      <option>June</option>
                                                      <option>July</option>
                                                    </select>
                                                  </div>
                                                </h3>
                                                <div class="row sales-report">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h2>March 2018</h2>
                                                        <p>Top Application Report</p>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6 ">
                                                        <h1 class="text-right text-success m-t-20">$3,690</h1> </div>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>NAME</th>
                                                                <th>STATUS</th>
                                                                <th>DATE</th>
                                                                <th>Value</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td class="txt-oflo">Agile admin</td>
                                                                <td><span class="label label-success label-rouded">Basic</span> </td>
                                                                <td class="txt-oflo">April 18, 2018</td>
                                                                <td><span class="text-success">$24</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td class="txt-oflo">Real Homes WP Theme</td>
                                                                <td><span class="label label-info label-rouded">Advanced</span></td>
                                                                <td class="txt-oflo">April 19, 2018</td>
                                                                <td><span class="text-info">$1250</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td class="txt-oflo">Medical Pro WP Theme</td>
                                                                <td><span class="label label-danger label-rouded">Pending</span></td>
                                                                <td class="txt-oflo">April 20, 2018</td>
                                                                <td><span class="text-danger">-$24</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>4</td>
                                                                <td class="txt-oflo">Hosting press html</td>
                                                                <td><span class="label label-warning label-rouded">Basic</span></td>
                                                                <td class="txt-oflo">April 21, 2018</td>
                                                                <td><span class="text-success">$24</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>5</td>
                                                                <td class="txt-oflo">Helping Hands WP Theme</td>
                                                                <td><span class="label label-success label-rouded">Intermediate</span></td>
                                                                <td class="txt-oflo">April 22, 2018</td>
                                                                <td><span class="text-success">$24</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>6</td>
                                                                <td class="txt-oflo">Digital Agency PSD</td>
                                                                <td><span class="label label-success label-rouded">Basic</span> </td>
                                                                <td class="txt-oflo">April 23, 2018</td>
                                                                <td><span class="text-danger">-$14</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td>7</td>
                                                                <td class="txt-oflo">Helping Hands WP Theme</td>
                                                                <td><span class="label label-warning label-rouded">Intermediate</span></td>
                                                                <td class="txt-oflo">April 22, 2018</td>
                                                                <td><span class="text-success">$64</span></td>
                                                            </tr>
                                                        </tbody>
                                                    </table> <!-- <a href="#">Check all the sales</a> --> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

 


                                </div>


                            </div>
                            <!-- /#page-wrapper -->
                        </div>
                        <!-- /#wrapper -->
                        <!-- jQuery -->
                        <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
                        <!-- Bootstrap Core JavaScript -->
                        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
                        <!-- Menu Plugin JavaScript -->
                        <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
                        <!--slimscroll JavaScript -->
                        <script src="js/jquery.slimscroll.js"></script>
                        <!--Wave Effects -->
                        <script src="js/waves.js"></script>
                        <!--Counter js -->
                        <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
                        <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
                        <!-- Custom Theme JavaScript -->
                        <script src="js/custom.min.js"></script>
                        <script src="public/assets/global/plugins/moment.min.js"></script>
                        <!--Style Switcher -->
                        <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
                        <!-- Date Picker Plugin JavaScript -->
                        <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                        <!-- Date range Plugin JavaScript -->
                        <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
                        <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
                        <script src="plugins/bower_components/morrisjs/morris.js"></script>
                        <script src="js/morris-data.js"></script>

                        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

                        <script>
                            var ctx = document.getElementById("myChart").getContext('2d');
                            var myChart = new Chart(ctx, {
                                type: 'bar',
                                data: {
                                    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                                    datasets: [{
                                        label: '# of Votes',
                                        data: [12, 19, 3, 5, 2, 3],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 206, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(153, 102, 255, 0.2)',
                                            'rgba(255, 159, 64, 0.2)'
                                        ],
                                        borderColor: [
                                            'rgba(255,99,132,1)',
                                            'rgba(54, 162, 235, 1)',
                                            'rgba(255, 206, 86, 1)',
                                            'rgba(75, 192, 192, 1)',
                                            'rgba(153, 102, 255, 1)',
                                            'rgba(255, 159, 64, 1)'
                                        ],
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>

                        <script>
                            var ctx = document.getElementById("pieChart").getContext('2d');
                            var pieChart = new Chart(ctx, {
                                type: 'doughnut',
                                data: {
                                    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                                    datasets: [{
                                        label: '# of Votes',
                                        data: [12, 19, 3, 5, 2, 3],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 206, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(153, 102, 255, 0.2)',
                                            'rgba(255, 159, 64, 0.2)'
                                        ],
                                        borderColor: [
                                            'rgba(255,99,132,1)',
                                            'rgba(54, 162, 235, 1)',
                                            'rgba(255, 206, 86, 1)',
                                            'rgba(75, 192, 192, 1)',
                                            'rgba(153, 102, 255, 1)',
                                            'rgba(255, 159, 64, 1)'
                                        ],
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>

                        <script>
                            var ctx = document.getElementById("radarChart").getContext('2d');
                            var radarChart = new Chart(ctx, {
                                type: 'radar',
                                data: {
                                    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                                    datasets: [{
                                        label: '# of Votes',
                                        data: [12, 19, 3, 5, 2, 3],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 206, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(153, 102, 255, 0.2)',
                                            'rgba(255, 159, 64, 0.2)'
                                        ],
                                        borderColor: [
                                            'rgba(255,99,132,1)',
                                            'rgba(54, 162, 235, 1)',
                                            'rgba(255, 206, 86, 1)',
                                            'rgba(75, 192, 192, 1)',
                                            'rgba(153, 102, 255, 1)',
                                            'rgba(255, 159, 64, 1)'
                                        ],
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>

                        <script>
                            var ctx = document.getElementById("line").getContext('2d');
                            var line = new Chart(ctx, {
                                type: 'line',
                                data: {
                                    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                                    datasets: [{
                                        label: '# of Votes',
                                        data: [12, 19, 3, 5, 2, 3],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 206, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(153, 102, 255, 0.2)',
                                            'rgba(255, 159, 64, 0.2)'
                                        ],
                                        borderColor: [
                                            'rgba(255,99,132,1)',
                                            'rgba(54, 162, 235, 1)',
                                            'rgba(255, 206, 86, 1)',
                                            'rgba(75, 192, 192, 1)',
                                            'rgba(153, 102, 255, 1)',
                                            'rgba(255, 159, 64, 1)'
                                        ],
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>


                        <!--Style Switcher -->

                        <?php echo $extra_js; ?>
                        <script>
                            // Date Picker
                            jQuery('.mydatepicker, #datepicker').datepicker();
                            jQuery('#datepicker-autoclose').datepicker({
                                autoclose: true,
                                todayHighlight: true
                            });

                        </script>

</body>

</html>