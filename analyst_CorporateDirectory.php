





<?php
require_once 'func/controlDAO.php' ;
require("func/data/connect.php");

$ifPopulated =  (new controlDAO())->getCorporateDirectory()->selectOnecorporateDirectoryByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".@$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>



<!DOCTYPE html>



<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

        <?php 
          include("req/header_main.php") ;
          

        ?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
                   <div class="row bg-title" style="background: #9ea1f1;">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title"  style="color: #ffffff;" >Finsec Admin [Acceptance]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>


                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <!-- row -->
                            <div class="row">
                                <form name ="capital_requirements" method="post" action="func/controller/analyst_corporate_directory.php">
                                    <input type="hidden" name ="email" value="<?=@$_GET['email'];?>" >
                                <div class="col-lg-12 col-md-9 col-sm-12 col-xs-12 ">
                                    <div class="inbox-center">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="30">
                                                        <div class="checkbox m-t-0 m-b-0 ">
                                                            <input id="checkbox0" type="checkbox" class="checkbox-toggle" value="check all">
                                                            <label for="checkbox0"></label>
                                                        </div>
                                                    </th>

                                                    <th colspan="2">
                                                        <h3>Corporate Directory Verifications</h3>
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Duration in stated business line  </td>
                                                <td class="max-texts"><?=@$duration_in_stated_business_line;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="350" class="hidden-xs">Customer Base (% split) Corporates  </td>
                                                <td class="max-texts"><?=@$customer_basecorporates;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Customer Base (% split) Induviduals  </td>
                                                <td class="max-texts"><?=@$customer_baseindividuals;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Business Year End  </td>
                                                <td class="max-texts"><?=@$business_year_end;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Business Premises are  </td>
                                                <td class="max-texts"><?=@$business_premises_are;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Name of holding company  </td>
                                                <td class="max-texts"><?=@$name_of_holding_company;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Contact person Name  </td>
                                                <td class="max-texts"><?=@$contact_person_name;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Contact person Position  </td>
                                                <td class="max-texts"><?=@$contact_person_position;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Contact person Cell  </td>
                                                <td class="max-texts"><?=@$contact_person_cell;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Contact person Tel  </td>
                                                <td class="max-texts"><?=@$contact_person_telephone;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Contact person Email  </td>
                                                <td class="max-texts"><?=@$contact_person_email;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Company Secretary Address  </td>
                                                <td class="max-texts"><?=@$company_secretary_address;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Attorney & Office Address   </td>
                                                <td class="max-texts"><?=@$attorney_office_address;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Accountant & Office (Address)   </td>
                                                <td class="max-texts"><?=@$accountant_office_address;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Auditors & Office (Address)  </td>
                                                <td class="max-texts"><?=@$auditors_office_address;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Principal Bankers (Address)   </td>
                                                <td class="max-texts"><?=@$principal_bankers_address;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Authorised share capital  </td>
                                                <td class="max-texts"><?=@$authorised_share_capital;?></td>
                                            </tr>

                                            <tr class="unread">
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                                <td width="150" class="hidden-xs">Issued share capital  </td>
                                                <td class="max-texts"><?=@$issued_share_capital;?></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-7 m-t-20">
                                            <div class="btn-group pull-left">
                                                <a href="analyst_CapitalRequirements.php?email=<?=@$_GET['email'];?>" class="btn btn-default waves-effect">[ << Capital Requirements ]</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-5 m-t-20">
                                            <div class="btn-group pull-right">
                                                <button type="submit" class="btn btn-default green waves-effect">[Company Overview >> ]</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>