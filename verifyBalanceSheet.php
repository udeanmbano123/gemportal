





<?php

require_once 'func/controlDAO.php' ;
$ifPopulated =  (new controlDAO())->getBalanceSheetValues()-> selectBalanceSheetByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>



<!DOCTYPE html>



<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

        <?php 
          include("req/header_main.php") ;
          

        ?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Finsec Admin [Acceptance]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-2  inbox-panel">
                                    <div>
                                        <div class="list-group mail-list "> 
                                          <a href="admin_home.php" class="list-group-item ">Home </a>
                                          <a href="f_dashboard.php" class="list-group-item ">Pending Acceptance </a>
                                          <a href="f_dashboard_accept.php" class="list-group-item active">Accepted </a>
                                          <a href="f_dashboard_rejected.php" class="list-group-item">Rejected </a>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-10 mail_listing">
                                    
                                         <div class="col-md-12 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #868e97;">
                        <div class="login-content" style ="margin-top:0px;">
                         <h4 class="text-uppercase">BALANCE SHEET</h4>
                            <form action="func/controller/verifyBalanceSheetController.php" class="login-form" method="post">
                            <input type ="hidden" value="signup" name="signup"/>
                            <input type ="hidden" value= "<?=@$id ?>"  name ="id"/>
                             <input type ="hidden" value= "<?=@$_GET['email'] ?>"  name ="email"/>
                            <input type ="hidden" value= "balancesheet"  name ="form"/>

                            
                            
                                
                            <div class="form-group">
                              <div class="row">
                                <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Year:</b> <?=@$ifPopulated[$i]['year'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Year ".@$ifPopulated[$i]['year'] ?>" />
                                


                            <?php
                               }
                            ?>  




                               
                              
                               </div>
                            </div>                            
                            <div class="form-group">
                                 <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Is Audited:</b> <?=@$ifPopulated[$i]['is_audited'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Is Audited ".@$ifPopulated[$i]['is_audited'] ?>" />
                                


                            <?php
                               }
                            ?>     
                                





                            </div>     
                            <div class="form-group">
                                 <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>non current assets:</b> <?=@$ifPopulated[$i]['fixed_assets'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "non current assets ".@$ifPopulated[$i]['fixed_assets'] ?>" />
                                


                            <?php
                               }
                            ?>     
                                 
                                 
                                 
                                 

                            </div>     
                            <div class="form-group">
                              <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Current Assets:</b> <?=@$ifPopulated[$i]['current_assets'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Current Assets ".@$ifPopulated[$i]['current_assets'] ?>" />
                                


                            <?php
                               }
                            ?>  
                            </div>     
                             <div class="form-group">
                              
                               <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Total Assets:</b> <?=@$ifPopulated[$i]['total_assets'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Total Assets ".@$ifPopulated[$i]['total_assets'] ?>" />
                                


                            <?php
                               }
                            ?>  
                            </div>
                            <div class="form-group">
                              <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Equity  and Liability:</b> <?=@$ifPopulated[$i]['equity_and _liability'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Equity and Liabilities ".@$ifPopulated[$i]['equity_and _liability'] ?>" />
                                


                            <?php
                               }
                            ?>  
                              

                            </div>


                            <div class="form-group">
                              
                              <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Capital Reserves:</b> <?=@$ifPopulated[$i]['capital_and_reserves'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Capital Reserves ".@$ifPopulated[$i]['capital_and_reserves'] ?>" />
                                


                            <?php
                               }
                            ?>  
                              

                            </div>


                            </div>     
                             <div class="form-group">
                              <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Issued Share Capital:</b> <?=@$ifPopulated[$i]['issued_share_capital'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Issued Share Capital ".@$ifPopulated[$i]['issued_share_capital'] ?>" />
                                


                            <?php
                               }
                            ?>  

                            </div>
                            <div class="form-group">
                              <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Share Premium:</b> <?=@$ifPopulated[$i]['share_premium'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Share premium ".@$ifPopulated[$i]['share_premium'] ?>" />
                                


                            <?php
                               }
                            ?>  

                            </div>
                            <div class="form-group">
                               <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>revenue reserves:</b> <?=@$ifPopulated[$i]['revenue_reserves'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "revenue reserves ".@$ifPopulated[$i]['revenue_reserves'] ?>" />
                                


                            <?php
                               }
                            ?>  
                            </div>     
                             <div class="form-group">
                                <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>capital reserves:</b> <?=@$ifPopulated[$i]['capital_reserves'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "capital reserves ".@$ifPopulated[$i]['capital_reserves'] ?>" />
                                


                            <?php
                               }
                            ?>  

                            </div>
                            <div class="form-group">
                               <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>equity attributed:</b> <?=@$ifPopulated[$i]['equity_attributed'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "capital reserves ".@$ifPopulated[$i]['equity_attributed'] ?>" />
                                


                            <?php
                               }
                            ?>  

                            </div>
                             <div class="form-group">
                                 <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Non controlling interest:</b> <?=@$ifPopulated[$i]['non_controlling_interest'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "non controlling interest ".@$ifPopulated[$i]['non_controlling_interest'] ?>" />
                                


                            <?php
                               }
                            ?>
                            <div class="form-group">
                                 <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Shareholder's Equity:</b> <?=@$ifPopulated[$i]['shareholders_equity'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "shareholders equity ".@$ifPopulated[$i]['shareholders_equity'] ?>" />
                                


                            <?php
                               }
                            ?> 
                            </div>
                            <div class="form-group">
                                  <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Shareholder's Equity:</b> <?=@$ifPopulated[$i]['non_current_liabilities'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "non current liabilities ".@$ifPopulated[$i]['non_current_liabilities'] ?>" />
                                


                            <?php
                               }
                            ?> 
                            </div>
                             <div class="form-group">
                                <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Long Term Borrowings:</b> <?=@$ifPopulated[$i]['long_term_borrowings'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Long Term Borrowings: ".@$ifPopulated[$i]['long_term_borrowings'] ?>" />
                                


                            <?php
                               }
                            ?> 

                            </div>
                            <div class="form-group">
                                 <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Long Term Borrowings:</b> <?=@$ifPopulated[$i]['long_term_borrowings'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "deffered tax liabilities: ".@$ifPopulated[$i]['long_term_borrowings'] ?>" />
                                


                            <?php
                               }
                            ?> 
                            </div>
                            <div class="form-group">
                                <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Current Liabilities:</b> <?=@$ifPopulated[$i]['current_liabilities'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "current_liabilities: ".@$ifPopulated[$i]['current_liabilities'] ?>" />
                                


                            <?php
                               }
                            ?> 
                            </div>
                            <div class="form-group">
                              
                                <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Short Term Borrowings  :</b> <?=@$ifPopulated[$i]['short_term_borrowings '];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "short term borrowings : ".@$ifPopulated[$i]['short_term_borrowings '] ?>" />
                                


                            <?php
                               }
                            ?> 
                            </div>
                            <div class="form-group">
                                <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>trade and other payables  :</b> <?=@$ifPopulated[$i]['trade_and_other_payables '];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "trade and other payables : ".@$ifPopulated[$i]['trade_and_other_payables '] ?>" />
                                


                            <?php
                               }
                            ?> 
                            </div> 
                             <div class="form-group">
                                <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>current tax liability :</b> <?=@$ifPopulated[$i]['current_tax_liability '];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "current tax liability : ".@$ifPopulated[$i]['current_tax_liability'] ?>" />
                                


                            <?php
                               }
                            ?> 
                            </div> 
                            <div class="form-group">
                                <?php
                                foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>total liabilities :</b> <?=@$ifPopulated[$i]['total_liabilities '];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "total liabilities : ".@$ifPopulated[$i]['total_liabilities'] ?>" />
                                


                            <?php
                               }
                            ?> 

                            </div>
                                          
                            <div >
                              <a href ="verifyCashflow.php? & email=<?php echo @$_GET['email'] ?>"  class="btn btn-primary">[<< back]</a> &nbsp;&nbsp;&nbsp;&nbsp;  <button type="submit" class="btn btn-primary">Next</button>
                               </div>
                          </form>

                            </div>

                            





                    </div>

<!--                                         <tr>
                                           <td colspan="4" style="text-align:center">No Appliacction found</td>
                                        </tr>  -->
                                        
                                    
                                        
                                     
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>