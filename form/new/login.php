<?php

include("../config/DbConnect.php") ; 

$db = new DbConnect();
$conn = $db->connect();
$username = $_POST['username'] ; 
$password = $_POST['password'] ; 

$result = login($conn, $username , $password) ;

// echo "results = > " . $result;
if ($result == 1 ) {
	# code... Login Successfully 
	header("Location: ../dashboard.php") ; 
}
if ($result == 2) {
	# code... password does not match 
	header("Location: ../index.php?sms=Password does not match please try again") ; 
}
if ($result == 3) {
	# code... User has been deactivated contact adminstrator  
	header("Location: ../index.php?sms=User has been deactivated contact adminstrator") ; 
}
if ($result == 4) {
	# code... User is not found please try a different username
	header("Location: ../index.php?sms=User is not found please try a different username") ; 
}
// 	else{
// 	header("Location: ../index.php?sms=Login error please try again") ; 
// }


function login($conn , $email , $password){

		$sql = "SELECT *  FROM users WHERE users_phonenumber = '".$email."' OR users_email = '".$email."'  "  ;
		echo $sql ; 
		$result = mysqli_query($conn , $sql)  ; 
		require_once ('../config/PassHash.php');
		session_start() ; 
		//id, users_company, users_fullname, users_username, users_password, users_email, users_phonenumber, users_position, users_status, users_created_date
		if (mysqli_num_rows($result) > 0) {
		    while ($row = mysqli_fetch_assoc($result)){
		    	if ($row['users_status'] =="1") {
		            if (PassHash::check_password($row['users_password'], $password)) {
		            	$_SESSION['users_id'] = $row['users_id']; 
		            	$_SESSION['users_company_name'] = $row['users_company_name']; 
		            	$_SESSION['users_company_regnum'] = $row['users_company_regnum']; 
		            	$_SESSION['users_company_dateofincorp'] = $row['users_company_dateofincorp']; 
		            	$_SESSION['users_company_country'] = $row['users_company_country']; 
		            	$_SESSION['users_form_name'] = $row['users_form_name']; 
		            	$_SESSION['users_fullname'] = $row['users_fullname']; 
		            	$_SESSION['users_username'] = $row['users_username']; 
		            	$_SESSION['users_password'] = $row['users_password']; 
		            	$_SESSION['users_email'] = $row['users_email']; 
		            	$_SESSION['users_phonenumber'] = $row['users_phonenumber']; 
		            	$_SESSION['users_position'] = $row['users_position']; 
		            	$_SESSION['users_status'] = $row['users_status']; 
		            	$_SESSION['users_created_date'] = $row['users_created_date'];
		            	return 1 ;          	
		            } else {
		                return 2 ;
		            }
		         }	
	            else{
	            	return 3 ;
	            }		

		    } 
		} else {
		    return 4;
		}	
}


?>