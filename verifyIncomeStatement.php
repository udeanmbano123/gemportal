





<?php

require_once 'func/controlDAO.php' ;
$ifPopulated =  (new controlDAO())->getIncomeStatementValues()->selectIncomeStatementByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;

    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>



<!DOCTYPE html>



<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

        <?php 
          include("req/header_main.php") ;
          

        ?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Finsec Admin [Acceptance]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-2  inbox-panel">
                                    <div>
                                        <div class="list-group mail-list "> 
                                          <a href="admin_home.php" class="list-group-item ">Home </a>
                                          <a href="f_dashboard.php" class="list-group-item ">Pending Acceptance </a>
                                          <a href="f_dashboard_accept.php" class="list-group-item active">Accepted </a>
                                          <a href="f_dashboard_rejected.php" class="list-group-item">Rejected </a>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-10 mail_listing">
                                    
                                         <div class="col-md-12 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #868e97;">
                        <div class="login-content" style ="margin-top:0px;">
                         <h4 class="text-uppercase">INCOME STATEMENT</h4>
                            <form action="func/controller/verifyIncomeStatementController.php" class="login-form" method="post">
                            <input type ="hidden" value="signup" name="signup"/>
                            <input type ="hidden" value= "<?=@$_GET['email']?>"  name ="email"/>
                            <input type ="hidden" value= "incomestatement"  name ="form"/>


                            
                            
                                
                            <div class="form-group">
                              <div class="row">
                            <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Year:</b> <?=@$ifPopulated[$i]['year'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Year ".@$ifPopulated[$i]['year'] ?>" />
                                


                            <?php
                               }
                            ?>                                




                               
                              
                               </div>
                            </div>                            
                            <div class="form-group">
                             <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Is Audited:</b> <?=@$ifPopulated[$i]['is_audited'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Is Audited ".@$ifPopulated[$i]['is_audited'] ?>" />
                                


                            <?php
                               }
                            ?>     
                                
                               
                                
                                
                                





                            </div>     
                            <div class="form-group">
                                  <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Revenue:</b> <?=@$ifPopulated[$i]['revenue_or_turnover'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Revenue ".@$ifPopulated[$i]['revenue_or_turnover'] ?>" />
                                


                            <?php
                               }
                               ?>
                                 
                                 
                                 
                                 
                                 

                            </div>     
                            <div class="form-group">
                               <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Operating Costs:</b> <?=@$ifPopulated[$i]['less_operating_costs'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Less Operating Expenses ".@$ifPopulated[$i]['less_operating_costs'] ?>" />
                                


                            <?php
                               }
                               ?>
                                
                            </div>     
                             <div class="form-group">
                                <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>EBIT:</b> <?=@$ifPopulated[$i]['EBIT'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "EBIT ".@$ifPopulated[$i]['EBIT'] ?>" />
                                


                            <?php
                               }
                               ?>
                                

                            </div>
                            <div class="form-group">
                               <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Finance Costs:</b> <?=@$ifPopulated[$i]['finance_costs'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Finance Costs ".@$ifPopulated[$i]['finance_costs'] ?>" />
                                


                            <?php
                               }
                               ?>
                              

                            </div>


                            <div class="form-group">
                                <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Cost of Sales:</b> <?=@$ifPopulated[$i]['cost_of_sales'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Cost of Sales ".@$ifPopulated[$i]['cost_of_sales'] ?>" />
                                


                            <?php
                               }
                               ?>



                            </div>     
                             <div class="form-group">
                              <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Gross Profit:</b> <?=@$ifPopulated[$i]['gross_profit'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Gross Profit ".@$ifPopulated[$i]['gross_profit'] ?>" />
                                


                            <?php
                               }
                               ?>


                            </div>
                            <div class="form-group">
                                <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Earnings before Tax:</b> <?=@$ifPopulated[$i]['earnings_before_taxation'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Earnings Before Tax ".@$ifPopulated[$i]['earnings_before_taxation'] ?>" />
                                


                            <?php
                               }
                               ?>

                            </div>
                            <div class="form-group">
                                 <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Profit or Loss after Tax:</b> <?=@$ifPopulated[$i]['profit_or_loss_after_taxation'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Profit or Loss after Tax ".@$ifPopulated[$i]['profit_or_loss_after_taxation'] ?>" />
                                


                            <?php
                               }
                               ?>

                            </div>     
                             <div class="form-group">
                                  <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Ratained Profit:</b> <?=@$ifPopulated[$i]['retained_profit'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Retained Profit ".@$ifPopulated[$i]['retained_profit'] ?>" />
                                


                            <?php
                               }
                               ?>

                            </div>
                            <div class="form-group">
                                 <?php
                               foreach($ifPopulated as $i => $item) {
                            ?>
                                <b>Profit or Loss Carried Fwd:</b> <?=@$ifPopulated[$i]['profit_or_loss_carried_fwd '];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="<?php echo "Profit or Loss Carried fwd ".@$ifPopulated[$i]['profit_or_loss_carried_fwd  '] ?>" />
                                


                            <?php
                               }
                               ?>
                            </div>
                             
                            
                                          
                            <div >
                                <a href ="verifyBoardDirectors.php? & email=<?php echo @$_GET['email'] ?>"  class="btn btn-primary">[<< back]</a> &nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary">Next</button>
                               </div>
                          </form>

                            </div>

                            





                    </div>

<!--                                         <tr>
                                           <td colspan="4" style="text-align:center">No Appliacction found</td>
                                        </tr>  -->
                                        
                                    
                                        
                                     
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>