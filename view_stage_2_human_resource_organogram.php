<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "" ; 
$page_number = "9" ;
$thy_stage = "2" ;
$thy_qsn = "48/472" ;
$thy_qp = round(48/472 * 100, 2);
$my_title = "HUMAN RESOURCE ORGANOGRAM" ;
$extra_js = "" ; 
@$msg=$_GET['msg'];
$main_page_title = "Intermediate Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header_admin.php") ;

require("func/data/connect.php");

?>
                <div class="col-md-8 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title my_custom_header_main">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <form action="#"  class="login-form" method="post">
                            <div class="white-box-main">
                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>List :
                                      <a href="stage_2_human_resource_organogram_edit.php?action=Add" >[ Add New ] </a> <p><?php echo "<br><font color='green'  >".@$msg. " </font>" ;?></p>
                                    </h5> 
                                    <table class="table table-bordered  color-bordered-table success-bordered-table">
                                      <thead>

                                      <tr>
                                         <th>Employee name</th>
                                         <th>Key Position</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php


                                      $ifPopulated =  (new controlDAO())->gethumanResourceOrganogram()->selecthumanResourceOrganogramByEmail($_GET['email']) ;


                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="2" style="text-align:center">No employees found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# human_resource_organogram_id, user_id, ,
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>
                                                         <input type ='hidden' value = 'set' name ='human_resource'/>
                                                         ".@$ifPopulated[$i]['employee_name']."</td>
                                                         <td>".@$ifPopulated[$i]['key_position']."</td>
                                                         
                                                        
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>

                                      </tbody>
                                    </table>
                                </div>

                            </div>
                            </div>



                    </div>

                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <?php
                                if($others->getUserPemission($_SESSION['email']) =="Analyst") {
                                    ?>
                                    <a href="analyst_pending_applications.php" class="btn green btn-outline">[<< Back
                                        Pending Applications]</a>
                                    <?php
                                }
                                if($others->getUserPemission($_SESSION['email']) == "Financier") {
                                    ?>
                                    <a href="fin_applicants.php" class="btn green btn-outline">[<< Back
                                        View Applicants Lists]</a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <button type="button"  class="js-print-link btn blue btn-outline pull-right" >[ PRINT ] </button>
                            </div>
                        </div>


                    </div>

                    </form>
                </div>



<?php
require_once("require/footer_admin.php") ;
?>