<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$my_title  = "CAPITAL REQUIREMENTS"   ;
$page_title = "FINSEC | Stage One" ;
$extra_css = "
    <link href='plugins/bower_components/custom-select/custom-select.css' rel='stylesheet' type='text/css' />
    <link href='plugins/bower_components/bootstrap-select/bootstrap-select.min.css' rel='stylesheet' />
" ;
$thy_stage = "1" ;
$thy_qsn = "16/472" ;
$thy_qp = round(16/472 * 100, 2);
$page_number = "1" ;
$extra_js = "
        <script src='plugins/bower_components/custom-select/custom-select.min.js' type='text/javascript'></script>
        <script src='plugins/bower_components/bootstrap-select/bootstrap-select.min.js' type='text/javascript'></script>

        <script>
        jQuery(document).ready(function() {
            $('.select2').select2();
        });
        </script>

" ;
@$error=$_GET['error'];
$main_page_title = "Application View" ;
$first_screen = "true" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header_admin.php") ;
require("func/data/connect.php");

$user_email=$_GET['email'];
 $ifPopulated =  (new controlDAO())->getCapitalRequirements()->selectOneCapitalReqByEmail($_GET['email']) ;
  if(!$ifPopulated){
    //echo "There is no data " ;
  }else{
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
      }
   }

?>

                <div class="print col-md-8 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                        <div class="login-content" style ="margin-top:0px;">
                            <div class="row bg-title my_custom_header_main">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                                <!-- /.col-lg-12 -->
                            </div>                            

                         <!-- <h1 class="text-uppercase"> <?php echo $my_title ; ?></h1> -->
                            <form action="#"  class="login-form" method="post">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                            </div>
                                <div class="white-box-main">
                            <div class="row" >
                                <div class="col-xs-6">
                                    *Company name:
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Name" name="company_name" value="<?=@$company_name;?>" />
                                </div>
                                <div class="col-xs-6">
                                    *Company registration number:
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Reg Number" name="company_registration_number" value="<?=@$company_registration_number;?>" required/>
                                     <p><?php echo "<br><font color='red'  >".@$_GET['reg_error']. " </font>" ;?></p>      
                                </div>
                            </div>  
                            <div class="row">  
                                <div class="col-xs-4">
                                    *Date of Incorporation:
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                           type="text" autocomplete="off" placeholder="Date" name="date_of_incorporation"
                                           value="<?=@$date_of_incorporation;?>" required/>
                                </div>
                                <div class="col-xs-4">
                                    *Country of Incorporation:
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                           type="text" autocomplete="off" placeholder="Date" name="date_of_incorporation"
                                           value="<?=@$country_of_incorporation;?>" required/>


                                    <!--<input  form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Country"   value="--><?//=@$country_of_incorporation;?><!--" required/>-->
                                </div>
                                <div class="col-xs-4">
                                    *Region:
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                           type="text" autocomplete="off" placeholder="Date" name="date_of_incorporation"
                                           value="<?=@$region_of_incorporation;?>" required/>



                                    <!--<input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Region" name=""  value="--><?//=@$country_of_incorporation;?><!--" required/>-->
                                </div>

                            </div>

                            <div class="row">  
                                <div class="col-xs-4">
                                    Type of Entity
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                           type="text" autocomplete="off" placeholder="Date" name="date_of_incorporation"
                                           value="<?=@$type_of_entity;?>" required/>

                                </div>

                                <div class="col-xs-4">
                                    Business Sector
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                           type="text" autocomplete="off" placeholder="Date" name="date_of_incorporation"
                                           value="<?=@$business_sector;?>" required/>


                                </div>

                                <div class="col-xs-4">
                                    *Nature of business:
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                           type="text" autocomplete="off" placeholder="Date" name="date_of_incorporation"
                                           value="<?=@$nature_of_business;?>" />


                                    <!--<input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Business Nature" name=""  value="--><?//=@$nature_of_business;?><!--" required/>-->
                                </div>

                            </div>
                            <div class="row">  
                                <div class="col-xs-6">
                                    *Telephone:
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="number" min="0"
                                            title="Phone number remaining 9 digit with 0-9"
                                           autocomplete="off" placeholder="Phone number remaining 9 digit with 0-9" name="telephone"  value="<?=@$telephone;?>" required/>
                                     <p><?php echo "<br><font color='red'  >".@$_GET['tele_error']. " </font>" ;?></p> 
                                </div>
                                <div class="col-xs-6">
                                    *Fax Number :
                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Fax" name="fax_number"  value="<?=@$fax_number;?>" required/>
                                     <p><?php echo "<br><font color='red'  >".@$_GET['fax_error']. " </font>" ;?></p> 
                                </div>

                            </div>

                             <div class="row">  
                                <div class="col-xs-4">
                                    Registered Office Physical Address: 
                                    <textarea disabled class="form-control form-control-solid placeholder-no-fix form-group" name ="registered_office_physical_address"  rows="4" cols="50"><?=@$registered_office_physical_address;?></textarea>
                                </div>
                                <div class="col-xs-4">
                                    Postal Address :
                                    <textarea disabled class="form-control form-control-solid placeholder-no-fix form-group" name ="postal_address"  rows="4" cols="50"><?=@$postal_address;?></textarea>
                                </div>
                                <div class="col-xs-4">
                                    Principal Place of Business (Address) :
                                    <textarea disabled class="form-control form-control-solid placeholder-no-fix form-group" name ="principal_place_of_business"  rows="4" cols="50"><?=@$principal_place_of_business;?></textarea>
                                </div>

                            </div>
                             <div class="row">  
                                <div class="col-xs-12">
                                    Capital to be raised : 
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <fieldset>
                                                <legend>Equity Requirements</legend>
                                                <div class="row">
                                                <div class="col-xs-12">
                                                    *Total Equity
                                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="$" name="raised_equity" value="<?=@$raised_equity;?>" required/>
                                                     <p><?php echo "<br><font color='red'  >".@$error. " </font>" ;?></p>
                                                </div>
                                                <div class="col-xs-4">
                                                    <i style="font-size: 10px;">*Capex</i>
                                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                                           type="text" autocomplete="off" placeholder="$" name="raised_equity_capex"
                                                           value="<?=@$raised_equity_capex;?>" required/>
                                                </div>
                                                <div class="col-xs-4">
                                                    <i style="font-size: 10px;">*Business Development</i>
                                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                                           type="text" autocomplete="off" placeholder="$" name="raised_equity_bd"
                                                           value="<?=@$raised_equity_bd;?>" required/>
                                                </div>
                                                <div class="col-xs-4">
                                                    <i style="font-size: 10px;">*General Working Capital</i>
                                                    <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                                           type="text" autocomplete="off" placeholder="$" name="raised_equity_gwc"
                                                           value="<?=@$raised_equity_gwc;?>" required/>
                                                </div>
                                            </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-xs-6">
                                            <fieldset>
                                                <legend>Debt Requirements</legend>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        *Total Debt
                                                        <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                                               type="text" autocomplete="off" placeholder="$" name="raised_debt"
                                                               value="<?=@$raised_debt;?>" required/>
                                                        <p><?php echo "<br><font color='red'  >".@$error. " </font>" ;?></p>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <i style="font-size: 10px;">*Capex</i>
                                                        <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                                               type="text" autocomplete="off" placeholder="$" name="raised_debt_capex"
                                                               value="<?=@$raised_debt_capex;?>" required/>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <i style="font-size: 10px;">*Business Development</i>
                                                        <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                                               type="text" autocomplete="off" placeholder="$" name="raised_debt_bd"
                                                               value="<?=@$raised_debt_bd;?>" required/>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <i style="font-size: 10px;">*General Working Capital</i>
                                                        <input disabled class="form-control form-control-solid placeholder-no-fix form-group"
                                                               type="text" autocomplete="off" placeholder="$" name="raised_debt_gwc"
                                                               value="<?=@$raised_debt_gwc;?>" required/>
                                                    </div>
                                                </div>
                                            </fieldset>

                                        </div>
                                        <div class="col-xs-6">
                                            *% share under offer
                                            <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="$" name="share_on_offer" value="<?=@$share_on_offer;?>" required/>
                                             <p><?php echo "<br><font color='red'  >".@$error. " </font>" ;?></p>
                                        </div>


                                        <div class="col-xs-6">
                                            *Other
                                            <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="$" name="raised_other"  value="<?=@$raised_other;?>" required/>
                                            <p><?php echo "<br><font color='red'  >".@$error. " </font>" ;?></p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-xs-12">
                                    Purpose of funds :
                                    <textarea disabled class="form-control form-control-solid placeholder-no-fix form-group" name ="purpose_of_funds"  rows="4" cols="50"><?=@$purpose_of_funds;?></textarea>

                                </div>

                            </div>

                                </div>

                    </div>


                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <?php
                                if($others->getUserPemission($_SESSION['email']) =="Analyst") {
                                    ?>
                                    <a href="analyst_pending_applications.php" class="btn green btn-outline">[<< Back
                                        Pending Applications]</a>
                                    <?php
                                }
                                if($others->getUserPemission($_SESSION['email']) == "Financier") {
                                    ?>
                                    <a href="fin_applicants.php" class="btn green btn-outline">[<< Back
                                        View Applicants Lists]</a>
                                    <?php
                                }
                                    ?>
                            </div>
                            <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                            <button type="button"  class="js-print-link btn blue btn-outline pull-right" >[ PRINT ] </button>
                        </div>
                    </div>


                </div>

                </form>
            </div>



<?php
require_once("require/footer_admin.php") ;
?>