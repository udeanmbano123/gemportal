<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ;
$extra_css = "    <!-- Editable CSS -->
     <link href='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css' rel='stylesheet'>" ;
$page_number = "10" ;
$extra_js = "    <!-- Editable -->
  <script src='//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.js'></script>
  
    " ;
$thy_stage = "3" ;
$thy_qsn = "183/472" ;
$thy_qp = round(183/472 * 100, 2);
$my_title = "CASH FLOW  6 - 9" ;
@$msg=$_GET['msg'];
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg'];

$cashflow = "true" ;

require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;

if($others->searchCF($_SESSION['email']) == 0) {
    $others->setCompanyCashFlow($_SESSION['email'] , '2015') ;
    $others->setCompanyCashFlow($_SESSION['email'] , '2016') ;
    $others->setCompanyCashFlow($_SESSION['email'] , '2017') ;
    $others->setCompanyCashFlow($_SESSION['email'] , '2018') ;
    $others->setCompanyCashFlow($_SESSION['email'] , '2019') ;
    $others->setCompanyCashFlow($_SESSION['email'] , '2020') ;
    $others->setCompanyCashFlow($_SESSION['email'] , '2021') ;
    $others->setCompanyCashFlow($_SESSION['email'] , '2022') ;
}

$main=(new controlDAO())->getlast2yrs();
$get_financials_cashflows= $main->selectCashflows($_SESSION['email']) ;
$get_net_cashflows= $main->NetCash($_SESSION['email']) ;
$ifPopulated =  (new controlDAO())->getuploadFinancials()->selectOneuploadFinancialsByEmailCashFlow($_SESSION['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
    }
    $setOption .= "<input type ='hidden' value = '".@$id."' name ='id'/>" ;
    //echo $user_id ;
}


?>
    <script language="javascript">
        setInterval(function(){
            window.location.reload(1);
        }, 30000);
    </script>
    <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
        <div class="login-content" style ="margin-top:0px;padding:0px;">
            <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                    <ol class="breadcrumb">
                        <li class="dropdown">
                            <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                [ Previous forms of this stage ]
                            </a>
                            <ul class="dropdown-menu mailbox animated ">
                                <li>
                                    <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                </li>
                                <li>
                                    <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                </li>
                                <li>
                                    <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                </li>
                                <li>
                                    <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                </li>
                            </ul>
                            <!-- /.dropdown-messages -->
                        </li>
                        <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                        <h3 class="box-title">Cash Flow Checked </h3>
                        <form action="func/controller/finCashFlowController.php"  enctype="multipart/form-data"    class="login-form" method="post">
                            <?=@$setOption;?>
                            <input type="hidden" value="<?php echo $_SESSION['email']?>"  name="user_id">

                            <table class="table table-bordered responsive">
                                <thead>
                                <th class="c-gray">#</th>
                                <th class="c-gray">Is Audited</th>
                                <th class="c-gray"> Upload File</th>
                                <th class="c-gray"></th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>  <input type="checkbox"
                                            <?php
                                            if(@$is_audited =="yes"){
                                                echo "checked" ;
                                            }

                                            ?> value="yes" name="is_audited"  /></td>
                                    <td>  <input type="file" name="upload_file"  /></td>
                                    <td> <a target="_blank" href="<?=$doc_url;?>"> <?=@$doc_url;?></a> </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <button type="submit" class="btn green btn-outline pull-right" >[ Save ] </button>
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box-main">
                        <h3 class="box-title">CONSOLIDATED STATEMENT OF CASH FLOWS</h3>
                        <p class="text-muted">*Just click on word which you want to change and enter , page refreshes every 30 seconds to update figures</p>




                        <table id="cashflow_table" class="table editable-table table-bordered m-b-0">
                            <thead class="text-uppercase">
                            <th width="330" class="c-gray"></th>
                            <th colspan="3" class="c-gray">HISTORICAL</th>
                            <th colspan="5" class="c-gray">PROJECTED</th>
                            </thead>
                            <tbody>
                            <colgroup>
                                <col  style="background-color:#f5f5f5;">
                                <col span="3" style="background-color:yellow">
                                <col span="5" style="background-color:pink">
                            </colgroup>

                            <tr>
                                <td>Year</td><td>2015</td><td>2016</td><td>2017</td><td>2018</td><td>2019</td><td>2020</td><td>2021</td><td>2022</td>
                            </tr>
                            <tr>
                                <td>(Loss)/Profit  before tax</td>
                                <?php

                                    for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                        echo "<td data-name = 'cash_flow_LossProfitbeforetax' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_LossProfitbeforetax']."</td>" ;
                                    }
                                ?>

                            </tr>

                            <tr>
                                <td>Adjustments to reconcile profit before tax to net cash flows (+/-) </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'cash_flow_Adjustments' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_Adjustments']."</td>" ;
                                }
                                ?>

                            </tr>
                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;">
                                <td>Net cashflows from operating activities</td>
                                <?php

                                for($i = 0  ; $i < count($get_net_cashflows) ; $i++ ) {
                                    echo "<td >".$get_net_cashflows[$i]['Total']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Net cash used in investing activities (+/-) </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'cash_flow_NetCashInvestingactivities' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_NetCashInvestingactivities']."</td>" ;

                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Net cashflows used in financing activities (+/-)</td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'cash_flow_Netfinancingactivities' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_Netfinancingactivities']."</td>" ;

                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Cash and cash equivalents at the beginning of the year</td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'cash_flow_Cashequivalents' class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_cash_flow_id']."'>".$get_financials_cashflows[$i]['cash_flow_Cashequivalents']."</td>" ;

                                }
                                ?>
                            </tr>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <div class="login-footer">
            <div class="row bs-reset">
                <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                    <a href ="stage_3_fin_profitandloss.php" class="btn green btn-outline">[<< Back]</a>
                </div>
                <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                <a href ="stage_3_personal_financial_commitments.php" type="submit" class="btn green btn-outline pull-right" >[ Next >>] </a>
            </div>
        </div>
    </div>

    </div>
<?php

require_once("require/footer.php") ;

?>