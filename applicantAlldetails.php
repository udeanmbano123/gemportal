<?php

require_once 'func/controlDAO.php' ;
$ifPopulated =  (new controlDAO())->getdirectorsShareholding()->selectdirectorsShareholdingByEmail($_GET['email']);
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>



<!DOCTYPE html>



<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

        <?php 
          include("req/header_approver.php") ;
          

        ?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Finsec Admin [Acceptance]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-2  inbox-panel">
                                    <div>
                                        <div class="list-group mail-list "> 
                                          <a href="admin_home.php" class="list-group-item ">Home </a>
                                          <a href="f_aprover_dashboard.php" class="list-group-item ">Pending Approvals </a>
                                          <a href="f_dashboard_accept.php" class="list-group-item active">Approved </a>
                                          <a href="f_dashboard_rejected.php" class="list-group-item">Rejected </a>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-10 mail_listing">
                                    
                                         <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                        <div class="login-content" style ="margin-top:0px;">
                         <h4 class="text-uppercase">APPLICANT DETAILS</h4>
                            <form action="func/controller/verifyBoardDirectorsController.php" class="login-form" method="post">
                            <input type ="hidden" value="signup" name="signup"/>
                            <input type ="hidden" value= "<?=@$_GET['email'] ?>"  name ="email"/>
                            <input type ="hidden" value= "boarddirectors"  name ="form"/>
                            <div class="row">  
                                <div class="col-xs-12">
                                   
                                    <table class="table table-bordered">
                                      <thead>

                                      <tr>
                                         <th>Stage</th>
                                         <th>Data</th>
                                        
                                      </tr>
                                      </thead>
                                      <tbody>
                                     
                                      <tr>
                                      <td>Basic</td>
                                      <td><ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ view forms ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_1_capital_requirements.php?email=<?php echo $_GET['email'] ?>"> 1. CAPITAL REQUIREMENTS </a>
                                            </li>
                                            <li>    
                                                <a class="text-left" href="stage_1_corporate_directory.php?email=<?php echo $_GET['email'] ?>"> 2. CORPORATE DIRECTORY  <b class='font-green'></b></a>
                                            </li>
                                            <li>    
                                                <a class="text-left" href="stage_1_overview_of_company.php?email=<?php echo $_GET['email'] ?>"> 3. OVERVIEW OF COMPANY <b class='font-green'></b></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                   
                                    
                                </ol></td>
                                      
                                    </tr>
                                    <tr>
                                      <td>Intermediate</td>
                                      <td><ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ view forms ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_2_proprietors_partners_principapls_directors_profiles.php?email=<?php echo $_GET['email'] ?>"> 1. PROPRIETORS AND PRINCIPAL PROFILE</a>
                                            </li>
                                            <li>    
                                                <a class="text-left" href="stage_2_directors_shareholding.php?email=<?php echo $_GET['email'] ?>"> 2. DIRECTORS'S SHAREHOLDING  <b class='font-green'></b></a>
                                            </li>
                                            <li>    
                                                <a class="text-left" href="stage_2_board_committees.php?email=<?php echo $_GET['email'] ?>"> 3. BOARD COMMITTEES <b class='font-green'></b></a>
                                            </li>
                                             <li>    
                                                <a class="text-left" href="stage_2_corporate_structure.php?email=<?php echo $_GET['email'] ?>"> 4. CORPORATE STRUCTURE <b class='font-green'></b></a>
                                            </li>
                                             <li>    
                                                <a class="text-left" href="stage_2_human_resource_organogram.php?email=<?php echo $_GET['email'] ?>"> 5. HUMAN RESOURCE ORGANOGRAM <b class='font-green'></b></a>
                                            </li>
                                            <li>    
                                                <a class="text-left" href="stage_2_shareholders.php?email=<?php echo $_GET['email'] ?>"> 6.SHAREHOLDERS <b class='font-green'></b></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                   
                                    
                                </ol></td>
                                      
                                    </tr>
                                    <tr>
                                      <td>Advanced</td>
                                      <td><ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ view forms ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_material_asset_transactions.php.?email=<?php echo $_GET['email'] ?>"> 1. MATERIAL ASSET</a>
                                            </li>
                                            <li>    
                                                <a class="text-left" href="stage_3_material_litigation_claims.php?email=<?php echo $_GET['email'] ?>"> 2. MATERIAL LITIGATION <b class='font-green'></b></a>
                                            </li>
                                           
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                   
                                    
                                </ol></td>
                                      
                                    </tr>
                                      </tbody>
                                    </table>
                                </div>

                            </div>
                            

                            
                            
                                
                            
                                      
                            <div >
                                <a href ="f_approver_dashboard.php"  class="btn btn-primary">[<< back]</a>
                               </div>
                          </form>

                            </div>

                            





                    </div>

<!--                                         <tr>
                                           <td colspan="4" style="text-align:center">No Appliacction found</td>
                                        </tr>  -->
                                        
                                    
                                        
                                     
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>