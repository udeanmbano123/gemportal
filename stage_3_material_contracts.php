<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ; 
$extra_css = "" ;
$thy_stage = "3" ;
$thy_qsn = "55/472" ;
$page_number = "10" ;
$thy_qp = round(55/472 * 100, 2);
$my_title = "MATERIAL AND OTHER THIRD PARTY CONTRACTS  1 - 9 " ;
$extra_js = "" ; 
$main_page_title = "Advanced Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
require("func/data/connect.php");
                              $user_email=$_SESSION['email'];
                       $sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
                   $result2=mysqli_query($con,$sql2);
                      while($row2=mysqli_fetch_array($result2)){
                        $usertype=$row2['users_type'];
                      }
if($usertype=='applicant'){
 $ifPopulated =  (new controlDAO())->getmaterialContracts()->getmaterialContractsbyEmail(@$_SESSION['email']) ;
 if(!$ifPopulated){
    //echo "There is no data " ;
    // $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

    }else{
    // $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
     }
       // $setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
    //echo $user_id ;
  }
}

//$ifPopulated =  (new controlDAO())->getCorporateDirectory()->selectOnecorporateDirectoryByEmail($_SESSION['email']) ;
//if(!$ifPopulated){
//    //echo "There is no data " ;
//    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;
//
//}else{
//    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
//    foreach($ifPopulated as $i => $item) {
//        extract($ifPopulated[$i]);
//    }
//    $setOption .= "<input type ='hidden' value = '".@$corporate_structure_id."' name ='corporate_structure_id'/>" ;
//    //echo $user_id ;
//}
?>
            
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <form action="func/controller/material_contractsController.php" enctype="multipart/form-data" class="login-form" method="post">
                            <input type="hidden" value="upload" name="upload">
                             <input type="hidden" value="<?php echo $_SESSION['email'];?>" name="user_id">
                            <div class="white-box-main">
                            <div class="row">  
                                <div class="col-xs-12">
                                    <b>MATERIAL AND OTHER THIRD PARTY CONTRACTS :</b> 
                                    <div class="row">
                                        <div class="col-xs-12">
                                            

                                            1.  Contracts restricting  the [COMPANY]’s freedom to carry  on its business as it deems fit or restrict  its ability to transfer its business;
                                             <a href="func/controller/materialContractsUploads/<?=@$restrictions;?>"> <?php echo @$restrictions;?></a>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="file" autocomplete="off" placeholder="name" name="ristricts" value="<?php echo @$restrictions;?>"  />
                                        </div>
                                       <div class="col-xs-12">
                                            2.  Contracts of material capital commitments;
                                             <a href="func/controller/materialContractsUploads/<?=@$capital_committments;?>"> <?php echo @$capital_committments;?></a>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="file" autocomplete="off" placeholder="position" name="capital_commitments" value="<?php echo @$capital_committments;?>"/>
                                        </div>
                                       <div class="col-xs-12">
                                            3.  Contracts with subsidiaries of other group companies OR in which any of the Directors has an interest.
                                             <a href="func/controller/materialContractsUploads/<?=@$subgroup;?>"> <?php echo @$subgroup;?></a>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="file" autocomplete="off" placeholder="cellphone" name="subgroup" />
                                        </div>
                                       <div class="col-xs-12">
                                            4.  Agreements to which the Company or any Subsidiary is a party that cannot be disclosed due to any non-disclosure covenant;
                                              <a href="func/controller/materialContractsUploads/<?=@$undisclosed_contracts;?>"> <?php echo @$undisclosed_contracts;?></a>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="file" autocomplete="off" placeholder="telephone" name="undisclosed" />
                                        </div>
                                         <div class="col-xs-12">
                                            5.  Material joint venture, participation, teaming, partnership or cooperative agreements to which the Company is a party;
                                              <a href="func/controller/materialContractsUploads/<?=@$joint_venture;?>"> <?php echo @$joint_venture;?></a>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="file" autocomplete="off" placeholder="email address" name="joint_venture" />
                                        </div>
                                         <div class="col-xs-12">
                                            6.  Contracts terminated for default, convenience or show-cause notices.
                                            <a href="func/controller/materialContractsUploads/<?=@$terminated_contracts;?>"> <?php echo @$terminated_contracts;?></a>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="file" autocomplete="off" placeholder="email address" name="terminated_contracts" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                        


                    </div>


                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_2_review.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <!-- <a href="stage_3_material_asset_transactions.php" class="btn green btn-outline pull-right" >[ Next >>] </a> -->
                                 <button type="submit" class="btn green uppercase btn-outline pull-right">[Next>>]</button>
                            </div>
                            </form>
                        </div>
                    </div>


                </div>
<?php

require_once("require/footer.php") ;

?>