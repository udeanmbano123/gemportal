<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "" ; 
$page_number = "5" ; 
$extra_js = "" ;
$thy_stage = "2" ;
$main_page_title = "Intermediate Stage" ;
@$label=$_GET['action'];
@$id=$_GET['id'];
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;

$ifPopulated =  (new controlDAO())->getdirectors()->selectdirectorsByDirectorId(@$id) ;
foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
?>
<div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                         <h1 class="text-uppercase "> <?php if ($label){
                            echo 'Add Directors';
                         }
                         else {
                          echo  'Edit Directors';
                         }  ; ?></h1>
                        <form action="func/controller/directorsController.php"  class="login-form" method="post">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Please complete all the required field with * asterisks  </span>
                            </div>
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                             <input type ="hidden" value= "<?= @$_GET['id'] ?>"  name ="id"/>
                            <div class="row">
                                
                                
                           
                                    	<div class="col-xs-4">
                                    *Title:
                                            <select class="form-control form-control-solid placeholder-no-fix form-group"  style="padding-bottom: 2px;"
                                                    name="title" tabindex="-98" aria-required="true">
                                                <option <?=(@$title == "Mr" ) ? 'selected="selected"' : '';?> value="Mr">Mr</option>
                                                <option <?=(@$title == "Mrs" ) ? 'selected="selected"' : '';?> value="Mrs">Mrs</option>
                                                <option <?=(@$title == "Miss" ) ? 'selected="selected"' : '';?> value="Miss">Miss</option>
                                                <option <?=(@$title == "Ms." ) ? 'selected="selected"' : '';?> value="Ms.">Ms.</option>
                                                <option <?=(@$title == "Sir." ) ? 'selected="selected"' : '';?> value="Sir.">Sir.</option>
                                                <option <?=(@$title == "Esq." ) ? 'selected="selected"' : '';?> value="Esq.">Esq.</option>
                                                <option <?=(@$title == "Rev." ) ? 'selected="selected"' : '';?> value="Rev.">Rev.</option>
                                                <option <?=(@$title == "Dr." ) ? 'selected="selected"' : '';?> value="Dr.">Dr.</option>
                                                <option <?=(@$title == "Prof." ) ? 'selected="selected"' : '';?> value="Prof.">Prof.</option>
                                            </select>
                                </div>
                                        <div class="col-xs-4">
                                            *Forenames
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="forenames" name="forenames"  value="<?=@$forename;?>" required/>
                                        </div>
                                       <div class="col-xs-4">
                                            *Surname
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Surname" name="surname"  value="<?=@$surname;?>" required/>
                                        </div>

                                    

                                
                            </div>  
                            <div class="row">  
                                <div class="col-xs-4">
                                    *Age:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group"
                                           type="number" min="0" placeholder="age" name="age" value="<?=@$age;?>" required/>
                                </div>
                                <div class="col-xs-4">
                                    *Citizenship
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="citizenship" name="citizenship" value="<?=@$citizenship;?>" required/>
                                    
                                </div>                                
                                <div class="col-xs-4">
                                    *Address
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="address" name="address" value="<?=@$address;?>" required/>
                                </div>
                                <div class="row"> 
                               <div class="col-xs-4">
                               	<button type="submit" class="btn btn-primary">Save</button>
                               </div>
                                </div>


                            </div>