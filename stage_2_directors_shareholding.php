<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "" ;
$my_title = "DIRECTORS' SHAREHOLDING  2 - 6";
$page_number = "6" ;
$thy_stage = "2" ;
$thy_qsn = "45/472" ;
$thy_qp = round(45/472 * 100, 2);
$extra_js = "" ; 
@$msg=$_GET['msg'];
$main_page_title = "Intermediate Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;

require("func/data/connect.php");
                              $user_email=$_SESSION['email'];
                       $sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
                   $result2=mysqli_query($con,$sql2);
                      while($row2=mysqli_fetch_array($result2)){
                        $usertype=$row2['users_type'];
                      }
?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5>
                            </div>
                            <?php if($usertype=='applicant'){
                              ?>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_2_proprietors_partners_principapls_directors_profiles.php"> <strong>PROPRIETORS, PARTNERS, PRINCIPALS AND DIRECTORS PROFILES </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_1_review.php"> <strong>STAGE 1 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <?php } ?>
                            <!-- /.col-lg-12 -->
                        </div>

                        <form action="func/controller/stagedirectors_shareholdingController.php"  class="login-form" method="post">
                            <div class="white-box-main">
                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>List of directors shareholding : 
                                      <a href="stage_2_directors_shareholding_edit.php?action=Add">[ Add New ]</a><p><?php echo "<br><font color='green'  >".@$msg. " </font>" ;?></p>
                                    </h5> 
                                    <table class="table table-bordered">
                                      <thead>

                                      <tr>
                                         <th>Directors's name</th>
                                         <th>Direct & Indirect Equity interest</th>
                                         <th>Shareholding</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                      $ifPopulated =  (new controlDAO())->getdirectorsShareholding()->selectdirectorsShareholdingByEmail($_SESSION['email']) ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="3" style="text-align:center">No directors shareholding found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# directors_shareholding_id, user_id, , ,
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td><input type ='hidden' value = 'set' name ='directors_shareholding'/>
                                                         ".@$ifPopulated[$i]['directors_name']."</td>
                                                         <td>".@$ifPopulated[$i]['direct_indirect_equity_interest']."</td>
                                                         <td>".@$ifPopulated[$i]['shareholding']."</td>
                                                         <td><a href='stage_2_directors_shareholding_edit.php?id=".@$ifPopulated[$i]['directors_shareholding_id']."'>[Edit]</a> | <a href ='./func/controller/directorsShareholdingController.php?id=".@$ifPopulated[$i]['directors_shareholding_id']."'>[Delete]</a></td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>


                                      </tbody>
                                    </table>
                                </div>

                            </div>
                            </div>



                    </div>


                    <div class="login-footer">
                      <?php if($usertype=='applicant'){
                      ?>
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_2_proprietors_partners_principapls_directors_profiles.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <button type="submit" class="btn green btn-outline pull-right" >[ Next >>] </button>
                            </div>
                        </div>
                         <?php } elseif ($usertype=='Administrator') {
                              ?>
                             <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <a href="applicantAlldetails.php?email=<?php echo $_GET['email'] ?>" class="btn green uppercase btn-outline pull-right">[<< Back]</a>
                            </div>
                            <?php } ?>
                    </div>

                    </form>

                </div>

    <!--  First form of the dialog
          The Company’s Products/Services  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">DIRECTORS' SHAREHOLDING IN ESCROW SYSTEM</h4> </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Directors's name  :</label>
                            <input type="text" class="form-control" id="recipient-name1"> </div>
                        <div class="form-group">
                            <label for="recipient-name2" class="control-label">Direct & Indirect Equity interest :</label>
                            <input type="text" class="form-control" id="recipient-name3"> </div>
                        <div class="form-group">
                            <label for="recipient-name4" class="control-label">Shareholding %  :</label>
                            <input type="text" class="form-control" id="recipient-name5"> </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>



<?php


require_once("require/footer.php") ;

?>