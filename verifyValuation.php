<?php
require_once 'func/controlDAO.php' ;



$ifPopulated =  (new controlDAO())->getCapitalRequirements()->selectOneCapitalReqByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
    }
    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Finsec | Capital Online</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
</head>
<body class="fix-sidebar fix-header">
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
    <?php
    include("req/header_main.php") ;
    @$email=$_GET['email'];


    ?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title" style="background: #9ea1f1;">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title"  style="color: #ffffff;" >Finsec Admin [Acceptance]</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-12">
                                <form name ="capital_requirements" method="post" action="func/controller/analyst_valuation.php">
                                    <input type="hidden" name ="email" value="<?=@$_GET['email'];?>" >
                                    <div class="col-md-12 login-container bs-reset mt-login-5-bsfix">
                                        <div class="login-content" style ="margin-top:0px;">
                                        <h4> DCF VALUATION SCHEDULE (SUCCESS SCENARIO)
                                        </h4>
                                        <table id="fin_ratios" class="table editable-table table-responsive table-stripped table-bordered m-b-0">
                                            <thead class="text-uppercase">
                                            <th width="330" class="c-gray"></th>
                                            <th colspan="1" class="c-gray"></th>
                                            <th colspan="1" class="c-gray">YEAR 1</th>
                                            <th colspan="1" class="c-gray">YEAR 2</th>
                                            <th colspan="1" class="c-gray">YEAR 3</th>
                                            <th colspan="1" class="c-gray">YEAR 4</th>
                                            <th colspan="1" class="c-gray">YEAR 5</th>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Net cash flow (Free Cash Flow)	</td>
                                                <td>		 $(2,000,000.0)</td>
                                                <td>$1,000,000 	</td>
                                                <td> $1,500,000 	</td>
                                                <td> $1,780,000 </td>
                                                <td>	 $2,042,397 	</td>
                                                <td style="text-align: right;"> $5,093,762 </td>
                                            </tr>
                                            <tr>
                                                <td>Present value factor	</td>
                                                <td>		100%	</td>
                                                <td>13%</td>
                                                <td>	13%	</td>
                                                <td>13%</td>
                                                <td>	13%	</td>
                                                <td style="text-align: right;">13%</td>
                                            </tr>
                                            <tr>
                                                <td>Discount Factor		</td>
                                                <td></td>
                                                <td>		89%	</td>
                                                <td>79%	</td>
                                                <td>70%	</td>
                                                <td>62%	</td>
                                                <td style="text-align: right;">55%</td>
                                            </tr>
                                            <tr>
                                                <td>PV of cash flow	</td>
                                                <td>		 $(2,000,000)</td>
                                                <td>	 $888,889</td>
                                                <td> 	 $1,185,185 </td>
                                                <td>	 $1,250,151 </td>
                                                <td>	 $1,275,058 </td>
                                                <td style="text-align: right;">	 $2,826,676 </td>
                                            </tr>
                                            <tr>
                                                <td>Cumulative NPV		</td>
                                                <td>	 $(2,000,000)</td>
                                                <td>	 $(1,111,111)</td>
                                                <td>	 $74,074 </td>
                                                <td>	 $1,324,225 </td>
                                                <td>	 $2,599,283 </td>
                                                <td style="text-align: right;">	 $5,425,959 </td>
                                            </tr>
                                            <tr>
                                                <td >Terminal Value		</td>
                                                <td colspan="6" style="text-align: right;">$48,512,017 </td>
                                            </tr>
                                            <tr>
                                                <td >PV of Terminal Value	</td>
                                                <td colspan="6" style="text-align: right;">$26,920,723 </td>
                                            </tr>
                                            <tr>
                                                <td>Net present value		</td>
                                                <td>	 $5,425,959 		</td>
                                                <td colspan="4"></td>
                                                <td  style="text-align: right;">$32,346,682 </td>
                                            </tr>
                                            <tr>
                                                <td>Internal rate of return	</td>
                                                <td colspan="6"  style="text-align: right;">53.2%</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-7 m-t-20">
                                            <div class="btn-group pull-left">
                                                <a href="verifyFinancialRatios.php?email=<?=@$_GET['email'];?>" class="btn btn-default waves-effect">[ Financial Ratios ]</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-5 m-t-20">
                                            <div class="btn-group pull-right">
                                                <button type="submit" class="btn btn-default waves-effect">[Analyst Recommendation >> ]</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!--Counter js -->
<script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/custom.min.js"></script>
<script src="public/assets/global/plugins/moment.min.js"></script>
<!--Style Switcher -->
<script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

</script>
</body>
</html>