





<?php
require_once 'func/controlDAO.php' ;
require("func/data/connect.php");

$ifPopulated =  (new controlDAO())->getCapitalRequirements()->selectOneCapitalReqByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>



<!DOCTYPE html>



<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

        <?php 
          include("req/header_main.php") ;
          

        ?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Finsec Admin [Acceptance]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">


                                <div class="col-md-12 mail_listing">
                                    
                                         <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                        <div class="login-content" style ="margin-top:0px;">
                         <h4 class="text-uppercase">CAPITAL REQUIREMENTS VERIFICATION</h4>
                            <form action="func/controller/verifyCapitalRequirementsController.php" class="login-form" method="post">
                            <input type ="hidden" value="signup" name="signup"/>
                            <input type ="hidden" value= "<?=@$_GET['email'] ?>"  name ="email"/>
                            <input type ="hidden" value= "capital"  name ="form"/>
                            
                            <div class="row">
                                <div class="col-xs-2">
                                    <b>Company name<b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Name" name="company_name"  value="<?=@$company_name;?>"  readonly/>
                                  </div>
                                  <div class="col-xs-2">
                                     <input   name="pem[]" type="checkbox" 
                                                                          
                                       value="1" />
                                   </div>
                               

                                <div class="col-xs-2">
                                  <b>  Company registration number</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Reg Number" name="company_registration_number" value="<?=@$company_registration_number;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                <input   name="pem[]" type="checkbox" 
                                      
                                      value="2" />
                                </div>
                                </div>
                            </div>  
                            <div class="row">  
                                <div class="col-xs-2">
                                    Date of Incorporation:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Date" name="date_of_incorporation" value="<?=@$date_of_incorporation;?>" readonly/>
                                  </div>
                                    <div class="col-xs-2">
                                    <input  name="pem[]" type="checkbox" 
                                      
                                       value="3" />
                                  </div>
                                  <div class="col-xs-2">
                                    <b>Country of Incorporation:</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Country" name="country_of_incorporation"  value="<?=@$country_of_incorporation;?>" readonly/>                     
                                 </div>
                                  <div class="col-xs-2">
                                 <input  name="pem[]" type="checkbox" 
                                      
                                       value="4" />
                                  </div>
                                </div>
                              
                            <div class="row">  
                                <div class="col-xs-2">
                                    <b>Type of Entity</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Country" name="entity_type"  value="<?=@$type_of_entity;?>" readonly/>
                                </div>
                                 <div class="col-xs-2">
                                 <input  name="pem[]" type="checkbox" 
                                      
                                       value="5" />
                                  </div>


                                <div class="col-xs-2">
                                    Business Sector
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Country" name="country_of_incorporation"  value="<?=@$business_sector;?>" readonly/>
                                   
                                </div>
                                <div class="col-xs-2">
                                  <input  type="checkbox"  name="pem[]"  value ="6" />
                                </div>

                               
                            </div>
                            <div class="row">  
                                <div class="col-xs-2">
                                   <b> Telephone: </b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Phone" name="telephone"  value="<?=@$telephone;?>" readonly/>

                                </div>
                                <div class="col-xs-2">
                                  <input  name="pem[]"  type="checkbox"  value="7" />
                                </div>
                                <div class="col-xs-2">
                                    <b>Fax Number :</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Fax" name="fax_number"  value="<?=@$fax_number;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                  <input  type="checkbox"  name="pem[]"  value ="8" />
                                </div>

                            </div>

                             <div class="row">
                              <div class="col-xs-2">
                                    <b>Nature of business:</b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Business Nature" name="nature_of_business"  value="<?=@$nature_of_business;?>" readonly/>

                                </div>
                                <div class="col-xs-2">
                                  <input  type="checkbox"  name="pem[]"  value ="9" />
                                </div>
  
                                <div class="col-xs-2">
                                    Registered Office Physical Address: 
                                   <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Fax" name="fax_number"  value="<?=@$registered_office_physical_address;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                  <input  type="checkbox"  name="pem[]"  value ="10" />
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-2">
                                    Postal Address :
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Fax" name="fax_number"  value="<?=@$postal_address;?>" readonly/>
                                    
                                </div>
                                <div class="col-xs-2">
                                  <input  name="pem[]"  type="checkbox"  value="11" />

                                </div>

                                <div class="col-xs-2">
                                    <b>Principal Place of Business (Address) </b>
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Fax" name="fax_number"  value="<?=@$principal_place_of_business;?>" readonly/>
                                </div>
                                <div class="col-xs-2">
                                  <input  type="checkbox"  name="pem[]"  value ="12" />
                                </div>

                            </div>
                             
                                    <div class="row">
                                        <div class="col-xs-2">
                                           <b> Equity</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="$" name="raised_equity" value="<?=@$raised_equity;?>" readonly/>
                                        </div>
                                        <div class="col-xs-2">
                                          <input  type="checkbox"  name="pem[]"  value ="13" />
                                           </div>
                                            <div class="col-xs-2">
                                           <b> %Share on Offer </b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="$" name="share_on_offer"  value="<?=@$share_on_offer;?>" readonly/>
                                        </div>
                                        <div class="col-xs-2">
                                         <input  type="checkbox"  name="pem[]"    value ="15" />
                                          </div>

                                      
                                       
                                    </div>
                                
                                <div class="row">
                                  <div class="col-xs-2">
                                            <b>Other</b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="$" name="raised_other"  value="<?=@$raised_other;?>" readonly/>
                                        </div>
                                        <div class="col-xs-2">
                                        <input  type="checkbox"  name="pem[]"  value ="16" />
                                          </div>
                                           <div class="col-xs-2">
                                           <b> Debt </b>
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="$" name="raised_debt"  value="<?=@$raised_debt;?>" readonly/>
                                        </div>
                                        <div class="col-xs-2">
                                         <input  type="checkbox"  name="pem[]"    value ="14" />
                                          </div>
                                        </div>
                                        <div class="row">

                                <div class="col-xs-2">
                                    <b>Purpose of funds :</b>
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="$" name="raised_other"  value="<?=@$purpose_of_funds;?>" readonly/>
                                      </div>
                                     
                                     <div class="col-xs-2">
                                        <input  type="checkbox"  name="pem[]"  value ="17" />
                                          </div>
                                       </div>
                               


                            
                            
                            
                                
                           
                                                 
                            <div >
                                <a href ="analyst_pending_applications.php ?>"  class="btn btn-primary">[<< back]</a> &nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary">Next</button>
                               </div>
                          </form>

                            </div>

                            





                    </div>

<!--                                         <tr>
                                           <td colspan="4" style="text-align:center">No Appliacction found</td>
                                        </tr>  -->
                                        
                                    
                                        
                                     
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>