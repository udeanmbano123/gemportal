





<?php
require_once 'func/controlDAO.php' ;
require("func/data/connect.php");

$ifPopulated =  (new controlDAO())->getCapitalRequirements()->selectOneCapitalReqByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>



<!DOCTYPE html>



<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

        <?php 
          include("req/header_main.php") ;
          

        ?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
                   <div class="row bg-title" style="background: #9ea1f1;">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title"  style="color: #ffffff;" >Finsec Admin [Acceptance]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>


                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <!-- row -->
                            <div class="row">
                                <form name ="capital_requirements" method="post" action="func/controller/analyst_capital_requirement.php">
                                    <input type="hidden" name ="email" value="<?=@$_GET['email'];?>" >
                                <div class="col-lg-12 col-md-9 col-sm-12 col-xs-12 ">
                                    <div class="inbox-center">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>


                                                    <th colspan="2">
                                                        <h3>Capital Requirements Verifications</h3>
                                                    </th>
                                                    <th width="120">
                                                        <div class="checkbox m-t-0 m-b-0 ">

                                                            <input id="checkbox0" type="checkbox" class="checkbox-toggle" value="check all">
                                                            <label for="checkbox0">Check Validation</label>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            <tr class="unread">
                                                <td width="350" class="hidden-xs">Company name </td>
                                                <td class="max-texts"><?=@$company_name;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Company registration number </td>
                                                <td class="max-texts"><?=@$company_registration_number;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Date of Incorporation </td>
                                                <td class="max-texts"><?=@$date_of_incorporation;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Country of Incorporation</td>
                                                <td class="max-texts"><?=@$country_of_incorporation;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Region</td>
                                                <td class="max-texts"><?=@$region_of_incorporation;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Type of Entity</td>
                                                <td class="max-texts"><?=@$type_of_entity;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Business Sector</td>
                                                <td class="max-texts"><?=@$business_sector;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Nature of business</td>
                                                <td class="max-texts"><?=@$nature_of_business;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Telephone </td>
                                                <td class="max-texts"><?=@$telephone;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Registered Office Physical Address </td>
                                                <td class="max-texts"><?=@$registered_office_physical_address;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Postal Address  </td>
                                                <td class="max-texts"><?=@$postal_address;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">
                                                <td  class="hidden-xs">Principal Place of Business (Address)  </td>
                                                <td class="max-texts"><?=@$principal_place_of_business;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Fax Number </td>
                                                <td class="max-texts"><?=@$fax_number;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">
                                                <td  class="hidden-xs">Total Equity </td>
                                                <td class="max-texts"><?=@$raised_equity;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">
                                                <td  class="hidden-xs">Equity Capex </td>
                                                <td class="max-texts"><?=@$raised_equity_capex;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Equity Business Development </td>
                                                <td class="max-texts"><?=@$raised_equity_bd;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">
                                                <td  class="hidden-xs">Equity General Working Capital </td>
                                                <td class="max-texts"><?=@$raised_equity_gwc;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">
                                                <td  class="hidden-xs">Total Debt </td>
                                                <td class="max-texts"><?=@$raised_debt;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">
                                                <td  class="hidden-xs">Debt Capex </td>
                                                <td class="max-texts"><?=@$raised_debt_capex;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">
                                                <td  class="hidden-xs">Debt Business Development </td>
                                                <td class="max-texts"><?=@$raised_debt_bd;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">
                                                <td  class="hidden-xs">Debt General Working Capital </td>
                                                <td class="max-texts"><?=@$raised_debt_gwc;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">% share under offer </td>
                                                <td class="max-texts"><?=@$share_on_offer;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Other </td>
                                                <td class="max-texts"><?=@$raised_other;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="unread">

                                                <td  class="hidden-xs">Purpose of funds </td>
                                                <td class="max-texts"><?=@$purpose_of_funds;?></td>
                                                <td>
                                                    <div class="checkbox m-t-0 m-b-0">
                                                        <input type="checkbox" name ="" >
                                                        <label for="checkbox0"></label>
                                                    </div>
                                                </td>
                                            </tr>


                                            </tbody>

                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-7 m-t-20">
                                            <div class="btn-group pull-left">
                                                <a href="analyst_pending_applications.php" class="btn btn-default waves-effect">[ Pending Lists ]</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-5 m-t-20">
                                            <div class="btn-group pull-right">
                                                <button type="submit" class="btn btn-default waves-effect">[Corporate Directory >> ]</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>