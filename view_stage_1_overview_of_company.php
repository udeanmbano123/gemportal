<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;

$page_title = "Finsec | Stage One" ; 
$extra_css = "" ;
$thy_stage = "1" ;
$page_number = "4" ;
$thy_qsn = "43/472" ;
$thy_qp = round(43/472 * 100, 2);
$extra_js = "" ; 
$main_page_title = "Basic Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header_admin.php") ;
$my_title  = "COMPANY OVERVIEW"   ;
@$msg=$_GET['msg'];
@$raw_msg=$_GET['raw_msg'];
@$supplier_msg=$_GET['supplier_msg'];
require("func/data/connect.php");
$user_email=$_GET['email'];

  $ifPopulated =  (new controlDAO())->getcompanyOverview()->selectOneCompanyOverviewByEmail($_GET['email']) ;
 if(!$ifPopulated){

    }else{

    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
     }

  }

  $count_products = 0 ;
  $count_raw_materials = 0 ;


?>
                   
                <div class="col-md-8 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title my_custom_header_main">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <!-- /.col-lg-12 -->
                        </div>

                        <form action="#"  class="login-form" method="post">

                            <div class="white-box-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        Introduction, History and Major Milestones:
                                         <textarea disabled class="form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50" name="company_overview_history"><?=@$company_overview_history;?></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h5>The Company’s Products/Services :
                                        </h5>
                                        <table class="table table-bordered  color-bordered-table success-bordered-table">
                                          <thead>
                                          <tr>
                                             <th>Product / Service Name</th>
                                             <th>Description of Product</th>
                                             <th>Sales Volume (% of Total Sales)</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          <?php
                                              $ifPopulated =  (new controlDAO())->getcompanyOverviewProductsServices()->selectCompanyOverviewProductsServicesByEmail($_GET['email']) ;
                                              if(!$ifPopulated){
                                              //echo "There is no data " ;
                                                  $count_products = 0 ;
                                                echo '<tr>
                                                         <td colspan="5" style="text-align:center">No Products found</td>
                                                      </tr>' ;
                                              }else{
                                              $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                              foreach($ifPopulated as $i => $item) {
                                                  //company_overview_products_services_id, user_id, company_overview_id, product_service_name, description_of_product, sales_volume
                                                    extract($ifPopulated[$i]);
                                                    $count_products++;
                                                  echo "<tr>
                                                             <td>
                                                             <input type ='hidden' value= 'set'  name ='products'/>
                                                                <span id='product_service_name".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                                ".@$ifPopulated[$i]['product_service_name']."
                                                                </span>
                                                             </td>
                                                             <td>
                                                                 <span id='description_of_product".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                                 ".@$ifPopulated[$i]['description_of_product']."
                                                                 </span>
                                                             </td>
                                                             <td>
                                                                 <span id='sales_volume".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                                 ".@$ifPopulated[$i]['sales_volume']."
                                                                 </span>
                                                             </td>

                                                          </tr>" ;
                                              }
                                              //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                              //echo $user_id ;
                                              }
                                          ?>

                                          </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h5>The Company’s Major Raw Materials (In Respect to Manufacturers Only) :
    <!--                                      <a href="javascript:void()" data-toggle="modal" data-target="#exampleModalone" data-whatever="@mdo">[ Add New ]</a>-->

                                        </h5>
                                        <table class="table table-bordered  color-bordered-table info-bordered-table">
                                          <thead>
                                          <tr>
                                             <th>Raw Material Name</th>
                                             <th>Description of Raw Material</th>
                                             <th>Purchase Volume (% of Total Purchases)</th>
                                          </tr>
                                          </thead>
                                          <tbody>

                                          <?php
                                          $ifPopulated =  (new controlDAO())->getcompanyOverviewRawMaterials()->selectcompanyOverviewRawMaterialsByEmail($_GET['email']) ;
                                          if(!$ifPopulated){
                                              //echo "There is no data " ;
                                                $count_raw_materials = 0 ;
                                              echo '<tr>
                                                         <td colspan="3" style="text-align:center">No Materials found</td>
                                                      </tr>' ;
                                          }else{
                                              $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                              foreach($ifPopulated as $i => $item) {
                                                  //company_overview_products_services_id, user_id, company_overview_id, product_service_name, description_of_product, sales_volume
                                                  extract($ifPopulated[$i]);
                                                  $count_raw_materials++;
                                                  echo "<tr>
                                                             <td>
                                                             <input type ='hidden' value= 'set'  name ='rawmaterials'/>
                                                                 <span id='name".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                                 ".@$ifPopulated[$i]['name']."
                                                                 </span>
                                                             </td>
                                                             <td>
                                                                 <span id='desc".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                                 ".@$ifPopulated[$i]['desc']."
                                                                 </span>
                                                             </td>
                                                             <td>
                                                                 <span id='volume".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                                 ".@$ifPopulated[$i]['volume']."
                                                                 </span>
                                                             </td>
                                                          </tr>" ;
                                              }
                                              //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                              //echo $user_id ;
                                          }
                                          ?>

                                          </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h5>Major Customers and Distribution Channels :

                                        </h5>
                                        <table class="table table-bordered  color-bordered-table success-bordered-table">
                                          <thead>

                                          <tr>
                                             <th>Customer (Buyer)’s Name</th>
                                             <th>Location</th>
                                             <th>Years of Relationship</th>
                                             <th>% of Sales</th>
                                             <th>Trade Terms</th>
                                             <th>Distribution Channel</th>
                                          </tr>
                                          </thead>
                                          <tbody>

                                          <?php
                                          $ifPopulated =  (new controlDAO())->getcompanyOverviewCustomers()->selectcompanyOverviewCustomersByEmail($_GET['email']) ;
                                          if(!$ifPopulated){
                                              //echo "There is no data " ;

                                              echo '<tr>
                                                         <td colspan="6" style="text-align:center">No Customers  found</td>
                                                      </tr>' ;
                                          }else{
                                              $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                              foreach($ifPopulated as $i => $item) {
                                                  extract($ifPopulated[$i]);
                                                  echo "<tr>
                                                             <td>
                                                             <input type ='hidden' value= 'set'  name ='distributionchannels'/>
                                                             <span id='customer_name".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_name']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_location".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_location']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_years_rel".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_years_rel']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_sales".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_sales']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_trade_terms".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_trade_terms']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_distr_channel".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_distr_channel']."
                                                             </span>
                                                             </td>
                                                          </tr>" ;
                                              }
                                              //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                              //echo $user_id ;
                                          }
                                          ?>

                                          </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h5>Major Suppliers and Supply Channels :

    <!--                                      <a href="javascript:void()" data-toggle="modal" data-target="#exampleModalthree" data-whatever="@mdo">[ Add New ]</a>-->
                                        </h5>
                                        <table class="table table-bordered  color-bordered-table info-bordered-table">
                                          <thead>

                                          <tr>
                                             <th>Customer (Buyer)’s Name</th>
                                             <th>Location</th>
                                             <th>Years of Relationship</th>
                                             <th>% of Sales</th>
                                             <th>Trade Terms</th>
                                             <th>Distribution Channel</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          <?php
                                          $ifPopulated =  (new controlDAO())->getcompanyOverviewSuppliers()->selectcompanyOverviewCustomersByEmail($_GET['email']) ;
                                          if(!$ifPopulated){
                                              //echo "There is no data " ;

                                              echo '<tr>
                                                         <td colspan="6" style="text-align:center">No Suppliers found</td>
                                                      </tr>' ;
                                          }else{
                                              $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                              foreach($ifPopulated as $i => $item) {
                                                  //company_overview_suppliers_id, user_id, company_overview_id, , , , , ,
                                                  extract($ifPopulated[$i]);
                                                  echo "<tr>
                                                             <td>
                                                             <input type ='hidden' value= 'set'  name ='supplychannels'/>
                                                             <span id='de_customer_name".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_name']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_location".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_location']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_years_rel".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_years_rel']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_sales".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_sales']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_trade_terms".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_trade_terms']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_distr_channel".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_distr_channel']."
                                                             </span>
                                                             </td>
                                                                                                                      
                                                          </tr>" ;
                                              }
                                              //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                              //echo $user_id ;
                                          }
                                          ?>
                                          </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        The Company’s Protected Rights
                                        <br>
                                        1. <a href="<?=$company_overview_rights;?>"> <?=@$company_overview_rights;?></a>

                                    </div>

                                </div>

                            </div>


                    </div>


                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <?php
                                if($others->getUserPemission($_SESSION['email']) =="Analyst") {
                                    ?>
                                    <a href="analyst_pending_applications.php" class="btn green btn-outline">[<< Back
                                        Pending Applications]</a>
                                    <?php
                                }
                                if($others->getUserPemission($_SESSION['email']) == "Financier") {
                                    ?>
                                    <a href="fin_applicants.php" class="btn green btn-outline">[<< Back
                                        View Applicants Lists]</a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <button type="button"  class="js-print-link btn blue btn-outline pull-right" >[ PRINT ] </button>
                            </div>
                        </div>


                    </div>

                    </form>
                </div>



<?php
require_once("require/footer_admin.php") ;
?>