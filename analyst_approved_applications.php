<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

      	<?php 
      		include("req/header_main.php") ;
          require_once 'func/controlDAO.php' ;
        $others = (new controlDAO())->getOthers() ;
          $extra_js = "" ; 
      	?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title" style="background: #9ea1f1;">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title"  style="color: #ffffff;" >Finsec Analyst[Applications]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="advisor_dashboard.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">

                                <div class="col-md-12 mail_listing">
                                    <h1 class="box-title">Recommended Applications</h1><span><?php echo @$_GET['msg'] ; ?></span>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Company name </th>
                                            <th>Applicant Name</th>
                                            <th>Phone number</th>
                                            <th>Email</th>
                                            <th>Region</th>
                                            <th>Business Sector</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        $ifPopulated =  $others->getFinancierRecomendedApplications() ;
                                        if(!$ifPopulated){
                                            //echo "There is no data " ;

                                            echo '<tr>
                                                     <td colspan="7" style="text-align:center">No recommended applications found</td>
                                                  </tr>' ;
                                        }else{
                                            $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                            foreach($ifPopulated as $i => $item) {
                                                //# directors_shareholding_id, user_id, , ,
                                                //total_score, users_fullname, company_name, business_sector, region_of_incorporation, telephone, users_created_date, capital_required
                                                extract($ifPopulated[$i]);
                                                echo "<tr>
                                                          <td id = 'idnum".@$ifPopulated[$i]['capital_req_id']."'>".@($i+1)."</td>
                                                         <td>".@$ifPopulated[$i]['company_name']."</td>
                                                         <td>".@$ifPopulated[$i]['users_fullname']."</td>
                                                          <td>".@$ifPopulated[$i]['telephone']."</td>
                                                          <td>".@$ifPopulated[$i]['email']."</td>
                                                           <td>".@$ifPopulated[$i]['region_of_incorporation']."</td>
                                                           <td>".@$ifPopulated[$i]['business_sector']."</td>
                                                         <td>
                                                         <a href='analystsRecommendViewer.php?email=".@$ifPopulated[$i]['email']."'>[View Recommendation]</a> 
                                                         </td>                                                       
                                                      </tr>" ;
                                            }
                                            //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                            //echo $user_id ;
                                        }
                                        ?>


















                                        <!-- <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>
                                           <td><a href='#'>[View Details]</a> | <a href ='#'>[Accept]</a>| <a href ='#'>[Reject]</a></td>
                                        </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.row -->


            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                        <div class="modal-dialog" role="document">
                          <?php
                          $id=$_GET['id'];
                          echo $id;
        //                     $sql="SELECT * FROM `capital_requirements` WHERE `capital_req_id`='$id'";
        //                    $result=mysql_query($sql);
        //                    while($row=mysql_fetch_array($result)){
        //                    $name=$row2['company_name'];
        // $company=$row2['company_registration_number'];
        // $date=$row2['date_of_incorporation'];
        // $type=$row2['type_of_entity'];
        // $business=$row2['business_sector'];
        // $nature=$row2['nature_of_business'];
        // $equity=$row2['raised_equity'];
        // $other=$row2['raised_other'];
        // $nature=$row2['purpose_of_funds'];
// }
                          
                          ?>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="exampleModalLabel1"><b>The Company Overview Summary</b></h4> </div>
                                    <h5><b><?=@$company_name;?></b></h5>
                                    <h6>Capital Requirements</h6>
                                    <br>
                                    <!-- <input type="text" name="id_number" id = "tinashe"> <br> -->

                               <p style="font-size:16px;"> Company name is <b><?=@$company_name;?></b>,Registration number is <b><?=@$contact_person_name;?></b>,Date of Incorporation <b><?=@$date_of_incorporation;?></b>,Type of Entity is <b><?=@$type_of_entity;?> </b>,Business Sector <b><?=@$business_sector;?></b>,Nature of business <b><?=@$nature_of_business;?></b>,Raised Equity <b><?=@$raised_equity;?></b>,Raised Debt,Raised Other<b> <?=@$raised_other;?></b>,Purpose of Funds <b><?=@$purpose_of_funds;?></b></p>
                             </br>
                                    
                               
                                    
                                
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <a href ="detailedinformation.php" class="btn green btn-outline">[Detailed Information]</a>
                                    </div>
                            </div>
                        </div>
                    </div>

            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script type="text/javascript">
        function popup(){
        window.open ("stage_1_overview_of_company.php","mywindow","menubar=1,resizable=1,width=350,height=250");
      }
      </script>
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
      $(document).ready(function(){
          
       
        $(document).on('click', '.new_product', function(){
          $('input[name=edit_prod]').attr('name', 'create_prod');
          $('#exampleModal').modal('show');
          $('#eproduct_service_name').val('');
          $('#edescription_of_product').val('');
          $('#esales_volume').val('');
        });
        $(document).on('click', '.view_applicant_details', function(){
          var id=$(this).attr('id').split('_')[1];
          $('#tinashe').val(id) ;
          $.get("api/getdetails.php?id="+id , function(result){
                //alert(result) ;
          })
          $('#exampleModal').modal('show');


        });

        
      });
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>