<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "" ; 
$page_number = "5" ; 
$extra_js = "" ;
$thy_stage = "3" ;

$main_page_title = "Advanced Stage" ;
@$label=$_GET['action'];
@$id=$_GET['id'];
require_once("require/header.php") ;
require_once 'func/controlDAO.php' ;
$ifPopulated =  (new controlDAO())->getincomeStatement()->selectincomeStatementByID(@$id) ;
foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
?>


<div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                         <h1 class="text-uppercase "> <?php if ($label){
                            echo 'Add Income Statement';
                         }
                         else {
                          echo  'Edit Income Statement';
                         }  ; ?></h1>
                        <form action="func/controller/projectedincomeStatementController.php"  class="login-form" method="post">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Please complete all the required field with * asterisks  </span>
                            </div>
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                             <input type ="hidden" value= "<?= @$_GET['id'] ?>"  name ="id"/>
                             <input type ="hidden" value= "future"  name ="type"/>
                            <div class="row">
                                
                                
                           
                                    	<div class="col-xs-4">
                                    *Year:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Year" name="year" value="<?=@$year;?>" required/>
                                </div>
                                        <div class="col-xs-4">
                                            *%Gross Income
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Gross Income" name="income"  value="<?=@$gross_income_per;?>" required/>
                                        </div>
                                       <div class="col-xs-4">
                                            *Cost of Sales
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Cost of Sales" name="sales"  value="<?=@$cost_of_sales;?>" required/>
                                        </div>

                                    

                                
                            </div>  
                            <div class="row"> 
                            <div class="col-xs-4">
                                    *Gross Income:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Gross Income" name="gincome" value="<?=@$gross_income;?>" required/>
                                </div> 
                                <div class="col-xs-4">
                                    *Gross Profit:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Gross profit" name="gprofit" value="<?=@$gross_profit;?>" required/>
                                </div>
                                <div class="col-xs-4">
                                    *Net Profit
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Net Profit" name="nprofit" value="<?=@$net_profit;?>" required/>
                                    
                                </div>                                
                                
                                <div class="row"> 
                               <div class="col-xs-4">
                               	<button type="submit" class="btn btn-primary">Save</button>
                               </div>
                                </div>


                            </div>


   