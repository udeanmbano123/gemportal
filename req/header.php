<?php 
   session_start() ; 
   
?>

         <!-- Navigation -->
         <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
               <!-- <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a> -->
               <div class="top-left-part">
                  <a class="logo" href="dashboard.php">
                     <span class="hidden-xs"><img src="img/finlogo.png" width="100%" alt="home" /></span>
                  </a>
               </div>
<!--                <ul class="nav navbar-top-links navbar-left hidden-xs">
                  <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
               </ul> -->
               <ul class="nav navbar-top-links navbar-right pull-right">

                  <li class="dropdown">
                     <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo ucwords($_SESSION['users_fullname']) ; ?></b> </a>
                     <ul class="dropdown-menu dropdown-user animated flipInY">
                        <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                        <li><a href="#"><i class="ti-wallet"></i> My Form</a></li>
                        <li><a href="#"><i class="ti-email"></i> Notification</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="index.php?logout=true"><i class="fa fa-power-off"></i> Logout</a></li>
                     </ul>
                     <!-- /.dropdown-user -->
                  </li>
       
               </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
         </nav>
         <!-- Left navbar-header -->
         <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar" style="overflow-x: visible; overflow-y: hidden; width: auto; height: 100%;">
               <ul class="nav" id="side-menu">

                  <li class="user-pro">
                     <a href="#" class="waves-effect"><img src="plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span class="hide-menu"> <?php echo ucwords($_SESSION['users_fullname']) ; ?><span class="fa arrow"></span></span>
                     </a>
                     <ul class="nav nav-second-level">
                        <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Form</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-email"></i> Notification</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
                        <li><a href="index.php?logout=true"><i class="fa fa-power-off"></i> Logout</a></li>
                     </ul>
                  </li>
                  <!-- <li class="nav-small-cap m-t-10">--- Main Menu</li> -->
                  <li><a href="dashboard.php" style="font-size: 13px;" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu">DASHBOARD</span></a></li>
                  <li>
                     <a href="#" class="waves-effect" style="font-size: 13px;" ><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">FORM <span class="fa arrow"></span><span class="label label-rouded label-danger pull-right">1/14</span></span></a>
                     <ul class="nav nav-second-level">
                        <li><a href="form_1.php">1. CORPORATE DIRECTORY</a></li>
                        <li><a href="form_2.php">2. ENTERPRISE SIZE CATEGORISATION</a></li>
                        <li><a href="form_3.php">3. PROPRIETORS, PARTNERS, PRINCIPALS AND DIRECTORS PROFILES</a></li>
                        <li><a href="form_4.php">4. DIRECTORS’ SHAREHOLDING</a></li>
                        <li><a href="form_5.php">5. SHAREHOLDING STRUCTURE</a></li>
                        <!-- <li><a href="form_6.php">6. DERIVATIVE STOCK </a></li> -->
                        <!-- <li><a href="form_7.php">7. SHAREHOLDING BACKGROUND</a></li> -->
                        <li><a href="form_8.php">6. BOARD COMMITTEES</a></li>
                        <!-- <li><a href="form_9.php">9. CORPORATE STRUCTURE</a></li> -->
                        <li><a href="form_10.php">7. HUMAN RESOURCE ORGANOGRAM</a></li>
                        <li><a href="form_11.php">8. HUMAN RESOURCES PROFILES</a></li>
                        <li><a href="form_12.php">9. COMPANY OVERVIEW</a></li>
                        <li><a href="form_13.php">10. MATERIAL AND OTHER THIRD PARTY CONTRACTS</a></li>
                        <!-- <li><a href="form_14.php">14. MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)</a></li> -->
                        <li><a href="form_15.php">11. MATERIAL LITIGATION AND CLAIMS</a></li>
                        <li><a href="form_16.php">12. CAPITAL REQUIREMENTS</a></li>
                        <li><a href="form_17.php">13. CURRENT DEBT SCHEDULE</a></li>
                        <li><a href="form_18.php">14. PERSONAL FINANCIAL COMITMENTS</a></li>

                     </ul>
                  </li>



                  <li><a href="faq.php" style="font-size: 13px;" class="waves-effect"><i class="fa fa-circle-o text-success"></i> <span class="hide-menu">Faqs</span></a></li>
               </ul>
            </div>
         </div>
         <!-- Left navbar-header end -->