<?php 
   session_start() ; 
   
?>

         <!-- Navigation -->
         <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
               <!-- <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a> -->
               <div class="top-left-part">
                  <a class="logo" href="fin_dashboard.php">
                     <span class="hidden-xs"><img src="img/finlogo.png" width="100%" alt="home" /></span>
                  </a>
               </div>
               <ul class="nav navbar-top-links navbar-left hidden-xs">

                  <li><a href="fin_dashboard.php" class="open-close hidden-xs waves-effect waves-light active">Dashboard</a></li>
                  <li><a href="fin_applicants.php" class="open-close hidden-xs waves-effect waves-light active">Clients & Reports</a></li>

               </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                        <!-- /.dropdown-user -->
                    </li>

                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
         </nav>
