<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$my_title  = "CAPITAL REQUIREMENTS 1 - 5"   ;                      
$page_title = "FINSEC | Stage One" ;
$extra_css = "
    <link href='plugins/bower_components/custom-select/custom-select.css' rel='stylesheet' type='text/css' />
    <link href='plugins/bower_components/bootstrap-select/bootstrap-select.min.css' rel='stylesheet' />
" ;
$thy_stage = "1" ;
$thy_qsn = "16/472" ;
$thy_qp = round(16/472 * 100, 2);
$page_number = "1" ;
$extra_js = "
        <script src='plugins/bower_components/custom-select/custom-select.min.js' type='text/javascript'></script>
        <script src='plugins/bower_components/bootstrap-select/bootstrap-select.min.js' type='text/javascript'></script>

        <script>
        jQuery(document).ready(function() {
            $('.select2').select2();
        });
        </script>

" ;
@$error=$_GET['error'];
$main_page_title = "Basic Stage" ;
$first_screen = "true" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
$my_countries_lists  =  $others->getAllCountries() ;
$my_sectors_lists  = $others->getAllSectors() ;
$my_regions_lists  = $others->getAllRegion() ;
$my_business_nature  = $others->getAllBusinessNature() ;

require_once("require/header.php") ;
require("func/data/connect.php");
$user_email=$_SESSION['email'];
$sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
$result2=mysqli_query($con,$sql2);
while($row2=mysqli_fetch_array($result2)){
    $usertype=$row2['users_type'];
}



if($usertype=='applicant'){
 $ifPopulated =  (new controlDAO())->getCapitalRequirements()->selectOneCapitalReqByEmail($_SESSION['email']) ;
  if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

  }else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
      }
     $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
   }
}
elseif($usertype=='Administrator'){
      $ifPopulated =  (new controlDAO())->getCapitalRequirements()->selectOneCapitalReqByEmail($_GET['email']) ;
  if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

  }else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
      }
     $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
   }  
}
?>

                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                        <div class="login-content" style ="margin-top:0px;">
                            <div class="row bg-title my_custom_header_main">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                                    <?php if($usertype=='applicant'){ ?>
                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                    <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                    <ol class="breadcrumb">
                                        <li class="dropdown">
                                            <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                                [ Previous forms of this stage ]
                                            </a>
                                            <ul class="dropdown-menu mailbox animated ">
                                                <li>
                                                    <a class="text-left" href="index.php?logout=true"> <strong>LogOut </strong></a>
                                                </li>
                                            </ul>
                                            <!-- /.dropdown-messages -->
                                        </li>
                                        <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                    </ol>
                                </div>
                                <?php } ?>
                                <!-- /.col-lg-12 -->
                            </div>                            

                         <!-- <h1 class="text-uppercase"> <?php echo $my_title ; ?></h1> -->
                            <form action="func/controller/capitalRequirementsController.php"  class="login-form" method="post">
                                <?=$setOption;?>
                                <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Please complete all the required field with * asterisks

                                </span>
                            </div>
                                <div class="white-box-main">
                            <div class="row" >
                                <div class="col-xs-6">
                                    *Company name:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Name" name="company_name" value="<?=@$company_name;?>" />
                                </div>
                                <div class="col-xs-6">
                                    *Company registration number:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="number" autocomplete="off" placeholder="Reg Number" name="company_registration_number" value="<?=@$company_registration_number;?>" required/>
                                     <p><?php echo "<br><font color='red'  >".@$_GET['reg_error']. " </font>" ;?></p>      
                                </div>
                            </div>  
                            <div class="row">  
                                <div class="col-xs-4">
                                    *Date of Incorporation:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="date" autocomplete="off" placeholder="Date" name="date_of_incorporation" value="<?=@$date_of_incorporation;?>" required/>
                                </div>
                                <div class="col-xs-4">
                                    *Country of Incorporation:
                                    <select class=" mycustom_dropdown form-control select2 " name="country_of_incorporation" >
                                        <?php
                                        foreach($my_countries_lists as $country) { ?>
                                            <option  <?= (@$country_of_incorporation == $country['country_name'] ) ? 'selected="selected"' : '';?> value="<?= $country['country_name'] ?>"><?= $country['country_name'] ?></option>
                                            <?php
                                        } ?>
                                    </select>

                                    <!--<input  form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Country"   value="--><?//=@$country_of_incorporation;?><!--" required/>-->
                                </div>
                                <div class="col-xs-4">
                                    *Region:
                                    <select class=" mycustom_dropdown form-control select2 " name="region_of_incorporation" >
                                        <?php
                                        foreach($my_regions_lists as $region) { ?>
                                            <option  <?= (@$region_of_incorporation == $region['region_name'] ) ? 'selected="selected"' : '';?> value="<?= $region['region_name'] ?>"><?= $region['region_name'] ?></option>
                                            <?php
                                        } ?>
                                    </select>

                                    <!--<input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Region" name=""  value="--><?//=@$country_of_incorporation;?><!--" required/>-->
                                </div>

                            </div>

                            <div class="row">  
                                <div class="col-xs-4">
                                    Type of Entity
                                    <select name  = "type_of_entity" class="mycustom_dropdown form-control select2" required title="Please Select" >
                                        <option value = ""></option>
                                        <option <?=(@$type_of_entity == "Corporation" ) ? 'selected="selected"' : '';?> value = "Corporation">Corporation</option>
                                        <option <?=(@$type_of_entity == "Trust" ) ? 'selected="selected"' : '';?> value = "Trust">Trust</option>
                                        <option <?=(@$type_of_entity == "Partnership" ) ? 'selected="selected"' : '';?> value = "Partnership">Partnership</option>
                                        <option <?=(@$type_of_entity == "Association" ) ? 'selected="selected"' : '';?> value = "Association">Association</option>
                                        <option <?=(@$type_of_entity == "Non Profit" ) ? 'selected="selected"' : '';?> value = "Non Profit">Non Profit</option>
                                        <option <?=(@$type_of_entity == "Proprietorship" ) ? 'selected="selected"' : '';?> value = "Proprietorship">Proprietorship</option>
                                    </select> 
                                </div>

                                <div class="col-xs-4">
                                    Business Sector
                                    <select class=" mycustom_dropdown form-control select2 " name="business_sector" required title="Please Select" >
                                        <option value = ""></option>
                                        <?php
                                        foreach($my_sectors_lists as $sectors) { ?>
                                            <option  <?= (@$business_sector == $sectors['sector_name'] ) ? 'selected="selected"' : '';?> value="<?= $sectors['sector_name'] ?>"><?= $sectors['sector_name'] ?></option>
                                            <?php
                                        } ?>
                                    </select>

                                </div>

                                <div class="col-xs-4">
                                    *Nature of business:
                                    <select class=" mycustom_dropdown form-control select2 " name="nature_of_business" required title="Please Select" >
                                        <option value = ""></option>
                                        <?php
                                        foreach($my_business_nature as $bn) { ?>
                                            <option  <?= (@$nature_of_business == $bn['business_nature_name'] ) ? 'selected="selected"' : '';?> value="<?= $bn['business_nature_name'] ?>"><?= $bn['business_nature_name'] ?></option>
                                            <?php
                                        } ?>
                                   </select>
                                    <!--<input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Business Nature" name=""  value="--><?//=@$nature_of_business;?><!--" required/>-->
                                </div>

                            </div>
                            <div class="row">  
                                <div class="col-xs-6">
                                    *Telephone:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="number" min="0"
                                            title="Phone number remaining 9 digit with 0-9"
                                           autocomplete="off" placeholder="Phone number remaining 9 digit with 0-9" name="telephone"  value="<?=@$telephone;?>" required/>
                                     <p><?php echo "<br><font color='red'  >".@$_GET['tele_error']. " </font>" ;?></p> 
                                </div>
                                <div class="col-xs-6">
                                    *Fax Number :
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="number" autocomplete="off" placeholder="Fax" name="fax_number"  value="<?=@$fax_number;?>" required/>
                                     <p><?php echo "<br><font color='red'  >".@$_GET['fax_error']. " </font>" ;?></p> 
                                </div>

                            </div>

                             <div class="row">  
                                <div class="col-xs-4">
                                    Registered Office Physical Address: 
                                    <textarea class="form-control form-control-solid placeholder-no-fix form-group" name ="registered_office_physical_address"  rows="4" cols="50"><?=@$registered_office_physical_address;?></textarea>
                                </div>
                                <div class="col-xs-4">
                                    Postal Address :
                                    <textarea class="form-control form-control-solid placeholder-no-fix form-group" name ="postal_address"  rows="4" cols="50"><?=@$postal_address;?></textarea>
                                </div>
                                <div class="col-xs-4">
                                    Principal Place of Business (Address) :
                                    <textarea class="form-control form-control-solid placeholder-no-fix form-group" name ="principal_place_of_business"  rows="4" cols="50"><?=@$principal_place_of_business;?></textarea>
                                </div>

                            </div>
                             <div class="row">  
                                <div class="col-xs-12">
                                    Capital to be raised : 
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <fieldset>
                                                <legend>Equity Requirements</legend>
                                                <div class="row">
                                                <div class="col-xs-12">
                                                    *Total Equity
                                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="number" autocomplete="off" placeholder="$" name="raised_equity" value="<?=@$raised_equity;?>" required/>
                                                     <p><?php echo "<br><font color='red'  >".@$error. " </font>" ;?></p>
                                                </div>
                                                <div class="col-xs-4">
                                                    *Capex
                                                    <input class="form-control form-control-solid placeholder-no-fix form-group"
                                                           type="number" autocomplete="off" placeholder="$" name="raised_equity_capex"
                                                           value="<?=@$raised_equity_capex;?>" required/>
                                                </div>
                                                <div class="col-xs-4">
                                                    *Business Development
                                                    <input class="form-control form-control-solid placeholder-no-fix form-group"
                                                           type="number" autocomplete="off" placeholder="$" name="raised_equity_bd"
                                                           value="<?=@$raised_equity_bd;?>" required/>
                                                </div>
                                                <div class="col-xs-4">
                                                    *General Working Capital
                                                    <input class="form-control form-control-solid placeholder-no-fix form-group"
                                                           type="number" autocomplete="off" placeholder="$" name="raised_equity_gwc"
                                                           value="<?=@$raised_equity_gwc;?>" required/>
                                                </div>
                                            </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-xs-6">
                                            <fieldset>
                                                <legend>Debt Requirements</legend>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        *Total Debt
                                                        <input class="form-control form-control-solid placeholder-no-fix form-group"
                                                               type="number" autocomplete="off" placeholder="$" name="raised_debt"
                                                               value="<?=@$raised_debt;?>" required/>
                                                        <p><?php echo "<br><font color='red'  >".@$error. " </font>" ;?></p>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        *Capex
                                                        <input class="form-control form-control-solid placeholder-no-fix form-group"
                                                               type="number" autocomplete="off" placeholder="$" name="raised_debt_capex"
                                                               value="<?=@$raised_debt_capex;?>" required/>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        *Business Development
                                                        <input class="form-control form-control-solid placeholder-no-fix form-group"
                                                               type="number" autocomplete="off" placeholder="$" name="raised_debt_bd"
                                                               value="<?=@$raised_debt_bd;?>" required/>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        *General Working Capital
                                                        <input class="form-control form-control-solid placeholder-no-fix form-group"
                                                               type="number" autocomplete="off" placeholder="$" name="raised_debt_gwc"
                                                               value="<?=@$raised_debt_gwc;?>" required/>
                                                    </div>
                                                </div>
                                            </fieldset>

                                        </div>
                                        <div class="col-xs-6">
                                            *% share under offer
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="number" autocomplete="off" placeholder="$" name="share_on_offer" value="<?=@$share_on_offer;?>" required/>
                                             <p><?php echo "<br><font color='red'  >".@$error. " </font>" ;?></p>
                                        </div>


                                        <div class="col-xs-6">
                                            *Other
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="number" autocomplete="off" placeholder="$" name="raised_other"  value="<?=@$raised_other;?>" required/>
                                            <p><?php echo "<br><font color='red'  >".@$error. " </font>" ;?></p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-xs-12">
                                    Purpose of funds :
                                    <textarea class="form-control form-control-solid placeholder-no-fix form-group" name ="purpose_of_funds"  rows="4" cols="50"><?=@$purpose_of_funds;?></textarea>

                                </div>

                            </div>

                                </div>

                    </div>


                        <div class="login-footer">
                            <?php if($usertype=='applicant'){ ?>
                            <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="index.php" class="btn green btn-outline">Logout</a>
                            </div>
                            <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <button type="submit" class="btn green uppercase btn-outline pull-right">[Next >>]</button>
                            </div>
                        </div>
                        <?php } elseif ($usertype=='Administrator') {
                              ?>
                             <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <a href="applicantAlldetails.php?email=<?php echo $_GET['email'] ?>" class="btn green uppercase btn-outline pull-right">[<< Back]</a>
                            </div>
                            <?php } ?>

                        </div>

                    </form>

                </div>
<?php
require_once("require/footer.php") ;
?>