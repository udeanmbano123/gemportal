-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2018 at 05:26 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `capitalonline`
--
CREATE DATABASE IF NOT EXISTS `capitalonline` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `capitalonline`;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_level`
--

CREATE TABLE `applicant_level` (
  `level_id` int(255) NOT NULL,
  `send_to` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant_level`
--

INSERT INTO `applicant_level` (`level_id`, `send_to`, `user_id`) VALUES
(3, 'Financier', 'tinashe@ctrade.co.zw'),
(10, 'Financier', 'ronaldinho@gmail.com'),
(13, 'Financier', 'makazatinashe2000@gmail.com'),
(14, 'NONE', 'stacy@gmail.com'),
(15, 'NONE', 'takawira@gmail.com'),
(16, 'Financier', 'makowe@gmail.com'),
(17, 'NONE', 'financier@gmail.com'),
(18, 'NONE', 'analyst@gmail.com'),
(19, 'NONE', 'sango@gmail.com'),
(20, 'NONE', 'admin@gmail.com'),
(27, 'NONE', 'brain@ctrade.co.zw'),
(28, 'NONE', 'martin@gmail.com'),
(29, 'DONE', 'anthony@finsec.co.zw'),
(30, 'INTERMEDIATE', 'hudson@escrowgroup.org'),
(31, 'DONE', 'Timmy@escrowgroup.org'),
(32, 'BASIC', 'karl@escrowgroup.org'),
(33, 'BASIC', 'karlenko42@gmail.com'),
(34, 'ADVANCED', 'anthony@escrowgroup.org'),
(35, 'BASIC', 'welly.kc1@gmail.com'),
(36, 'ADVANCED', 'tario@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `approved_applications`
--

CREATE TABLE `approved_applications` (
  `id` int(255) NOT NULL,
  `applicant_id` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `done_by` varchar(145) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `approved_applications`
--

INSERT INTO `approved_applications` (`id`, `applicant_id`, `reason`, `done_by`) VALUES
(1, 'ronaldinho@gmail.com', ' ', NULL),
(4, 'tinashe@ctrade.co.zw', '', 'financier@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `balance_sheet`
--

CREATE TABLE `balance_sheet` (
  `balance_sheet_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `year` varchar(245) DEFAULT NULL,
  `is_audited` varchar(255) NOT NULL,
  `property_plant_and_equipment` varchar(255) NOT NULL,
  `investment_properties` varchar(255) NOT NULL,
  `intangible_assets` varchar(255) NOT NULL,
  `other_fixed_assets` varchar(255) NOT NULL,
  `total_non_current_assets` varchar(255) NOT NULL,
  `biological_assets` varchar(255) NOT NULL,
  `inventories` varchar(255) NOT NULL,
  `trade_and_receivables` varchar(255) NOT NULL,
  `prepayments` varchar(255) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `cash` varchar(255) NOT NULL,
  `other_current_assets` varchar(255) NOT NULL,
  `total_current_assets` varchar(255) NOT NULL,
  `total_assets` varchar(255) NOT NULL,
  `issue_share_capital` varchar(255) NOT NULL,
  `share_premium` varchar(255) NOT NULL,
  `revenue_reserves` varchar(255) NOT NULL,
  `capital_reserves` varchar(255) NOT NULL,
  `other_capital_and_revenue_reserves` varchar(255) NOT NULL,
  `equity_attributed_to_owners_of_parent` varchar(255) NOT NULL,
  `non_controlling_interest` varchar(255) NOT NULL,
  `total_equity` varchar(255) NOT NULL,
  `long_term_borrowings` varchar(255) NOT NULL,
  `deffered_tax_liabilities` varchar(255) NOT NULL,
  `other_non_current_liabilities` varchar(255) NOT NULL,
  `total_non_current_liabilities` varchar(255) NOT NULL,
  `short_term_borrowings` varchar(255) NOT NULL,
  `trade_and_other_payables` varchar(255) NOT NULL,
  `current_tax_liability` varchar(255) NOT NULL,
  `other_current_liabilities` varchar(255) NOT NULL,
  `total_current_liabilities` varchar(255) NOT NULL,
  `total_liabilities` varchar(255) NOT NULL,
  `total_equity_and_liabilities` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balance_sheet`
--

INSERT INTO `balance_sheet` (`balance_sheet_id`, `user_id`, `year`, `is_audited`, `property_plant_and_equipment`, `investment_properties`, `intangible_assets`, `other_fixed_assets`, `total_non_current_assets`, `biological_assets`, `inventories`, `trade_and_receivables`, `prepayments`, `bank`, `cash`, `other_current_assets`, `total_current_assets`, `total_assets`, `issue_share_capital`, `share_premium`, `revenue_reserves`, `capital_reserves`, `other_capital_and_revenue_reserves`, `equity_attributed_to_owners_of_parent`, `non_controlling_interest`, `total_equity`, `long_term_borrowings`, `deffered_tax_liabilities`, `other_non_current_liabilities`, `total_non_current_liabilities`, `short_term_borrowings`, `trade_and_other_payables`, `current_tax_liability`, `other_current_liabilities`, `total_current_liabilities`, `total_liabilities`, `total_equity_and_liabilities`) VALUES
(213, 'makazatinashe2000@gmail.com', '2015', '', '34143300   ', '36255860   ', '817074   ', '10040368   ', '81256602', '499429   ', '9111164   ', '8989383   ', '2741275   ', '1154837   ', '1154837   ', '228931   ', '23879856', '105136458', '3571023   ', '2898801   ', '', '26042724   ', '', '32512548', '', '32512548', '', '', '', '0', '', '', '', '', '0', '0', '32512548'),
(214, 'makazatinashe2000@gmail.com', '2016', '', '', '', '20', '   ', '20', '   ', '<br /><b>Notice</b>:  Undefined index: Inventories in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>221</b><br /> ', '   ', '   ', '', ' ', '   ', '0', '20', '   ', '   ', '', '   ', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(215, 'makazatinashe2000@gmail.com', '2018', '', '', '', '', '   ', '0', '   ', '   ', '   ', '   ', '', ' ', '   ', '0', '0', '   ', '   ', '', '   ', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(216, 'makazatinashe2000@gmail.com', '2019', '', '', '', '', '   ', '0', '   ', '   ', '   ', '   ', '', ' ', '   ', '0', '0', '   ', '   ', '', '   ', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(217, 'makazatinashe2000@gmail.com', '2020', '', '', '', '', '   ', '0', '   ', '   ', '   ', '   ', '', ' ', '   ', '0', '0', '   ', '   ', '', '   ', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(218, 'makowe@gmail.com', '2015', '', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>170</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>180</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>189</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>198</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>211</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>220</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>229</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>238</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>248</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>257</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>266</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>281</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>290</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>299</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(219, 'makowe@gmail.com', '2016', '', '', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>181</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>190</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>199</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>212</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>221</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>230</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>239</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>258</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>267</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>282</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>291</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>300</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(220, 'makowe@gmail.com', '2018', '', '', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>183</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>192</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>201</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>214</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>223</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>232</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>241</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>260</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>269</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>284</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>293</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>302</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(221, 'makowe@gmail.com', '2019', '', '', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>184</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>193</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>202</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>215</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>224</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>233</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>242</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>261</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>270</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>285</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>294</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>303</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(222, 'makowe@gmail.com', '2020', '', '', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>185</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>194</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>203</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>216</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>225</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>234</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>243</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>262</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>271</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>286</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>295</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>304</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `business_nature`
--

CREATE TABLE `business_nature` (
  `business_nature_id` int(255) NOT NULL,
  `business_nature_name` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_nature`
--

INSERT INTO `business_nature` (`business_nature_id`, `business_nature_name`, `user_id`) VALUES
(1, 'IT', '1');

-- --------------------------------------------------------

--
-- Table structure for table `business_sector`
--

CREATE TABLE `business_sector` (
  `business_sector_id` int(255) NOT NULL,
  `sector_name` varchar(255) NOT NULL,
  `users_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_sector`
--

INSERT INTO `business_sector` (`business_sector_id`, `sector_name`, `users_id`) VALUES
(1, 'Agriculture', 0),
(2, 'Education', 0),
(3, 'Finance', 0),
(4, 'Business', 0),
(5, 'Mining', 0),
(6, 'Utilities', 0),
(7, 'Retail', 0),
(8, 'Tobacco', 0),
(9, 'Warehousing', 0),
(10, 'Construction', 0),
(11, 'Textile', 0),
(12, 'IT', 0),
(13, 'Transport', 0),
(14, 'Food & Hospitality', 0),
(15, 'Others', 0);

-- --------------------------------------------------------

--
-- Table structure for table `capital_requirements`
--

CREATE TABLE `capital_requirements` (
  `capital_req_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `company_name` varchar(245) DEFAULT NULL,
  `company_registration_number` varchar(245) DEFAULT NULL,
  `date_of_incorporation` varchar(245) DEFAULT NULL,
  `country_of_incorporation` varchar(245) DEFAULT NULL,
  `region_of_incorporation` varchar(245) DEFAULT NULL,
  `type_of_entity` varchar(245) DEFAULT NULL,
  `business_sector` varchar(245) DEFAULT NULL,
  `nature_of_business` varchar(245) DEFAULT NULL,
  `business_nature` varchar(245) DEFAULT NULL,
  `telephone` varchar(245) DEFAULT NULL,
  `fax_number` varchar(245) DEFAULT NULL,
  `registered_office_physical_address` text,
  `postal_address` text,
  `principal_place_of_business` text,
  `raised_equity` varchar(245) DEFAULT NULL,
  `raised_debt` varchar(245) DEFAULT NULL,
  `raised_other` varchar(245) DEFAULT NULL,
  `purpose_of_funds` varchar(245) DEFAULT NULL,
  `app_status` int(255) NOT NULL DEFAULT '1',
  `share_on_offer` varchar(255) NOT NULL,
  `raised_debt_capex` varchar(245) DEFAULT NULL,
  `raised_debt_bd` varchar(245) DEFAULT NULL,
  `raised_debt_gwc` varchar(145) DEFAULT NULL,
  `raised_equity_capex` varchar(145) DEFAULT NULL,
  `raised_equity_bd` varchar(145) DEFAULT NULL,
  `raised_equity_gwc` varchar(145) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `capital_requirements`
--

INSERT INTO `capital_requirements` (`capital_req_id`, `user_id`, `company_name`, `company_registration_number`, `date_of_incorporation`, `country_of_incorporation`, `region_of_incorporation`, `type_of_entity`, `business_sector`, `nature_of_business`, `business_nature`, `telephone`, `fax_number`, `registered_office_physical_address`, `postal_address`, `principal_place_of_business`, `raised_equity`, `raised_debt`, `raised_other`, `purpose_of_funds`, `app_status`, `share_on_offer`, `raised_debt_capex`, `raised_debt_bd`, `raised_debt_gwc`, `raised_equity_capex`, `raised_equity_bd`, `raised_equity_gwc`) VALUES
(5, 'makazatinashe2000@gmail.com', 'Escrow System', '12345678909876', '2018-04-03', 'Zimababwe', 'Harare', 'Corporation', 'Agriculture', 'IT', '', '1234567881', '2221113321', ' Zb Center1', ' Zb Center2', ' Zb Center3', '11111', '65000', '1076', '12331213', 1, '1212', '4', '5', '6', '1', '2', '3'),
(6, 'tatty@gmail', 'delta', '12345', '12 january 2017', 'zimabwe', 'Bulawayo', 'Corporation', 'Agriculture', 'narture', '', '0771168282', '1233', '12 harare zimba', 'eee', 'harare', '1000', '5000', '2000', 'finance', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'anthony@gmail.com', 'escrow', '12345', '2018-04-10', 'zimbabwe', 'Bulawayo', 'Corporation', 'Agriculture', 'partnership', '', '08765566612', '12', 'harare', 'kadoma', 'harare', '20', '20', '20', 'developing', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'sane@gmail.com', 'hghgjgjhg', '345', '2018-04-03', 'zimabwean', 'Bulawayo', 'Corporation', 'Agriculture', 'harare', '', '1234545', '234', 'harare', 'harare', 'harare', '12323', '123123', '12312', 'mjfmfmfm', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'tinashe@ctrade.co.zw', 'Astro Mobile', 'Astro Mobile', '2018-04-12', 'Zimababwe', 'Harare', 'Corporation', 'Agriculture', 'IT', '', '234234', '3424234', 'sdfsdf', 'sdfsdf', 'sdfsdf', '123123', '12123', '123', 'sdfsf', 1, '1234', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'tinashe@ctrade.co.zw', 'makaaaa', '123456', '2018-04-10', 'Zimbabwe', 'Bulawayo', 'Partnership', 'Agriculture', 'sector', '', '1234565', '13444', 'hara', 'gdgd', 'hdhd', '12', '12', '12', 'gdhdhd', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'tariro@gmail.com', 'Tariro', '123456432', '2018-04-12', 'Zimbabwe', 'Bulawayo', 'Corporation', 'Agriculture', 'My Business', '', '0772 876187', '13732', 'My address', 'Myajdshsad', 'My hsghdhg', '378493913', '457362892', '27387890', 'My GF', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'tinashe@escrowgroup.org', 'Tinashe Company', '12234567', '2018-04-11', 'Zimbabwe', 'Mashonaland West', 'Corporation', 'Agriculture', 'Harare', '', '123456', '2345645', 'sdf', 'ffsgd', 'sd', '1321', '213', '123', 'sdf', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'tinashe@finsec.com', 'asdasd', '12231', '2018-04-13', 'zxczxc', 'Bulawayo', 'Corporation', 'Finance', 'asdasd', '', '12321', '2131', 'zcxzc', 'zxczxc', 'zxcxzc', '121312313', '123123', '123123', '1232', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'daddy@gmail.com', 'zimplats', '1234567', '2018-04-03', 'zimbabwe', 'Mashonaland West', 'Corporation', 'Agriculture', 'agric', '', '0771168282', '1334', 'harare', 'chinhoyi', 'harare', '20', '20', '20', 'wesly', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'ronaldinho@gmail.com', 'gaucho', '12344', '2018-04-03', 'zimbabwean', 'Mashonaland West', 'Corporation', 'Agriculture', 'agric', '', '077168282', '12', 'harare', 'harare', 'harare', '12', '12', '12', 'funds', 1, '', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'takawira@gmail.com', 'mulla', '0771168282', '2018-05-18', 'zimbabwe', 'Manicaland', 'Corporation', 'Finance', 'corporate', '', '0771168282', '12345', 'harare', 'mumba harare', 'harare', '420', '20', '20', 'schooling', 1, '20', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'makowe@gmail.com', 'test', '1234', '2018-05-18', 'zimbabwe', 'Masvingo', 'Trust', 'Finance', 'agric', '', '44353535', '12', 'harare', 'harare', 'harare', '20', '20', '20', 'test', 1, '20', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'mimie@gmail.com', 'New', '123456789', '2018-06-19', 'Zimbabwe', 'Bulawayo', 'Association', 'Mining', 'Secot', '', '123456', '123456', 'asdadf', 'asdasd', 'asdasd', '12313', '123123', '123123', 'gasdhjad asdjdhsdfhsdf  sdf sdfjsdfkjsdf', 1, '123123', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'anthony@finsec.co.zw', 'IMB', '123456789', '2018-06-12', 'Zimababwe', 'Harare', 'Corporation', 'Agriculture', 'IT', '', '+263772876187', '772876187', '4th floor construction house', '4th floor construction house', '4th floor construction house', '12324', '1234564', '0', 'Raising money to buy a big plant', 1, '1000000', '188', '1243', '1232', '100', '190', '490'),
(20, 'hudson@escrowgroup.org', 'Mangoma', '12561252', '2018-06-14', 'Zimababwe', 'Bulawayo', 'Corporation', 'Finance', 'IT', '', '12646423', '23562', 'tsydyt', 'gusdfgh', 'gegdf', '12873', '2534', '3456', 'ghhfgfd', 1, '265365', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'timmy@escrowgroup.org', 'Timmy', '12345', '2016-09-03', 'Zimababwe', 'Harare', 'Corporation', 'Tobacco', 'IT', '', '8389293', '2323', '', '', '', '50000', '10000', '0', '', 1, '20', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'karl@escrowgroup.org', 'gyyruyryuffkf', '545468454', '2018-06-11', 'Zimababwe', 'Harare', 'Corporation', 'Agriculture', 'IT', '', '787876876876', '87878787', 'jhkgkkk', 'hjvfjfjftfftfgfgfgfff', 'vjjgjfjvjjjjjjgj', '866868765', '742454346', '4649797979', 'ddfnddfddfdhfdhd', 1, '89978787778', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'karlenko42@gmail.com', 'Great Dzimbabwe Investments', 'H1211137J', '2012-11-05', 'Zimababwe', 'Harare', 'Corporation', 'Tobacco', 'IT', '', '+263716893984', '+263785208160', '56 Manchester rd', '24 Liverpool rd', 'Great Zimbabwe avenue', '3000000', '0', '0', 'Overhaul of factory, Up scaling production of units', 1, '30', '0', '0', '0', '1000000', '1000000', '1000000'),
(24, 'anthony@escrowgroup.org', 'VICTUALS INVESTMENTS (PVT) LTD', '12345', '2014-08-26', 'Zimababwe', 'Harare', 'Corporation', 'Agriculture', 'IT', '', '751559', '751559', 'Shop 1 and 2 Kamfinsa Shopping Centre,Greendale', 'Shop 1 and 2 Kamfinsa Shopping Centre,Greendale', 'Shop 1 and 2 Kamfinsa Shopping Centre,Greendale', '0', '250000', '0', 'Business development and Growth', 1, '0', '220000', '20000', '10000', '0', '0', '0'),
(25, 'tario@gmail.com', 'Microsoft', '132456432', '2018-06-21', 'Zimababwe', 'Harare', 'Corporation', 'Agriculture', 'IT', '', '0773987238', '234567643', 'Zb Center', 'Zb Center', 'Zb Center', '1000', '0', '0', 'New Project', 1, '10', '0', '0', '0', '100', '500', '300');

-- --------------------------------------------------------

--
-- Table structure for table `cash_flow`
--

CREATE TABLE `cash_flow` (
  `cash_flow_id` int(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `is_audited` varchar(255) NOT NULL,
  `profit_loss_before_tax` varchar(255) NOT NULL,
  `adjustments_to_reconcile_cash_before_tax_to_net_cashflows` varchar(255) NOT NULL,
  `net_cash_flow_from_operating_activities` varchar(255) NOT NULL,
  `netcash_used_in_investing_activities` varchar(255) NOT NULL,
  `netcash_used_in_financing_activities` varchar(255) NOT NULL,
  `cash_and_cash_equivalents_at_the_beginning_of_the_year` varchar(255) NOT NULL,
  `cash_flow_LossOperatingProfit` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_flow`
--

INSERT INTO `cash_flow` (`cash_flow_id`, `user_id`, `year`, `is_audited`, `profit_loss_before_tax`, `adjustments_to_reconcile_cash_before_tax_to_net_cashflows`, `net_cash_flow_from_operating_activities`, `netcash_used_in_investing_activities`, `netcash_used_in_financing_activities`, `cash_and_cash_equivalents_at_the_beginning_of_the_year`, `cash_flow_LossOperatingProfit`) VALUES
(1, 'tinashe@ctrade.co.zw', '2018', 'T', '', '', '', '', '', '', '0'),
(2, 'tinashe@ctrade.co.zw', '2019', 'T', '', '', '', '', '', '', '0'),
(17, 'ronaldinho@gmail.com', '2015', '', '', '', '', '', '', '', '0'),
(18, 'ronaldinho@gmail.com', '2016', '', '', '', '', '', '', '', '0'),
(19, 'ronaldinho@gmail.com', '2017', '', '', '', '', '', '', '', '0'),
(20, 'ronaldinho@gmail.com', '2018', '', '', '', '', '', '', '', '0'),
(133, 'makazatinashe2000@gmail.com', '2015', '', '', '', '0', '', '', '', '0'),
(134, 'makazatinashe2000@gmail.com', '2016', '', '', '', '0', '', '', '', '0'),
(135, 'makazatinashe2000@gmail.com', '2017', '', '', '', '0', '', '', '', '0'),
(136, 'makazatinashe2000@gmail.com', '2018', '', '', '', '0', '', '', '', '0'),
(137, 'makazatinashe2000@gmail.com', '2019', '', '', '', '0', '', '', '', '0'),
(138, 'makazatinashe2000@gmail.com', '2020', '', '', '', '0', '', '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `committees`
--

CREATE TABLE `committees` (
  `committees_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `committee_name` varchar(245) DEFAULT NULL,
  `chairman` varchar(245) DEFAULT NULL,
  `attendees` varchar(245) DEFAULT NULL,
  `quoraum` varchar(245) DEFAULT NULL,
  `meeting_frequency` varchar(245) DEFAULT NULL,
  `committee_responsibilities` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `committees`
--

INSERT INTO `committees` (`committees_id`, `user_id`, `committee_name`, `chairman`, `attendees`, `quoraum`, `meeting_frequency`, `committee_responsibilities`) VALUES
(1, '', '<>', 'efeff', 'dq', '', 'dddd', 'dddd'),
(2, '', '<>', 'fff', 'ffff', '', 'ffff', 'ffff'),
(6, 'makazatinashe2000@gmail.com', 'escrow', 'simba', 'developers', 'wena', '2', 'development'),
(7, 'anthony@gmail.com', 'fddgd', 'retete', '1323', 'hfjfjf', '12', 'gfhfhfjf'),
(8, 'tinashe@ctrade.co.zw', 'sadasd', 'asdasd', 'asdasd', 'asdas', 'asdasd', 'asdasd'),
(9, 'ronaldinho@gmail.com', 'zidane', 'zidane', 'wes', 'quora', '12', 'mumba'),
(10, 'makowe@gmail.com', 'test', 'test', 'test', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview`
--

CREATE TABLE `company_overview` (
  `company_overview_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_history` varchar(245) DEFAULT NULL,
  `company_overview_rights` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_overview`
--

INSERT INTO `company_overview` (`company_overview_id`, `user_id`, `company_overview_history`, `company_overview_rights`) VALUES
(2, 'makazatinashe2000@gmail.com', 'fghghjhggh', 'upload/AngularJSNotesForProfessionals.pdf'),
(3, 'tatty@gmail', '', 'upload/ch28.pdf'),
(4, 'anthony@gmail.com', '', 'upload/Android.pdf'),
(5, 'sane@gmail.com', '', 'upload/'),
(6, 'tinashe@ctrade.co.zw', 'sdfsdfsdfdsfsdfsf', 'upload/tinashe.xlsx'),
(7, 'tariro@gmail.com', 'sdfsdfsf', 'upload/FViewer - code.txt'),
(8, 'tinashe@escrowgroup.org', 'asdasd', 'upload/marketcommentary07032018.doc'),
(9, 'tinashe@finsec.com', '', 'upload/'),
(10, 'daddy@gmail.com', '', 'upload/'),
(11, 'ronaldinho@gmail.com', '', 'upload/'),
(12, 'takawira@gmail.com', '', 'upload/'),
(13, 'makowe@gmail.com', '', 'upload/'),
(14, 'anthony@finsec.co.zw', 'asdasd', 'upload/domain-key.txt'),
(15, 'hudson@escrowgroup.org', '', 'upload/'),
(16, 'timmy@escrowgroup.org', '', 'upload/'),
(17, 'anthony@escrowgroup.org', 'This text', 'upload/'),
(18, 'tario@gmail.com', 'Introduction, History and Major Milestones:', 'upload/');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview_customers`
--

CREATE TABLE `company_overview_customers` (
  `company_overview_customers_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_id` varchar(245) DEFAULT NULL,
  `customer_name` varchar(245) DEFAULT NULL,
  `customer_location` varchar(245) DEFAULT NULL,
  `customer_years_rel` varchar(245) DEFAULT NULL,
  `customer_sales` varchar(245) DEFAULT NULL,
  `customer_trade_terms` varchar(245) DEFAULT NULL,
  `customer_distr_channel` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_overview_customers`
--

INSERT INTO `company_overview_customers` (`company_overview_customers_id`, `user_id`, `company_overview_id`, `customer_name`, `customer_location`, `customer_years_rel`, `customer_sales`, `customer_trade_terms`, `customer_distr_channel`) VALUES
(4, 'makazatinashe2000@gmail.com', '', 'ddddfd', 'dfd', 'dfdfdf', 'dfef5', 'ff', 'fffgf1'),
(5, 'makazatinashe2000@gmail.com', '', 'ffffff', 'cdvvdvd', 'dvdv', 'dvdv', 'ddv', 'vdvtbvttfrgrtfr'),
(6, 'tatty@gmail', '', 'nvcncmcmc', 'mccmcmc', 'mnekdoedkd', 'mnvmcmc', 'mcmcmc,c,c', 'mcmcmcmc'),
(7, 'anthony@gmail.com', '', 'jfjfjfjf', 'xnxnxnxnx', '5464764', 'bcncncn', 'ncncmc', 'ncmncmc'),
(8, 'tinashe@ctrade.co.zw', '', 'dsfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview_products_services`
--

CREATE TABLE `company_overview_products_services` (
  `company_overview_products_services_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_id` varchar(245) DEFAULT NULL,
  `product_service_name` varchar(245) DEFAULT NULL,
  `description_of_product` varchar(245) DEFAULT NULL,
  `sales_volume` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_overview_products_services`
--

INSERT INTO `company_overview_products_services` (`company_overview_products_services_id`, `user_id`, `company_overview_id`, `product_service_name`, `description_of_product`, `sales_volume`) VALUES
(1, 'makazatinashe2000@gmail.com', '', 'product one', 'desc product', '10000'),
(5, 'makazatinashe2000@gmail.com', '', 'tapiwa chivaura', 'shares', 'seee'),
(6, 'makazatinashe2000@gmail.com', '', 'tanka', 'muza', '1234'),
(7, 'makazatinashe2000@gmail.com', '', 'Last Product', 'Desc Last', '40000'),
(8, 'tatty@gmail', '', 'ddd', 'ddd', 'ddd'),
(9, 'anthony@gmail.com', '', 'vfdgdgd', 'bddndnd', 'bndndndnddvfrfr'),
(10, 'tinashe@ctrade.co.zw', '', 'dffgs', 'dsfs', 'sdfsfd');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview_raw_materials`
--

CREATE TABLE `company_overview_raw_materials` (
  `company_overview_raw_materials_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_id` varchar(245) DEFAULT NULL,
  `name` varchar(245) DEFAULT NULL,
  `desc` varchar(245) DEFAULT NULL,
  `volume` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_overview_raw_materials`
--

INSERT INTO `company_overview_raw_materials` (`company_overview_raw_materials_id`, `user_id`, `company_overview_id`, `name`, `desc`, `volume`) VALUES
(1, 'makazatinashe2000@gmail.com', '', 'nnnnnnnnnnnnnnnnnnnnn', 'asd', 'asd'),
(2, 'makazatinashe2000@gmail.com', '', 'mmmmmmmmmmmmm', 'asd', 'asd'),
(3, '', '', 'hdjdjdjd', '546464t', 'bfhfhf'),
(4, '', '', 'dedd', 'ddeefe', 'cecweeeeeeeee232'),
(5, '', '', 'mulla', 'mubha', '1232dff'),
(10, 'makazatinashe2000@gmail.com', '', 'name', 'desc', 'volume'),
(11, 'makazatinashe2000@gmail.com', '', 'name', 'desc', 'volume'),
(12, 'tatty@gmail', '', 'ddmdmd', 'mdmdmdmd', ' dddmdmd'),
(13, 'anthony@gmail.com', '', 'fmfjfkjfkf', 'mfmvmvmvmv', 'vmvmjvmv'),
(14, 'tinashe@ctrade.co.zw', '', 'sdfsdf', 'sfsdf', 'sdfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview_suppliers`
--

CREATE TABLE `company_overview_suppliers` (
  `company_overview_suppliers_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_id` varchar(245) DEFAULT NULL,
  `customer_name` varchar(245) DEFAULT NULL,
  `customer_location` varchar(245) DEFAULT NULL,
  `customer_years_rel` varchar(245) DEFAULT NULL,
  `customer_sales` varchar(245) DEFAULT NULL,
  `customer_trade_terms` varchar(245) DEFAULT NULL,
  `customer_distr_channel` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_overview_suppliers`
--

INSERT INTO `company_overview_suppliers` (`company_overview_suppliers_id`, `user_id`, `company_overview_id`, `customer_name`, `customer_location`, `customer_years_rel`, `customer_sales`, `customer_trade_terms`, `customer_distr_channel`) VALUES
(1, 'makazatinashe2000@gmail.com', '', 'sdad123456', 'asd', 'asd', 'asd', 'asd', 'asd1'),
(2, 'makazatinashe2000@gmail.com', '1', 'asd', 'ads', 'asd', 'dsa', 'asd', 'asd'),
(3, 'makazatinashe2000@gmail.com', '', 'mumo', 'mudjdjd', 'nncncnfhfjfjf', '12332', 'ncncnc', 'nvnvnvnv'),
(4, 'anthony@gmail.com', '', 'bcbcbc', 'c  cmn m m', 'sgsgsgs', 'bbcbc', 'b cxbcbcbc', 'feteyeye'),
(5, 'tinashe@ctrade.co.zw', '', 'sdfsdf', 'sdfsdf', 'sfsf', 'sdfsdf', 'sdfsdf', 'sdfsdff'),
(6, 'makazatinashe2000@gmail.com', '', 'dvfsdf', 'sdfsdfsf', 'sdfsdf', 'sdfdsf', 'sdfsdf', 'sdff');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_directory`
--

CREATE TABLE `corporate_directory` (
  `corporate_directory_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `duration_in_stated_business_line` varchar(245) DEFAULT NULL,
  `customer_basecorporates` varchar(245) DEFAULT NULL,
  `customer_baseindividuals` varchar(245) DEFAULT NULL,
  `business_year_end` varchar(245) DEFAULT NULL,
  `business_premises_are` varchar(245) DEFAULT NULL,
  `name_of_holding_company` varchar(245) DEFAULT NULL,
  `contact_person_name` varchar(245) DEFAULT NULL,
  `contact_person_position` varchar(245) DEFAULT NULL,
  `contact_person_cell` varchar(245) DEFAULT NULL,
  `contact_person_telephone` varchar(245) DEFAULT NULL,
  `contact_person_email` varchar(245) DEFAULT NULL,
  `company_secretary_address` text,
  `attorney_office_address` text,
  `accountant_office_address` text,
  `auditors_office_address` text,
  `principal_bankers_address` text,
  `authorised_share_capital` varchar(245) DEFAULT NULL,
  `issued_share_capital` varchar(245) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `nominated_supervisor_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `corporate_directory`
--

INSERT INTO `corporate_directory` (`corporate_directory_id`, `user_id`, `duration_in_stated_business_line`, `customer_basecorporates`, `customer_baseindividuals`, `business_year_end`, `business_premises_are`, `name_of_holding_company`, `contact_person_name`, `contact_person_position`, `contact_person_cell`, `contact_person_telephone`, `contact_person_email`, `company_secretary_address`, `attorney_office_address`, `accountant_office_address`, `auditors_office_address`, `principal_bankers_address`, `authorised_share_capital`, `issued_share_capital`, `status`, `nominated_supervisor_id`) VALUES
(1, 'makazatinashe2000@gmail.com', '123', '213123', '12312', '123123', 'Self owned', '1231', 'Tinashe Makaza', '123', '0772876187', '0772876187', 'makazatinashe2000@gmail.com', ' ZB Center          ', '            ZB Center', ' ZB Center', ' ZB Center', ' ZB Center', '123', '1212', 'pending', 'dereck@gmail'),
(2, 'tatty@gmail', '12months', '12', 'moyo', '2019', 'Self owned', 'coper', 'tatemda chivaura', 'director', '077846476474', '1344', 'tatty@gmail', ' harareff', ' ffff', 'fff', 'fff', 'fff', '12', '12', '', ''),
(3, 'anthony@gmail.com', 'gfhfhfhf', 'jgngvnv', 'hfhfhfhjfjf', '2015', 'Self owned', 'hafsffd', 'anthony shumba', 'fsgsgs', '1323333', '23434444', 'anthony@gmail.com', ' andgdhdhd', ' fbhchcchc', 'hhfhfhfhf', 'bhfhfhf', 'cbcbfhbfhf', 'bhfhfhf', 'gfhfhfhfhf', '', ''),
(4, 'sane@gmail.com', 'kfkfkf', 'mvmvmvmv', ' vvvmv', '1234', 'Self owned', 'nfnfnf', 'sane ', 'jgjgjgj', 'jgjgjgjg', 'nmmgmg', 'sane@gmail.com', ' fjfjf  ', '   jfjfjfjf', 'mfmfmf', 'fjfjfj', 'mffkfkf', 'jgjgjg', 'jgjgjgjg', '', ''),
(5, 'tinashe@ctrade.co.zw', 'sdasd', 'asdasd', 'asdasd', 'asdasd', 'Self owned', 'asdasd', 'Tinashe Makazaasdasd', 'asdasd', 'asddasd', 'assdasd', 'tinashe@ctrade.co.zw', ' assdasd     ', '      asd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '', ''),
(6, 'tariro@gmail.com', '123123', 'dffsd', 'sdfsdf', 'dsfsdf', 'Self owned', 'dfsf', 'Tariro Makaza', 'sdf', 'dfsfd', 'sfdsf', 'tariro@gmail.com', ' sdf', ' dsf', 'sdf', 'sf', 'sdf', 'sdfsf', 'sdfsf', '', ''),
(7, 'tinashe@escrowgroup.org', 'sdffsdf', 'sdf', 'sdf', 'sdf', 'Self owned', 'sdf', 'Tinashe Test', 'dsf', 'dsdsf', '1233312', 'tinashe@escrowgroup.org', ' sdfsdf', ' sfdsdf', 'sdfsdf', 'sdfs', 'sdfsdf', 'sdfsdf', 'sdfsdf', '', ''),
(8, 'tinashe@finsec.com', 'asdasd', 'asdasd', 'asdasd', 'asdads', 'Self owned', 'asdasd', 'Tinashe Makaza', 'asdad', 'asdasd', 'asda', 'tinashe@finsec.com', ' asd', ' asd', 'asd', 'asd', 'asd', 'asd', 'asd', '', ''),
(9, 'daddy@gmail.com', '3 years', 'test', 'test', '2019', 'Mortgaged', '12', 'daddy daddy', 'staff', '0771168282', '858589595', 'daddy@gmail.com', 'test', ' test', 'test', 'test', 'test', 'test', 'test', '', ''),
(10, 'ronaldinho@gmail.com', '2years', '12345', 'wes', '2018', 'Self owned', 'escrow', 'ronaldinho gaucho', 'senior developer', '0771168282', '0771168282', 'ronaldinho@gmail.com', ' harare    ', '     harare', 'harare', 'harare', 'harare', 'isssue', 'gaucho', '', ''),
(11, 'takawira@gmail.com', '2 years', 'business', 'mumba', '2019', 'Mortgaged', 'mumba', 'takawira takawira', 'mumba', '0771168282', '123344', 'takawira@gmail.com', ' harare', ' harare', 'harare', 'mubhwa', 'harare', '12', '24', '', ''),
(12, 'makowe@gmail.com', '3 years', 'test', 'test', '2019', 'Mortgaged', 'weny', 'makowe', 'muma', '0771168282', '1234', 'makowe@gmail.com', 'harare ', ' harare', 'harare', 'harare', 'wenny', '12', '12', '', ''),
(13, 'anthony@finsec.co.zw', '12', '12', '123', '2086', 'Self owned', '123', 'Anthony Shumba', 'IT', '772876187', '772876187', 'makazatinashe2000@gmail.com', '4th floor construction house  ', '  4th floor construction house', '4th floor construction house', '4th floor construction house', '4th floor construction house', '1213313', '123123', '', ''),
(14, 'hudson@escrowgroup.org', '256356', '76632556', '65655', '56556', 'Self owned', '5656', 'Hudson Nyamariva', 'IT', '3245678', '12345678', 'hudson@escrowgroup.org', 'Zb Center ', '  Zb Center', 'Zb Center', 'Zb Center', 'Zb Center', '2563562', '25345623', '', ''),
(15, 'timmy@escrowgroup.org', '2', '32', '23', '2018', 'Rented', 'None', 'Timmy', 'CEO', '0678018-', '89808080', 'timmy@escrowgroup.org', ' None', ' N', 'N', 'N', 'N', '2800', '2899', '', ''),
(16, 'karl@escrowgroup.org', '3', '20', '25', 'march', 'Self owned', 'escrow', 'Dereck', 'ceo', '846868788', '57755555', 'karl@escrowgroup.org', ' fftfyfkyufy', ' hgfygy', 'uhuhhuguu', 'ggggiy', 'gcgcghcghchg', '200024', '10000000', '', ''),
(17, 'karlenko42@gmail.com', '5', '10', '2', 'March ', 'Self owned', 'None', 'Yuan Pound', 'C.E.O', '+263716893984', '+263785208160', 'karlenko42@gmail.com', '34 Reddington rd\r\nWind Park\r\nHarare', ' 45 Hulffing Dawn rd\r\nHog Park \r\nHarare', '76 Ferrec\r\nJenning Park\r\nHarare', '7th floor Franklin Building\r\nCnr 1st and Kwame Nkurumah\r\nHarare\r\n', '7th floor Franklin Building\r\nCnr 1st and Kwame Nkurumah\r\nHarare', '10000000', '4000000', '', ''),
(18, 'anthony@escrowgroup.org', '3 years', 'corporates', '3', '2018', 'Self owned', 'Victuals (PVT) LTD', 'Rob Spencer', 'Managing Director', '0785006225', '751559', 'anthony@escrowgroup.org', ' Shop 2 and 4 Kamfinsa Shopping Centre,Greendale  ', '   Shop 5 and 6 Kamfinsa Shopping Centre,Greendale', 'Shop 7 and 9 Kamfinsa Shopping Centre,Greendale', 'Shop 10 and 11 Kamfinsa Shopping Centre,Greendale', 'Shop 1 and 2 Kamfinsa Shopping Centre,Greendale', '1000', '100', '', ''),
(19, 'tario@gmail.com', '2000', '200', '100', '2017', 'Self owned', 'Micorsoft', 'Tario Mazhange', 'IT', '077939494', '09876422', 'tario@gmail.com', ' Zb Center', ' Zb Center', 'Zb Center', 'Zb Center', 'Zb Center', '800000', '10000', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_structure`
--

CREATE TABLE `corporate_structure` (
  `corporate_structure_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `desc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `corporate_structure`
--

INSERT INTO `corporate_structure` (`corporate_structure_id`, `user_id`, `desc`) VALUES
(1, 'makazatinashe2000@gmail.com', '<p><span style=\"background-color: rgb(250, 250, 250);\"><span style=\"font-size: 14px;\">Details of </span><span style=\"font-style: italic; font-size: 14px;\">ownership </span><span style=\"font-size: 14px;\">structure</span></span></p><ul><li><span style=\"background-color: rgb(250, 250, 250);\"><span style=\"font-size: 14px;\"> where company</span></span></li></ul><table class=\"table table-bordered\"><tbody><tr><td>asd</td><td>asd</td><td>sad</td><td>asd</td><td>asd</td></tr><tr><td>asd</td><td>ads</td><td>asd</td><td>das</td><td>asd</td></tr></tbody></table><ul><li><span style=\"background-color: rgb(250, 250, 250);\"><span style=\"font-size: 14px;\"><br> is a subsidiary </span></span></li><li><span style=\"background-color: rgb(250, 250, 250);\"><span style=\"font-size: 14px;\">or part of other </span></span></li><li><span style=\"background-color: rgb(250, 250, 250);\"><span style=\"font-size: 14px;\">companies/corporate </span><span style=\"font-weight: bold; font-size: 14px;\">bodies</span></span></li></ul><p><span style=\"background-color: rgb(250, 250, 250);\"><span style=\"font-weight: bold; font-size: 14px;\"><br></span></span></p>'),
(2, 'tatty@gmail', 'ssss'),
(3, 'anthony@gmail.com', 'asnthkvckckvtrgtt'),
(4, 'sane@gmail.com', 'nfnfnfnf'),
(5, 'tinashe@ctrade.co.zw', 'adasdasd'),
(6, 'tariro@gmail.com', ''),
(7, 'tinashe@escrowgroup.org', ''),
(8, 'daddy@gmail.com', 'test'),
(9, 'ronaldinho@gmail.com', 'mulla'),
(10, 'makowe@gmail.com', ''),
(11, 'anthony@finsec.co.zw', '<p>gefggsd</p>'),
(12, 'timmy@escrowgroup.org', '<p><br></p>'),
(13, 'tario@gmail.com', '<p>sdfadsdafvsdf&nbsp;</p>'),
(14, 'anthony@escrowgroup.org', '<p><br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `director_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `title` varchar(245) DEFAULT NULL,
  `forename` varchar(245) DEFAULT NULL,
  `surname` varchar(245) DEFAULT NULL,
  `age` varchar(245) DEFAULT NULL,
  `citizenship` varchar(245) DEFAULT NULL,
  `address` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`director_id`, `user_id`, `title`, `forename`, `surname`, `age`, `citizenship`, `address`) VALUES
(6, '', '', '', '', '', '', ''),
(7, '', '', '', '', '', '', ''),
(8, '', '', '', '', '', '', ''),
(9, '', '', '', '', '', '', ''),
(10, '', '', '', '', '', '', ''),
(12, 'makazatinashe2000@gmail.com', 'mr', 'tinashe', 'chivaura', '23', 'Zimbabwean', 'harare'),
(13, 'anthony@gmail.com', 'mr', 'fmfmfmf', 'nfnfff', '256', 'Zimbabwean', 'harare'),
(14, 'tinashe@ctrade.co.zw', 'Mrs', 'asdas', 'asdasd', 'asdad', 'asdasd', 'asdasd'),
(15, 'tinashe@ctrade.co.zw', 'asdasd', 'sadasd', 'asd', 'asd', 'asda', 'assd'),
(16, 'daddy@gmail.com', 'mr', 'mbano', 'mbano', '23', 'Zimbabwean', '12 chiedza'),
(17, 'ronaldinho@gmail.com', 'Mr', 'mwana', 'mwana', '34', 'Zimbabwean', 'harare'),
(18, 'makazatinashe2000@gmail.com', 'mr', 'mulla', 'mulla', '25', 'mulla', 'mulaa'),
(19, 'ronaldinho@gmail.com', 'mr', 'alaba', 'alaba', '67', 'Zimbabwean', '12 chiedza'),
(20, 'makowe@gmail.com', 'mr', 'takudzwa', 'makurusi', '30', 'Zimbabwean', 'harare'),
(22, 'anthony@finsec.co.zw', 'wewer', 'werwer', 'werwer', 'wer', 'wer', 'wer'),
(23, 'anthony@escrowgroup.org', 'Mr', 'Robert Que', 'Spencer', '58', 'American', 'No 4 Cambridge Drive Greendale Harare'),
(24, 'tario@gmail.com', 'Mr', 'Tinashe', 'Makaza', '12', 'Zimbabwe', '4th floor construction house'),
(25, 'anthony@escrowgroup.org', 'Mr', 'Dennis ', 'Wallah', '42', 'Zimbabwean', 'No 4 Lemington Road Greystone Harare'),
(26, 'anthony@escrowgroup.org', 'Mr ', 'Dean Nield', 'Geranios', '38', 'Zimbabwean', '34 North Rd,Greendale Harare');

-- --------------------------------------------------------

--
-- Table structure for table `directors_shareholding`
--

CREATE TABLE `directors_shareholding` (
  `directors_shareholding_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `directors_name` varchar(300) DEFAULT NULL,
  `direct_indirect_equity_interest` varchar(245) DEFAULT NULL,
  `shareholding` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directors_shareholding`
--

INSERT INTO `directors_shareholding` (`directors_shareholding_id`, `user_id`, `directors_name`, `direct_indirect_equity_interest`, `shareholding`) VALUES
(1, 'makazatinashe2000@gmail.com', 'Tinashe', '12', '34'),
(3, '', '', 'gaza', '<>gaza'),
(4, 'makazatinashe2000@gmail.com', 'tamika', '12', '10'),
(5, 'anthony@gmail.com', 'bdhdhdhdhd', '12', '35'),
(6, 'tinashe@ctrade.co.zw', 'sddf', 'fdsdf', 'sfdsdf'),
(7, 'daddy@gmail.com', 'tawanda', '20', '23'),
(8, 'ronaldinho@gmail.com', 'kaka', '20', '20'),
(10, 'anthony@finsec.co.zw', 'asdasd', '123', '12123'),
(11, 'tario@gmail.com', 'asdasd', '12324', '123243'),
(12, 'anthony@escrowgroup.org', 'Robert Spencer', '40', '40'),
(13, 'anthony@escrowgroup.org', 'Dennis Wallah', '30', '30'),
(14, 'anthony@escrowgroup.org', 'Dean Nield Geranios', '30', '30');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(245) DEFAULT NULL,
  `designation` varchar(245) DEFAULT NULL,
  `age` varchar(245) DEFAULT NULL,
  `gender` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `designation`, `age`, `gender`) VALUES
(1, 'Tinashe Makaza', 'IT Manager (Me)', '20', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `enterprise_size_categorisation`
--

CREATE TABLE `enterprise_size_categorisation` (
  `enterprise_size_categorisation_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `staff_levels` varchar(245) DEFAULT NULL,
  `annual_turnover` varchar(245) DEFAULT NULL,
  `gross_value_of_assets` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enterprise_size_categorisation`
--

INSERT INTO `enterprise_size_categorisation` (`enterprise_size_categorisation_id`, `user_id`, `staff_levels`, `annual_turnover`, `gross_value_of_assets`) VALUES
(1, 'makazatinashe2000@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(2, 'tatty@gmail', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(3, 'anthony@gmail.com', '6 to 40 employees', '$50,001 to $500,000', 'Up to $50,000'),
(4, 'sane@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(5, 'tinashe@ctrade.co.zw', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(6, 'tariro@gmail.com', '6 to 40 employees', '$50,001 to $500,000', '$2,000,001 and above'),
(7, 'tinashe@escrowgroup.org', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(8, 'tinashe@finsec.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(9, 'daddy@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(10, 'ronaldinho@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(11, 'takawira@gmail.com', '6 to 40 employees', '$50,001 to $500,000', '$2,000,001 and above'),
(12, 'makowe@gmail.com', '6 to 40 employees', '$50,001 to $500,000', '$50,001 to $1,000,000'),
(13, 'anthony@finsec.co.zw', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(14, 'hudson@escrowgroup.org', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(15, 'timmy@escrowgroup.org', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(16, 'karl@escrowgroup.org', '41 to 75 employees', '$50,001 to $500,000', '$50,001 to $1,000,000'),
(17, 'anthony@escrowgroup.org', '6 to 40 employees', '$50,001 to $500,000', 'Up to $50,000'),
(18, 'tario@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000');

-- --------------------------------------------------------

--
-- Table structure for table `fin_balance_sheet`
--

CREATE TABLE `fin_balance_sheet` (
  `fin_balance_sheet_id` int(11) NOT NULL,
  `non_current_assests_Propertyplantandequipment` varchar(255) NOT NULL,
  `non_current_assests_Investmentproperties` varchar(255) NOT NULL,
  `non_current_assests_Intangibleassets` varchar(255) NOT NULL,
  `non_current_assests_Other` varchar(255) NOT NULL,
  `current_assets_Biologicalassets` varchar(255) NOT NULL,
  `current_assets_Inventories` varchar(255) NOT NULL,
  `current_assets_Tradeotherreceivables` varchar(255) NOT NULL,
  `current_assets_Prepayments` varchar(255) NOT NULL,
  `current_assets_Bank` varchar(255) NOT NULL,
  `current_assets_Cash` varchar(255) NOT NULL,
  `current_assets_Other` varchar(255) NOT NULL,
  `equity_attributed_Issuedsharecapital` varchar(255) NOT NULL,
  `equity_attributed_Sharepremium` varchar(255) NOT NULL,
  `equity_attributed_Revenuereserves` varchar(255) NOT NULL,
  `equity_attributed_Capitalreserves` varchar(255) NOT NULL,
  `equity_attributed_Other` varchar(255) NOT NULL,
  `non_controlling_interest` varchar(255) NOT NULL,
  `non_current_liabilities_Longtermborrowings` varchar(255) NOT NULL,
  `non_current_liabilities_Defferredtaxliabilities` varchar(255) NOT NULL,
  `non_current_liabilities_Other` varchar(255) NOT NULL,
  `current_liabilities_Shorttermborrowings` varchar(255) NOT NULL,
  `current_liabilities_Tradeotherpayables` varchar(255) NOT NULL,
  `current_liabilities_Currenttaxliability` varchar(255) NOT NULL,
  `current_liabilities_Other` varchar(255) NOT NULL COMMENT '\n\n',
  `user_id` varchar(145) DEFAULT NULL,
  `year` varchar(145) DEFAULT NULL,
  `is_audited` varchar(245) DEFAULT NULL COMMENT '\n',
  `OtherTotal_Ordinary_shares` varchar(245) DEFAULT NULL,
  `OtherMarket_price_per_share` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fin_balance_sheet`
--

INSERT INTO `fin_balance_sheet` (`fin_balance_sheet_id`, `non_current_assests_Propertyplantandequipment`, `non_current_assests_Investmentproperties`, `non_current_assests_Intangibleassets`, `non_current_assests_Other`, `current_assets_Biologicalassets`, `current_assets_Inventories`, `current_assets_Tradeotherreceivables`, `current_assets_Prepayments`, `current_assets_Bank`, `current_assets_Cash`, `current_assets_Other`, `equity_attributed_Issuedsharecapital`, `equity_attributed_Sharepremium`, `equity_attributed_Revenuereserves`, `equity_attributed_Capitalreserves`, `equity_attributed_Other`, `non_controlling_interest`, `non_current_liabilities_Longtermborrowings`, `non_current_liabilities_Defferredtaxliabilities`, `non_current_liabilities_Other`, `current_liabilities_Shorttermborrowings`, `current_liabilities_Tradeotherpayables`, `current_liabilities_Currenttaxliability`, `current_liabilities_Other`, `user_id`, `year`, `is_audited`, `OtherTotal_Ordinary_shares`, `OtherMarket_price_per_share`) VALUES
(1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '100', 'makazatinashe2000@gmail.com', '2015', '0', '1', '2'),
(2, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'makazatinashe2000@gmail.com', '2016', '0', '0', '3'),
(3, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'makazatinashe2000@gmail.com', '2017', '0', '0', '0'),
(4, '0', '0', '0', '0', '354', '0', '0', '0', '0', '0', '100', '0', '0', '0', '0', '18545', '0', '0', '0', '0', '0', '0', '0', '0', 'makazatinashe2000@gmail.com', '2018', '0', '0', '0'),
(5, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'makazatinashe2000@gmail.com', '2019', '0', '0', '0'),
(6, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'makazatinashe2000@gmail.com', '2020', '0', '0', '0'),
(7, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'makazatinashe2000@gmail.com', '2021', '0', '0', '0'),
(8, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'makazatinashe2000@gmail.com', '2022', '0', '0', '0'),
(9, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'anthony@finsec.co.zw', '2015', '0', '0', '0'),
(10, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'anthony@finsec.co.zw', '2016', '0', '0', '0'),
(11, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '100', '0', 'anthony@finsec.co.zw', '2017', '0', '0', '0'),
(12, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'anthony@finsec.co.zw', '2018', '0', '0', '0'),
(13, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'anthony@finsec.co.zw', '2019', '0', '0', '0'),
(14, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'anthony@finsec.co.zw', '2020', '0', '0', '0'),
(15, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'anthony@finsec.co.zw', '2021', '0', '0', '0'),
(16, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'anthony@finsec.co.zw', '2022', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `fin_cash_flow`
--

CREATE TABLE `fin_cash_flow` (
  `fin_cash_flow_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `year` varchar(245) DEFAULT NULL,
  `is_audited` varchar(255) NOT NULL,
  `cash_flow_LossProfitbeforetax` varchar(255) NOT NULL,
  `cash_flow_Adjustments` varchar(255) NOT NULL,
  `cash_flow_NetCashInvestingactivities` varchar(255) NOT NULL,
  `cash_flow_Netfinancingactivities` varchar(255) NOT NULL,
  `cash_flow_Cashequivalents` varchar(255) NOT NULL,
  `cash_flow_LossOperatingProfit` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fin_cash_flow`
--

INSERT INTO `fin_cash_flow` (`fin_cash_flow_id`, `user_id`, `year`, `is_audited`, `cash_flow_LossProfitbeforetax`, `cash_flow_Adjustments`, `cash_flow_NetCashInvestingactivities`, `cash_flow_Netfinancingactivities`, `cash_flow_Cashequivalents`, `cash_flow_LossOperatingProfit`) VALUES
(1, 'makazatinashe2000@gmail.com', '2015', '1', '123', '1', '1', '1', '1', '0'),
(2, 'makazatinashe2000@gmail.com', '2016', '1', '2', '2', '2', '2', '2', '0'),
(3, 'makazatinashe2000@gmail.com', '2017', '1', '3', '3', '3', '3', '3', '0'),
(4, 'makazatinashe2000@gmail.com', '2018', '1', '4', '4', '4', '4', '4', '1'),
(5, 'makazatinashe2000@gmail.com', '2019', '1', '5', '23.6137', '5', '5', '5', '2'),
(6, 'makazatinashe2000@gmail.com', '2020', '1', '6', '123123', '123123', '1223', '234234', '3'),
(7, 'makazatinashe2000@gmail.com', '2021', '1', '7', '123123', '123123', '24243', '234234', '4'),
(8, 'makazatinashe2000@gmail.com', '2022', '1', '8', '123123', '123123', '24243', '123454', '5'),
(9, 'anthony@finsec.co.zw', '2015', '0', '0', '0', '0', '0', '0', '0'),
(10, 'anthony@finsec.co.zw', '2016', '0', '0', '0', '0', '0', '0', '0'),
(11, 'anthony@finsec.co.zw', '2017', '0', '0', '0', '0', '0', '0', '0'),
(12, 'anthony@finsec.co.zw', '2018', '0', '0', '0', '0', '0', '0', '0'),
(13, 'anthony@finsec.co.zw', '2019', '0', '0', '0', '0', '0', '0', '0'),
(14, 'anthony@finsec.co.zw', '2020', '0', '0', '0', '0', '0', '0', '0'),
(15, 'anthony@finsec.co.zw', '2021', '0', '0', '0', '0', '0', '0', '0'),
(16, 'anthony@finsec.co.zw', '2022', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `fin_profit_loss`
--

CREATE TABLE `fin_profit_loss` (
  `fin_profit_loss_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `year` varchar(245) DEFAULT NULL,
  `is_audited` varchar(255) NOT NULL,
  `profit_loss_Saleofgoods` varchar(255) NOT NULL,
  `profit_loss_Renderingofservices` varchar(255) NOT NULL,
  `profit_loss_OtherRevenues` varchar(255) NOT NULL,
  `profit_loss_CostofSales` varchar(255) NOT NULL,
  `profit_loss_Otheroperatingincome` varchar(255) NOT NULL,
  `profit_loss_Otheroperatingexpenses` varchar(255) NOT NULL,
  `profit_loss_Staffcosts` varchar(255) NOT NULL,
  `profit_loss_Depreciationarmotisation` varchar(255) NOT NULL,
  `profit_loss_Increaseinfairvalueadjustmentsimpairments` varchar(255) NOT NULL,
  `profit_loss_Decreaseinfairvalueadjustmentsimpairments` varchar(255) NOT NULL,
  `profit_loss_Financecosts` varchar(255) NOT NULL,
  `profit_loss_Financeincome` varchar(255) NOT NULL,
  `profit_loss_Incometaxexpense` varchar(255) NOT NULL,
  `profit_loss_AttributabletoEquityholdersoftheparent` varchar(255) NOT NULL,
  `profit_loss_AttributabletoNoncontrollingintrests` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fin_profit_loss`
--

INSERT INTO `fin_profit_loss` (`fin_profit_loss_id`, `user_id`, `year`, `is_audited`, `profit_loss_Saleofgoods`, `profit_loss_Renderingofservices`, `profit_loss_OtherRevenues`, `profit_loss_CostofSales`, `profit_loss_Otheroperatingincome`, `profit_loss_Otheroperatingexpenses`, `profit_loss_Staffcosts`, `profit_loss_Depreciationarmotisation`, `profit_loss_Increaseinfairvalueadjustmentsimpairments`, `profit_loss_Decreaseinfairvalueadjustmentsimpairments`, `profit_loss_Financecosts`, `profit_loss_Financeincome`, `profit_loss_Incometaxexpense`, `profit_loss_AttributabletoEquityholdersoftheparent`, `profit_loss_AttributabletoNoncontrollingintrests`) VALUES
(1, 'makazatinashe2000@gmail.com', '2015', '0', '1.2', '23', '23.9099', '0', '0', '12323', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(2, 'makazatinashe2000@gmail.com', '2016', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(3, 'makazatinashe2000@gmail.com', '2017', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(4, 'makazatinashe2000@gmail.com', '2018', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(5, 'makazatinashe2000@gmail.com', '2019', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(6, 'makazatinashe2000@gmail.com', '2020', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(7, 'makazatinashe2000@gmail.com', '2021', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(8, 'makazatinashe2000@gmail.com', '2022', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(9, 'anthony@finsec.co.zw', '2015', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(10, 'anthony@finsec.co.zw', '2016', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(11, 'anthony@finsec.co.zw', '2017', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(12, 'anthony@finsec.co.zw', '2018', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '23', '0', '0'),
(13, 'anthony@finsec.co.zw', '2019', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(14, 'anthony@finsec.co.zw', '2020', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(15, 'anthony@finsec.co.zw', '2021', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(16, 'anthony@finsec.co.zw', '2022', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `financial_financier_companies`
--

CREATE TABLE `financial_financier_companies` (
  `financial_advisors_companies_id` int(255) NOT NULL,
  `users_id` int(255) NOT NULL,
  `names` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financial_financier_companies`
--

INSERT INTO `financial_financier_companies` (`financial_advisors_companies_id`, `users_id`, `names`, `type`) VALUES
(1, 0, 'enerst and young', 'info@ey.co.zw'),
(2, 0, 'mulla', 'info@mulla.co.zw'),
(3, 0, 'muma', 'info@moma.com'),
(4, 0, 'delta', 'info@delta.co.zw'),
(5, 0, 'makaza', 'gazaland@makaza.com'),
(6, 0, 'FA', 'FA@gmail.com'),
(7, 0, 'guga', 'gubhaaa@fion.com');

-- --------------------------------------------------------

--
-- Table structure for table `financial_year`
--

CREATE TABLE `financial_year` (
  `financial_year_id` int(11) NOT NULL,
  `year` varchar(145) DEFAULT NULL,
  `desc` varchar(145) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financial_year`
--

INSERT INTO `financial_year` (`financial_year_id`, `year`, `desc`) VALUES
(1, '2015', NULL),
(2, '2016', NULL),
(3, '2017', NULL),
(4, '2018', NULL),
(5, '2019', NULL),
(6, '2020', NULL),
(7, '2021', NULL),
(8, '2022', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `financier_clients_status`
--

CREATE TABLE `financier_clients_status` (
  `id` int(255) NOT NULL,
  `financier_id` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_financier_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financier_clients_status`
--

INSERT INTO `financier_clients_status` (`id`, `financier_id`, `client_id`, `client_financier_status`) VALUES
(1, 'moyo@mail', 'ronaldinho@gmail.com', 'viewed');

-- --------------------------------------------------------

--
-- Table structure for table `human_resource_organogram`
--

CREATE TABLE `human_resource_organogram` (
  `human_resource_organogram_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `employee_name` varchar(245) DEFAULT NULL,
  `key_position` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `human_resource_organogram`
--

INSERT INTO `human_resource_organogram` (`human_resource_organogram_id`, `user_id`, `employee_name`, `key_position`) VALUES
(1, 'makazatinashe2000@gmail.com', 'Tinashe Chivaura', 'Developer'),
(3, 'makazatinashe2000@gmail.com', 'Dereck Chamboko', 'software engineer'),
(4, 'anthony@gmail.com', 'gdgdgd', 'nfnffmf21'),
(5, 'tinashe@ctrade.co.zw', 'sadasd', 'asdasd'),
(6, 'ronaldinho@gmail.com', 'gaucho', 'senior developer'),
(7, 'anthony@escrowgroup.org', 'Manager', 'Manger'),
(8, 'anthony@escrowgroup.org', 'Shift Manager', 'Shift Manager'),
(9, 'anthony@escrowgroup.org', 'Controller', 'Controller'),
(10, 'anthony@escrowgroup.org', 'Chicken Chef', 'Chicken Chef'),
(11, 'anthony@escrowgroup.org', 'Chicken Chef', 'Chicken Chef'),
(12, 'anthony@escrowgroup.org', 'Wraps/Chips Chef', 'Wraps/Chips Chef'),
(13, 'anthony@escrowgroup.org', 'Pizza Chef', 'Pizza Chef'),
(14, 'anthony@escrowgroup.org', 'Pizza Chef', 'Pizza Chef'),
(15, 'anthony@escrowgroup.org', 'Pizza Chef', 'Pizza Chef'),
(16, 'anthony@escrowgroup.org', 'Cashier', 'Cashier'),
(17, 'anthony@escrowgroup.org', 'Cashier', 'Cashier'),
(18, 'anthony@escrowgroup.org', 'Cashier', 'Cashier');

-- --------------------------------------------------------

--
-- Table structure for table `individual_permission`
--

CREATE TABLE `individual_permission` (
  `id` int(255) NOT NULL,
  `permission_id` int(255) NOT NULL,
  `user_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `material_asset_transactions`
--

CREATE TABLE `material_asset_transactions` (
  `material_asset_transactions_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `desc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_asset_transactions`
--

INSERT INTO `material_asset_transactions` (`material_asset_transactions_id`, `user_id`, `desc`) VALUES
(1, 'makazatinashe2000@gmail.com', '<span style=\"background-color: rgb(250, 250, 250); font-weight: bold;\">Insert details of material asset transactions (including between subsidiaries or with holding company) within the two preceding years including; any material property or asset disposals; any issue or agreement to issue any shares; any acquisition or intention to acquire property which is to be paid for wholly or partly out of the borrowings sought</span>'),
(2, 'anthony@gmail.com', 'eertgtggtg233'),
(3, 'tinashe@escrowgroup.org', ''),
(4, 'tinashe@ctrade.co.zw', '<p>qerwqweqwe</p>'),
(5, 'ronaldinho@gmail.com', 'gaucho'),
(6, 'makowe@gmail.com', ''),
(7, 'anthony@finsec.co.zw', '<p><span style=\"font-size: 11px; text-align: start; background-color: rgb(250, 250, 250);\">Insert details of material asset transactions (including between subsidiaries or with holding company) within the two preceding years including; any material property or asset disposals; any issue or agreement to issue any shares; any acquisition or intention to acquire property which is to be paid for wholly or partly out of the borrowings sought</span><br></p>'),
(8, 'timmy@escrowgroup.org', '<p><br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `material_contracts`
--

CREATE TABLE `material_contracts` (
  `material_contracts_id` int(255) NOT NULL,
  `users_id` varchar(255) NOT NULL,
  `restrictions` varchar(255) NOT NULL,
  `capital_committments` varchar(255) NOT NULL,
  `subgroup` varchar(255) NOT NULL,
  `undisclosed_contracts` varchar(255) NOT NULL,
  `joint_venture` varchar(255) NOT NULL,
  `terminated_contracts` varchar(255) NOT NULL,
  `app_status` int(255) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_contracts`
--

INSERT INTO `material_contracts` (`material_contracts_id`, `users_id`, `restrictions`, `capital_committments`, `subgroup`, `undisclosed_contracts`, `joint_venture`, `terminated_contracts`, `app_status`) VALUES
(4, '0', '1391600104CodeIgniter-Sample-Chapter-Chapter-4-UsingCI-to-Simplify-Databases.pdf', '9780735621978_chapter_07.pdf', 'EntityFramework.pdf', 'ch28.pdf', 'Programming ASP.NET MVC 5.pdf', 'SQL_Databases.pdf', 1),
(6, 'ronaldinho@gmail.com', 'AngularJSNotesForProfessionals.pdf', 'ASP.NET and Web Programming.pdf', 'Minutes - 27th Technical Committee Meeting.docx', 'image.png', 'mvc_php_intro.pdf', 'SQL_Databases.pdf', 1),
(11, 'tinashe@escrowgroup.org', '', '', '', '', '', '', 1),
(12, 'tinashe@ctrade.co.zw', 'screen shot 2017-03-06 at 104520 am.png', 'domain-crt.txt', 'domain-csr.txt', 'ctrader.PNG', 'ctrader.PNG', 'screen shot 2017-03-06 at 104520 am.png', 1),
(15, '', 'Android.pdf', '', '', '', '', '', 1),
(16, 'makazatinashe2000@gmail.com', 'Android.pdf', 'AngularJSNotesForProfessionals.pdf', 'ch28.pdf', 'doc_4.pdf', 'doc_5.pdf', 'Structured Query Language.pdf', 1),
(17, 'makowe@gmail.com', '', '', '', '', '', '', 1),
(18, 'anthony@finsec.co.zw', '', '', '', '', '', '', 1),
(19, 'timmy@escrowgroup.org', '4sight Repo System.pdf', '4sight Repo System.pdf', '4sight Repo System.pdf', '10-22495_jgr_v1_i3_p5.pdf', '10-22495_cocv10i1c7art6.pdf', '003-the-repo-market.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `material_litigation`
--

CREATE TABLE `material_litigation` (
  `material_litigation_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `desc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_litigation`
--

INSERT INTO `material_litigation` (`material_litigation_id`, `user_id`, `desc`) VALUES
(1, 'makazatinashe2000@gmail.com', '<span style=\"background-color: rgb(250, 250, 250);\">Insert details in resp<span style=\"font-style: italic;\">ect to an</span>y legal or arbitration p<span style=\"text-decoration-line: underline;\">roceeding</span>s pending or threatened against [company], such as consent decrees, judgments, injunctions, orders, settlement agreements, administrative<span style=\"font-weight: bold;\"> proceedings, inquiries, investigations, petitions or complai</span>nts including customer and employee lawsuits.]:</span>'),
(2, 'anthony@gmail.com', 'hfhfhfhff'),
(3, 'tinashe@escrowgroup.org', ''),
(4, 'tinashe@ctrade.co.zw', '<p>qweqweqwe</p>'),
(5, 'ronaldinho@gmail.com', 'gaucho'),
(6, 'makowe@gmail.com', ''),
(7, 'anthony@finsec.co.zw', '<p>Insert details in respect to any legal or arbitration proceedings pending or threatened against [company], such as consent decrees, judgments, injunctions, orders, settlement agreements, administrative proceedings, inquiries, investigations, petitions or complaints including customer and employee lawsuits.]:<span style=\"color: rgb(0, 0, 0); font-family: \" open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 11px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" 400;=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(250,=\"\" 250,=\"\" 250);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" display:=\"\" inline=\"\" !important;=\"\" float:=\"\" none;\"=\"\">Insert details in respect to any legal or arbitration proceedings pending or threatened against [company], such as consent decrees, judgments, injunctions, orders, settlement agreements, administrative proceedings, inquiries, investigations, petitions or complaints including customer and employee lawsuits.]:</span><br></p>'),
(8, 'timmy@escrowgroup.org', '<p><br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `para_countries`
--

CREATE TABLE `para_countries` (
  `country_id` int(255) NOT NULL,
  `country_name` varchar(255) NOT NULL,
  `users_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `para_countries`
--

INSERT INTO `para_countries` (`country_id`, `country_name`, `users_id`) VALUES
(1, 'Zimababwe', ''),
(2, 'South Africa', ''),
(3, 'Zambia', ''),
(4, 'Tanzania', ''),
(5, 'Nigeria', ''),
(6, 'Ghana', ''),
(7, 'Kenya', ''),
(8, 'Namibia', ''),
(9, 'Egypt', ''),
(10, 'Morrocco', ''),
(11, 'mane', 'tinashe@gmail.com'),
(12, 'america', 'tinashe@gmail.com'),
(13, 'mozambique', 'tinashe@gmail.com'),
(14, 'BRAZIL', 'tinashe@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `para_regions`
--

CREATE TABLE `para_regions` (
  `region_id` int(255) NOT NULL,
  `region_name` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `para_regions`
--

INSERT INTO `para_regions` (`region_id`, `region_name`, `country_id`) VALUES
(1, 'Harare', '1'),
(2, 'Bulawayo', '1'),
(3, 'Manicaland', '1'),
(4, 'Mashonaland Central', '1'),
(5, 'Mashonaland East', '1'),
(6, 'Mashonaland West', '1'),
(7, 'Masvingo', '1'),
(8, 'Matabeleland North', '1'),
(9, 'Matabeleland South', '1'),
(10, 'Midlands', '1');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `parameter_id` int(255) NOT NULL,
  `data` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`parameter_id`, `data`, `value`) VALUES
(1, 'threshold score', '20'),
(2, 'tinashe@gmail.com', 'huya'),
(3, 'mum', '12');

-- --------------------------------------------------------

--
-- Table structure for table `past_scores`
--

CREATE TABLE `past_scores` (
  `id` int(255) NOT NULL,
  `score_cate_id` int(255) NOT NULL,
  `form_input` varchar(255) NOT NULL,
  `score` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `past_scores`
--

INSERT INTO `past_scores` (`id`, `score_cate_id`, `form_input`, `score`) VALUES
(1, 1, 'company_name', 1),
(2, 1, 'Company_registration_number', 7),
(3, 1, 'Date_of_Incorporation', 1),
(4, 1, 'Country_of_Incorporation', 1),
(5, 1, 'Type_of_Entity', 1),
(6, 1, 'Business_Sector', 1),
(7, 1, 'Nature_of_business', 1),
(8, 1, 'Telephone', 1),
(9, 1, 'Fax_Number', 1),
(10, 1, 'Registered_Office_Physical_Address', 1),
(11, 1, 'Principal_Place_of_Business', 1),
(12, 1, 'Postal_Address', 1),
(13, 1, 'Equity', 1),
(14, 1, 'share_under_offer', 1),
(15, 1, 'Debt', 1),
(16, 1, 'Other', 1),
(17, 1, 'Purpose_of_funds', 1),
(18, 2, 'Duration_in_stated_business_line', 1),
(19, 2, 'Corporates', 1),
(20, 2, 'Induviduals', 1),
(21, 2, 'Business_Year_End', 1),
(22, 2, 'Business_Premises_are', 1),
(23, 2, 'Name_of_holding_company', 1),
(24, 2, 'Name', 1),
(25, 2, 'Position', 1),
(26, 2, 'Tel', 1),
(27, 2, 'Cell', 1),
(28, 2, 'Email', 1),
(29, 2, 'Company_Secretary_Address', 1),
(30, 2, 'Attorney_office_Address ', 1),
(31, 2, 'Accountant_Office', 1),
(32, 2, 'Auditors_Office', 1),
(33, 2, 'Principal_Bankers', 1),
(34, 2, 'Authorised_share_capital', 1),
(35, 2, 'Issued_share_capital ', 1),
(36, 3, 'Staff_Levels', 1),
(37, 3, 'Gross_Value_of_Assets', 1),
(38, 4, 'Introduction_History_and_Major_Milestones', 1),
(39, 4, 'Company_Products_Services', 1),
(40, 4, 'Company_Major_Raw_Materials', 1),
(41, 4, 'Major_Customers_Distribution_Channel', 1),
(42, 4, 'Major_Suppliers_Channel', 1),
(57, 4, 'The_Company_Protected_Rights ', 1),
(58, 5, 'proprietors_partners_directors_profile', 1),
(59, 6, 'directors_shareholding', 1),
(60, 7, 'board_committees', 1),
(61, 8, 'corporate_structure', 1),
(62, 9, 'human_resource_organogram', 1),
(63, 10, 'company_shareholder', 1),
(64, 11, 'contracts_restrictions', 1),
(65, 11, 'contracts_of_ material_capital_commitments', 1),
(66, 11, 'contracts_with subsidiaries_of_other_group companies', 1),
(67, 11, 'undisclosed_contracts', 1),
(68, 11, 'material_joint_venture', 1),
(69, 11, 'terminated_contracts', 1),
(70, 12, 'material_asset_transactions', 1),
(71, 11, 'material_litigation_and_claims', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `Id` int(11) NOT NULL,
  `EmailAddress` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Company` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Price` varchar(50) CHARACTER SET utf8 NOT NULL,
  `PaymentStatus` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PostedStatus` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PaynowRef` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PollUrl` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `payments_ref` varchar(145) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`Id`, `EmailAddress`, `Company`, `Price`, `PaymentStatus`, `PostedStatus`, `PaynowRef`, `PollUrl`, `Date`, `payments_ref`) VALUES
(2, 'tinashe@ctrade.co.zw', 'Old Mutual', '200', 'rejected', '', '', '', '2018-04-12 19:19:38', '3'),
(8, 'tinashe@escrowgroup.org', 'IMARA', '100.00', 'Cancelled', '', '1075470', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=e1604c46-e4e9-4b6b-a752-fedb301c3cbc', '2018-04-12 22:59:05', '492321'),
(9, 'tinashe@finsec.com', 'Deloite', '100.00', 'Cancelled', '', '1076229', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=e6819ced-2016-4576-9b62-9c72e8ae6bfc', '2018-04-13 10:35:22', '445684'),
(10, 'makazatinashe2000@gmail.com', 'Enerst n Young', '100', 'PENDING', '', '', '', '2018-04-13 15:43:38', '286959'),
(11, 'makazatinashe2000@gmail.com', 'Enerst n Young', '100', 'PENDING', '', '', '', '2018-04-13 15:46:58', '693618'),
(12, 'makazatinashe2000@gmail.com', 'First Mutual', '100', 'PENDING', '', '', '', '2018-04-13 15:47:39', '614682'),
(13, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'Cancelled', '', '', '', '2018-04-17 14:18:35', '243893'),
(14, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'PENDING', '', '', '', '2018-04-17 14:45:27', '697436'),
(15, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'PENDING', '', '', '', '2018-04-17 14:45:42', '326483'),
(16, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'PENDING', '', '', '', '2018-04-17 14:49:11', '610754'),
(21, 'daddy@gmail.com', 'FINSEC', '100', 'accepted', '', '', '', '2018-04-24 14:39:15', '254797'),
(22, 'ronaldinho@gmail.com', 'FINSEC', '100', 'accepted', '', '', '', '2018-04-26 22:07:11', '696859'),
(23, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-07 13:02:00', '413659'),
(24, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-09 11:40:49', '115216'),
(25, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-11 13:31:22', '637341'),
(26, 'makowe@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-18 09:23:40', '645169'),
(27, 'makowe@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-18 09:23:42', '646212'),
(28, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-06-08 21:35:19', '839379'),
(30, 'makazatinashe2000@gmail.com', 'FINSEC', '100.00', 'Cancelled', '', '1234794', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=01e83c88-d3d8-412a-8fa4-3c2417c6b7a5', '2018-06-11 16:41:29', '631655'),
(31, 'makazatinashe2000@gmail.com', 'FINSEC', '100.00', 'Cancelled', '', '1234795', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=99495f82-8e65-4c81-8b8f-fdee21e738c4', '2018-06-11 16:42:22', '183578'),
(32, 'anthony@finsec.co.zw', 'FINSEC', '100.00', 'Cancelled', '', '1236451', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=d950318b-6474-47b2-8c7e-7dd9440dd538', '2018-06-12 11:07:26', '361968'),
(33, 'timmy@escrowgroup.org', 'FINSEC', '100.00', 'Cancelled', '', '1237083', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=3e56fab3-37cb-434d-80ea-ddc4791243bb', '2018-06-12 14:52:17', '573648'),
(34, 'timmy@escrowgroup.org', 'FINSEC', '100', 'cancelled', '', '', '', '2018-06-12 14:53:22', '392758'),
(35, 'timmy@escrowgroup.org', 'FINSEC', '100', 'cancelled', '', '', '', '2018-06-12 14:53:26', '496194'),
(36, 'timmy@escrowgroup.org', 'FINSEC', '100.00', 'Cancelled', '', '1237103', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=93b16c73-75a7-4e0d-bbfa-65c7c8f6bb57', '2018-06-12 14:58:30', '528878'),
(37, 'tario@gmail.com', 'FINSEC', '100.00', 'Cancelled', '', '1253045', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=e9a3bf8d-ef6a-48cd-a0ba-3697441c6fac', '2018-06-18 17:02:11', '875332'),
(38, 'anthony@escrowgroup.org', 'FINSEC', '100.00', 'Cancelled', '', '1253112', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=8bfcd7c2-142b-4150-b6ca-5ede143806d4', '2018-06-18 17:22:05', '682138');

-- --------------------------------------------------------

--
-- Table structure for table `permission_group`
--

CREATE TABLE `permission_group` (
  `group_id` int(255) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `page_ids` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_group`
--

INSERT INTO `permission_group` (`group_id`, `group_name`, `page_ids`) VALUES
(1, 'Financier', '3,7'),
(2, 'Financial Advisor', '7,4,5'),
(3, 'Acceptor', '3');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `permission_id` int(11) NOT NULL,
  `page` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `page`, `user_type`, `title`) VALUES
(3, 'f_dashboard.php', 'Financial_Advisor', 'Acceptance'),
(4, 'f_ver_dashboard.php', 'Financial_Advisor', 'Review & Verification'),
(5, 'f_rec_dashboard.php', 'Financial_Advisor', 'Report Compilation'),
(6, 'f_rec_dashboard.php', 'Financial_Advisor ', 'Report Compilation'),
(7, 'f_approver_dashboard.php', 'Financial_Advisor', 'Approver'),
(8, 'dashboard.php', 'Financial_Advisor', 'Home'),
(9, 'fin_dashboard.php', 'Financier', 'Financier');

-- --------------------------------------------------------

--
-- Table structure for table `personal_financial`
--

CREATE TABLE `personal_financial` (
  `personal_financial_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `guarantor_name` varchar(245) DEFAULT NULL,
  `financial_institution` varchar(245) DEFAULT NULL,
  `facility_type` varchar(245) DEFAULT NULL,
  `facility_amount` varchar(245) DEFAULT NULL,
  `monthly_installment` varchar(245) DEFAULT NULL,
  `balance` varchar(245) DEFAULT NULL,
  `tenure` varchar(245) DEFAULT NULL,
  `start_date` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personal_financial`
--

INSERT INTO `personal_financial` (`personal_financial_id`, `user_id`, `guarantor_name`, `financial_institution`, `facility_type`, `facility_amount`, `monthly_installment`, `balance`, `tenure`, `start_date`) VALUES
(6, '', '', '', '', '', '', '', '', ''),
(7, '', '', '', '', '', '', '', '', ''),
(8, '', '', '', '', '', '', '', '', ''),
(9, '', '', '', '', '', '', '', '', ''),
(10, 'anthony@gmail.com', 'hvjvjfjgf', 'mbmbmbm', 'mbvmbmb', 'nfnvnv', 'nfjfjfmjf', 'gjjgjg', 'nmvmvmv', 'hfhfhfhf'),
(11, 'makazatinashe2000@gmail.com', 'dsf', 'sdfsdf', 'sfdsdf', '1232', '12', '123', '2', '04/18/2018'),
(12, 'anthony@finsec.co.zw', 'hdufhj', 'uhgdfghj', 'hjfdgh', 'fgjh', 'hjfdjhg', 'hjdjfh', 'hjdfjhhj', 'hjdfhj'),
(13, 'tinashe@ctrade.co.zw', 'qwee', 'qweqwe', 'qwewqe', 'qweqw', 'qweq', 'qweqwe', 'qweqwe', 'qweq');

-- --------------------------------------------------------

--
-- Table structure for table `profit_loss`
--

CREATE TABLE `profit_loss` (
  `profit_loss_id` int(255) NOT NULL,
  `users_id` varchar(255) NOT NULL,
  `is_audited` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `cost_of_goods` varchar(255) NOT NULL,
  `rendering_of_services` varchar(255) NOT NULL,
  `other` varchar(255) NOT NULL,
  `total_revenue` varchar(255) NOT NULL,
  `cost_of_sales` varchar(255) NOT NULL,
  `gross_profit` varchar(255) NOT NULL,
  `other_operating_income` varchar(255) NOT NULL,
  `other_operating_expenses` varchar(255) NOT NULL,
  `staff_costs` varchar(255) NOT NULL,
  `depreciation_and_ammortisation` varchar(255) NOT NULL,
  `operating_profit` varchar(255) NOT NULL,
  `increase_in_fair_value_adjustments_impairments` varchar(255) NOT NULL,
  `decrease_in_fair_value_adjustments_impairments` varchar(255) NOT NULL,
  `finance_costs` varchar(255) NOT NULL,
  `finance_income` varchar(255) NOT NULL,
  `profit_loss_before_tax` varchar(255) NOT NULL,
  `income_tax_expense` varchar(255) NOT NULL,
  `profit_loss_for_the_year` varchar(255) NOT NULL,
  `equity_holders_of_parent` varchar(255) NOT NULL,
  `non_controlling_interests` varchar(255) NOT NULL,
  `total_attributed` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profit_loss`
--

INSERT INTO `profit_loss` (`profit_loss_id`, `users_id`, `is_audited`, `year`, `cost_of_goods`, `rendering_of_services`, `other`, `total_revenue`, `cost_of_sales`, `gross_profit`, `other_operating_income`, `other_operating_expenses`, `staff_costs`, `depreciation_and_ammortisation`, `operating_profit`, `increase_in_fair_value_adjustments_impairments`, `decrease_in_fair_value_adjustments_impairments`, `finance_costs`, `finance_income`, `profit_loss_before_tax`, `income_tax_expense`, `profit_loss_for_the_year`, `equity_holders_of_parent`, `non_controlling_interests`, `total_attributed`) VALUES
(80, 'makazatinashe2000@gmail.com', '', '2015', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(81, 'makazatinashe2000@gmail.com', '', '2016', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(82, 'makazatinashe2000@gmail.com', '', '2017', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(83, 'makazatinashe2000@gmail.com', '', '2018', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(84, 'makazatinashe2000@gmail.com', '', '2019', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(85, 'makazatinashe2000@gmail.com', '', '2020', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `recommendations`
--

CREATE TABLE `recommendations` (
  `recommendation_id` int(255) NOT NULL,
  `applicant_id` varchar(255) NOT NULL,
  `recommendation` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recommendations`
--

INSERT INTO `recommendations` (`recommendation_id`, `applicant_id`, `recommendation`) VALUES
(1, 'ronaldinho@gmail.com', 'ronaldinho@gmail.com'),
(2, 'makazatinashe2000@gmail.com', 'This is my RECOMMENDATION'),
(3, 'makowe@gmail.com', 'outstanding'),
(4, 'tinashe@ctrade.co.zw', '<p>This Is the recomendations</p>');

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `id` int(255) NOT NULL,
  `score_cate_id` int(255) NOT NULL,
  `form_input` varchar(255) NOT NULL,
  `score` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`id`, `score_cate_id`, `form_input`, `score`) VALUES
(1, 1, 'company_name', 1),
(2, 1, 'company_registration_number', 1),
(3, 1, 'date_of_incorporation', 1),
(4, 1, 'country_of_incorporation', 1),
(5, 1, 'region_of_incorporation', 1),
(6, 1, 'type_of_entity', 1),
(7, 1, 'business_sector', 1),
(8, 1, 'nature_of_business', 1),
(9, 1, 'business_nature', 1),
(10, 1, 'telephone', 1),
(11, 1, 'fax_number', 1),
(12, 1, 'registered_office_physical_address', 1),
(13, 1, 'postal_address', 1),
(14, 1, 'principal_place_of_business', 1),
(15, 1, 'raised_equity', 1),
(16, 1, 'raised_debt', 1),
(17, 1, 'raised_other', 1),
(18, 1, 'purpose_of_funds', 1),
(19, 1, 'capital_req_id', 1),
(20, 1, 'share_on_offer', 1),
(21, 2, 'duration_in_stated_business_line', 1),
(22, 2, 'customer_basecorporates', 1),
(23, 2, 'customer_baseindividuals', 1),
(24, 2, 'business_year_end', 1),
(25, 2, 'business_premises_are', 1),
(26, 2, 'name_of_holding_company', 1),
(27, 2, 'contact_person_name', 1),
(28, 2, 'contact_person_position', 1),
(29, 2, 'contact_person_cell', 1),
(30, 2, 'contact_person_telephone', 1),
(31, 2, 'contact_person_email', 1),
(32, 2, 'company_secretary_address', 1),
(33, 2, 'attorney_office_address', 1),
(34, 2, 'accountant_office_address', 1),
(35, 2, 'auditors_office_address', 1),
(36, 2, 'principal_bankers_address', 1),
(37, 2, 'authorised_share_capital', 1),
(38, 2, 'issued_share_capital', 1),
(39, 2, 'corporate_directory_id', 1),
(40, 3, 'staff_levels', 1),
(41, 3, 'annual_turnover', 1),
(42, 3, 'gross_value_of_assets', 1),
(43, 4, 'company_overview_history', 1),
(44, 4, 'products', 1),
(45, 4, 'rawmaterials', 1),
(46, 4, 'distributionchannels', 1),
(47, 4, 'supplychannels', 1),
(48, 4, 'company_overview_rights', 1),
(49, 5, 'list_of_directors', 1),
(50, 6, 'directors_shareholding', 1),
(51, 7, 'board_committees', 1),
(52, 8, 'desc', 1),
(53, 9, 'human_resource', 1),
(54, 10, 'shareholder', 1),
(55, 11, 'ristricts', 1),
(56, 11, 'capital_commitments', 1),
(57, 11, 'subgroup', 1),
(58, 11, 'undisclosed', 1),
(59, 11, 'joint_venture', 1),
(60, 11, 'terminated_contracts', 1),
(61, 12, 'material_asset_transactions_desc', 1),
(62, 13, 'material_litigation_desc', 1),
(63, 17, 'fin_commitment', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scores_cate`
--

CREATE TABLE `scores_cate` (
  `id` int(255) NOT NULL,
  `scores_category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores_cate`
--

INSERT INTO `scores_cate` (`id`, `scores_category`) VALUES
(1, 'capital_req'),
(2, 'corporate_directory'),
(3, 'enterprise_category'),
(4, 'company_overview'),
(5, 'proprietors_partners_principals_and_directors_profile'),
(6, 'directors_shareholding'),
(7, 'board_committees'),
(8, 'corporate_structure'),
(9, 'human_resouce_organogram'),
(10, 'company_shareholders'),
(11, 'material_and_other_third_party_contracts'),
(12, 'material_asset_transactions'),
(13, 'material_litigation_and_claims'),
(14, 'balance_sheet'),
(15, 'cash_flow'),
(16, 'income_statement'),
(17, 'financial_committments');

-- --------------------------------------------------------

--
-- Table structure for table `scores_total`
--

CREATE TABLE `scores_total` (
  `scores_total_id` int(11) NOT NULL,
  `scores_total_user` varchar(245) DEFAULT NULL,
  `scores_id` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores_total`
--

INSERT INTO `scores_total` (`scores_total_id`, `scores_total_user`, `scores_id`) VALUES
(2, 'makazatinashe2000@gmail.com', '2'),
(3, 'makazatinashe2000@gmail.com', '3'),
(4, 'makazatinashe2000@gmail.com', '4'),
(6, 'makazatinashe2000@gmail.com', '6'),
(8, 'makazatinashe2000@gmail.com', '7'),
(9, 'makazatinashe2000@gmail.com', '8'),
(10, 'makazatinashe2000@gmail.com', '10'),
(11, 'makazatinashe2000@gmail.com', '11'),
(13, 'makazatinashe2000@gmail.com', '13'),
(14, 'makazatinashe2000@gmail.com', '14'),
(15, 'makazatinashe2000@gmail.com', '15'),
(16, 'makazatinashe2000@gmail.com', '16'),
(17, 'makazatinashe2000@gmail.com', '17'),
(18, 'makazatinashe2000@gmail.com', '18'),
(19, 'makazatinashe2000@gmail.com', '19'),
(20, 'makazatinashe2000@gmail.com', '20'),
(22, 'makazatinashe2000@gmail.com', '5'),
(65, 'anthony@finsec.co.zw', '2'),
(66, 'anthony@finsec.co.zw', '3'),
(67, 'anthony@finsec.co.zw', '4'),
(68, 'anthony@finsec.co.zw', '5'),
(69, 'anthony@finsec.co.zw', '6'),
(70, 'anthony@finsec.co.zw', '7'),
(71, 'anthony@finsec.co.zw', '8'),
(72, 'anthony@finsec.co.zw', '10'),
(73, 'anthony@finsec.co.zw', '11'),
(74, 'anthony@finsec.co.zw', '12'),
(75, 'anthony@finsec.co.zw', '13'),
(76, 'anthony@finsec.co.zw', '14'),
(77, 'anthony@finsec.co.zw', '15'),
(78, 'anthony@finsec.co.zw', '16'),
(79, 'anthony@finsec.co.zw', '17'),
(80, 'anthony@finsec.co.zw', '18'),
(81, 'anthony@finsec.co.zw', '19'),
(82, 'anthony@finsec.co.zw', '20'),
(83, 'anthony@finsec.co.zw', '1'),
(84, 'anthony@finsec.co.zw', '21'),
(85, 'anthony@finsec.co.zw', '22'),
(86, 'anthony@finsec.co.zw', '23'),
(87, 'anthony@finsec.co.zw', '24'),
(88, 'anthony@finsec.co.zw', '25'),
(89, 'anthony@finsec.co.zw', '26'),
(90, 'anthony@finsec.co.zw', '27'),
(91, 'anthony@finsec.co.zw', '28'),
(92, 'anthony@finsec.co.zw', '29'),
(93, 'anthony@finsec.co.zw', '30'),
(94, 'anthony@finsec.co.zw', '31'),
(95, 'anthony@finsec.co.zw', '32'),
(96, 'anthony@finsec.co.zw', '33'),
(97, 'anthony@finsec.co.zw', '34'),
(98, 'anthony@finsec.co.zw', '35'),
(99, 'anthony@finsec.co.zw', '36'),
(100, 'anthony@finsec.co.zw', '37'),
(101, 'anthony@finsec.co.zw', '38'),
(102, 'anthony@finsec.co.zw', ''),
(103, 'anthony@finsec.co.zw', ''),
(104, 'anthony@finsec.co.zw', ''),
(105, 'anthony@finsec.co.zw', '40'),
(106, 'anthony@finsec.co.zw', '41'),
(107, 'anthony@finsec.co.zw', '42'),
(108, 'anthony@finsec.co.zw', '43'),
(109, 'anthony@finsec.co.zw', '48'),
(113, 'anthony@finsec.co.zw', '50'),
(114, 'anthony@finsec.co.zw', '49'),
(117, 'anthony@finsec.co.zw', '52'),
(119, 'anthony@finsec.co.zw', '54'),
(120, 'anthony@finsec.co.zw', '92'),
(121, 'anthony@finsec.co.zw', '61'),
(122, 'anthony@finsec.co.zw', '62'),
(123, 'anthony@finsec.co.zw', '63'),
(124, 'hudson@escrowgroup.org', '2'),
(125, 'hudson@escrowgroup.org', '3'),
(126, 'hudson@escrowgroup.org', '4'),
(127, 'hudson@escrowgroup.org', '5'),
(128, 'hudson@escrowgroup.org', '6'),
(129, 'hudson@escrowgroup.org', '7'),
(130, 'hudson@escrowgroup.org', '8'),
(131, 'hudson@escrowgroup.org', '10'),
(132, 'hudson@escrowgroup.org', '11'),
(133, 'hudson@escrowgroup.org', '13'),
(134, 'hudson@escrowgroup.org', '14'),
(135, 'hudson@escrowgroup.org', '15'),
(136, 'hudson@escrowgroup.org', '16'),
(137, 'hudson@escrowgroup.org', '17'),
(138, 'hudson@escrowgroup.org', '20'),
(139, 'hudson@escrowgroup.org', '1'),
(140, 'hudson@escrowgroup.org', '19'),
(141, 'hudson@escrowgroup.org', '12'),
(142, 'hudson@escrowgroup.org', '18'),
(143, 'hudson@escrowgroup.org', '21'),
(144, 'hudson@escrowgroup.org', '22'),
(145, 'hudson@escrowgroup.org', '23'),
(146, 'hudson@escrowgroup.org', '24'),
(147, 'hudson@escrowgroup.org', '25'),
(148, 'hudson@escrowgroup.org', '26'),
(149, 'hudson@escrowgroup.org', '27'),
(150, 'hudson@escrowgroup.org', '28'),
(151, 'hudson@escrowgroup.org', '29'),
(152, 'hudson@escrowgroup.org', '30'),
(153, 'hudson@escrowgroup.org', '31'),
(154, 'hudson@escrowgroup.org', '32'),
(155, 'hudson@escrowgroup.org', '33'),
(156, 'hudson@escrowgroup.org', '34'),
(157, 'hudson@escrowgroup.org', '35'),
(158, 'hudson@escrowgroup.org', '36'),
(159, 'hudson@escrowgroup.org', '37'),
(160, 'hudson@escrowgroup.org', '38'),
(161, 'makazatinashe2000@gmail.com', '1'),
(162, 'makazatinashe2000@gmail.com', '12'),
(163, 'makazatinashe2000@gmail.com', '21'),
(164, 'makazatinashe2000@gmail.com', '22'),
(165, 'makazatinashe2000@gmail.com', '23'),
(166, 'makazatinashe2000@gmail.com', '24'),
(167, 'makazatinashe2000@gmail.com', '25'),
(168, 'makazatinashe2000@gmail.com', '26'),
(169, 'makazatinashe2000@gmail.com', '27'),
(170, 'makazatinashe2000@gmail.com', '28'),
(171, 'makazatinashe2000@gmail.com', '29'),
(172, 'makazatinashe2000@gmail.com', '30'),
(173, 'makazatinashe2000@gmail.com', '31'),
(179, 'makazatinashe2000@gmail.com', '37'),
(180, 'makazatinashe2000@gmail.com', '38'),
(181, 'makazatinashe2000@gmail.com', '40'),
(182, 'makazatinashe2000@gmail.com', '41'),
(183, 'makazatinashe2000@gmail.com', '42'),
(184, 'makazatinashe2000@gmail.com', '43'),
(185, 'makazatinashe2000@gmail.com', '44'),
(186, 'makazatinashe2000@gmail.com', '45'),
(187, 'makazatinashe2000@gmail.com', '46'),
(188, 'makazatinashe2000@gmail.com', '47'),
(189, 'makazatinashe2000@gmail.com', '48'),
(190, 'makazatinashe2000@gmail.com', '32'),
(191, 'makazatinashe2000@gmail.com', '33'),
(192, 'makazatinashe2000@gmail.com', '34'),
(193, 'makazatinashe2000@gmail.com', '35'),
(194, 'makazatinashe2000@gmail.com', '36'),
(195, 'makazatinashe2000@gmail.com', '49'),
(196, 'makazatinashe2000@gmail.com', '50'),
(197, 'makazatinashe2000@gmail.com', '51'),
(198, 'makazatinashe2000@gmail.com', '52'),
(199, 'makazatinashe2000@gmail.com', '53'),
(200, 'makazatinashe2000@gmail.com', '54'),
(201, 'makazatinashe2000@gmail.com', '61'),
(202, 'makazatinashe2000@gmail.com', '62'),
(203, 'makazatinashe2000@gmail.com', '63'),
(204, 'hudson@escrowgroup.org', '40'),
(205, 'hudson@escrowgroup.org', '41'),
(206, 'hudson@escrowgroup.org', '42'),
(207, 'hudson@escrowgroup.org', '48'),
(208, 'timmy@escrowgroup.org', '1'),
(209, 'timmy@escrowgroup.org', '2'),
(210, 'timmy@escrowgroup.org', '3'),
(211, 'timmy@escrowgroup.org', '4'),
(212, 'timmy@escrowgroup.org', '5'),
(213, 'timmy@escrowgroup.org', '6'),
(214, 'timmy@escrowgroup.org', '7'),
(215, 'timmy@escrowgroup.org', '8'),
(216, 'timmy@escrowgroup.org', '10'),
(217, 'timmy@escrowgroup.org', '11'),
(218, 'timmy@escrowgroup.org', '15'),
(219, 'timmy@escrowgroup.org', '16'),
(220, 'timmy@escrowgroup.org', '17'),
(221, 'timmy@escrowgroup.org', '20'),
(222, 'timmy@escrowgroup.org', '21'),
(223, 'timmy@escrowgroup.org', '22'),
(224, 'timmy@escrowgroup.org', '23'),
(225, 'timmy@escrowgroup.org', '24'),
(226, 'timmy@escrowgroup.org', '25'),
(227, 'timmy@escrowgroup.org', '26'),
(228, 'timmy@escrowgroup.org', '27'),
(229, 'timmy@escrowgroup.org', '28'),
(230, 'timmy@escrowgroup.org', '29'),
(231, 'timmy@escrowgroup.org', '30'),
(232, 'timmy@escrowgroup.org', '31'),
(233, 'timmy@escrowgroup.org', '32'),
(234, 'timmy@escrowgroup.org', '33'),
(235, 'timmy@escrowgroup.org', '34'),
(236, 'timmy@escrowgroup.org', '35'),
(237, 'timmy@escrowgroup.org', '36'),
(238, 'timmy@escrowgroup.org', '37'),
(239, 'timmy@escrowgroup.org', '38'),
(240, 'timmy@escrowgroup.org', '40'),
(241, 'timmy@escrowgroup.org', '41'),
(242, 'timmy@escrowgroup.org', '42'),
(243, 'timmy@escrowgroup.org', '48'),
(244, 'timmy@escrowgroup.org', '52'),
(245, 'timmy@escrowgroup.org', '61'),
(246, 'timmy@escrowgroup.org', '62'),
(248, 'tinashe@ctrade.co.zw', '2'),
(249, 'tinashe@ctrade.co.zw', '3'),
(250, 'tinashe@ctrade.co.zw', '4'),
(251, 'tinashe@ctrade.co.zw', '5'),
(252, 'tinashe@ctrade.co.zw', '6'),
(253, 'tinashe@ctrade.co.zw', '7'),
(254, 'tinashe@ctrade.co.zw', '8'),
(255, 'tinashe@ctrade.co.zw', '10'),
(256, 'tinashe@ctrade.co.zw', '11'),
(257, 'tinashe@ctrade.co.zw', '12'),
(258, 'tinashe@ctrade.co.zw', '13'),
(259, 'tinashe@ctrade.co.zw', '14'),
(260, 'tinashe@ctrade.co.zw', '15'),
(261, 'tinashe@ctrade.co.zw', '16'),
(262, 'tinashe@ctrade.co.zw', '17'),
(263, 'tinashe@ctrade.co.zw', '18'),
(264, 'tinashe@ctrade.co.zw', '20'),
(265, 'tinashe@ctrade.co.zw', '1'),
(266, 'tinashe@ctrade.co.zw', '21'),
(267, 'tinashe@ctrade.co.zw', '22'),
(268, 'tinashe@ctrade.co.zw', '23'),
(269, 'tinashe@ctrade.co.zw', '24'),
(270, 'tinashe@ctrade.co.zw', '25'),
(271, 'tinashe@ctrade.co.zw', '26'),
(272, 'tinashe@ctrade.co.zw', '27'),
(273, 'tinashe@ctrade.co.zw', '28'),
(274, 'tinashe@ctrade.co.zw', '29'),
(275, 'tinashe@ctrade.co.zw', '30'),
(276, 'tinashe@ctrade.co.zw', '31'),
(277, 'tinashe@ctrade.co.zw', '32'),
(278, 'tinashe@ctrade.co.zw', '33'),
(279, 'tinashe@ctrade.co.zw', '34'),
(280, 'tinashe@ctrade.co.zw', '35'),
(281, 'tinashe@ctrade.co.zw', '36'),
(282, 'tinashe@ctrade.co.zw', '37'),
(283, 'tinashe@ctrade.co.zw', '38'),
(284, 'tinashe@ctrade.co.zw', '40'),
(285, 'tinashe@ctrade.co.zw', '41'),
(286, 'tinashe@ctrade.co.zw', '42'),
(287, 'tinashe@ctrade.co.zw', '43'),
(288, 'tinashe@ctrade.co.zw', '44'),
(289, 'tinashe@ctrade.co.zw', '45'),
(290, 'tinashe@ctrade.co.zw', '46'),
(291, 'tinashe@ctrade.co.zw', '47'),
(292, 'tinashe@ctrade.co.zw', '48'),
(293, 'tinashe@ctrade.co.zw', '49'),
(294, 'tinashe@ctrade.co.zw', '50'),
(295, 'tinashe@ctrade.co.zw', '51'),
(296, 'tinashe@ctrade.co.zw', '52'),
(297, 'tinashe@ctrade.co.zw', '53'),
(298, 'tinashe@ctrade.co.zw', '54'),
(299, 'tinashe@ctrade.co.zw', '61'),
(300, 'tinashe@ctrade.co.zw', '62'),
(301, 'tinashe@ctrade.co.zw', '63'),
(302, 'karl@escrowgroup.org', '1'),
(303, 'karl@escrowgroup.org', '2'),
(304, 'karl@escrowgroup.org', '3'),
(305, 'karl@escrowgroup.org', '4'),
(306, 'karl@escrowgroup.org', '5'),
(307, 'karl@escrowgroup.org', '6'),
(308, 'karl@escrowgroup.org', '7'),
(309, 'karl@escrowgroup.org', '8'),
(310, 'karl@escrowgroup.org', '10'),
(311, 'karl@escrowgroup.org', '11'),
(312, 'karl@escrowgroup.org', '12'),
(313, 'karl@escrowgroup.org', '13'),
(314, 'karl@escrowgroup.org', '14'),
(315, 'karl@escrowgroup.org', '15'),
(316, 'karl@escrowgroup.org', '16'),
(317, 'karl@escrowgroup.org', '17'),
(318, 'karl@escrowgroup.org', '18'),
(319, 'karl@escrowgroup.org', '20'),
(320, 'karl@escrowgroup.org', '21'),
(321, 'karl@escrowgroup.org', '22'),
(322, 'karl@escrowgroup.org', '23'),
(323, 'karl@escrowgroup.org', '24'),
(324, 'karl@escrowgroup.org', '25'),
(325, 'karl@escrowgroup.org', '26'),
(326, 'karl@escrowgroup.org', '27'),
(327, 'karl@escrowgroup.org', '28'),
(328, 'karl@escrowgroup.org', '29'),
(329, 'karl@escrowgroup.org', '30'),
(330, 'karl@escrowgroup.org', '31'),
(331, 'karl@escrowgroup.org', '32'),
(332, 'karl@escrowgroup.org', '33'),
(333, 'karl@escrowgroup.org', '34'),
(334, 'karl@escrowgroup.org', '35'),
(335, 'karl@escrowgroup.org', '36'),
(336, 'karl@escrowgroup.org', '37'),
(337, 'karl@escrowgroup.org', '38'),
(338, 'karl@escrowgroup.org', '40'),
(339, 'karl@escrowgroup.org', '41'),
(340, 'karl@escrowgroup.org', '42'),
(341, 'makazatinashe2000@gmail.com', ''),
(342, 'makazatinashe2000@gmail.com', ''),
(343, 'makazatinashe2000@gmail.com', ''),
(344, 'makazatinashe2000@gmail.com', ''),
(345, 'makazatinashe2000@gmail.com', ''),
(346, 'makazatinashe2000@gmail.com', ''),
(347, 'makazatinashe2000@gmail.com', ''),
(348, 'makazatinashe2000@gmail.com', ''),
(349, 'makazatinashe2000@gmail.com', ''),
(350, 'makazatinashe2000@gmail.com', ''),
(351, 'makazatinashe2000@gmail.com', ''),
(352, 'makazatinashe2000@gmail.com', ''),
(353, 'karlenko42@gmail.com', '1'),
(354, 'karlenko42@gmail.com', '2'),
(355, 'karlenko42@gmail.com', '3'),
(356, 'karlenko42@gmail.com', '4'),
(357, 'karlenko42@gmail.com', '5'),
(358, 'karlenko42@gmail.com', '6'),
(359, 'karlenko42@gmail.com', '7'),
(360, 'karlenko42@gmail.com', '8'),
(361, 'karlenko42@gmail.com', '10'),
(362, 'karlenko42@gmail.com', '11'),
(363, 'karlenko42@gmail.com', '12'),
(364, 'karlenko42@gmail.com', '13'),
(365, 'karlenko42@gmail.com', '14'),
(366, 'karlenko42@gmail.com', '15'),
(367, 'karlenko42@gmail.com', ''),
(368, 'karlenko42@gmail.com', ''),
(369, 'karlenko42@gmail.com', ''),
(370, 'karlenko42@gmail.com', '16'),
(371, 'karlenko42@gmail.com', ''),
(372, 'karlenko42@gmail.com', ''),
(373, 'karlenko42@gmail.com', ''),
(374, 'karlenko42@gmail.com', '17'),
(375, 'karlenko42@gmail.com', '18'),
(376, 'karlenko42@gmail.com', '20'),
(377, 'karlenko42@gmail.com', '21'),
(378, 'karlenko42@gmail.com', '22'),
(379, 'karlenko42@gmail.com', '23'),
(380, 'karlenko42@gmail.com', '24'),
(381, 'karlenko42@gmail.com', '25'),
(382, 'karlenko42@gmail.com', '26'),
(383, 'karlenko42@gmail.com', '27'),
(384, 'karlenko42@gmail.com', '28'),
(385, 'karlenko42@gmail.com', '29'),
(386, 'karlenko42@gmail.com', '30'),
(387, 'karlenko42@gmail.com', '31'),
(388, 'karlenko42@gmail.com', '32'),
(389, 'karlenko42@gmail.com', '33'),
(390, 'karlenko42@gmail.com', '34'),
(391, 'karlenko42@gmail.com', '35'),
(392, 'karlenko42@gmail.com', '36'),
(393, 'karlenko42@gmail.com', '37'),
(394, 'karlenko42@gmail.com', '38'),
(395, 'anthony@escrowgroup.org', '1'),
(396, 'anthony@escrowgroup.org', '2'),
(397, 'anthony@escrowgroup.org', '3'),
(398, 'anthony@escrowgroup.org', '4'),
(399, 'anthony@escrowgroup.org', '5'),
(400, 'anthony@escrowgroup.org', '6'),
(401, 'anthony@escrowgroup.org', '7'),
(402, 'anthony@escrowgroup.org', '8'),
(403, 'anthony@escrowgroup.org', '10'),
(404, 'anthony@escrowgroup.org', '11'),
(405, 'anthony@escrowgroup.org', '12'),
(406, 'anthony@escrowgroup.org', '13'),
(407, 'anthony@escrowgroup.org', '14'),
(408, 'anthony@escrowgroup.org', '15'),
(409, 'anthony@escrowgroup.org', ''),
(410, 'anthony@escrowgroup.org', ''),
(411, 'anthony@escrowgroup.org', ''),
(412, 'anthony@escrowgroup.org', '16'),
(413, 'anthony@escrowgroup.org', ''),
(414, 'anthony@escrowgroup.org', ''),
(415, 'anthony@escrowgroup.org', ''),
(416, 'anthony@escrowgroup.org', '17'),
(417, 'anthony@escrowgroup.org', '18'),
(418, 'anthony@escrowgroup.org', '20'),
(419, 'anthony@escrowgroup.org', '21'),
(420, 'anthony@escrowgroup.org', '22'),
(421, 'anthony@escrowgroup.org', '23'),
(422, 'anthony@escrowgroup.org', '24'),
(423, 'anthony@escrowgroup.org', '25'),
(424, 'anthony@escrowgroup.org', '26'),
(425, 'anthony@escrowgroup.org', '27'),
(426, 'anthony@escrowgroup.org', '28'),
(427, 'anthony@escrowgroup.org', '29'),
(428, 'anthony@escrowgroup.org', '30'),
(429, 'anthony@escrowgroup.org', '31'),
(430, 'anthony@escrowgroup.org', '32'),
(431, 'anthony@escrowgroup.org', '33'),
(432, 'anthony@escrowgroup.org', '34'),
(433, 'anthony@escrowgroup.org', '35'),
(434, 'anthony@escrowgroup.org', '36'),
(435, 'anthony@escrowgroup.org', '37'),
(436, 'anthony@escrowgroup.org', '38'),
(437, 'anthony@escrowgroup.org', '40'),
(438, 'anthony@escrowgroup.org', '41'),
(439, 'anthony@escrowgroup.org', '42'),
(440, 'anthony@finsec.co.zw', ''),
(441, 'anthony@finsec.co.zw', ''),
(442, 'anthony@finsec.co.zw', ''),
(443, 'anthony@finsec.co.zw', ''),
(444, 'anthony@finsec.co.zw', ''),
(445, 'anthony@finsec.co.zw', ''),
(446, 'anthony@escrowgroup.org', '43'),
(447, 'anthony@escrowgroup.org', '48'),
(448, 'anthony@escrowgroup.org', ''),
(449, 'anthony@escrowgroup.org', ''),
(450, 'anthony@escrowgroup.org', ''),
(451, 'anthony@escrowgroup.org', ''),
(452, 'anthony@escrowgroup.org', ''),
(453, 'anthony@escrowgroup.org', ''),
(454, 'tario@gmail.com', '1'),
(455, 'tario@gmail.com', '2'),
(456, 'tario@gmail.com', '3'),
(457, 'tario@gmail.com', '4'),
(458, 'tario@gmail.com', '5'),
(459, 'tario@gmail.com', '6'),
(460, 'tario@gmail.com', '7'),
(461, 'tario@gmail.com', '8'),
(462, 'tario@gmail.com', '10'),
(463, 'tario@gmail.com', '11'),
(464, 'tario@gmail.com', '12'),
(465, 'tario@gmail.com', '13'),
(466, 'tario@gmail.com', '14'),
(467, 'tario@gmail.com', '15'),
(468, 'tario@gmail.com', ''),
(469, 'tario@gmail.com', ''),
(470, 'tario@gmail.com', ''),
(471, 'tario@gmail.com', '16'),
(472, 'tario@gmail.com', ''),
(473, 'tario@gmail.com', ''),
(474, 'tario@gmail.com', ''),
(475, 'tario@gmail.com', '17'),
(476, 'tario@gmail.com', '18'),
(477, 'tario@gmail.com', '20'),
(478, 'tario@gmail.com', '21'),
(479, 'tario@gmail.com', '22'),
(480, 'tario@gmail.com', '23'),
(481, 'tario@gmail.com', '24'),
(482, 'tario@gmail.com', '25'),
(483, 'tario@gmail.com', '26'),
(484, 'tario@gmail.com', '27'),
(485, 'tario@gmail.com', '28'),
(486, 'tario@gmail.com', '29'),
(487, 'tario@gmail.com', '30'),
(488, 'tario@gmail.com', '31'),
(489, 'tario@gmail.com', '32'),
(490, 'tario@gmail.com', '33'),
(491, 'tario@gmail.com', '34'),
(492, 'tario@gmail.com', '35'),
(493, 'tario@gmail.com', '36'),
(494, 'tario@gmail.com', '37'),
(495, 'tario@gmail.com', '38'),
(496, 'tario@gmail.com', '40'),
(497, 'tario@gmail.com', '41'),
(498, 'tario@gmail.com', '42'),
(499, 'tario@gmail.com', '43'),
(500, 'tario@gmail.com', '48'),
(501, 'tario@gmail.com', '49'),
(502, 'tario@gmail.com', '50'),
(503, 'tario@gmail.com', '52'),
(504, 'anthony@escrowgroup.org', ''),
(505, 'anthony@escrowgroup.org', ''),
(506, 'anthony@escrowgroup.org', ''),
(507, 'anthony@escrowgroup.org', ''),
(508, 'anthony@escrowgroup.org', ''),
(509, 'anthony@escrowgroup.org', ''),
(510, 'anthony@escrowgroup.org', '49'),
(511, 'anthony@escrowgroup.org', '50'),
(512, 'anthony@escrowgroup.org', '52'),
(513, 'anthony@escrowgroup.org', '53');

-- --------------------------------------------------------

--
-- Table structure for table `shareholders_shareholding`
--

CREATE TABLE `shareholders_shareholding` (
  `directors_shareholding_id` int(11) NOT NULL,
  `user_id` varchar(245) DEFAULT NULL,
  `directors_name` varchar(245) DEFAULT NULL,
  `direct_indirect_equity_interest` varchar(245) DEFAULT NULL,
  `shareholding` varchar(245) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shareholders_shareholding`
--

INSERT INTO `shareholders_shareholding` (`directors_shareholding_id`, `user_id`, `directors_name`, `direct_indirect_equity_interest`, `shareholding`) VALUES
(1, 'makazatinashe2000@gmail.com', 'tawanda', '2', '2'),
(2, 'anthony@gmail.com', 'nfcnfjnfjfj', '13232', 'jfjfjfjf'),
(3, 'tinashe@ctrade.co.zw', 'sadasd', 'asdasd', 'adasd'),
(4, 'daddy@gmail.com', 'test', '12', '12'),
(5, 'ronaldinho@gmail.com', 'scholari', '12', '24'),
(6, 'anthony@finsec.co.zw', 'sfhdahfgsfh', 'hghgdfh', 'fghhgsdgh');

-- --------------------------------------------------------

--
-- Table structure for table `user_scores`
--

CREATE TABLE `user_scores` (
  `id` int(11) NOT NULL,
  `user_scores_category_id` int(100) NOT NULL,
  `user_scores_id` varchar(255) NOT NULL,
  `user_scores_total` text NOT NULL,
  `scores_total` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_scores`
--

INSERT INTO `user_scores` (`id`, `user_scores_category_id`, `user_scores_id`, `user_scores_total`, `scores_total`) VALUES
(1, 1, 'makazatinashe2000@gmail.com', '[{\"1\":1,\"2\":1,\"3\":1,\"4\":1,\"5\":1,\"6\":1,\"7\":1,\"8\":1,\"9\":1,\"10\":1,\"11\":1,\"12\":1,\"13\":1,\"14\":1,\"15\":1,\"16\":1,\"17\":1}]', '37'),
(2, 2, 'makowe@gmail.com', '[{\"1\":1,\"2\":1,\"3\":1,\"4\":1,\"5\":1,\"6\":1,\"7\":1,\"8\":1,\"9\":1,\"10\":1,\"11\":1,\"12\":1,\"13\":1,\"14\":1,\"15\":1,\"16\":1,\"17\":1}]', '49'),
(4, 1, 'brain@ctrade.co.zw', '0', '0'),
(5, 1, 'martin@gmail.com', '0', '0'),
(6, 1, 'anthony@finsec.co.zw', '0', '0'),
(7, 1, 'hudson@escrowgroup.org', '0', '0'),
(8, 1, 'Timmy@escrowgroup.org', '0', '0'),
(9, 1, 'karl@escrowgroup.org', '0', '0'),
(10, 1, 'karlenko42@gmail.com', '0', '0'),
(11, 1, 'anthony@escrowgroup.org', '0', '0'),
(12, 1, 'welly.kc1@gmail.com', '0', '0'),
(13, 1, 'tario@gmail.com', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `users_fullname` varchar(345) DEFAULT NULL,
  `users_username` varchar(245) DEFAULT NULL,
  `users_password` varchar(345) DEFAULT NULL,
  `users_email` varchar(345) DEFAULT NULL,
  `users_phonenumber` varchar(245) DEFAULT NULL,
  `users_position` varchar(145) DEFAULT NULL,
  `users_status` varchar(45) DEFAULT NULL,
  `users_created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `users_company_name` varchar(305) DEFAULT NULL,
  `users_company_regnum` varchar(245) DEFAULT NULL,
  `users_company_dateofincorp` date DEFAULT NULL,
  `users_company_country` varchar(245) DEFAULT NULL,
  `users_form_name` varchar(415) DEFAULT NULL,
  `users_type` varchar(255) NOT NULL,
  `users_advance_score` varchar(145) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `users_fullname`, `users_username`, `users_password`, `users_email`, `users_phonenumber`, `users_position`, `users_status`, `users_created_date`, `users_company_name`, `users_company_regnum`, `users_company_dateofincorp`, `users_company_country`, `users_form_name`, `users_type`, `users_advance_score`) VALUES
(13, 'Tinashe Makaza', '', 'tinashem', 'makazatinashe2000@gmail.com', '0772876187', '', '', '2018-03-01 16:17:34', '', '', '0000-00-00', '', '', 'applicant', '1'),
(14, 'tinashe chivaura', 'akatendeka', '12345', 'tinashe@gmail.com', NULL, NULL, NULL, '2018-04-03 16:25:39', NULL, NULL, NULL, NULL, NULL, 'Administrator', NULL),
(28, 'Tinashe Makaza', '', 'asdfghjkl', 'tinashe@ctrade.co.zw', '772876187', 'applicant', 'applicant', '2018-04-12 12:57:05', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', '1'),
(31, 'Tinashe Makaza', '', 'tinashe', 'tinashe@finsec.com', '+263772876187', 'applicant', 'applicant', '2018-04-13 10:33:38', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', '1'),
(39, 'makowe', '', '12345', 'makowe@gmail.com', '12233434', 'applicant', 'applicant', '2018-05-18 09:18:39', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(40, 'financier', '', '12345', 'financier@gmail.com', '0771168282', 'Financier', 'Financier', '2018-05-18 12:29:22', 'Financier', 'Financier', '0000-00-00', 'Financier', 'Financier', 'Financier', NULL),
(41, 'analyst', '', '12345', 'analyst@gmail.com', '0771168282', 'Analyst', 'Analyst', '2018-05-18 12:29:58', 'Analyst', 'Analyst', '0000-00-00', 'Analyst', 'Analyst', 'Analyst', NULL),
(43, 'admin', '', '12345', 'admin@gmail.com', '12355', 'Administrator', 'Administrator', '2018-05-18 12:42:15', 'Administrator', 'Administrator', '0000-00-00', 'Administrator', 'Administrator', 'Administrator', NULL),
(44, 'Millicent Vinyu', '', 'asdfghjkl2017', 'mimie@gmail.com', '0772876187', 'applicant', 'applicant', '2018-05-31 09:59:08', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(47, 'Brain Dzimbani', '', 'asdfghjkl', 'brain@ctrade.co.zw', '1325521', 'applicant', 'applicant', '2018-06-11 15:50:23', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(48, 'Martin Moyo', '', 'asdfghjkl', 'martin@gmail.com', '3425367843', 'applicant', 'applicant', '2018-06-11 15:51:17', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(49, 'Anthony Shumba', '', 'asdfghjkl', 'anthony@finsec.co.zw', '345678939', 'applicant', 'applicant', '2018-06-11 16:14:03', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', '1'),
(50, 'Hudson Nyamariva', '', 'hudson', 'hudson@escrowgroup.org', '42135424364', 'applicant', 'applicant', '2018-06-12 12:09:58', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(51, 'Timmy', '', '12345', 'Timmy@escrowgroup.org', '09898989', 'applicant', 'applicant', '2018-06-12 14:47:26', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', '1'),
(52, 'Dereck', '', 'password', 'karl@escrowgroup.org', '014445', 'applicant', 'applicant', '2018-06-13 11:06:18', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(53, 'Yuan Pound', '', 'password', 'karlenko42@gmail.com', '+263716893984', 'applicant', 'applicant', '2018-06-18 15:46:23', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(54, 'Rob Spencer', '', '12345', 'anthony@escrowgroup.org', '0785006225', 'applicant', 'applicant', '2018-06-18 15:54:13', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', '1'),
(55, 'wellington Chikwanah', '', '*komborero8', 'welly.kc1@gmail.com', '0779520582', 'applicant', 'applicant', '2018-06-18 16:52:15', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(56, 'Tario Mazhange', '', 'asdfghjkl', 'tario@gmail.com', '2636322223', 'applicant', 'applicant', '2018-06-18 16:53:54', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', '1');

-- --------------------------------------------------------

--
-- Table structure for table `verification_and_review`
--

CREATE TABLE `verification_and_review` (
  `id` int(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `correct_items` varchar(1000) NOT NULL,
  `form` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verification_and_review`
--

INSERT INTO `verification_and_review` (`id`, `user_id`, `correct_items`, `form`) VALUES
(122, 'ronaldinho@gmail.com', '1', 'capital'),
(123, 'ronaldinho@gmail.com', '2', 'capital'),
(124, 'ronaldinho@gmail.com', '3', 'capital'),
(125, 'ronaldinho@gmail.com', '4', 'capital'),
(126, 'ronaldinho@gmail.com', '5', 'capital'),
(127, 'ronaldinho@gmail.com', '6', 'capital'),
(128, 'ronaldinho@gmail.com', '7', 'capital'),
(129, 'ronaldinho@gmail.com', '8', 'capital'),
(130, 'ronaldinho@gmail.com', '9', 'capital'),
(131, 'ronaldinho@gmail.com', '10', 'capital'),
(132, 'ronaldinho@gmail.com', '11', 'capital'),
(133, 'ronaldinho@gmail.com', '12', 'capital'),
(134, 'ronaldinho@gmail.com', '13', 'capital'),
(135, 'ronaldinho@gmail.com', '16', 'capital'),
(136, 'ronaldinho@gmail.com', '14', 'capital'),
(137, 'ronaldinho@gmail.com', '17', 'capital'),
(138, 'ronaldinho@gmail.com', '18', 'corporatedirectory'),
(139, 'ronaldinho@gmail.com', '19', 'corporatedirectory'),
(140, 'ronaldinho@gmail.com', '20', 'corporatedirectory'),
(141, 'ronaldinho@gmail.com', '21', 'corporatedirectory'),
(142, 'ronaldinho@gmail.com', '22', 'corporatedirectory'),
(143, 'ronaldinho@gmail.com', '23', 'corporatedirectory'),
(144, 'ronaldinho@gmail.com', '24', 'corporatedirectory'),
(145, 'ronaldinho@gmail.com', '25', 'corporatedirectory'),
(146, 'ronaldinho@gmail.com', '17', 'boarddirectors'),
(151, 'makazatinashe2000@gmail.com', '12', 'boarddirectors'),
(152, 'makazatinashe2000@gmail.com', '1', 'capital'),
(153, 'makazatinashe2000@gmail.com', '2', 'capital'),
(154, 'makazatinashe2000@gmail.com', '12', 'capital'),
(155, 'makazatinashe2000@gmail.com', '16', 'capital'),
(156, 'makazatinashe2000@gmail.com', '18', 'corporatedirectory'),
(157, 'makazatinashe2000@gmail.com', '19', 'corporatedirectory');

-- --------------------------------------------------------

--
-- Table structure for table `verification_scoring_parameter`
--

CREATE TABLE `verification_scoring_parameter` (
  `verification_id` int(11) NOT NULL,
  `input_id` varchar(255) NOT NULL,
  `input_name` varchar(255) NOT NULL,
  `score` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `verification_scoring_parameter`
--

INSERT INTO `verification_scoring_parameter` (`verification_id`, `input_id`, `input_name`, `score`) VALUES
(1, '1', 'Company_name', '1'),
(2, '2', 'Company_registration_number', '1'),
(3, '3', 'Date_of_Incorporation', '1'),
(4, '4', 'Country_of_Incorporation', '1'),
(5, '5', 'Type_of_Entity', '1'),
(6, '6', 'Business_Sector', '1'),
(7, '7', 'Telephone', '1'),
(8, '8', 'Fax_Number', '1'),
(9, '9', 'Nature_of_business', '1'),
(10, '10', 'Registered_Office_Physical_Address', '1'),
(11, '11', 'Postal_Address', '1'),
(12, '12', 'Principal_Place_of_Business', '1'),
(13, '13', 'Equity', '1'),
(14, '14', 'Debt', '1'),
(15, '15', 'Share_on_Offer', '1'),
(16, '16', 'other', '1'),
(17, '17', 'purpose_of_funds', '1'),
(18, '18', 'Duration_in_stated_business_line', '1'),
(19, '19', 'Corporates', '1'),
(20, '20', 'Induviduals', '1'),
(21, '21', 'Principal_Bankers ', '1'),
(22, '22', 'Business_Year_End', '1'),
(23, '23', 'Business_Premises_are', '1'),
(24, '24', 'Name_of_holding_company', '1'),
(25, '25', 'Contact_person_Name', '1'),
(26, '26', 'Contact_person_Position', '1'),
(27, '27', 'Contact_person_Cell', '1'),
(28, '28', 'Tel', '1'),
(29, '29', 'Email', '1'),
(30, '30', 'Company_Secretary_Address', '1'),
(31, '31', 'Attorney_Office_Address', '1'),
(32, '32', 'Auditors_Office_Address', '1'),
(33, '33', 'Accountant_Office_Address', '1'),
(34, '34', 'Authorised_share_capital', '1'),
(35, '35', 'Issued_share_capital', '1'),
(36, '36', 'board_directors', '1'),
(37, '36', 'board_directors', '1');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view1`
-- (See below for the actual view)
--
CREATE TABLE `view1` (
`capital_req_id` int(11)
,`user_id` varchar(245)
,`company_name` varchar(245)
,`company_registration_number` varchar(245)
,`date_of_incorporation` varchar(245)
,`country_of_incorporation` varchar(245)
,`type_of_entity` varchar(245)
,`business_sector` varchar(245)
,`nature_of_business` varchar(245)
,`business_nature` varchar(245)
,`telephone` varchar(245)
,`fax_number` varchar(245)
,`registered_office_physical_address` text
,`postal_address` text
,`principal_place_of_business` text
,`raised_equity` varchar(245)
,`raised_debt` varchar(245)
,`raised_other` varchar(245)
,`purpose_of_funds` varchar(245)
);

-- --------------------------------------------------------

--
-- Structure for view `view1`
--
DROP TABLE IF EXISTS `view1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view1`  AS  select `capital_requirements`.`capital_req_id` AS `capital_req_id`,`capital_requirements`.`user_id` AS `user_id`,`capital_requirements`.`company_name` AS `company_name`,`capital_requirements`.`company_registration_number` AS `company_registration_number`,`capital_requirements`.`date_of_incorporation` AS `date_of_incorporation`,`capital_requirements`.`country_of_incorporation` AS `country_of_incorporation`,`capital_requirements`.`type_of_entity` AS `type_of_entity`,`capital_requirements`.`business_sector` AS `business_sector`,`capital_requirements`.`nature_of_business` AS `nature_of_business`,`capital_requirements`.`business_nature` AS `business_nature`,`capital_requirements`.`telephone` AS `telephone`,`capital_requirements`.`fax_number` AS `fax_number`,`capital_requirements`.`registered_office_physical_address` AS `registered_office_physical_address`,`capital_requirements`.`postal_address` AS `postal_address`,`capital_requirements`.`principal_place_of_business` AS `principal_place_of_business`,`capital_requirements`.`raised_equity` AS `raised_equity`,`capital_requirements`.`raised_debt` AS `raised_debt`,`capital_requirements`.`raised_other` AS `raised_other`,`capital_requirements`.`purpose_of_funds` AS `purpose_of_funds` from `capital_requirements` where (`capital_requirements`.`capital_req_id` = 5) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicant_level`
--
ALTER TABLE `applicant_level`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `approved_applications`
--
ALTER TABLE `approved_applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balance_sheet`
--
ALTER TABLE `balance_sheet`
  ADD PRIMARY KEY (`balance_sheet_id`);

--
-- Indexes for table `business_nature`
--
ALTER TABLE `business_nature`
  ADD PRIMARY KEY (`business_nature_id`);

--
-- Indexes for table `business_sector`
--
ALTER TABLE `business_sector`
  ADD PRIMARY KEY (`business_sector_id`);

--
-- Indexes for table `capital_requirements`
--
ALTER TABLE `capital_requirements`
  ADD PRIMARY KEY (`capital_req_id`);

--
-- Indexes for table `cash_flow`
--
ALTER TABLE `cash_flow`
  ADD PRIMARY KEY (`cash_flow_id`);

--
-- Indexes for table `committees`
--
ALTER TABLE `committees`
  ADD PRIMARY KEY (`committees_id`);

--
-- Indexes for table `company_overview`
--
ALTER TABLE `company_overview`
  ADD PRIMARY KEY (`company_overview_id`);

--
-- Indexes for table `company_overview_customers`
--
ALTER TABLE `company_overview_customers`
  ADD PRIMARY KEY (`company_overview_customers_id`);

--
-- Indexes for table `company_overview_products_services`
--
ALTER TABLE `company_overview_products_services`
  ADD PRIMARY KEY (`company_overview_products_services_id`);

--
-- Indexes for table `company_overview_raw_materials`
--
ALTER TABLE `company_overview_raw_materials`
  ADD PRIMARY KEY (`company_overview_raw_materials_id`);

--
-- Indexes for table `company_overview_suppliers`
--
ALTER TABLE `company_overview_suppliers`
  ADD PRIMARY KEY (`company_overview_suppliers_id`);

--
-- Indexes for table `corporate_directory`
--
ALTER TABLE `corporate_directory`
  ADD PRIMARY KEY (`corporate_directory_id`);

--
-- Indexes for table `corporate_structure`
--
ALTER TABLE `corporate_structure`
  ADD PRIMARY KEY (`corporate_structure_id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`director_id`);

--
-- Indexes for table `directors_shareholding`
--
ALTER TABLE `directors_shareholding`
  ADD PRIMARY KEY (`directors_shareholding_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enterprise_size_categorisation`
--
ALTER TABLE `enterprise_size_categorisation`
  ADD PRIMARY KEY (`enterprise_size_categorisation_id`);

--
-- Indexes for table `fin_balance_sheet`
--
ALTER TABLE `fin_balance_sheet`
  ADD PRIMARY KEY (`fin_balance_sheet_id`);

--
-- Indexes for table `fin_cash_flow`
--
ALTER TABLE `fin_cash_flow`
  ADD PRIMARY KEY (`fin_cash_flow_id`);

--
-- Indexes for table `fin_profit_loss`
--
ALTER TABLE `fin_profit_loss`
  ADD PRIMARY KEY (`fin_profit_loss_id`);

--
-- Indexes for table `financial_financier_companies`
--
ALTER TABLE `financial_financier_companies`
  ADD PRIMARY KEY (`financial_advisors_companies_id`);

--
-- Indexes for table `financial_year`
--
ALTER TABLE `financial_year`
  ADD PRIMARY KEY (`financial_year_id`);

--
-- Indexes for table `financier_clients_status`
--
ALTER TABLE `financier_clients_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `human_resource_organogram`
--
ALTER TABLE `human_resource_organogram`
  ADD PRIMARY KEY (`human_resource_organogram_id`);

--
-- Indexes for table `individual_permission`
--
ALTER TABLE `individual_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_asset_transactions`
--
ALTER TABLE `material_asset_transactions`
  ADD PRIMARY KEY (`material_asset_transactions_id`);

--
-- Indexes for table `material_contracts`
--
ALTER TABLE `material_contracts`
  ADD PRIMARY KEY (`material_contracts_id`);

--
-- Indexes for table `material_litigation`
--
ALTER TABLE `material_litigation`
  ADD PRIMARY KEY (`material_litigation_id`);

--
-- Indexes for table `para_countries`
--
ALTER TABLE `para_countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `para_regions`
--
ALTER TABLE `para_regions`
  ADD PRIMARY KEY (`region_id`);

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`parameter_id`);

--
-- Indexes for table `past_scores`
--
ALTER TABLE `past_scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `permission_group`
--
ALTER TABLE `permission_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `personal_financial`
--
ALTER TABLE `personal_financial`
  ADD PRIMARY KEY (`personal_financial_id`);

--
-- Indexes for table `profit_loss`
--
ALTER TABLE `profit_loss`
  ADD PRIMARY KEY (`profit_loss_id`);

--
-- Indexes for table `recommendations`
--
ALTER TABLE `recommendations`
  ADD PRIMARY KEY (`recommendation_id`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scores_cate`
--
ALTER TABLE `scores_cate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scores_total`
--
ALTER TABLE `scores_total`
  ADD PRIMARY KEY (`scores_total_id`);

--
-- Indexes for table `shareholders_shareholding`
--
ALTER TABLE `shareholders_shareholding`
  ADD PRIMARY KEY (`directors_shareholding_id`);

--
-- Indexes for table `user_scores`
--
ALTER TABLE `user_scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- Indexes for table `verification_and_review`
--
ALTER TABLE `verification_and_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verification_scoring_parameter`
--
ALTER TABLE `verification_scoring_parameter`
  ADD PRIMARY KEY (`verification_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicant_level`
--
ALTER TABLE `applicant_level`
  MODIFY `level_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `approved_applications`
--
ALTER TABLE `approved_applications`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `balance_sheet`
--
ALTER TABLE `balance_sheet`
  MODIFY `balance_sheet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;
--
-- AUTO_INCREMENT for table `business_nature`
--
ALTER TABLE `business_nature`
  MODIFY `business_nature_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `business_sector`
--
ALTER TABLE `business_sector`
  MODIFY `business_sector_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `capital_requirements`
--
ALTER TABLE `capital_requirements`
  MODIFY `capital_req_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `cash_flow`
--
ALTER TABLE `cash_flow`
  MODIFY `cash_flow_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT for table `committees`
--
ALTER TABLE `committees`
  MODIFY `committees_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `company_overview`
--
ALTER TABLE `company_overview`
  MODIFY `company_overview_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `company_overview_customers`
--
ALTER TABLE `company_overview_customers`
  MODIFY `company_overview_customers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `company_overview_products_services`
--
ALTER TABLE `company_overview_products_services`
  MODIFY `company_overview_products_services_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `company_overview_raw_materials`
--
ALTER TABLE `company_overview_raw_materials`
  MODIFY `company_overview_raw_materials_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `company_overview_suppliers`
--
ALTER TABLE `company_overview_suppliers`
  MODIFY `company_overview_suppliers_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `corporate_directory`
--
ALTER TABLE `corporate_directory`
  MODIFY `corporate_directory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `corporate_structure`
--
ALTER TABLE `corporate_structure`
  MODIFY `corporate_structure_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `director_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `directors_shareholding`
--
ALTER TABLE `directors_shareholding`
  MODIFY `directors_shareholding_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `enterprise_size_categorisation`
--
ALTER TABLE `enterprise_size_categorisation`
  MODIFY `enterprise_size_categorisation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `fin_balance_sheet`
--
ALTER TABLE `fin_balance_sheet`
  MODIFY `fin_balance_sheet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `fin_cash_flow`
--
ALTER TABLE `fin_cash_flow`
  MODIFY `fin_cash_flow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `fin_profit_loss`
--
ALTER TABLE `fin_profit_loss`
  MODIFY `fin_profit_loss_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `financial_financier_companies`
--
ALTER TABLE `financial_financier_companies`
  MODIFY `financial_advisors_companies_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `financier_clients_status`
--
ALTER TABLE `financier_clients_status`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `human_resource_organogram`
--
ALTER TABLE `human_resource_organogram`
  MODIFY `human_resource_organogram_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `individual_permission`
--
ALTER TABLE `individual_permission`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `material_asset_transactions`
--
ALTER TABLE `material_asset_transactions`
  MODIFY `material_asset_transactions_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `material_contracts`
--
ALTER TABLE `material_contracts`
  MODIFY `material_contracts_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `material_litigation`
--
ALTER TABLE `material_litigation`
  MODIFY `material_litigation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `para_countries`
--
ALTER TABLE `para_countries`
  MODIFY `country_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `para_regions`
--
ALTER TABLE `para_regions`
  MODIFY `region_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `parameter_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `past_scores`
--
ALTER TABLE `past_scores`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `permission_group`
--
ALTER TABLE `permission_group`
  MODIFY `group_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `personal_financial`
--
ALTER TABLE `personal_financial`
  MODIFY `personal_financial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `profit_loss`
--
ALTER TABLE `profit_loss`
  MODIFY `profit_loss_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `recommendations`
--
ALTER TABLE `recommendations`
  MODIFY `recommendation_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `scores_cate`
--
ALTER TABLE `scores_cate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `scores_total`
--
ALTER TABLE `scores_total`
  MODIFY `scores_total_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=514;
--
-- AUTO_INCREMENT for table `shareholders_shareholding`
--
ALTER TABLE `shareholders_shareholding`
  MODIFY `directors_shareholding_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_scores`
--
ALTER TABLE `user_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `verification_and_review`
--
ALTER TABLE `verification_and_review`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;
--
-- AUTO_INCREMENT for table `verification_scoring_parameter`
--
ALTER TABLE `verification_scoring_parameter`
  MODIFY `verification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
