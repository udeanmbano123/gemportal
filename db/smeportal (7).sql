-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2018 at 02:05 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smeportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicant_level`
--

CREATE TABLE IF NOT EXISTS `applicant_level` (
  `level_id` int(255) NOT NULL AUTO_INCREMENT,
  `send_to` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `applicant_level`
--

INSERT INTO `applicant_level` (`level_id`, `send_to`, `user_id`) VALUES
(3, 'financier', 'tinashe@ctrade.co.zw'),
(10, 'Financier', 'ronaldinho@gmail.com'),
(13, 'Financier', 'makazatinashe2000@gmail.com'),
(14, 'stacy@gmail.com', 'BASIC'),
(15, 'takawira@gmail.com', 'BASIC'),
(16, 'Financier', 'makowe@gmail.com'),
(17, 'BASIC', 'financier@gmail.com'),
(18, 'BASIC', 'analyst@gmail.com'),
(19, 'BASIC', 'sango@gmail.com'),
(20, 'BASIC', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `approver_rejections`
--

CREATE TABLE IF NOT EXISTS `approver_rejections` (
  `reject_id` int(255) NOT NULL AUTO_INCREMENT,
  `applicant_id` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  PRIMARY KEY (`reject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `balance_sheet`
--

CREATE TABLE IF NOT EXISTS `balance_sheet` (
  `balance_sheet_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `year` varchar(245) DEFAULT NULL,
  `is_audited` varchar(255) NOT NULL,
  `property_plant_and_equipment` varchar(255) NOT NULL,
  `investment_properties` varchar(255) NOT NULL,
  `intangible_assets` varchar(255) NOT NULL,
  `other_fixed_assets` varchar(255) NOT NULL,
  `total_non_current_assets` varchar(255) NOT NULL,
  `biological_assets` varchar(255) NOT NULL,
  `inventories` varchar(255) NOT NULL,
  `trade_and_receivables` varchar(255) NOT NULL,
  `prepayments` varchar(255) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `cash` varchar(255) NOT NULL,
  `other_current_assets` varchar(255) NOT NULL,
  `total_current_assets` varchar(255) NOT NULL,
  `total_assets` varchar(255) NOT NULL,
  `issue_share_capital` varchar(255) NOT NULL,
  `share_premium` varchar(255) NOT NULL,
  `revenue_reserves` varchar(255) NOT NULL,
  `capital_reserves` varchar(255) NOT NULL,
  `other_capital_and_revenue_reserves` varchar(255) NOT NULL,
  `equity_attributed_to_owners_of_parent` varchar(255) NOT NULL,
  `non_controlling_interest` varchar(255) NOT NULL,
  `total_equity` varchar(255) NOT NULL,
  `long_term_borrowings` varchar(255) NOT NULL,
  `deffered_tax_liabilities` varchar(255) NOT NULL,
  `other_non_current_liabilities` varchar(255) NOT NULL,
  `total_non_current_liabilities` varchar(255) NOT NULL,
  `short_term_borrowings` varchar(255) NOT NULL,
  `trade_and_other_payables` varchar(255) NOT NULL,
  `current_tax_liability` varchar(255) NOT NULL,
  `other_current_liabilities` varchar(255) NOT NULL,
  `total_current_liabilities` varchar(255) NOT NULL,
  `total_liabilities` varchar(255) NOT NULL,
  `total_equity_and_liabilities` varchar(255) NOT NULL,
  PRIMARY KEY (`balance_sheet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=223 ;

--
-- Dumping data for table `balance_sheet`
--

INSERT INTO `balance_sheet` (`balance_sheet_id`, `user_id`, `year`, `is_audited`, `property_plant_and_equipment`, `investment_properties`, `intangible_assets`, `other_fixed_assets`, `total_non_current_assets`, `biological_assets`, `inventories`, `trade_and_receivables`, `prepayments`, `bank`, `cash`, `other_current_assets`, `total_current_assets`, `total_assets`, `issue_share_capital`, `share_premium`, `revenue_reserves`, `capital_reserves`, `other_capital_and_revenue_reserves`, `equity_attributed_to_owners_of_parent`, `non_controlling_interest`, `total_equity`, `long_term_borrowings`, `deffered_tax_liabilities`, `other_non_current_liabilities`, `total_non_current_liabilities`, `short_term_borrowings`, `trade_and_other_payables`, `current_tax_liability`, `other_current_liabilities`, `total_current_liabilities`, `total_liabilities`, `total_equity_and_liabilities`) VALUES
(213, 'makazatinashe2000@gmail.com', '2015', '', '34143300   ', '36255860   ', '817074   ', '10040368   ', '81256602', '499429   ', '9111164   ', '8989383   ', '2741275   ', '1154837   ', '1154837   ', '228931   ', '23879856', '105136458', '3571023   ', '2898801   ', '', '26042724   ', '', '32512548', '', '32512548', '', '', '', '0', '', '', '', '', '0', '0', '32512548'),
(214, 'makazatinashe2000@gmail.com', '2016', '', '', '', '20', '   ', '20', '   ', '<br /><b>Notice</b>:  Undefined index: Inventories in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>221</b><br /> ', '   ', '   ', '', ' ', '   ', '0', '20', '   ', '   ', '', '   ', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(215, 'makazatinashe2000@gmail.com', '2018', '', '', '', '', '   ', '0', '   ', '   ', '   ', '   ', '', ' ', '   ', '0', '0', '   ', '   ', '', '   ', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(216, 'makazatinashe2000@gmail.com', '2019', '', '', '', '', '   ', '0', '   ', '   ', '   ', '   ', '', ' ', '   ', '0', '0', '   ', '   ', '', '   ', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(217, 'makazatinashe2000@gmail.com', '2020', '', '', '', '', '   ', '0', '   ', '   ', '   ', '   ', '', ' ', '   ', '0', '0', '   ', '   ', '', '   ', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(218, 'makowe@gmail.com', '2015', '', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>170</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>180</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>189</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>198</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>211</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>220</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>229</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>238</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>248</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>257</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>266</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>281</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>290</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: i in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>299</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(219, 'makowe@gmail.com', '2016', '', '', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>181</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>190</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>199</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>212</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>221</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>230</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>239</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>258</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>267</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>282</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>291</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>300</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(220, 'makowe@gmail.com', '2018', '', '', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>183</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>192</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>201</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>214</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>223</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>232</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>241</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>260</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>269</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>284</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>293</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: c in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>302</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(221, 'makowe@gmail.com', '2019', '', '', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>184</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>193</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>202</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>215</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>224</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>233</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>242</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>261</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>270</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>285</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>294</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: d in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>303</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0'),
(222, 'makowe@gmail.com', '2020', '', '', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>185</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>194</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>203</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>216</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: a in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>225</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>234</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>243</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>262</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>271</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '0', '0', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>286</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>295</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '<br /><b>Notice</b>:  Undefined variable: e in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>304</b><br /><br /><b>Notice</b>:  Undefined index:  in <b>C:xampphtdocssmeportalstage_3_financials_balancesheet.php</b> on line <b>', '', '0', '', '0', '', '', '', '0', '', '', '', '', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `business_sector`
--

CREATE TABLE IF NOT EXISTS `business_sector` (
  `business_sector_id` int(255) NOT NULL AUTO_INCREMENT,
  `sector_name` varchar(255) NOT NULL,
  `users_id` int(255) NOT NULL,
  PRIMARY KEY (`business_sector_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `business_sector`
--

INSERT INTO `business_sector` (`business_sector_id`, `sector_name`, `users_id`) VALUES
(1, 'agriculture1', 0),
(2, 'development', 0),
(3, 'finance', 0);

-- --------------------------------------------------------

--
-- Table structure for table `capital_requirements`
--

CREATE TABLE IF NOT EXISTS `capital_requirements` (
  `capital_req_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `company_name` varchar(245) DEFAULT NULL,
  `company_registration_number` varchar(245) DEFAULT NULL,
  `date_of_incorporation` varchar(245) DEFAULT NULL,
  `country_of_incorporation` varchar(245) DEFAULT NULL,
  `type_of_entity` varchar(245) DEFAULT NULL,
  `business_sector` varchar(245) DEFAULT NULL,
  `nature_of_business` varchar(245) DEFAULT NULL,
  `business_nature` varchar(245) DEFAULT NULL,
  `telephone` varchar(245) DEFAULT NULL,
  `fax_number` varchar(245) DEFAULT NULL,
  `registered_office_physical_address` text,
  `postal_address` text,
  `principal_place_of_business` text,
  `raised_equity` varchar(245) DEFAULT NULL,
  `raised_debt` varchar(245) DEFAULT NULL,
  `raised_other` varchar(245) DEFAULT NULL,
  `purpose_of_funds` varchar(245) DEFAULT NULL,
  `app_status` int(255) NOT NULL DEFAULT '1',
  `share_on_offer` varchar(255) NOT NULL,
  PRIMARY KEY (`capital_req_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `capital_requirements`
--

INSERT INTO `capital_requirements` (`capital_req_id`, `user_id`, `company_name`, `company_registration_number`, `date_of_incorporation`, `country_of_incorporation`, `type_of_entity`, `business_sector`, `nature_of_business`, `business_nature`, `telephone`, `fax_number`, `registered_office_physical_address`, `postal_address`, `principal_place_of_business`, `raised_equity`, `raised_debt`, `raised_other`, `purpose_of_funds`, `app_status`, `share_on_offer`) VALUES
(5, 'makazatinashe2000@gmail.com', 'Escrow Systems mobhala', '12345678909876', '2018-04-03', 'zimbabwe', 'Corporation', 'Agriculture', 'IT', '', '1234567881', '2221113321', 'Zb Center', ' Zb Center', ' Zb Center', '11111', '65000', '1000', 'New Project1\r\n', 1, '12'),
(6, 'tatty@gmail', 'delta', '12345', '12 january 2017', 'zimabwe', 'Corporation', 'Agriculture', 'narture', '', '0771168282', '1233', '12 harare zimba', 'eee', 'harare', '1000', '5000', '2000', 'finance', 1, ''),
(7, 'anthony@gmail.com', 'escrow', '12345', '2018-04-10', 'zimbabwe', 'Corporation', 'Agriculture', 'partnership', '', '08765566612', '12', 'harare', 'kadoma', 'harare', '20', '20', '20', 'developing', 1, ''),
(8, 'sane@gmail.com', 'hghgjgjhg', '345', '2018-04-03', 'zimabwean', 'Corporation', 'Agriculture', 'harare', '', '1234545', '234', 'harare', 'harare', 'harare', 'jfjfjf', 'mvmvmv', 'hdhdhdhdhd', 'mjfmfmfm', 1, ''),
(9, 'tinashe@ctrade.co.zw', 'sasdasd', '23456576', '2018-04-12', 'sdfsdf', 'Corporation', 'Agriculture', 'dsfsd', '', '234234', '3424234', 'sdfsdf', 'sdfsdf', 'sdfsdf', '123123', '12123', '123', 'sdfsf', 1, ''),
(10, 'tinashe@ctrade.co.zw', 'makaaaa', '123456', '2018-04-10', 'Zimbabwe', 'Partnership', 'Agriculture', 'sector', '', '1234565', '13444', 'hara', 'gdgd', 'hdhd', '12', '12', '12', 'gdhdhd', 1, ''),
(11, 'tariro@gmail.com', 'Tariro', '123456432', '2018-04-12', 'Zimbabwe', 'Corporation', 'Agriculture', 'My Business', '', '0772 876187', '13732', 'My address', 'Myajdshsad', 'My hsghdhg', '378493913', '457362892', '27387890', 'My GF', 1, ''),
(12, 'tinashe@escrowgroup.org', 'Tinashe Company', '12234567', '2018-04-11', 'Zimbabwe', 'Corporation', 'Agriculture', 'Harare', '', '123456', '2345645', 'sdf', 'ffsgd', 'sd', 'sdf', 'sdf', 'dsf', 'sdf', 1, ''),
(13, 'tinashe@finsec.com', 'asdasd', '12231', '2018-04-13', 'zxczxc', 'Corporation', 'Finance', 'asdasd', '', '12321', '2131', 'zcxzc', 'zxczxc', 'zxcxzc', '121312313', '123123', '123123', '1232', 1, ''),
(14, 'daddy@gmail.com', 'zimplats', '1234567', '2018-04-03', 'zimbabwe', 'Corporation', 'Agriculture', 'agric', '', '0771168282', '1334', 'harare', 'chinhoyi', 'harare', '20', '20', '20', 'wesly', 1, ''),
(15, 'ronaldinho@gmail.com', 'gaucho', '12344', '2018-04-03', 'zimbabwean', 'Corporation', 'Agriculture', 'agric', '', '077168282', '12', 'harare', 'harare', 'harare', '12', '12', '12', 'funds', 1, ''),
(16, 'takawira@gmail.com', 'mulla', '0771168282', '2018-05-18', 'zimbabwe', 'Corporation', 'Finance', 'corporate', '', '0771168282', '12345', 'harare', 'mumba harare', 'harare', '420', '20', '20', 'schooling', 1, '20'),
(17, 'makowe@gmail.com', 'test', '1234', '2018-05-18', 'zimbabwe', 'Trust', 'Finance', 'agric', '', '44353535', '12', 'harare', 'harare', 'harare', '20', '20', '20', 'test', 1, '20');

-- --------------------------------------------------------

--
-- Table structure for table `cash_flow`
--

CREATE TABLE IF NOT EXISTS `cash_flow` (
  `cash_flow_id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `is_audited` varchar(255) NOT NULL,
  `profit_loss_before_tax` varchar(255) NOT NULL,
  `adjustments_to_reconcile_cash_before_tax_to_net_cashflows` varchar(255) NOT NULL,
  `net_cash_flow_from_operating_activities` varchar(255) NOT NULL,
  `netcash_used_in_investing_activities` varchar(255) NOT NULL,
  `netcash_used_in_financing_activities` varchar(255) NOT NULL,
  `cash_and_cash_equivalents_at_the_beginning_of_the_year` varchar(255) NOT NULL,
  PRIMARY KEY (`cash_flow_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=139 ;

--
-- Dumping data for table `cash_flow`
--

INSERT INTO `cash_flow` (`cash_flow_id`, `user_id`, `year`, `is_audited`, `profit_loss_before_tax`, `adjustments_to_reconcile_cash_before_tax_to_net_cashflows`, `net_cash_flow_from_operating_activities`, `netcash_used_in_investing_activities`, `netcash_used_in_financing_activities`, `cash_and_cash_equivalents_at_the_beginning_of_the_year`) VALUES
(1, 'tinashe@ctrade.co.zw', '2018', 'T', '', '', '', '', '', ''),
(2, 'tinashe@ctrade.co.zw', '2019', 'T', '', '', '', '', '', ''),
(17, 'ronaldinho@gmail.com', '2015', '', '', '', '', '', '', ''),
(18, 'ronaldinho@gmail.com', '2016', '', '', '', '', '', '', ''),
(19, 'ronaldinho@gmail.com', '2017', '', '', '', '', '', '', ''),
(20, 'ronaldinho@gmail.com', '2018', '', '', '', '', '', '', ''),
(133, 'makazatinashe2000@gmail.com', '2015', '', '', '', '0', '', '', ''),
(134, 'makazatinashe2000@gmail.com', '2016', '', '', '', '0', '', '', ''),
(135, 'makazatinashe2000@gmail.com', '2017', '', '', '', '0', '', '', ''),
(136, 'makazatinashe2000@gmail.com', '2018', '', '', '', '0', '', '', ''),
(137, 'makazatinashe2000@gmail.com', '2019', '', '', '', '0', '', '', ''),
(138, 'makazatinashe2000@gmail.com', '2020', '', '', '', '0', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `committees`
--

CREATE TABLE IF NOT EXISTS `committees` (
  `committees_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `committee_name` varchar(245) DEFAULT NULL,
  `chairman` varchar(245) DEFAULT NULL,
  `attendees` varchar(245) DEFAULT NULL,
  `quoraum` varchar(245) DEFAULT NULL,
  `meeting_frequency` varchar(245) DEFAULT NULL,
  `committee_responsibilities` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`committees_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `committees`
--

INSERT INTO `committees` (`committees_id`, `user_id`, `committee_name`, `chairman`, `attendees`, `quoraum`, `meeting_frequency`, `committee_responsibilities`) VALUES
(1, '', '<>', 'efeff', 'dq', '', 'dddd', 'dddd'),
(2, '', '<>', 'fff', 'ffff', '', 'ffff', 'ffff'),
(6, 'makazatinashe2000@gmail.com', 'escrow', 'simba', 'developers', 'wena', '2', 'development'),
(7, 'anthony@gmail.com', 'fddgd', 'retete', '1323', 'hfjfjf', '12', 'gfhfhfjf'),
(8, 'tinashe@ctrade.co.zw', 'sadasd', 'asdasd', 'asdasd', 'asdas', 'asdasd', 'asdasd'),
(9, 'ronaldinho@gmail.com', 'zidane', 'zidane', 'wes', 'quora', '12', 'mumba'),
(10, 'makowe@gmail.com', 'test', 'test', 'test', 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview`
--

CREATE TABLE IF NOT EXISTS `company_overview` (
  `company_overview_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_history` varchar(245) DEFAULT NULL,
  `company_overview_rights` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`company_overview_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `company_overview`
--

INSERT INTO `company_overview` (`company_overview_id`, `user_id`, `company_overview_history`, `company_overview_rights`) VALUES
(2, 'makazatinashe2000@gmail.com', 'fghghjhggh', 'upload/AngularJSNotesForProfessionals.pdf'),
(3, 'tatty@gmail', '', 'upload/ch28.pdf'),
(4, 'anthony@gmail.com', '', 'upload/Android.pdf'),
(5, 'sane@gmail.com', '', 'upload/'),
(6, 'tinashe@ctrade.co.zw', 'sdfsdfsdfdsfsdfsf', 'upload/tinashe.xlsx'),
(7, 'tariro@gmail.com', 'sdfsdfsf', 'upload/FViewer - code.txt'),
(8, 'tinashe@escrowgroup.org', 'asdasd', 'upload/marketcommentary07032018.doc'),
(9, 'tinashe@finsec.com', '', 'upload/'),
(10, 'daddy@gmail.com', '', 'upload/'),
(11, 'ronaldinho@gmail.com', '', 'upload/'),
(12, 'takawira@gmail.com', '', 'upload/'),
(13, 'makowe@gmail.com', '', 'upload/');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview_customers`
--

CREATE TABLE IF NOT EXISTS `company_overview_customers` (
  `company_overview_customers_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_id` varchar(245) DEFAULT NULL,
  `customer_name` varchar(245) DEFAULT NULL,
  `customer_location` varchar(245) DEFAULT NULL,
  `customer_years_rel` varchar(245) DEFAULT NULL,
  `customer_sales` varchar(245) DEFAULT NULL,
  `customer_trade_terms` varchar(245) DEFAULT NULL,
  `customer_distr_channel` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`company_overview_customers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `company_overview_customers`
--

INSERT INTO `company_overview_customers` (`company_overview_customers_id`, `user_id`, `company_overview_id`, `customer_name`, `customer_location`, `customer_years_rel`, `customer_sales`, `customer_trade_terms`, `customer_distr_channel`) VALUES
(4, 'makazatinashe2000@gmail.com', '', 'ddddfd', 'dfd', 'dfdfdf', 'dfef5', 'ff', 'fffgf1'),
(5, 'makazatinashe2000@gmail.com', '', 'ffffff', 'cdvvdvd', 'dvdv', 'dvdv', 'ddv', 'vdvtbvttfrgrtfr'),
(6, 'tatty@gmail', '', 'nvcncmcmc', 'mccmcmc', 'mnekdoedkd', 'mnvmcmc', 'mcmcmc,c,c', 'mcmcmcmc'),
(7, 'anthony@gmail.com', '', 'jfjfjfjf', 'xnxnxnxnx', '5464764', 'bcncncn', 'ncncmc', 'ncmncmc'),
(8, 'tinashe@ctrade.co.zw', '', 'dsfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf', 'sdfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview_products_services`
--

CREATE TABLE IF NOT EXISTS `company_overview_products_services` (
  `company_overview_products_services_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_id` varchar(245) DEFAULT NULL,
  `product_service_name` varchar(245) DEFAULT NULL,
  `description_of_product` varchar(245) DEFAULT NULL,
  `sales_volume` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`company_overview_products_services_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `company_overview_products_services`
--

INSERT INTO `company_overview_products_services` (`company_overview_products_services_id`, `user_id`, `company_overview_id`, `product_service_name`, `description_of_product`, `sales_volume`) VALUES
(1, 'makazatinashe2000@gmail.com', '', 'product one', 'desc product', '10000'),
(5, 'makazatinashe2000@gmail.com', '', 'tapiwa chivaura', 'shares', 'seee'),
(6, 'makazatinashe2000@gmail.com', '', 'tanka', 'muza', '1234'),
(7, 'makazatinashe2000@gmail.com', '', 'Last Product', 'Desc Last', '40000'),
(8, 'tatty@gmail', '', 'ddd', 'ddd', 'ddd'),
(9, 'anthony@gmail.com', '', 'vfdgdgd', 'bddndnd', 'bndndndnddvfrfr'),
(10, 'tinashe@ctrade.co.zw', '', 'dffgs', 'dsfs', 'sdfsfd');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview_raw_materials`
--

CREATE TABLE IF NOT EXISTS `company_overview_raw_materials` (
  `company_overview_raw_materials_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_id` varchar(245) DEFAULT NULL,
  `name` varchar(245) DEFAULT NULL,
  `desc` varchar(245) DEFAULT NULL,
  `volume` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`company_overview_raw_materials_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `company_overview_raw_materials`
--

INSERT INTO `company_overview_raw_materials` (`company_overview_raw_materials_id`, `user_id`, `company_overview_id`, `name`, `desc`, `volume`) VALUES
(1, 'makazatinashe2000@gmail.com', '', 'nnnnnnnnnnnnnnnnnnnnn', 'asd', 'asd'),
(2, 'makazatinashe2000@gmail.com', '', 'mmmmmmmmmmmmm', 'asd', 'asd'),
(3, '', '', 'hdjdjdjd', '546464t', 'bfhfhf'),
(4, '', '', 'dedd', 'ddeefe', 'cecweeeeeeeee232'),
(5, '', '', 'mulla', 'mubha', '1232dff'),
(10, 'makazatinashe2000@gmail.com', '', 'name', 'desc', 'volume'),
(11, 'makazatinashe2000@gmail.com', '', 'name', 'desc', 'volume'),
(12, 'tatty@gmail', '', 'ddmdmd', 'mdmdmdmd', ' dddmdmd'),
(13, 'anthony@gmail.com', '', 'fmfjfkjfkf', 'mfmvmvmvmv', 'vmvmjvmv'),
(14, 'tinashe@ctrade.co.zw', '', 'sdfsdf', 'sfsdf', 'sdfsdf');

-- --------------------------------------------------------

--
-- Table structure for table `company_overview_suppliers`
--

CREATE TABLE IF NOT EXISTS `company_overview_suppliers` (
  `company_overview_suppliers_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `company_overview_id` varchar(245) DEFAULT NULL,
  `customer_name` varchar(245) DEFAULT NULL,
  `customer_location` varchar(245) DEFAULT NULL,
  `customer_years_rel` varchar(245) DEFAULT NULL,
  `customer_sales` varchar(245) DEFAULT NULL,
  `customer_trade_terms` varchar(245) DEFAULT NULL,
  `customer_distr_channel` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`company_overview_suppliers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `company_overview_suppliers`
--

INSERT INTO `company_overview_suppliers` (`company_overview_suppliers_id`, `user_id`, `company_overview_id`, `customer_name`, `customer_location`, `customer_years_rel`, `customer_sales`, `customer_trade_terms`, `customer_distr_channel`) VALUES
(1, 'makazatinashe2000@gmail.com', '', 'sdad123456', 'asd', 'asd', 'asd', 'asd', 'asd1'),
(2, 'makazatinashe2000@gmail.com', '1', 'asd', 'ads', 'asd', 'dsa', 'asd', 'asd'),
(3, 'makazatinashe2000@gmail.com', '', 'mumo', 'mudjdjd', 'nncncnfhfjfjf', '12332', 'ncncnc', 'nvnvnvnv'),
(4, 'anthony@gmail.com', '', 'bcbcbc', 'c  cmn m m', 'sgsgsgs', 'bbcbc', 'b cxbcbcbc', 'feteyeye'),
(5, 'tinashe@ctrade.co.zw', '', 'sdfsdf', 'sdfsdf', 'sfsf', 'sdfsdf', 'sdfsdf', 'sdfsdff');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_directory`
--

CREATE TABLE IF NOT EXISTS `corporate_directory` (
  `corporate_directory_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `duration_in_stated_business_line` varchar(245) DEFAULT NULL,
  `customer_basecorporates` varchar(245) DEFAULT NULL,
  `customer_baseindividuals` varchar(245) DEFAULT NULL,
  `business_year_end` varchar(245) DEFAULT NULL,
  `business_premises_are` varchar(245) DEFAULT NULL,
  `name_of_holding_company` varchar(245) DEFAULT NULL,
  `contact_person_name` varchar(245) DEFAULT NULL,
  `contact_person_position` varchar(245) DEFAULT NULL,
  `contact_person_cell` varchar(245) DEFAULT NULL,
  `contact_person_telephone` varchar(245) DEFAULT NULL,
  `contact_person_email` varchar(245) DEFAULT NULL,
  `company_secretary_address` text,
  `attorney_office_address` text,
  `accountant_office_address` text,
  `auditors_office_address` text,
  `principal_bankers_address` text,
  `authorised_share_capital` varchar(245) DEFAULT NULL,
  `issued_share_capital` varchar(245) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `nominated_supervisor_id` varchar(255) NOT NULL,
  PRIMARY KEY (`corporate_directory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `corporate_directory`
--

INSERT INTO `corporate_directory` (`corporate_directory_id`, `user_id`, `duration_in_stated_business_line`, `customer_basecorporates`, `customer_baseindividuals`, `business_year_end`, `business_premises_are`, `name_of_holding_company`, `contact_person_name`, `contact_person_position`, `contact_person_cell`, `contact_person_telephone`, `contact_person_email`, `company_secretary_address`, `attorney_office_address`, `accountant_office_address`, `auditors_office_address`, `principal_bankers_address`, `authorised_share_capital`, `issued_share_capital`, `status`, `nominated_supervisor_id`) VALUES
(1, 'makazatinashe2000@gmail.com', '123', '213123', '12312', '123123', 'Self owned', '1231', 'Tinashe Makaza', '123', '0772876187', '0772876187', 'makazatinashe2000@gmail.com', ' 12312                                                                                                             ', '                                                                                                    asdasd', 'adsasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'pending', 'dereck@gmail'),
(2, 'tatty@gmail', '12months', '12', 'moyo', '2019', 'Self owned', 'coper', 'tatemda chivaura', 'director', '077846476474', '1344', 'tatty@gmail', ' harareff', ' ffff', 'fff', 'fff', 'fff', '12', '12', '', ''),
(3, 'anthony@gmail.com', 'gfhfhfhf', 'jgngvnv', 'hfhfhfhjfjf', '2015', 'Self owned', 'hafsffd', 'anthony shumba', 'fsgsgs', '1323333', '23434444', 'anthony@gmail.com', ' andgdhdhd', ' fbhchcchc', 'hhfhfhfhf', 'bhfhfhf', 'cbcbfhbfhf', 'bhfhfhf', 'gfhfhfhfhf', '', ''),
(4, 'sane@gmail.com', 'kfkfkf', 'mvmvmvmv', ' vvvmv', '1234', 'Self owned', 'nfnfnf', 'sane ', 'jgjgjgj', 'jgjgjgjg', 'nmmgmg', 'sane@gmail.com', ' fjfjf  ', '   jfjfjfjf', 'mfmfmf', 'fjfjfj', 'mffkfkf', 'jgjgjg', 'jgjgjgjg', '', ''),
(5, 'tinashe@ctrade.co.zw', 'sdasd', 'asdasd', 'asdasd', 'asdasd', 'Self owned', 'asdasd', 'Tinashe Makazaasdasd', 'asdasd', 'asddasd', 'assdasd', 'tinashe@ctrade.co.zw', ' assdasd    ', '     asd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '', ''),
(6, 'tariro@gmail.com', '123123', 'dffsd', 'sdfsdf', 'dsfsdf', 'Self owned', 'dfsf', 'Tariro Makaza', 'sdf', 'dfsfd', 'sfdsf', 'tariro@gmail.com', ' sdf', ' dsf', 'sdf', 'sf', 'sdf', 'sdfsf', 'sdfsf', '', ''),
(7, 'tinashe@escrowgroup.org', 'sdffsdf', 'sdf', 'sdf', 'sdf', 'Self owned', 'sdf', 'Tinashe Test', 'dsf', 'dsdsf', '1233312', 'tinashe@escrowgroup.org', ' sdfsdf', ' sfdsdf', 'sdfsdf', 'sdfs', 'sdfsdf', 'sdfsdf', 'sdfsdf', '', ''),
(8, 'tinashe@finsec.com', 'asdasd', 'asdasd', 'asdasd', 'asdads', 'Self owned', 'asdasd', 'Tinashe Makaza', 'asdad', 'asdasd', 'asda', 'tinashe@finsec.com', ' asd', ' asd', 'asd', 'asd', 'asd', 'asd', 'asd', '', ''),
(9, 'daddy@gmail.com', '3 years', 'test', 'test', '2019', 'Mortgaged', '12', 'daddy daddy', 'staff', '0771168282', '858589595', 'daddy@gmail.com', 'test', ' test', 'test', 'test', 'test', 'test', 'test', '', ''),
(10, 'ronaldinho@gmail.com', '2years', '12345', 'wes', '2018', 'Self owned', 'escrow', 'ronaldinho gaucho', 'senior developer', '0771168282', '0771168282', 'ronaldinho@gmail.com', ' harare    ', '     harare', 'harare', 'harare', 'harare', 'isssue', 'gaucho', '', ''),
(11, 'takawira@gmail.com', '2 years', 'business', 'mumba', '2019', 'Mortgaged', 'mumba', 'takawira takawira', 'mumba', '0771168282', '123344', 'takawira@gmail.com', ' harare', ' harare', 'harare', 'mubhwa', 'harare', '12', '24', '', ''),
(12, 'makowe@gmail.com', '3 years', 'test', 'test', '2019', 'Mortgaged', 'weny', 'makowe', 'muma', '0771168282', '1234', 'makowe@gmail.com', 'harare ', ' harare', 'harare', 'harare', 'wenny', '12', '12', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_structure`
--

CREATE TABLE IF NOT EXISTS `corporate_structure` (
  `corporate_structure_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`corporate_structure_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `corporate_structure`
--

INSERT INTO `corporate_structure` (`corporate_structure_id`, `user_id`, `desc`) VALUES
(1, 'makazatinashe2000@gmail.com', 'tawanda chivaura CEO'),
(2, 'tatty@gmail', 'ssss'),
(3, 'anthony@gmail.com', 'asnthkvckckvtrgtt'),
(4, 'sane@gmail.com', 'nfnfnfnf'),
(5, 'tinashe@ctrade.co.zw', 'adasdasd'),
(6, 'tariro@gmail.com', ''),
(7, 'tinashe@escrowgroup.org', ''),
(8, 'daddy@gmail.com', 'test'),
(9, 'ronaldinho@gmail.com', 'mulla'),
(10, 'makowe@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE IF NOT EXISTS `directors` (
  `director_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `title` varchar(245) DEFAULT NULL,
  `forename` varchar(245) DEFAULT NULL,
  `surname` varchar(245) DEFAULT NULL,
  `age` varchar(245) DEFAULT NULL,
  `citizenship` varchar(245) DEFAULT NULL,
  `address` text,
  PRIMARY KEY (`director_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`director_id`, `user_id`, `title`, `forename`, `surname`, `age`, `citizenship`, `address`) VALUES
(6, '', '', '', '', '', '', ''),
(7, '', '', '', '', '', '', ''),
(8, '', '', '', '', '', '', ''),
(9, '', '', '', '', '', '', ''),
(10, '', '', '', '', '', '', ''),
(12, 'makazatinashe2000@gmail.com', 'mr', 'tinashe', 'chivaura', '23', 'Zimbabwean', 'harare'),
(13, 'anthony@gmail.com', 'mr', 'fmfmfmf', 'nfnfff', '256', 'Zimbabwean', 'harare'),
(14, 'tinashe@ctrade.co.zw', 'Mrs', 'asdas', 'asdasd', 'asdad', 'asdasd', 'asdasd'),
(15, 'tinashe@ctrade.co.zw', 'asdasd', 'sadasd', 'asd', 'asd', 'asda', 'assd'),
(16, 'daddy@gmail.com', 'mr', 'mbano', 'mbano', '23', 'Zimbabwean', '12 chiedza'),
(17, 'ronaldinho@gmail.com', 'Mr', 'mwana', 'mwana', '34', 'Zimbabwean', 'harare'),
(18, 'makazatinashe2000@gmail.com', 'mr', 'mulla', 'mulla', '25', 'mulla', 'mulaa'),
(19, 'ronaldinho@gmail.com', 'mr', 'alaba', 'alaba', '67', 'Zimbabwean', '12 chiedza'),
(20, 'makowe@gmail.com', 'mr', 'takudzwa', 'makurusi', '30', 'Zimbabwean', 'harare');

-- --------------------------------------------------------

--
-- Table structure for table `directors_shareholding`
--

CREATE TABLE IF NOT EXISTS `directors_shareholding` (
  `directors_shareholding_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `directors_name` varchar(300) DEFAULT NULL,
  `direct_indirect_equity_interest` varchar(245) DEFAULT NULL,
  `shareholding` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`directors_shareholding_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `directors_shareholding`
--

INSERT INTO `directors_shareholding` (`directors_shareholding_id`, `user_id`, `directors_name`, `direct_indirect_equity_interest`, `shareholding`) VALUES
(1, 'makazatinashe2000@gmail.com', 'Tinashe', '12', '34'),
(3, '', '', 'gaza', '<>gaza'),
(4, 'makazatinashe2000@gmail.com', 'tamika', '12', '10'),
(5, 'anthony@gmail.com', 'bdhdhdhdhd', '12', '35'),
(6, 'tinashe@ctrade.co.zw', 'sddf', 'fdsdf', 'sfdsdf'),
(7, 'daddy@gmail.com', 'tawanda', '20', '23'),
(8, 'ronaldinho@gmail.com', 'kaka', '20', '20');

-- --------------------------------------------------------

--
-- Table structure for table `enterprise_size_categorisation`
--

CREATE TABLE IF NOT EXISTS `enterprise_size_categorisation` (
  `enterprise_size_categorisation_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `staff_levels` varchar(245) DEFAULT NULL,
  `annual_turnover` varchar(245) DEFAULT NULL,
  `gross_value_of_assets` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`enterprise_size_categorisation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `enterprise_size_categorisation`
--

INSERT INTO `enterprise_size_categorisation` (`enterprise_size_categorisation_id`, `user_id`, `staff_levels`, `annual_turnover`, `gross_value_of_assets`) VALUES
(1, 'makazatinashe2000@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(2, 'tatty@gmail', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(3, 'anthony@gmail.com', '6 to 40 employees', '$50,001 to $500,000', 'Up to $50,000'),
(4, 'sane@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(5, 'tinashe@ctrade.co.zw', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(6, 'tariro@gmail.com', '6 to 40 employees', '$50,001 to $500,000', '$2,000,001 and above'),
(7, 'tinashe@escrowgroup.org', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(8, 'tinashe@finsec.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(9, 'daddy@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(10, 'ronaldinho@gmail.com', 'Up to 5 employees', 'Up to $50,000', 'Up to $50,000'),
(11, 'takawira@gmail.com', '6 to 40 employees', '$50,001 to $500,000', '$2,000,001 and above'),
(12, 'makowe@gmail.com', '6 to 40 employees', '$50,001 to $500,000', '$50,001 to $1,000,000');

-- --------------------------------------------------------

--
-- Table structure for table `financial_advisors_companies`
--

CREATE TABLE IF NOT EXISTS `financial_advisors_companies` (
  `financial_advisors_companies_id` int(255) NOT NULL AUTO_INCREMENT,
  `users_id` int(255) NOT NULL,
  `names` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`financial_advisors_companies_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `financial_advisors_companies`
--

INSERT INTO `financial_advisors_companies` (`financial_advisors_companies_id`, `users_id`, `names`, `type`) VALUES
(1, 0, 'enerst and young', 'FA'),
(2, 0, 'mulla', ''),
(3, 0, 'muma', 'moma'),
(4, 0, 'delta', 'muma'),
(5, 0, 'makaza', 'gazaland'),
(6, 0, 'FA', 'FA'),
(7, 0, 'guga', 'gubhaaa');

-- --------------------------------------------------------

--
-- Table structure for table `financier_clients_status`
--

CREATE TABLE IF NOT EXISTS `financier_clients_status` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `financier_id` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_financier_status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `financier_clients_status`
--

INSERT INTO `financier_clients_status` (`id`, `financier_id`, `client_id`, `client_financier_status`) VALUES
(1, 'moyo@mail', 'ronaldinho@gmail.com', 'viewed');

-- --------------------------------------------------------

--
-- Table structure for table `human_resource_organogram`
--

CREATE TABLE IF NOT EXISTS `human_resource_organogram` (
  `human_resource_organogram_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `employee_name` varchar(245) DEFAULT NULL,
  `key_position` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`human_resource_organogram_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `human_resource_organogram`
--

INSERT INTO `human_resource_organogram` (`human_resource_organogram_id`, `user_id`, `employee_name`, `key_position`) VALUES
(1, 'makazatinashe2000@gmail.com', 'Tinashe Chivaura', 'Developer'),
(3, 'makazatinashe2000@gmail.com', 'Dereck Chamboko', 'software engineer'),
(4, 'anthony@gmail.com', 'gdgdgd', 'nfnffmf21'),
(5, 'tinashe@ctrade.co.zw', 'sadasd', 'asdasd'),
(6, 'ronaldinho@gmail.com', 'gaucho', 'senior developer');

-- --------------------------------------------------------

--
-- Table structure for table `individual_permission`
--

CREATE TABLE IF NOT EXISTS `individual_permission` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `permission_id` int(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `material_asset_transactions`
--

CREATE TABLE IF NOT EXISTS `material_asset_transactions` (
  `material_asset_transactions_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`material_asset_transactions_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `material_asset_transactions`
--

INSERT INTO `material_asset_transactions` (`material_asset_transactions_id`, `user_id`, `desc`) VALUES
(1, 'makazatinashe2000@gmail.com', ' tinashe chivaura'),
(2, 'anthony@gmail.com', 'eertgtggtg233'),
(3, 'tinashe@escrowgroup.org', ''),
(4, 'tinashe@ctrade.co.zw', ''),
(5, 'ronaldinho@gmail.com', 'gaucho'),
(6, 'makowe@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `material_contracts`
--

CREATE TABLE IF NOT EXISTS `material_contracts` (
  `material_contracts_id` int(255) NOT NULL AUTO_INCREMENT,
  `users_id` varchar(255) NOT NULL,
  `restrictions` varchar(255) NOT NULL,
  `capital_committments` varchar(255) NOT NULL,
  `subgroup` varchar(255) NOT NULL,
  `undisclosed_contracts` varchar(255) NOT NULL,
  `joint_venture` varchar(255) NOT NULL,
  `terminated_contracts` varchar(255) NOT NULL,
  `app_status` int(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`material_contracts_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `material_contracts`
--

INSERT INTO `material_contracts` (`material_contracts_id`, `users_id`, `restrictions`, `capital_committments`, `subgroup`, `undisclosed_contracts`, `joint_venture`, `terminated_contracts`, `app_status`) VALUES
(4, '0', '1391600104CodeIgniter-Sample-Chapter-Chapter-4-UsingCI-to-Simplify-Databases.pdf', '9780735621978_chapter_07.pdf', 'EntityFramework.pdf', 'ch28.pdf', 'Programming ASP.NET MVC 5.pdf', 'SQL_Databases.pdf', 1),
(6, 'ronaldinho@gmail.com', 'AngularJSNotesForProfessionals.pdf', 'ASP.NET and Web Programming.pdf', 'Minutes - 27th Technical Committee Meeting.docx', 'image.png', 'mvc_php_intro.pdf', 'SQL_Databases.pdf', 1),
(11, 'tinashe@escrowgroup.org', '', '', '', '', '', '', 1),
(12, 'tinashe@ctrade.co.zw', '', '', '', '', '', '', 1),
(15, '', 'Android.pdf', '', '', '', '', '', 1),
(16, 'makazatinashe2000@gmail.com', 'Android.pdf', 'AngularJSNotesForProfessionals.pdf', 'ch28.pdf', 'doc_4.pdf', 'doc_5.pdf', 'Structured Query Language.pdf', 1),
(17, 'makowe@gmail.com', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `material_litigation`
--

CREATE TABLE IF NOT EXISTS `material_litigation` (
  `material_litigation_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`material_litigation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `material_litigation`
--

INSERT INTO `material_litigation` (`material_litigation_id`, `user_id`, `desc`) VALUES
(1, 'makazatinashe2000@gmail.com', 'tinashe chivaura'),
(2, 'anthony@gmail.com', 'hfhfhfhff'),
(3, 'tinashe@escrowgroup.org', ''),
(4, 'tinashe@ctrade.co.zw', ''),
(5, 'ronaldinho@gmail.com', 'gaucho'),
(6, 'makowe@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE IF NOT EXISTS `parameters` (
  `parameter_id` int(255) NOT NULL AUTO_INCREMENT,
  `data` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`parameter_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`parameter_id`, `data`, `value`) VALUES
(1, 'threshold score', '16'),
(2, 'tinashe@gmail.com', 'huya'),
(3, 'mum', '12');

-- --------------------------------------------------------

--
-- Table structure for table `para_countries`
--

CREATE TABLE IF NOT EXISTS `para_countries` (
  `country_id` int(255) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) NOT NULL,
  `users_id` varchar(255) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `para_countries`
--

INSERT INTO `para_countries` (`country_id`, `country_name`, `users_id`) VALUES
(1, 'zimababwe1', ''),
(2, 'South Africa1rfrfrdfrr', ''),
(3, 'Zambia', ''),
(4, 'Tanzania', ''),
(5, 'Nigeria', ''),
(6, 'Ghana', ''),
(7, 'Kenya', ''),
(8, 'Namibia', ''),
(9, 'Egypt', ''),
(10, 'Morrocco', ''),
(11, 'mane', 'tinashe@gmail.com'),
(12, 'america', 'tinashe@gmail.com'),
(13, 'mozambique', 'tinashe@gmail.com'),
(14, 'BRAZIL', 'tinashe@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EmailAddress` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Company` varchar(50) CHARACTER SET utf8 NOT NULL,
  `Price` varchar(50) CHARACTER SET utf8 NOT NULL,
  `PaymentStatus` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PostedStatus` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PaynowRef` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `PollUrl` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `payments_ref` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`Id`, `EmailAddress`, `Company`, `Price`, `PaymentStatus`, `PostedStatus`, `PaynowRef`, `PollUrl`, `Date`, `payments_ref`) VALUES
(2, 'tinashe@ctrade.co.zw', 'Old Mutual', '200', 'rejected', '', '', '', '2018-04-12 19:19:38', '3'),
(8, 'tinashe@escrowgroup.org', 'IMARA', '100.00', 'Cancelled', '', '1075470', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=e1604c46-e4e9-4b6b-a752-fedb301c3cbc', '2018-04-12 22:59:05', '492321'),
(9, 'tinashe@finsec.com', 'Deloite', '100.00', 'Cancelled', '', '1076229', 'https://www.paynow.co.zw/Interface/CheckPayment/?guid=e6819ced-2016-4576-9b62-9c72e8ae6bfc', '2018-04-13 10:35:22', '445684'),
(10, 'makazatinashe2000@gmail.com', 'Enerst n Young', '100', 'PENDING', '', '', '', '2018-04-13 15:43:38', '286959'),
(11, 'makazatinashe2000@gmail.com', 'Enerst n Young', '100', 'PENDING', '', '', '', '2018-04-13 15:46:58', '693618'),
(12, 'makazatinashe2000@gmail.com', 'First Mutual', '100', 'PENDING', '', '', '', '2018-04-13 15:47:39', '614682'),
(13, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'Cancelled', '', '', '', '2018-04-17 14:18:35', '243893'),
(14, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'PENDING', '', '', '', '2018-04-17 14:45:27', '697436'),
(15, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'PENDING', '', '', '', '2018-04-17 14:45:42', '326483'),
(16, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'PENDING', '', '', '', '2018-04-17 14:49:11', '610754'),
(21, 'daddy@gmail.com', 'FINSEC', '100', 'accepted', '', '', '', '2018-04-24 14:39:15', '254797'),
(22, 'ronaldinho@gmail.com', 'FINSEC', '100', 'accepted', '', '', '', '2018-04-26 22:07:11', '696859'),
(23, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-07 13:02:00', '413659'),
(24, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-09 11:40:49', '115216'),
(25, 'makazatinashe2000@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-11 13:31:22', '637341'),
(26, 'makowe@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-18 09:23:40', '645169'),
(27, 'makowe@gmail.com', 'FINSEC', '100', 'cancelled', '', '', '', '2018-05-18 09:23:42', '646212');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `page`, `user_type`, `title`) VALUES
(3, 'f_dashboard.php', 'Financial_Advisor', 'Acceptance'),
(4, 'f_ver_dashboard.php', 'Financial_Advisor', 'Review & Verification'),
(5, 'f_rec_dashboard.php', 'Financial_Advisor', 'Report Compilation'),
(6, 'f_rec_dashboard.php', 'Financial_Advisor ', 'Report Compilation'),
(7, 'f_approver_dashboard.php', 'Financial_Advisor', 'Approver'),
(8, 'dashboard.php', 'Financial_Advisor', 'Home'),
(9, 'fin_dashboard.php', 'Financier', 'Financier');

-- --------------------------------------------------------

--
-- Table structure for table `permission_group`
--

CREATE TABLE IF NOT EXISTS `permission_group` (
  `group_id` int(255) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) NOT NULL,
  `page_ids` text NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `permission_group`
--

INSERT INTO `permission_group` (`group_id`, `group_name`, `page_ids`) VALUES
(1, 'Financier', '3,7'),
(2, 'Financial Advisor', '7,4,5'),
(3, 'Acceptor', '3');

-- --------------------------------------------------------

--
-- Table structure for table `personal_financial`
--

CREATE TABLE IF NOT EXISTS `personal_financial` (
  `personal_financial_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `guarantor_name` varchar(245) DEFAULT NULL,
  `financial_institution` varchar(245) DEFAULT NULL,
  `facility_type` varchar(245) DEFAULT NULL,
  `facility_amount` varchar(245) DEFAULT NULL,
  `monthly_installment` varchar(245) DEFAULT NULL,
  `balance` varchar(245) DEFAULT NULL,
  `tenure` varchar(245) DEFAULT NULL,
  `start_date` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`personal_financial_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `personal_financial`
--

INSERT INTO `personal_financial` (`personal_financial_id`, `user_id`, `guarantor_name`, `financial_institution`, `facility_type`, `facility_amount`, `monthly_installment`, `balance`, `tenure`, `start_date`) VALUES
(6, '', '', '', '', '', '', '', '', ''),
(7, '', '', '', '', '', '', '', '', ''),
(8, '', '', '', '', '', '', '', '', ''),
(9, '', '', '', '', '', '', '', '', ''),
(10, 'anthony@gmail.com', 'hvjvjfjgf', 'mbmbmbm', 'mbvmbmb', 'nfnvnv', 'nfjfjfmjf', 'gjjgjg', 'nmvmvmv', 'hfhfhfhf');

-- --------------------------------------------------------

--
-- Table structure for table `profit_loss`
--

CREATE TABLE IF NOT EXISTS `profit_loss` (
  `profit_loss_id` int(255) NOT NULL AUTO_INCREMENT,
  `users_id` varchar(255) NOT NULL,
  `is_audited` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `cost_of_goods` varchar(255) NOT NULL,
  `rendering_of_services` varchar(255) NOT NULL,
  `other` varchar(255) NOT NULL,
  `total_revenue` varchar(255) NOT NULL,
  `cost_of_sales` varchar(255) NOT NULL,
  `gross_profit` varchar(255) NOT NULL,
  `other_operating_income` varchar(255) NOT NULL,
  `other_operating_expenses` varchar(255) NOT NULL,
  `staff_costs` varchar(255) NOT NULL,
  `depreciation_and_ammortisation` varchar(255) NOT NULL,
  `operating_profit` varchar(255) NOT NULL,
  `increase_in_fair_value_adjustments_impairments` varchar(255) NOT NULL,
  `decrease_in_fair_value_adjustments_impairments` varchar(255) NOT NULL,
  `finance_costs` varchar(255) NOT NULL,
  `finance_income` varchar(255) NOT NULL,
  `profit_loss_before_tax` varchar(255) NOT NULL,
  `income_tax_expense` varchar(255) NOT NULL,
  `profit_loss_for_the_year` varchar(255) NOT NULL,
  `equity_holders_of_parent` varchar(255) NOT NULL,
  `non_controlling_interests` varchar(255) NOT NULL,
  `total_attributed` varchar(255) NOT NULL,
  PRIMARY KEY (`profit_loss_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

--
-- Dumping data for table `profit_loss`
--

INSERT INTO `profit_loss` (`profit_loss_id`, `users_id`, `is_audited`, `year`, `cost_of_goods`, `rendering_of_services`, `other`, `total_revenue`, `cost_of_sales`, `gross_profit`, `other_operating_income`, `other_operating_expenses`, `staff_costs`, `depreciation_and_ammortisation`, `operating_profit`, `increase_in_fair_value_adjustments_impairments`, `decrease_in_fair_value_adjustments_impairments`, `finance_costs`, `finance_income`, `profit_loss_before_tax`, `income_tax_expense`, `profit_loss_for_the_year`, `equity_holders_of_parent`, `non_controlling_interests`, `total_attributed`) VALUES
(80, 'makazatinashe2000@gmail.com', '', '2015', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(81, 'makazatinashe2000@gmail.com', '', '2016', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(82, 'makazatinashe2000@gmail.com', '', '2017', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(83, 'makazatinashe2000@gmail.com', '', '2018', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(84, 'makazatinashe2000@gmail.com', '', '2019', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0'),
(85, 'makazatinashe2000@gmail.com', '', '2020', '', '', '', '0', '', '0', '', '', '', '', '0', '', '', '', '', '0', '', '0', '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `recommendations`
--

CREATE TABLE IF NOT EXISTS `recommendations` (
  `recommendation_id` int(255) NOT NULL AUTO_INCREMENT,
  `applicant_id` varchar(255) NOT NULL,
  `recommendation` varchar(255) NOT NULL,
  PRIMARY KEY (`recommendation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `recommendations`
--

INSERT INTO `recommendations` (`recommendation_id`, `applicant_id`, `recommendation`) VALUES
(1, 'ronaldinho@gmail.com', 'ronaldinho@gmail.com'),
(2, 'makazatinashe2000@gmail.com', 'satisfactory information'),
(3, 'makowe@gmail.com', 'outstanding');

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE IF NOT EXISTS `scores` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `score_cate_id` int(255) NOT NULL,
  `form_input` varchar(255) NOT NULL,
  `score` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`id`, `score_cate_id`, `form_input`, `score`) VALUES
(1, 1, 'company_name', 1),
(2, 1, 'Company_registration_number', 1),
(3, 1, 'Date_of_Incorporation', 1),
(4, 1, 'Country_of_Incorporation', 1),
(5, 1, 'Type_of_Entity', 1),
(6, 1, 'Business_Sector', 1),
(7, 1, 'Nature_of_business', 1),
(8, 1, 'Telephone', 1),
(9, 1, 'Fax_Number', 1),
(10, 1, 'Registered_Office_Physical_Address', 1),
(11, 1, 'Principal_Place_of_Business', 1),
(12, 1, 'Postal_Address', 1),
(13, 1, 'Equity', 1),
(14, 1, 'share_under_offer', 1),
(15, 1, 'Debt', 1),
(16, 1, 'Other', 1),
(17, 1, 'Purpose_of_funds', 1),
(18, 2, 'Duration_in_stated_business_line', 1),
(19, 2, 'Corporates', 1),
(20, 2, 'Induviduals', 1),
(21, 2, 'Business_Year_End', 1),
(22, 2, 'Business_Premises_are', 1),
(23, 2, 'Name_of_holding_company', 1),
(24, 2, 'Name', 1),
(25, 2, 'Position', 1),
(26, 2, 'Tel', 1),
(27, 2, 'Cell', 1),
(28, 2, 'Email', 1),
(29, 2, 'Company_Secretary_Address', 1),
(30, 2, 'Attorney_office_Address ', 1),
(31, 2, 'Accountant_Office', 1),
(32, 2, 'Auditors_Office', 1),
(33, 2, 'Principal_Bankers', 1),
(34, 2, 'Authorised_share_capital', 1),
(35, 2, 'Issued_share_capital ', 1),
(36, 3, 'Staff_Levels', 1),
(37, 3, 'Gross_Value_of_Assets', 1),
(38, 4, 'Introduction_History_and_Major_Milestones', 1),
(39, 4, 'Company_Products_Services', 1),
(40, 4, 'Company_Major_Raw_Materials', 1),
(41, 4, 'Major_Customers_Distribution_Channel', 1),
(42, 4, 'Major_Suppliers_Channel', 1),
(57, 4, 'The_Company_Protected_Rights ', 1),
(58, 5, 'proprietors_partners_directors_profile', 1),
(59, 6, 'directors_shareholding', 1),
(60, 7, 'board_committees', 1),
(61, 8, 'corporate_structure', 1),
(62, 9, 'human_resource_organogram', 1),
(63, 10, 'company_shareholder', 1),
(64, 11, 'contracts_restrictions', 1),
(65, 11, 'contracts_of_ material_capital_commitments', 1),
(66, 11, 'contracts_with subsidiaries_of_other_group companies', 1),
(67, 11, 'undisclosed_contracts', 1),
(68, 11, 'material_joint_venture', 1),
(69, 11, 'terminated_contracts', 1),
(70, 12, 'material_asset_transactions', 1),
(71, 11, 'material_litigation_and_claims', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scores_cate`
--

CREATE TABLE IF NOT EXISTS `scores_cate` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `scores_category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `scores_cate`
--

INSERT INTO `scores_cate` (`id`, `scores_category`) VALUES
(1, 'capital_req'),
(2, 'corporate_directory'),
(3, 'enterprise_category'),
(4, 'company_overview'),
(5, 'proprietors_partners_principals_and_directors_profile'),
(6, 'directors_shareholding'),
(7, 'board_committees'),
(8, 'corporate_structure'),
(9, 'human_resouce_organogram'),
(10, 'company_shareholders'),
(11, 'material_and_other_third_party_contracts'),
(12, 'material_asset_transactions'),
(13, 'material_litigation_and_claims'),
(14, 'balance_sheet'),
(15, 'cash_flow'),
(16, 'income_statement'),
(17, 'financial_committments');

-- --------------------------------------------------------

--
-- Table structure for table `shareholders_shareholding`
--

CREATE TABLE IF NOT EXISTS `shareholders_shareholding` (
  `directors_shareholding_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(245) DEFAULT NULL,
  `directors_name` varchar(245) DEFAULT NULL,
  `direct_indirect_equity_interest` varchar(245) DEFAULT NULL,
  `shareholding` varchar(245) DEFAULT NULL,
  PRIMARY KEY (`directors_shareholding_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `shareholders_shareholding`
--

INSERT INTO `shareholders_shareholding` (`directors_shareholding_id`, `user_id`, `directors_name`, `direct_indirect_equity_interest`, `shareholding`) VALUES
(1, 'makazatinashe2000@gmail.com', 'tawanda', '2', '2'),
(2, 'anthony@gmail.com', 'nfcnfjnfjfj', '13232', 'jfjfjfjf'),
(3, 'tinashe@ctrade.co.zw', 'sadasd', 'asdasd', 'adasd'),
(4, 'daddy@gmail.com', 'test', '12', '12'),
(5, 'ronaldinho@gmail.com', 'scholari', '12', '24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_fullname` varchar(345) DEFAULT NULL,
  `users_username` varchar(245) DEFAULT NULL,
  `users_password` varchar(345) DEFAULT NULL,
  `users_email` varchar(345) DEFAULT NULL,
  `users_phonenumber` varchar(245) DEFAULT NULL,
  `users_position` varchar(145) DEFAULT NULL,
  `users_status` varchar(45) DEFAULT NULL,
  `users_created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `users_company_name` varchar(305) DEFAULT NULL,
  `users_company_regnum` varchar(245) DEFAULT NULL,
  `users_company_dateofincorp` date DEFAULT NULL,
  `users_company_country` varchar(245) DEFAULT NULL,
  `users_form_name` varchar(415) DEFAULT NULL,
  `users_type` varchar(255) NOT NULL,
  `users_advance_score` varchar(145) DEFAULT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `users_fullname`, `users_username`, `users_password`, `users_email`, `users_phonenumber`, `users_position`, `users_status`, `users_created_date`, `users_company_name`, `users_company_regnum`, `users_company_dateofincorp`, `users_company_country`, `users_form_name`, `users_type`, `users_advance_score`) VALUES
(13, 'Tinashe Makaza', '', 'tinashe', 'makazatinashe2000@gmail.com', '0772876187', '', '', '2018-03-01 16:17:34', '', '', '0000-00-00', '', '', 'applicant', NULL),
(14, 'tinashe chivaura', 'akatendeka', '12345', 'tinashe@gmail.com', NULL, NULL, NULL, '2018-04-03 16:25:39', NULL, NULL, NULL, NULL, NULL, 'Administrator', NULL),
(28, 'Tinashe Makaza', '', 'asdfghjkl', 'tinashe@ctrade.co.zw', '772876187', 'applicant', 'applicant', '2018-04-12 12:57:05', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', '1'),
(31, 'Tinashe Makaza', '', 'tinashe', 'tinashe@finsec.com', '+263772876187', 'applicant', 'applicant', '2018-04-13 10:33:38', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', '1'),
(39, 'makowe', '', '12345', 'makowe@gmail.com', '12233434', 'applicant', 'applicant', '2018-05-18 09:18:39', 'applicant', 'applicant', '0000-00-00', 'applicant', 'applicant', 'applicant', NULL),
(40, 'financier', '', '12345', 'financier@gmail.com', '0771168282', 'Financier', 'Financier', '2018-05-18 12:29:22', 'Financier', 'Financier', '0000-00-00', 'Financier', 'Financier', 'Financier', NULL),
(41, 'analyst', '', '12345', 'analyst@gmail.com', '0771168282', 'Analyst', 'Analyst', '2018-05-18 12:29:58', 'Analyst', 'Analyst', '0000-00-00', 'Analyst', 'Analyst', 'Analyst', NULL),
(43, 'admin', '', '12345', 'admin@gmail.com', '12355', 'Administrator', 'Administrator', '2018-05-18 12:42:15', 'Administrator', 'Administrator', '0000-00-00', 'Administrator', 'Administrator', 'Administrator', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_scores`
--

CREATE TABLE IF NOT EXISTS `user_scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_scores_category_id` int(100) NOT NULL,
  `user_scores_id` varchar(255) NOT NULL,
  `user_scores_total` text NOT NULL,
  `scores_total` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_scores`
--

INSERT INTO `user_scores` (`id`, `user_scores_category_id`, `user_scores_id`, `user_scores_total`, `scores_total`) VALUES
(1, 1, 'makazatinashe2000@gmail.com', '[{"1":1,"2":1,"3":1,"4":1,"5":1,"6":1,"7":1,"8":1,"9":1,"10":1,"11":1,"12":1,"13":1,"14":1,"15":1,"16":1,"17":1}]', '35'),
(2, 2, 'makowe@gmail.com', '[{"1":1,"2":1,"3":1,"4":1,"5":1,"6":1,"7":1,"8":1,"9":1,"10":1,"11":1,"12":1,"13":1,"14":1,"15":1,"16":1,"17":1}]', '49');

-- --------------------------------------------------------

--
-- Table structure for table `verification_and_review`
--

CREATE TABLE IF NOT EXISTS `verification_and_review` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `correct_items` varchar(1000) NOT NULL,
  `form` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=158 ;

--
-- Dumping data for table `verification_and_review`
--

INSERT INTO `verification_and_review` (`id`, `user_id`, `correct_items`, `form`) VALUES
(122, 'ronaldinho@gmail.com', '1', 'capital'),
(123, 'ronaldinho@gmail.com', '2', 'capital'),
(124, 'ronaldinho@gmail.com', '3', 'capital'),
(125, 'ronaldinho@gmail.com', '4', 'capital'),
(126, 'ronaldinho@gmail.com', '5', 'capital'),
(127, 'ronaldinho@gmail.com', '6', 'capital'),
(128, 'ronaldinho@gmail.com', '7', 'capital'),
(129, 'ronaldinho@gmail.com', '8', 'capital'),
(130, 'ronaldinho@gmail.com', '9', 'capital'),
(131, 'ronaldinho@gmail.com', '10', 'capital'),
(132, 'ronaldinho@gmail.com', '11', 'capital'),
(133, 'ronaldinho@gmail.com', '12', 'capital'),
(134, 'ronaldinho@gmail.com', '13', 'capital'),
(135, 'ronaldinho@gmail.com', '16', 'capital'),
(136, 'ronaldinho@gmail.com', '14', 'capital'),
(137, 'ronaldinho@gmail.com', '17', 'capital'),
(138, 'ronaldinho@gmail.com', '18', 'corporatedirectory'),
(139, 'ronaldinho@gmail.com', '19', 'corporatedirectory'),
(140, 'ronaldinho@gmail.com', '20', 'corporatedirectory'),
(141, 'ronaldinho@gmail.com', '21', 'corporatedirectory'),
(142, 'ronaldinho@gmail.com', '22', 'corporatedirectory'),
(143, 'ronaldinho@gmail.com', '23', 'corporatedirectory'),
(144, 'ronaldinho@gmail.com', '24', 'corporatedirectory'),
(145, 'ronaldinho@gmail.com', '25', 'corporatedirectory'),
(146, 'ronaldinho@gmail.com', '17', 'boarddirectors'),
(151, 'makazatinashe2000@gmail.com', '12', 'boarddirectors'),
(152, 'makazatinashe2000@gmail.com', '1', 'capital'),
(153, 'makazatinashe2000@gmail.com', '2', 'capital'),
(154, 'makazatinashe2000@gmail.com', '12', 'capital'),
(155, 'makazatinashe2000@gmail.com', '16', 'capital'),
(156, 'makazatinashe2000@gmail.com', '18', 'corporatedirectory'),
(157, 'makazatinashe2000@gmail.com', '19', 'corporatedirectory');

-- --------------------------------------------------------

--
-- Table structure for table `verification_scoring_parameter`
--

CREATE TABLE IF NOT EXISTS `verification_scoring_parameter` (
  `verification_id` int(11) NOT NULL AUTO_INCREMENT,
  `input_id` varchar(255) NOT NULL,
  `input_name` varchar(255) NOT NULL,
  `score` varchar(255) NOT NULL,
  PRIMARY KEY (`verification_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `verification_scoring_parameter`
--

INSERT INTO `verification_scoring_parameter` (`verification_id`, `input_id`, `input_name`, `score`) VALUES
(1, '1', 'Company_name', '1'),
(2, '2', 'Company_registration_number', '1'),
(3, '3', 'Date_of_Incorporation', '1'),
(4, '4', 'Country_of_Incorporation', '1'),
(5, '5', 'Type_of_Entity', '1'),
(6, '6', 'Business_Sector', '1'),
(7, '7', 'Telephone', '1'),
(8, '8', 'Fax_Number', '1'),
(9, '9', 'Nature_of_business', '1'),
(10, '10', 'Registered_Office_Physical_Address', '1'),
(11, '11', 'Postal_Address', '1'),
(12, '12', 'Principal_Place_of_Business', '1'),
(13, '13', 'Equity', '1'),
(14, '14', 'Debt', '1'),
(15, '15', 'Share_on_Offer', '1'),
(16, '16', 'other', '1'),
(17, '17', 'purpose_of_funds', '1'),
(18, '18', 'Duration_in_stated_business_line', '1'),
(19, '19', 'Corporates', '1'),
(20, '20', 'Induviduals', '1'),
(21, '21', 'Principal_Bankers ', '1'),
(22, '22', 'Business_Year_End', '1'),
(23, '23', 'Business_Premises_are', '1'),
(24, '24', 'Name_of_holding_company', '1'),
(25, '25', 'Contact_person_Name', '1'),
(26, '26', 'Contact_person_Position', '1'),
(27, '27', 'Contact_person_Cell', '1'),
(28, '28', 'Tel', '1'),
(29, '29', 'Email', '1'),
(30, '30', 'Company_Secretary_Address', '1'),
(31, '31', 'Attorney_Office_Address', '1'),
(32, '32', 'Auditors_Office_Address', '1'),
(33, '33', 'Accountant_Office_Address', '1'),
(34, '34', 'Authorised_share_capital', '1'),
(35, '35', 'Issued_share_capital', '1'),
(36, '36', 'board_directors', '1'),
(37, '36', 'board_directors', '1');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view1`
--
CREATE TABLE IF NOT EXISTS `view1` (
`capital_req_id` int(11)
,`user_id` varchar(245)
,`company_name` varchar(245)
,`company_registration_number` varchar(245)
,`date_of_incorporation` varchar(245)
,`country_of_incorporation` varchar(245)
,`type_of_entity` varchar(245)
,`business_sector` varchar(245)
,`nature_of_business` varchar(245)
,`business_nature` varchar(245)
,`telephone` varchar(245)
,`fax_number` varchar(245)
,`registered_office_physical_address` text
,`postal_address` text
,`principal_place_of_business` text
,`raised_equity` varchar(245)
,`raised_debt` varchar(245)
,`raised_other` varchar(245)
,`purpose_of_funds` varchar(245)
);
-- --------------------------------------------------------

--
-- Structure for view `view1`
--
DROP TABLE IF EXISTS `view1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view1` AS select `capital_requirements`.`capital_req_id` AS `capital_req_id`,`capital_requirements`.`user_id` AS `user_id`,`capital_requirements`.`company_name` AS `company_name`,`capital_requirements`.`company_registration_number` AS `company_registration_number`,`capital_requirements`.`date_of_incorporation` AS `date_of_incorporation`,`capital_requirements`.`country_of_incorporation` AS `country_of_incorporation`,`capital_requirements`.`type_of_entity` AS `type_of_entity`,`capital_requirements`.`business_sector` AS `business_sector`,`capital_requirements`.`nature_of_business` AS `nature_of_business`,`capital_requirements`.`business_nature` AS `business_nature`,`capital_requirements`.`telephone` AS `telephone`,`capital_requirements`.`fax_number` AS `fax_number`,`capital_requirements`.`registered_office_physical_address` AS `registered_office_physical_address`,`capital_requirements`.`postal_address` AS `postal_address`,`capital_requirements`.`principal_place_of_business` AS `principal_place_of_business`,`capital_requirements`.`raised_equity` AS `raised_equity`,`capital_requirements`.`raised_debt` AS `raised_debt`,`capital_requirements`.`raised_other` AS `raised_other`,`capital_requirements`.`purpose_of_funds` AS `purpose_of_funds` from `capital_requirements` where (`capital_requirements`.`capital_req_id` = 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
