<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ; 
$extra_css = "" ;
$thy_stage = "3" ;
$thy_qsn = "55/472" ;
$page_number = "10" ;
$thy_qp = round(55/472 * 100, 2);
$my_title = "Company CORPORATE DIRECTORY" ;
$extra_js = "" ; 
$main_page_title = "Advanced Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header_admin.php") ;
require("func/data/connect.php");

 $ifPopulated =  (new controlDAO())->getmaterialContracts()->getmaterialContractsbyEmail(@$_GET['email']) ;
 if(!$ifPopulated){
    //echo "There is no data " ;
    // $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

    }else{
    // $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
     }
       // $setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
    //echo $user_id ;
  }


?>
            
                <div class="col-md-8 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title my_custom_header_main">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <!-- /.col-lg-12 -->
                        </div>

                        <form action="#" class="login-form" method="post">
                            <input type="hidden" value="upload" name="upload">
                            <div class="white-box-main">
                            <div class="row">  
                                <div class="col-xs-12">
                                    <b>MATERIAL AND OTHER THIRD PARTY CONTRACTS :</b> 
                                    <div class="row">
                                        <div class="col-xs-12">
                                            

                                            1.  Contracts restricting  the [COMPANY]’s freedom to carry  on its business as it deems fit or restrict  its ability to transfer its business;
                                             <a href="func/controller/materialContractsUploads/<?=@$restrictions;?>"> <?php echo @$restrictions;?></a>
                                        </div>
                                       <div class="col-xs-12">
                                            2.  Contracts of material capital commitments;
                                           <a href="func/controller/materialContractsUploads/<?=@$capital_committments;?>"> <?php echo @$capital_committments;?></a>
                                        </div>
                                       <div class="col-xs-12">
                                            3.  Contracts with subsidiaries of other group companies OR in which any of the Directors has an interest.
                                           <a href="func/controller/materialContractsUploads/<?=@$subgroup;?>"> <?php echo @$subgroup;?></a>
                                        </div>
                                       <div class="col-xs-12">
                                            4.  Agreements to which the Company or any Subsidiary is a party that cannot be disclosed due to any non-disclosure covenant;
                                           <a href="func/controller/materialContractsUploads/<?=@$undisclosed_contracts;?>"> <?php echo @$undisclosed_contracts;?></a>

                                        </div>
                                         <div class="col-xs-12">
                                            5.  Material joint venture, participation, teaming, partnership or cooperative agreements to which the Company is a party;
                                              <a href="func/controller/materialContractsUploads/<?=@$joint_venture;?>"> <?php echo @$joint_venture;?></a>
                                            </div>
                                         <div class="col-xs-12">
                                            6.  Contracts terminated for default, convenience or show-cause notices.
                                             <a href="func/controller/materialContractsUploads/<?=@$terminated_contracts;?>"> <?php echo @$terminated_contracts;?></a>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                        


                    </div>


                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <?php
                                if($others->getUserPemission($_SESSION['email']) =="Analyst") {
                                    ?>
                                    <a href="analyst_pending_applications.php" class="btn green btn-outline">[<< Back
                                        Pending Applications]</a>
                                    <?php
                                }
                                if($others->getUserPemission($_SESSION['email']) == "Financier") {
                                    ?>
                                    <a href="fin_applicants.php" class="btn green btn-outline">[<< Back
                                        View Applicants Lists]</a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <button type="button"  class="js-print-link btn blue btn-outline pull-right" >[ PRINT ] </button>
                            </div>
                        </div>


                    </div>

                    </form>
                </div>



<?php
require_once("require/footer_admin.php") ;
?>