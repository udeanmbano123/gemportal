<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Finsec | Capital Online</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">


    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
</head>
<body class="fix-sidebar fix-header">
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">

    <?php
    include("req/header_financier.php");
    $extra_js = "    <!-- jQuery -->
    <script src='plugins/bower_components/jquery/dist/jquery.min.js'></script>
    <!-- Bootstrap Core JavaScript -->
    <script src='bootstrap/dist/js/bootstrap.min.js'></script>
    <!-- Menu Plugin JavaScript -->
    <script src='plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js'></script>
    <!--slimscroll JavaScript -->
    <script src='js/jquery.slimscroll.js'></script>
    <!--Wave Effects -->
    <script src='js/waves.js'></script>
    <!--weather icon -->
    <script src='plugins/bower_components/skycons/skycons.js'></script>
    <!--Morris JavaScript -->
    <script src='plugins/bower_components/raphael/raphael-min.js'></script>
    <script src='plugins/bower_components/morrisjs/morris.js'></script>
    <!-- jQuery for carousel -->
    <script src='plugins/bower_components/owl.carousel/owl.carousel.min.js'></script>
    <script src='plugins/bower_components/owl.carousel/owl.custom.js'></script>
    <!-- Sparkline chart JavaScript -->
    <script src='plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js'></script>
    <script src='plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js'></script>
    <!--Counter js -->
    <script src='plugins/bower_components/waypoints/lib/jquery.waypoints.js'></script>
    <script src='plugins/bower_components/counterup/jquery.counterup.min.js'></script>
    <!-- Custom Theme JavaScript -->
    <script src='js/custom.min.js'></script>
    <script src='js/widget.js'></script>
    <!--Style Switcher -->
    <script src='plugins/bower_components/styleswitcher/jQuery.style.switcher.js'></script>";
    ?><?php
    function asDollars($value) {
        return '$' . number_format($value, 2);
    }
function asFigure($value) {
        return '' . number_format($value,0);
    }

    //include_once "/func/config/DbConnect.php";
    require_once 'func/controlDAO.php' ;
    $others = (new controlDAO())->getOthers() ;
    $getApplcations = $others->getRecomendedApplications() ;
    $getApplcations_scores = $others->getRecomendedApplicationsRankScores() ;
    $base="capitalonline";
    $server="localhost";
    $user="root";
    $pass="";
    include("func/config/db.class.php");
    $db = new DB($base, $server, $user, $pass);
    $my_regions_lists  = $others->getAllRegion() ;
    $my_sectors_lists  = $others->getAllSectors() ;
    $mysqli = new mysqli($server,$user,$pass,$base);

    ?>


    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title"  style="    background: #a67c00;">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title" style="color: #ffffff;">Financier </h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="fin_dashboard.php">Dashboard</a></li>
                        <li class="active"><a href="#">Admin </a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <!-- .row -->
            <div class="row">
                <div class="col-sm-12">

                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-2  inbox-panel">
                                <div>
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-xs-12">
                                                <div class="white-box">
                                                    <h3 class="box-title" style="text-transform: UPPERCASE;padding-bottom:  16px;">Sector Analysis </h3>
                                                    <ul class="basic-list" style="font-size:7pt">
                                                        <?php
                                                        foreach($my_sectors_lists as $region) { ?>
                                                            <li><?= $region['sector_name'] ?>   <span class="pull-right"><?= asDollars($others->getSectorValue($region['sector_name'])); ?>   |   <?= asFigure($others->getSectorCount($region['sector_name'])); ?></span></li>
                                                            <?php
                                                        } ?>

                                                    </ul>
                                                </div>
                                                <div class="white-box">
                                                    <h3 class="box-title" style="text-transform: UPPERCASE;padding-bottom:  16px;">Regional Analysis </h3>
                                                    <ul class="basic-list" style="font-size:7pt">
                                                        <?php
                                                        foreach($my_regions_lists as $region) { ?>
                                                            <li><?= $region['region_name'] ?>   <span class="pull-right"><?= asDollars($others->getRegionValue($region['region_name'])); ?>   |   <?= asFigure($others->getRegionCount($region['region_name'])); ?></span></li>
                                                            <?php
                                                        } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 ">
                                <!-- DASHBOARD -->
                                <div class=" col-lg-12 col-sm-6">
                                    <!-- .row -->
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-12 col-xs-12">
                                            <div class="white-box" style="border: 1px solid;">
                                                <h3 class="box-title">Recommended Appication Ranked by Scores
                                                </h3>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>COMPANY NAME</th>
                                                            <th>APPLICANT NAME</th>
                                                            <th>SCORES</th>
                                                            <th>VALUE</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <?php
                                                        //scores_total_user, total_score, users_fullname, users_created_date, capital_required
                                                        for($i = 0  ; $i < count($getApplcations_scores) ; $i++ ) {
                                                            echo "<tr><td>".($i+1)."</td>" ;
                                                            echo "<td>".$getApplcations_scores[$i]['company_name']."</td>" ;
                                                            echo "<td>".$getApplcations_scores[$i]['users_fullname']."</td>" ;
                                                            echo "<td>".$getApplcations_scores[$i]['total_score']."</td>" ;
                                                            echo "<td>".asDollars($getApplcations_scores[$i]['capital_required'])."</td></tr>" ;
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table> <!-- <a href="#">Check all the sales</a> --> </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-12">
                                            <div class="white-box" style="border: 1px solid;">
                                                <h3 class="box-title">Recent Recommended Applications
                                                </h3>
                                                <div class="table-responsive">

                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>FULLNAME</th>
                                                            <th>EMAIL</th>
                                                            <th>TOTAL SCORE</th>
                                                            <th>Value</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        //scores_total_user, total_score, users_fullname, users_created_date, capital_required
                                                        for($i = 0  ; $i < count($getApplcations) ; $i++ ) {
                                                            echo "<tr><td>".($i+1)."</td>" ;
                                                            echo "<td>".$getApplcations[$i]['users_fullname']."</td>" ;
                                                            echo "<td>".$getApplcations[$i]['scores_total_user']."</td>" ;
                                                            echo "<td>".$getApplcations[$i]['total_score']."</td>" ;
                                                            echo "<td>".asDollars($getApplcations[$i]['capital_required'])."</td></tr>" ;
                                                        }
                                                        ?>

                                                        </tbody>
                                                    </table> <!-- <a href="#">Check all the sales</a> --> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <!-- .row -->
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small> Basic Registrations</h3>
                                                <div class="stats-row">
                                                    <div class="stat-item">
                                                        <h6>Count</h6> <b>                                                    <?php
                                                            echo $count = $db->countOf("applicant_level","send_to='BASIC' ");
                                                            ?></b></div>
                                                    <div class="stat-item">
                                                        <h6>Value</h6> <b><?=asDollars($others->getStageValue("BASIC")) ;?></b></div>
                                                </div>
                                                <div id="sparkline8"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title"><small class="pull-right m-t-10 text-danger"><i class="fa fa-sort-desc"></i> 18% High then last month</small>Intermediate Registrations</h3>
                                                <div class="stats-row">
                                                    <div class="stat-item">
                                                        <h6>Count</h6> <b>
                                                            <?php
                                                            echo $count = $db->countOf("applicant_level","send_to='INTERMEDIATE' ");
                                                            ?></b></div>
                                                    <div class="stat-item">
                                                        <h6>Value </h6> <b><?=asDollars($others->getStageValue("INTERMEDIATE")) ;?></b></div>
                                                </div>
                                                <div id="sparkline9"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small>Advanced Registrations </h3>
                                                <div class="stats-row">
                                                    <div class="stat-item">
                                                        <h6>Count </h6> <b>
                                                            <?php
                                                            echo $count = $db->countOf("applicant_level","send_to='ADVANCED' ");
                                                            ?></b></div>
                                                    <div class="stat-item">
                                                        <h6>Value </h6> <b><?=asDollars($others->getStageValue("ADVANCED")) ;?></b></div>
                                                </div>
                                                <div id="sparkline10"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <!-- row -->
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="white-box_ text-center bg-purple">
                                                <h1 class="text-white counter">
                                                    <?php
                                                    echo $count = $db->countOf("users","users_type='applicant' ");
                                                    ?>
                                                </h1>
                                                <p class="text-white">Total Number</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="white-box_ text-center bg-info">
                                                <h1 class="text-white counter">
                                                    <?php
                                                    echo $count = $db->countOf("applicant_level","send_to='BASIC' ");
                                                    ?>
                                                </h1>
                                                <p class="text-white">Basic Stage </p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="white-box_ text-center bg-theme-dark">
                                                <h1 class="counter text-white">
                                                    <?php
                                                    echo $count = $db->countOf("applicant_level","send_to='INTERMEDIATE' ");
                                                    ?>
                                                </h1>
                                                <p class="text-white">Intermediate stage</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12 col-sm-6">
                                            <div class="white-box_ text-center bg-success">
                                                <h1 class="text-white counter">
                                                    <?php
                                                    echo $count = $db->countOf("applicant_level","send_to='ADVANCED' ");
                                                    ?>
                                                </h1>
                                                <p class="text-white">Advanced stage</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /row -->

                                    <!-- .row -->
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title">Total Value</h3>
                                                <div class="text-right"> <span class="text-muted"></span>
                                                    <h1> <?php
                                                        $number = $others->getStageValueAll();
                                                        echo asDollars($number);
                                                        ?> </h1> </div><!--  <span class="text-success">20%</span> -->
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title">Basic Value </h3>
                                                <div class="text-right"> <span class="text-muted"></span>
                                                    <h1> <?=asDollars($others->getStageValue("BASIC")) ;?></h1> </div> <!-- <span class="text-danger">30%</span> -->
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title">Intermediate Value</h3>
                                                <div class="text-right"> <span class="text-muted"></span>
                                                    <h1> <?=asDollars($others->getStageValue("INTERMEDIATE")) ;?></h1> </div> <!-- <span class="text-info">60%</span> -->
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="white-box my_cnr">
                                                <h3 class="box-title">Advanced  Value</h3>
                                                <div class="text-right"> <span class="text-muted"></span>
                                                    <h1> <?=asDollars($others->getStageValue("ADVANCED")) ;?></h1> </div> <!-- <span class="text-inverse">80%</span> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->


                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="white-box">
                                                <div class="row row-in">
                                                    <div class="col-lg-3 col-sm-6 row-in-br">
                                                        <div class="col-in row">
                                                            <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
                                                                <h5 class="text-muted vb">Total recieved </h5> </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                <h3 class="counter text-right m-t-15 text-danger">
                                                                    <?php
                                                                    echo $count = $db->countOf("users","users_type='applicant' ");
                                                                    ?>
                                                                </h3>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                                        <div class="col-in row">
                                                            <div class="col-md-8 col-sm-8 col-xs-8"> <i class="ti-pencil-alt"></i>
                                                                <h5 class="text-muted vb">Qualified</h5> </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                                <h3 class="counter text-right m-t-15 text-info">
                                                                    <?=$others->getQualified() ;?>
                                                                </h3> </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6 row-in-br">
                                                        <div class="col-in row">
                                                            <div class="col-md-8 col-sm-8 col-xs-8"> <i class="ti-mouse-alt"></i>
                                                                <h5 class="text-muted vb">Analyst Recomended </h5> </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                                <h3 class="counter text-right m-t-15 text-success"><?php echo  $count = $db->countOf("applicant_level","send_to='financier' ");?></h3> </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-sm-6  b-0">
                                                        <div class="col-in row">
                                                            <div class="col-md-8 col-sm-8 col-xs-8"> <i class="ti-receipt"></i>
                                                                <h5 class="text-muted vb">Financier Accepted</h5> </div>
                                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                                <h3 class="counter text-right m-t-15 text-warning"><?php echo  $count = $db->countOfAll("approved_applications");?></h3> </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="progress">
                                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--row -->





                                </div>


                            </div>
                            <!-- /#page-wrapper -->
                        </div>
                        <!-- /#wrapper -->
                        <!-- jQuery -->
                        <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
                        <!-- Bootstrap Core JavaScript -->
                        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
                        <!-- Menu Plugin JavaScript -->
                        <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
                        <!--slimscroll JavaScript -->
                        <script src="js/jquery.slimscroll.js"></script>
                        <!--Wave Effects -->
                        <script src="js/waves.js"></script>
                        <!--Counter js -->
                        <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
                        <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
                        <!-- Custom Theme JavaScript -->
                        <script src="js/custom.min.js"></script>
                        <script src="public/assets/global/plugins/moment.min.js"></script>
                        <!--Style Switcher -->
                        <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
                        <!-- Date Picker Plugin JavaScript -->
                        <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                        <!-- Date range Plugin JavaScript -->
                        <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
                        <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>


                        <!--Style Switcher -->

                        <?php echo $extra_js; ?>
                        <script>
                            // Date Picker
                            jQuery('.mydatepicker, #datepicker').datepicker();
                            jQuery('#datepicker-autoclose').datepicker({
                                autoclose: true,
                                todayHighlight: true
                            });

                        </script>

</body>

</html>