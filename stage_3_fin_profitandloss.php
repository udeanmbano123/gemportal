<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ;
$extra_css = "    <!-- Editable CSS -->
     <link href='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css' rel='stylesheet'>" ;
$page_number = "10" ;
$extra_js = "    <!-- Editable -->
  <script src='//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.js'></script>
  
    " ;
$thy_stage = "3" ;
$thy_qsn = "183/472" ;
$thy_qp = round(183/472 * 100, 2);
$my_title = "PROFIT OR LOSS  5 - 9" ;
@$msg=$_GET['msg'];
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg'];
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
$profitloss = "true" ;
if($others->searchPL($_SESSION['email']) == 0) {
    $others->setCompanyProfitLoss($_SESSION['email'] , '2015') ;
    $others->setCompanyProfitLoss($_SESSION['email'] , '2016') ;
    $others->setCompanyProfitLoss($_SESSION['email'] , '2017') ;
    $others->setCompanyProfitLoss($_SESSION['email'] , '2018') ;
    $others->setCompanyProfitLoss($_SESSION['email'] , '2019') ;
    $others->setCompanyProfitLoss($_SESSION['email'] , '2020') ;
    $others->setCompanyProfitLoss($_SESSION['email'] , '2021') ;
    $others->setCompanyProfitLoss($_SESSION['email'] , '2022') ;
}
$main=(new controlDAO())->getlast2yrs();
$get_financials_cashflows=  $main->selectProfitLoss($_SESSION['email']) ;
$get_TotalRevenue= $main->PrTotalRevenue($_SESSION['email']);
$get_GrossProfit= $main->GrossProfit($_SESSION['email']);
$get_OperatingProfit= $main->OperatingProfit($_SESSION['email']);
$get_ProfitBeforeTax= $main->PrBeforeTax($_SESSION['email']);
$get_ProfitLossYear= $main->PrLossYear($_SESSION['email']);
$get_TotalYear= $main->TotalYear($_SESSION['email']);
$ifPopulated =  (new controlDAO())->getuploadFinancials()->selectOneuploadFinancialsByEmailProfitLoss($_SESSION['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
    }
    $setOption .= "<input type ='hidden' value = '".@$id."' name ='id'/>" ;
    //echo $user_id ;
}


?>
    <script language="javascript">
        setInterval(function(){
            window.location.reload(1);
        }, 30000);
    </script>
    <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
        <div class="login-content" style ="margin-top:0px;padding:0px;">
            <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                    <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                    <ol class="breadcrumb">
                        <li class="dropdown">
                            <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                [ Previous forms of this stage ]
                            </a>
                            <ul class="dropdown-menu mailbox animated ">
                                <li>
                                    <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                </li>
                                <li>
                                    <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                </li>
                                <li>
                                    <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                </li>
                                <li>
                                    <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                </li>
                            </ul>
                            <!-- /.dropdown-messages -->
                        </li>
                        <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box">
                        <h3 class="box-title">  PROFIT OR LOSS Checked </h3>
                        <form action="func/controller/finProfitLossController.php"  enctype="multipart/form-data"   class="login-form" method="post">
                            <?=@$setOption;?>
                            <input type="hidden" value="<?php echo $_SESSION['email']?>"  name="user_id">

                            <table class="table table-bordered responsive">

                                <thead>
                                <th class="c-gray">#</th>
                                <th class="c-gray">Is Audited</th>
                                <th class="c-gray"> Upload File</th>
                                <th class="c-gray"></th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>  <input type="checkbox"
                                            <?php
                                            if(@$is_audited =="yes"){
                                                echo "checked" ;
                                            }

                                            ?> value="yes" name="is_audited"  /></td>
                                    <td>  <input type="file" name="upload_file"  /></td>
                                    <td> <a target="_blank" href="<?=$doc_url;?>"> <?=@$doc_url;?></a> </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <button type="submit" class="btn green btn-outline pull-right" >[ Save ] </button>
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="white-box-main">
                        <h3 class="box-title">CONSOLIDATED STATEMENT OF PROFIT OR LOSS</h3>
                        <p class="text-muted">*Just click on word which you want to change and enter , page refreshes every 30 seconds to update figures</p>
                        <table id="profitloss_table" class="table editable-table table-bordered m-b-0">
                            <thead class="text-uppercase">
                            <th width="290" class="c-gray"></th>
                            <th colspan="3" class="c-gray">HISTORICAL</th>
                            <th colspan="5" class="c-gray">PROJECTED</th>
                            </thead>
                            <tbody>
                            <colgroup>
                                <col  style="background-color:#f5f5f5;">
                                <col span="3" style="background-color:yellow">
                                <col span="5" style="background-color:pink">
                            </colgroup>

                            <tr>
                                <td>Year</td><td>2015</td><td>2016</td><td>2017</td><td>2018</td><td>2019</td><td>2020</td><td>2021</td><td>2022</td>
                            </tr>
                            <tr>
                                <td>Sale of goods </td>

                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Saleofgoods' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Saleofgoods']."</td>" ;
                                }
                                ?>

                            </tr>
                            <tr>
                                <td>Rendering of services  </td>

                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Renderingofservices' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Renderingofservices']."</td>" ;
                                }
                                ?>

                            </tr>
                            <tr>
                                <td>Other  Revenues </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_OtherRevenues' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_OtherRevenues']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;">
                                <td>Total Revenue </td>
                                <?php

                                for($i = 0  ; $i < count($get_TotalRevenue) ; $i++ ) {
                                    echo "<td >".$get_TotalRevenue[$i]['Total']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Cost of Sales </td>


                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_CostofSales' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_CostofSales']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;">
                                <td>Gross Profit</td>
                                <?php

                                for($i = 0  ; $i < count($get_GrossProfit) ; $i++ ) {
                                    echo "<td >".$get_GrossProfit[$i]['Total']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Other operating income </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Otheroperatingincome' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Otheroperatingincome']."</td>" ;
                                }
                                ?>

                            </tr>
                            <tr>
                                <td>Other operating expenses (-)</td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Otheroperatingexpenses' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Otheroperatingexpenses']."</td>" ;
                                }
                                ?>

                            </tr>
                            <tr>
                                <td>Staff costs (-) </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Staffcosts' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Staffcosts']."</td>" ;
                                }
                                ?>

                            </tr>
                            <tr>
                                <td>Depreciation and armotisation (-) </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Depreciationarmotisation' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Depreciationarmotisation']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;">
                                <td>Operating Profit</td>
                                <?php

                                for($i = 0  ; $i < count($get_OperatingProfit) ; $i++ ) {
                                    echo "<td >".$get_OperatingProfit[$i]['Total']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Increase in fair value adjustments impairments  </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Increaseinfairvalueadjustmentsimpairments' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Increaseinfairvalueadjustmentsimpairments']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Decrease in fair value adjustments and impairments (-) </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Decreaseinfairvalueadjustmentsimpairments' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Decreaseinfairvalueadjustmentsimpairments']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Finance costs (-)</td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Financecosts' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Financecosts']."</td>" ;
                                }
                                ?>

                            </tr>
                            <tr>
                                <td>Finance income </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Financeincome' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Financeincome']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;">
                                <td>Profit/Loss before tax</td>
                                <?php

                                for($i = 0  ; $i < count($get_ProfitBeforeTax) ; $i++ ) {
                                    echo "<td >".$get_ProfitBeforeTax[$i]['Total']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Income tax expense (-)</td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_Incometaxexpense' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_Incometaxexpense']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;">
                                <td>Profit/Loss for the year</td>
                                <?php

                                for($i = 0  ; $i < count($get_ProfitLossYear) ; $i++ ) {
                                    echo "<td >".$get_ProfitLossYear[$i]['Total']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Attributable to Equity holders of the parent </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_AttributabletoEquityholdersoftheparent' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_AttributabletoEquityholdersoftheparent']."</td>" ;
                                }
                                ?>
                            </tr>
                            <tr>
                                <td>Attributable to Non-controlling intrests </td>
                                <?php

                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                    echo "<td data-name = 'profit_loss_AttributabletoNoncontrollingintrests' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_profit_loss_id']."
                                    '>".$get_financials_cashflows[$i]['profit_loss_AttributabletoNoncontrollingintrests']."</td>" ;
                                }
                                ?>


                            </tr>
                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;">
                                <td>Total</td>
                                <?php

                                for($i = 0  ; $i < count($get_TotalYear) ; $i++ ) {
                                    echo "<td >".$get_TotalYear[$i]['Total']."</td>" ;
                                }
                                ?>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- /.row -->






        </div>
        <div class="login-footer">
            <div class="row bs-reset">
                <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                    <a href ="stage_3_fin_balancesheet.php" class="btn green btn-outline">[<< Back]</a>
                </div>
                <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                <a href ="stage_3_fin_cashflow.php" type="submit" class="btn green btn-outline pull-right" >[ Next >>] </a>
            </div>
        </div>
    </div>

    </div>
<?php

require_once("require/footer.php") ;

?>