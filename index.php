<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
<head>
        <meta charset="utf-8" />
        <title>Finsec | GEM Portal</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="SMEPortal GEM Portal" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="public/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="public/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="public/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="public/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
            <link href="css/style.css" rel="stylesheet">
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
<?php 
    if(isset($_GET['logout'])){
        session_start() ;
        session_destroy() ;
    }
?>
    <body class=" login">
        <!-- BEGIN : LOGIN PAGE 5-1 -->
        <div class="user-login-5">
            <div class="row bs-reset">
                <div class="col-md-6 bs-reset mt-login-5-bsfix"><!-- style="background-image:url(public/assets/pages/img/login/bg1.jpg)" -->
                    <div class ="row">
                        <div class="col-md-12 login-bg-n" >
                            
                            <div id="carousel-example-captions" data-ride="carousel" class="carousel slide  m-l-20 m-t-20">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-captions" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-captions" data-slide-to="1" class=""></li>
                                    <li data-target="#carousel-example-captions" data-slide-to="2" class=""></li>
                                </ol>
                                <div role="listbox" class="carousel-inner">
                                    <div class="item active"> <img src="plugins/images/big/img_1.png" alt="First slide image" data-square="384054">
                                        <div class="carousel-caption">
                                            <!-- <h3 class="text-white font-600">Model Showing off</h3> -->
                                            <!-- <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p> -->
                                        </div>
                                    </div>
                                    <div class="item"> <img src="plugins/images/big/img_2.png" alt="Second slide image">
                                        <div class="carousel-caption">
                                            <!-- <h3 class="text-white font-600">Mountaing Climbing</h3> -->
                                            <!-- <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p> -->
                                        </div>
                                    </div>
                                    <div class="item"> <img src="plugins/images/big/img_3.png" alt="Third slide image">
                                        <div class="carousel-caption">
                                            <!-- <h3 class="text-white font-600">Fire camp on hill</h3> -->
                                            <!-- <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p> -->
                                        </div>
                                    </div>
                                </div>
                                <a href="#carousel-example-captions" role="button" data-slide="prev" class="left carousel-control"> <span aria-hidden="true" class="fa fa-angle-left"></span> <span class="sr-only">Previous</span> </a>
                                <a href="#carousel-example-captions" role="button" data-slide="next" class="right carousel-control"> <span aria-hidden="true" class="fa fa-angle-right"></span> <span class="sr-only">Next</span> </a>
                            </div>

                      
                        </div>
                        <div class="col-md-12">
                            <h4 class="box-title m-b-20 m-l-20">Advanced FAQs</h4>
                            <div class="panel-group m-l-15" id="exampleAccordionDefault" aria-multiselectable="true" role="tablist">
                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeadingDefaultOne" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultOne" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultOne">#1 What is FINSEC GEM Portal?  </a> </div>
                                    <div class="panel-collapse collapse" id="exampleCollapseDefaultOne" aria-labelledby="exampleHeadingDefaultOne" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <p>
                                                FINSEC GEM Portal is an electronic platform that provides a great opportunity
                                                for companies to apply for listing on a recognised exchange for purposes of
                                                raising new or additional capital. The platform is targeted at Small to Medium
                                                Enterprises desiring to raise capital through issuing debt or equity securities
                                                tradable on a securities exchange
                                            </p>
                                            <p>
                                                This platform is designed to assist SME’s who may be facing difficulties in going through
                                                the listing requirements of the main exchanges. World over, trading platforms/exchanges
                                                for the shares of SMEs are known by different names such as Alternate Investment Markets
                                                or Growth Enterprises Market, or in the case of FINSEC, SME Board.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeadingDefaultTwo" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultTwo" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultTwo">#2 Who is FINSEC?  </a> </div>
                                    <div class="panel-collapse collapse" id="exampleCollapseDefaultTwo" aria-labelledby="exampleHeadingDefaultTwo" role="tabpanel" aria-expanded="false" style="height: 0px;">
                                        <div class="panel-body">
                                            <p>
                                                The Financial Securities Exchange (FINSEC) was registered on 27 September 2016, by the
                                                Securities and Exchange Commission of Zimbabwe (SECZ) as a Securities Exchange (Alternative Trading Platform)
                                                in terms of section 30 of the Securities & Exchange Act [Chapter 24:25], as read with the Securities
                                                (Alternative Trading Platform) Rules, S.I.100 of 2016.
                                                By virtue of the statutory licencing, FINSEC is the second securities exchange in Zimbabwe.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeadingDefaultThree" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultThree" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultThree">#3 What is an Alternative Trading Platform?  </a> </div>
                                    <div class="panel-collapse collapse" id="exampleCollapseDefaultThree" aria-labelledby="exampleHeadingDefaultThree" role="tabpanel" aria-expanded="false">
                                        <div class="panel-body">

                                            <p>It is an electronic platform which offers an electronic market or otherwise provides a place or facility for bringing together—<p>
                                            <ol>
                                                <li>primary market issuers of securities and investors who wish to purchase securities;</li>
                                                <li>secondary market sellers and buyers of securities;</li>
                                                <li>primary issuers and buyers, and secondary sellers and buyers of any other securities as may be prescribed or approved by the Commission; and</li>
                                            </ol>
                                            <p>That does not—</p>
                                            <ol>
                                                <li>set rules governing the conduct of participants other than the conduct of such participants’ trading with the Platform; or</li>
                                                <li>discipline or seek to control the participants other than by excluding them from trading;</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
<!--                                 <div class="panel">
                                    <div class="panel-heading" id="exampleHeadingDefaultFour" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultFour" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultFour">#4 What is the difference between the Alternative Trading Platform (ATP) and an Exchange?  </a> </div>
                                    <div class="panel-collapse collapse" id="exampleCollapseDefaultFour" aria-labelledby="exampleHeadingDefaultFour" role="tabpanel" aria-expanded="false">
                                        <div class="panel-body">
                                            <p>The difference between an ATP and an Exchange is that and an ATP does not—</p>
                                            <ol>
                                                <li> set rules governing the conduct of participants other than the conduct of such participants’ trading with the Platform; or</li>
                                                <li> discipline or seek to control the participants other than by excluding them from trading. </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeadingDefaultFive" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultFive" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultFive">#5 What is an Issuer?  </a> </div>
                                    <div class="panel-collapse collapse" id="exampleCollapseDefaultFive" aria-labelledby="exampleHeadingDefaultFive" role="tabpanel" aria-expanded="false">
                                        <div class="panel-body">
                                            <p>
                                                It is an organisation that registers, distributes, and sells a security
                                                on a Primary Market. An issuer can be a private company or a government
                                                or even an Exchange for derivatives such as equity and index futures and/or
                                                options. There are different kinds of financial instruments which can be
                                                issued: shares, bonds and derivatives.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeadingDefaultSix" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultSix" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultSix">#6 What are the benefits of listing on FINSEC SME Platform?  </a> </div>
                                    <div class="panel-collapse collapse" id="exampleCollapseDefaultSix" aria-labelledby="exampleHeadingDefaultSix" role="tabpanel" aria-expanded="false">
                                        <div class="panel-body">
                                            <p>It provides the SMEs with equity financing opportunities to grow their business - from expansion to acquisition.</p>
                                            <ol>
                                                <li>Equity Financing lowers the debt burden leading to lower financing cost and healthier balance sheet.</li>
                                                <li>Listing expands the investors base, which in turn helps in getting secondary equity financing, including private placement.</li>
                                                <li>It enhances company’s visibility. Media coverage provides SMEs with greater profile and credibility leading to increase in the value of its shares.</li>
                                                <li>It will bring about greater incentive for employees as they can participate in the ownership of the company and benefit from being its shareholders.</li>
                                                <li>Overall, the SME sector will grow better on two pillars of financial system i.e. banking and capital market.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="panel-heading" id="exampleHeadingDefaultSeven" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultSeven" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultSeven">#7 Who is the regulator of FINSEC?  </a> </div>
                                    <div class="panel-collapse collapse" id="exampleCollapseDefaultSeven" aria-labelledby="exampleHeadingDefaultSeven" role="tabpanel" aria-expanded="false">
                                        <div class="panel-body">
                                            <p>
                                                FINSEC is licensed and regulated by the Securities Exchange Commission of Zimbabwe (SECZ)
                                            </p>
                                        </div>
                                    </div>
                                </div>
<!--                                 <div class="panel">
                                    <div class="panel-heading" id="exampleHeadingDefaultEight" role="tab"> <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultEight" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultEight">#8 What are the investor protection mechanisms that are available in the capital markets?  </a> </div>
                                    <div class="panel-collapse collapse" id="exampleCollapseDefaultEight" aria-labelledby="exampleHeadingDefaultEight" role="tabpanel" aria-expanded="false">
                                        <div class="panel-body">
                                            <ol>
                                                <li>Regulation of all capital market players and activities – through licensing of capital market players, SECZ knows and monitors the activities of each and every market player and flushes out unscrupulous players. Continuous supervision of capital market players ensures that only strong institutions that uphold high ethical standards are allowed to operate.</li>
                                                <li>Establishment of an Independent Investor Protection Fund – SECZ facilitated the establishment of the Investor Protection Fund that is meant to provide an additional layer of protection for investors and offer some reimbursement for losses suffered as a direct result of a licensed player becoming insolvent.</li>
                                                <li>Existence of a Guarantee Fund – The guarantee Fund that is administered by the Securities Exchange is meant to guard against settlement failures by the members of the exchange (i.e. securities dealers).</li>
                                                <li>Professional Indemnity cover – All capital market players are mandated to have this insurance cover to cover against losses resulting from negligent or dishonesty of any of the staff of the licensed player.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-md-6 login-container bs-reset mt-login-5-bsfix">
                    <div class="login-content">
                        <img class="login-logo" src="img/Untitled-portal-two.png" width = "100%" style="left: 0;" />
                        <h1 class="text-uppercase">&nbsp;</h1>
                        <h1 class="text-uppercase">&nbsp;</h1>
                        <!-- <h1 class="text-uppercase">Finsec GEM Portal Login</h1> -->
                        <p><?php echo "<br><font color='green'  >".@$_GET['sms']. " </font>" ;?></p>
                        <p style="text-align:justify;">A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. </p>
                        <form action="func/controller/UserController.php" class="login-form" method="post">
                            <input type ="hidden" value="signup" name="signup"/>
                            <input type="hidden" name="type" value="applicant"/>
                            <h3 class="font-green">Create Account </h3>
                            <div class="alert alert-danger display-hide" style="margin-top:0px;">
                                <button class="close" data-close="alert"></button>
                                <span>Please fill in all the required details below.  </span>
                            </div>

                            <div class="form-group">
                                <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Full Name" name="name" required />

                            </div>                            
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Phone Number" name="phone"  required/>
                            </div>
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email"  required/>
                            </div>
                            <div class="form-group">
                                <input class="form-control placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Password" name="pass" required />
                            </div>

                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <div class="forgot-password">
                                        <a href="#" class="forget-password">Forgot Password?</a> <br>
                                        <a href="javascript:;" id="forget-password" class="forget-password">Login</a>
                                    </div>
                                      <button type="submit" class="btn btn-success uppercase pull-right">Create Account</button>

                                </div>
                            </div>

                        </form>

                        <!-- BEGIN CREATE ACCOUNT  FORM -->
                        <form class="forget-form" action="func/controller/UserController.php" method="post">

                            <input type ="hidden" value="login" name="login"/>
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Enter any username and password. </span>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Email" name="email" required/> </div>
                                <div class="col-xs-6">
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="password" autocomplete="off" placeholder="Password" name="pass" required/> </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="rem-password">
                                        <label class="rememberme mt-checkbox mt-checkbox-outline">
                                            <input type="checkbox" name="remember" value="1" /> Remember me
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <div class="forgot-password">
                                        <a href="#" class="forget-password">Forgot Password?</a> <br>
                                        <a href="#" id="back-btn" class="forget-password">Do not have an account</a>
                                    </div>
                                      <button type="submit" class="btn btn-success uppercase pull-right">Login</button>
<!--                                    <a class="btn btn-success uppercase pull-right" href="stage_1_capital_requirements.php">Sign In</a>                                    -->

                                </div>
                            </div>


                        </form>
                        <!-- END CREATE ACCOUNT FORM -->

                    </div>
                </div>
                <div class="col-md-12 ">
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6 login-container" style="min-height:0;">
                            <div class="row login-footer bs-reset">
                                <div class="col-md-2 bs-reset">
                                    <ul class="login-social">
                                        <li>
                                            <a href="javascript:;">
                                                <i class="icon-social-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <i class="icon-social-twitter"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4 bs-reset">
                                    <div class="login-copyright text-right">
                                        <p>Copyright &copy; <a href="http://www.escrowsystems.net/" target ="__blank">Escrow Systems</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END : LOGIN PAGE 5-1 -->
        <!--[if lt IE 9]>
<script src="public/assets/global/plugins/respond.min.js"></script>
<script src="public/assets/global/plugins/excanvas.min.js"></script> 
<script src="public/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="public/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="public/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="public/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="public/assets/pages/scripts/login-5.min.js" type="text/javascript"></script>
            <script src="js/custom.min.js"></script>
    <script src="js/dashboard1.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>

</html>