<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$my_title  = "ENTERPRISE SIZE CATEGORISATION  3 - 4"   ;                      
$page_title = "Finsec | Stage One" ; 
$extra_css = "" ; 
$page_number = "" ;
$thy_stage = "1" ;
$extra_js = "" ;
$thy_qsn = "37/472" ;
$thy_qp = round(37/472 * 100, 2);
$main_page_title = "Basic Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header_admin.php") ;
require("func/data/connect.php");
$user_email=$_GET['email'];
   $ifPopulated =  (new controlDAO())->getenterpriseSizeCategorisation()->selectOneEnterpriseSizeCategorisationBYEmail($_GET['email']) ;
    if(!$ifPopulated){

     }else{
     foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
      }

    }


?>
                <div class="col-md-8 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title my_custom_header_main">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <!-- /.col-lg-12 -->
                        </div>

                        <form action="#"  class="login-form" method="post">

                            <div class="white-box-main">

                                <div class="row">
                                    <div class="col-xs-12">
                                        Staff Levels :
                                        <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text"
                                               autocomplete="off" placeholder="issued" name="issued_share_capital"
                                               value="<?=@$staff_levels;?>" required/>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        Annual Turnover:
                                        <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text"
                                               autocomplete="off" placeholder="issued" name="issued_share_capital"
                                               value="<?=@$annual_turnover;?>" required/>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        Gross Value of Assets:
                                        <input disabled class="form-control form-control-solid placeholder-no-fix form-group" type="text"
                                               autocomplete="off" placeholder="issued" name="issued_share_capital"
                                               value="<?=@$gross_value_of_assets;?>" required/>


                                    </div>
                                </div>

                            </div>




                    </div>



                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <?php
                                if($others->getUserPemission($_SESSION['email']) =="Analyst") {
                                    ?>
                                    <a href="analyst_pending_applications.php" class="btn green btn-outline">[<< Back
                                        Pending Applications]</a>
                                    <?php
                                }
                                if($others->getUserPemission($_SESSION['email']) == "Financier") {
                                    ?>
                                    <a href="fin_applicants.php" class="btn green btn-outline">[<< Back
                                        View Applicants Lists]</a>
                                    <?php
                                }
                                ?>
                            </div>
                            <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <button type="button"  class="js-print-link btn blue btn-outline pull-right" >[ PRINT ] </button>
                            </div>
                        </div>


                    </div>

                    </form>
                </div>


<?php
require_once("require/footer_admin.php") ;
?>