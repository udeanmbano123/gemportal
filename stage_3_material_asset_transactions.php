<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ;
$extra_css = "    <!-- summernotes CSS -->
    <link href='plugins/bower_components/summernote/dist/summernote.css' rel='stylesheet' />" ;
$page_number = "10" ;
$extra_js = "    
    <script src='plugins/bower_components/summernote/dist/summernote.min.js'></script>
    <script>
    jQuery(document).ready(function() {
        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });
        $('.inline-editor').summernote({
            airMode: true
        });
    });
    window.edit = function() {
        $('.click2edit').summernote()
    }, window.save = function() {
        $('.click2edit').destroy()
    }
    </script>" ;
$thy_qsn = "56/472" ;
$thy_stage = "3" ;
$thy_qp = round(56/472 * 100, 2);
$my_title = "MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  2 - 7" ;
$main_page_title = "Advanced Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
require("func/data/connect.php");
                              $user_email=$_SESSION['email'];
                       $sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
                   $result2=mysqli_query($con,$sql2);
                      while($row2=mysqli_fetch_array($result2)){
                        $usertype=$row2['users_type'];
                      }
if($usertype=='applicant'){
 $ifPopulated =  (new controlDAO())->getmaterialAssetTransactions()->selectOnematerialAssetTransactionsByEmail($_SESSION['email']) ;
 if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

    }else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".@$material_asset_transactions_id."' name ='material_asset_transactions_id'/>" ;
    //echo $user_id ;
 }
}
elseif($usertype=='Administrator'){
     $ifPopulated =  (new controlDAO())->getmaterialAssetTransactions()->selectOnematerialAssetTransactionsByEmail($_GET['email']) ;
 if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

    }else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".@$material_asset_transactions_id."' name ='material_asset_transactions_id'/>" ;
    //echo $user_id ;
 }
}
?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                                <?php if($usertype=='applicant'){?>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <?php } ?>
                            <!-- /.col-lg-12 -->
                        </div>
                        <form action="func/controller/materialAssetTransactionsController.php"  class="login-form" method="post">
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                            <div class="white-box-main">
                            <div class="row">  
                                <div class="col-xs-12">
                                    Insert details of material asset transactions (including between subsidiaries or with holding company) within the two preceding years including; any material property or asset disposals; any issue or agreement to issue any shares; any acquisition or intention to acquire property which is to be paid for wholly or partly out of the borrowings sought
                                    <textarea   name ="desc" class="summernote form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"><?=@$desc;?></textarea>
                                </div>

                            </div>
                            </div>





                    </div>


                    <div class="login-footer">
                        <?php if($usertype=='applicant'){
                            ?>
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_3_material_contracts.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
<!--                                <a href="stage_3_material_litigation_claims.php" class="btn green btn-outline pull-right" >[ Next >>] </a>-->
                                <button type="submit" class="btn green uppercase btn-outline pull-right">[Next >>]</button>
                            </div>
                        </div>
                         <?php } elseif ($usertype=='Administrator') {
                              ?>
                             <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <a href="applicantAlldetails.php?email=<?php echo $_GET['email'] ?>" class="btn green uppercase btn-outline pull-right">[<< Back]</a>
                            </div>
                            <?php } ?>

                    </div>

                    </form>

                </div>
<?php

require_once("require/footer.php") ;

?>