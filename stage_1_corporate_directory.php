<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;

$page_title = "Finsec | Stage One" ; 
$extra_css = "" ;
$thy_stage = "1" ;
$thy_qsn = "34/472" ;
$thy_qp = round(34/472 * 100, 2);
$page_number = "2" ; 
$extra_js = "" ; 
$main_page_title = "Basic Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
require("func/data/connect.php");
                              $user_email=$_SESSION['email'];
                       $sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
                   $result2=mysqli_query($con,$sql2);
                      while($row2=mysqli_fetch_array($result2)){
                        $usertype=$row2['users_type'];
                      }
if($usertype=="applicant"){
 $my_title  = "CORPORATE DIRECTORY OF <b class='font-green'>".$_SESSION['company_name']."</b> 2 - 5"   ;
   $ifPopulated =  (new controlDAO())->getCorporateDirectory()->selectOnecorporateDirectoryByEmail($_SESSION['email']) ;
  if(!$ifPopulated){
    //echo "There is no data " ;
     $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;
    $contact_person_name= $_SESSION['name'] ;
    $contact_person_cell = $_SESSION['phone'];
    $contact_person_telephone = $_SESSION['phone'];
    $contact_person_email = $_SESSION['email'];
    }else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".$corporate_directory_id."' name ='corporate_directory_id'/>" ;
    //echo $user_id ;
     }
}
elseif($usertype=="Administrator")
  { 
    $my_title  = "CORPORATE DIRECTORY "."</b> 2 - 4"   ;
   $ifPopulated =  (new controlDAO())->getCorporateDirectory()->selectOnecorporateDirectoryByEmail($_GET['email']) ;
  if(!$ifPopulated){
    //echo "There is no data " ;
     $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;
    $contact_person_name= $_SESSION['name'] ;
    $contact_person_cell = $_SESSION['phone'];
    $contact_person_telephone = $_SESSION['phone'];
    $contact_person_email = $_SESSION['email'];
    }else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".$corporate_directory_id."' name ='corporate_directory_id'/>" ;
    //echo $user_id ;
     }
   }
?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                                <?php if($usertype=='applicant'){
                                    ?>

                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_1_capital_requirements.php"> 1. CAPITAL REQUIREMENTS </a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <?php } ?>
                            <!-- /.col-lg-12 -->
                        </div>
<!--                         <h1 class="text-uppercase "> -->
<!--                         </h1>-->
                        <form action="func/controller/corporateDirectoryController.php"  class="login-form" method="post">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Please complete all the required field with * asterisks  </span>
                            </div>
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                            <div class="white-box-main">
                                <div class="row">
                                    <div class="col-xs-4">
                                        *Duration in stated business line:
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Duration" name="duration_in_stated_business_line" value="<?=@$duration_in_stated_business_line;?>" required/>
                                    </div>
                                    <div class="col-xs-8">
                                        <b>Customer Base (% split):</b>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                *Corporates
                                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Corporates" name="customer_basecorporates"  value="<?=@$customer_basecorporates;?>" required/>
                                            </div>
                                           <div class="col-xs-6">
                                                *Individuals
                                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Individuals" name="customer_baseindividuals"  value="<?=@$customer_baseindividuals;?>" required/>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        *Business Year End:
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Year" name="business_year_end" value="<?=@$business_year_end;?>" required/>
                                    </div>
                                    <div class="col-xs-4">
                                        *Business Premises are
                                        <select name  = "business_premises_are" class="form-control form-control-solid placeholder-no-fix form-group"  style="padding-bottom: 2px;">
                                            <option  <?=(@$business_premises_are == "Self owned" ) ? 'selected="selected"' : '';?>  value = "Self owned">Self owned</option>
                                            <option  <?=(@$business_premises_are == "Mortgaged" ) ? 'selected="selected"' : '';?>  value = "Mortgaged">Mortgaged</option>
                                            <option  <?=(@$business_premises_are == "Rented" ) ? 'selected="selected"' : '';?>  value = "Rented">Rented</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        *Name of holding company
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Holding company" name="name_of_holding_company" value="<?=@$name_of_holding_company;?>" required/>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <b>Contact person :</b>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                *Name
                                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="name" name="contact_person_name"  value="<?=@$contact_person_name;?>" required/>
                                            </div>
                                           <div class="col-xs-4">
                                                *Position
                                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="position" name="contact_person_position"  value="<?=@$contact_person_position;?>" required/>
                                            </div>
                                           <div class="col-xs-4">
                                                *Cell
                                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="cellphone" name="contact_person_cell"  value="<?=@$contact_person_cell;?>" required/>
                                            </div>
                                           <div class="col-xs-4">
                                                *Tel
                                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="telephone" name="contact_person_telephone"  value="<?=@$contact_person_telephone;?>" required/>
                                            </div>
                                             <div class="col-xs-4">
                                                *Email
                                                <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="email address" name="contact_person_email"  value="<?=@$contact_person_email;?>" required/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-xs-4">
                                        *Company Secretary Address:
                                        <textarea name ="company_secretary_address" class="form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"><?=trim(@$company_secretary_address);?></textarea>
                                    </div>
                                    <div class="col-xs-4">
                                        *Attorney & Office Address :
                                        <textarea name ="attorney_office_address" class="form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"> <?=trim(@$attorney_office_address);?></textarea>
                                    </div>
                                    <div class="col-xs-4">
                                        *Accountant & Office (Address) :
                                        <textarea name ="accountant_office_address" class="form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"><?=trim(@$accountant_office_address);?></textarea>
                                    </div>

                                    <div class="col-xs-4">
                                        *Auditors & Office (Address) :
                                        <textarea name ="auditors_office_address" class="form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"><?=trim(@$auditors_office_address);?></textarea>
                                    </div>

                                    <div class="col-xs-4">
                                        *Principal Bankers (Address) :
                                        <textarea name ="principal_bankers_address" class="form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"><?=trim(@$principal_bankers_address);?></textarea>
                                    </div>

                                    <div class="col-xs-4">
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-xs-6">
                                        *Authorised share capital:
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="authorised" name="authorised_share_capital" value="<?=@$authorised_share_capital;?>" required/>
                                    </div>
                                    <div class="col-xs-6">
                                        *Issued share capital :
                                        <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="issued" name="issued_share_capital" value="<?=@$issued_share_capital;?>" required/>
                                    </div>

                                </div>
                            </div>




                    </div>


                    <div class="login-footer">
                        <?php if($usertype=='applicant'){
                            ?>
                        <div class="row bs-reset">
                            <div  style="padding:0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_1_capital_requirements.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;" class="col-xs-4 bs-reset">
                                <button type="submit" class="btn green uppercase btn-outline pull-right">[Next >>]</button>
                            </div>
                        </div>
                         <?php } elseif ($usertype=='Administrator') {
                              ?>
                             <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <a href="applicantAlldetails.php?email=<?php echo $_GET['email'] ?>" class="btn green uppercase btn-outline pull-right">[<< Back]</a>
                            </div>
                            <?php } ?>
                    </div>

                    </form>

                </div>
 <?php
    require_once("require/footer.php") ;
 ?>