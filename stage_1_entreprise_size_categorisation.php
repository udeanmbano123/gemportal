<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$my_title  = "ENTERPRISE SIZE CATEGORISATION  3 - 5"   ;
$page_title = "Finsec | Stage One" ; 
$extra_css = "" ; 
$page_number = "" ;
$thy_stage = "1" ;
$extra_js = "" ;
$thy_qsn = "37/472" ;
$thy_qp = round(37/472 * 100, 2);
$main_page_title = "Basic Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
require("func/data/connect.php");
                              $user_email=$_SESSION['email'];
                       $sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
                   $result2=mysqli_query($con,$sql2);
                      while($row2=mysqli_fetch_array($result2)){
                        $usertype=$row2['users_type'];
                      }

if($usertype=='applicant'){
   $ifPopulated =  (new controlDAO())->getenterpriseSizeCategorisation()->selectOneEnterpriseSizeCategorisationBYEmail($_SESSION['email']) ;
    if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

     }else{
     $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
      }
       $setOption .= "<input type ='hidden' value = '".@$enterprise_size_categorisation_id."' name ='enterprise_size_categorisation_id'/>" ;
    //echo $user_id ;
    }
}
elseif($usertype=='Administrator'){
    $ifPopulated =  (new controlDAO())->getenterpriseSizeCategorisation()->selectOneEnterpriseSizeCategorisationBYEmail($_GET['email']) ;
    if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

     }else{
     $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
      }
       $setOption .= "<input type ='hidden' value = '".@$enterprise_size_categorisation_id."' name ='enterprise_size_categorisation_id'/>" ;
    //echo $user_id ;
    }


 }

?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_1_capital_requirements.php"> 1. CAPITAL REQUIREMENTS </a>
                                            </li>
                                            <li>    
                                                <a class="text-left" href="stage_1_corporate_directory.php"> 2. CORPORATE DIRECTORY OF <b class='font-green'><?=$_SESSION['company_name']?></b></a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>                        
                         
                        <form action="func/controller/enterpriseSizeCategorisationController.php"  class="login-form" method="post">
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>

                            <div class="white-box-main">

                                <div class="row">
                                    <div class="col-xs-12">
                                        Staff Levels :
                                      <select name  = "staff_levels" class="form-control form-control-solid placeholder-no-fix form-group"  style="padding-bottom: 2px;">
                                            <option <?=(@$staff_levels == "Up to 5 employees" ) ? 'selected="selected"' : '';?>  value = "Up to 5 employees">Up to 5 employees</option>
                                            <option <?=(@$staff_levels == "6 to 40 employees" ) ? 'selected="selected"' : '';?>  value = "6 to 40 employees">6 to 40 employees</option>
                                            <option <?=(@$staff_levels == "41 to 75 employees" ) ? 'selected="selected"' : '';?> value = "41 to 75 employees">41 to 75 employees</option>
                                            <option <?=(@$staff_levels == "76 and above" ) ? 'selected="selected"' : '';?>       value = "76 and above">76 and above</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        Annual Turnover:
                                        <select name  = "annual_turnover" class="form-control form-control-solid placeholder-no-fix form-group"  style="padding-bottom: 2px;">
                                            <option <?=(@$annual_turnover == "Up to $50,000" ) ? 'selected="selected"' : '';?>              value = "Up to $50,000">Up to $50,000</option>
                                            <option <?=(@$annual_turnover == "$50,001 to $500,000" ) ? 'selected="selected"' : '';?>        value = "$50,001 to $500,000">$50,001 to $500,000</option>
                                            <option <?=(@$annual_turnover == "$500,001 to $1,000,000" ) ? 'selected="selected"' : '';?>     value = "$500,001 to $1,000,000">$500,001 to $1,000,000</option>
                                            <option <?=(@$annual_turnover == "$1,000,001 and above" ) ? 'selected="selected"' : '';?>       value = "$1,000,001 and above">$1,000,001 and above</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        Gross Value of Assets:
                                        <select name  = "gross_value_of_assets" class="form-control form-control-solid placeholder-no-fix form-group"  style="padding-bottom: 2px;">
                                            <option <?=(@$gross_value_of_assets == "Up to $50,000" ) ? 'selected="selected"' : '';?>              value = "Up to $50,000">Up to $50,000</option>
                                            <option <?=(@$gross_value_of_assets == "$50,001 to $1,000,000" ) ? 'selected="selected"' : '';?>      value = "$50,001 to $1,000,000">$50,001 to $1,000,000</option>
                                            <option <?=(@$gross_value_of_assets == "$1,000,001 to $2,000,000" ) ? 'selected="selected"' : '';?>   value = "$1,000,001 to $2,000,000">$1,000,001 to $2,000,000</option>
                                            <option <?=(@$gross_value_of_assets == "$2,000,001 and above" ) ? 'selected="selected"' : '';?>       value = "$2,000,001 and above">$2,000,001 and above</option>
                                        </select>

                                    </div>
                                </div>

                            </div>




                    </div>


                    <div class="login-footer">
                        <?php if($usertype=='applicant') { ?>
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_1_corporate_directory.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
<!--                                <a href="stage_1_overview_of_company.php" class="btn green btn-outline pull-right" >[ Next >>] </a>-->
                                <button type="submit" class="btn green uppercase btn-outline pull-right">[Next >>]</button>
                            </div>
                        </div>
                         <?php } elseif ($usertype=='Administrator') {
                              ?>
                             <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <a href="applicantAlldetails.php?email=<?php echo $_GET['email'] ?>" class="btn green uppercase btn-outline pull-right">[<< Back]</a>
                            </div>
                            <?php } ?>




                    </div>


                    </form>

                </div>
<?php

require_once("require/footer.php") ;

?>