<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "" ; 
$page_number = "5" ; 
$extra_js = "" ;
$thy_stage = "2" ;
$thy_qsn = "44/472" ;
$thy_qp = round(44/472 * 100, 2);
$my_title = "PROPRIETORS, PARTNERS, PRINCIPALS & DIRECTORS PROFILES 1-6" ;
$main_page_title = "Intermediate Stage" ; 
@$msg=$_GET['msg'];
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
require("func/data/connect.php");
$directors_count = 0 ;
         
                              $user_email=$_SESSION['email'];
                       $sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
                   $result2=mysqli_query($con,$sql2);
                      while($row2=mysqli_fetch_array($result2)){
                        $usertype=$row2['users_type'];
                      }               

?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                            <div class="row bg-title" style="background-color: #f5f5f5;">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                                  <?php if($usertype=='applicant'){
                                    ?>
                             
                                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                    <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                    <ol class="breadcrumb">
                                        <li class="dropdown">
                                            <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                                [ Previous forms of this stage ]
                                            </a>
                                            <ul class="dropdown-menu mailbox animated ">
                                                <li>
                                                    <a class="text-left" href="stage_1_review.php"> <strong>STAGE 1 REVIEW </strong></a>
                                                </li>
                                            </ul>
                                            <!-- /.dropdown-messages -->
                                        </li>
                                        <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                    </ol>
                                </div>
                                <?php } ?>
                                <!-- /.col-lg-12 -->
                            </div>                            

                        <form action="func/controller/stagedirectors_profilesController.php"  class="login-form" method="post">
                            <div class="white-box-main">
                                <div class="row">
                                <div class="col-xs-12">
                                    <h5>List of directors : 
                                        <a href="stage_2_proprietors_partners_principapls_directors_profiles_edit.php?action=Add" >[ Add New ]</a> <p><?php echo "<br><font color='green'  >".@$msg. " </font>" ;?></p>
                                    </h5> 
                                    <table class="table table-bordered">
                                      <thead>

                                      <tr>
                                         <th>Title</th>
                                         <th>Forename</th>
                                         <th>Surname</th>
                                         <th>Age</th>
                                         <th>Citizenship</th>
                                         <th>Address</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                             
                                      if($usertype=='applicant'){
                                      $ifPopulated =  (new controlDAO())->getdirectors()->selectdirectorsByEmail($_SESSION['email']) ;
                                    }
                                    elseif($usertype=='Administrator'){
                                       $ifPopulated =  (new controlDAO())->getdirectors()->selectdirectorsByEmail($_GET['email']) ;
                                    }
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="6" style="text-align:center">No directors found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# director_id, user_id, , , ,
                                              //  directors_count,
                                              $directors_count++;
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>
                                                         <input type = 'hidden' name ='list_of_directors' value='set'>
                                                         ".@$ifPopulated[$i]['title']."</td>
                                                         <td>".@$ifPopulated[$i]['forename']."</td>
                                                         <td>".@$ifPopulated[$i]['surname']."</td>
                                                         <td>".@$ifPopulated[$i]['age']."</td>
                                                         <td>".@$ifPopulated[$i]['citizenship']."</td>
                                                         <td>".@$ifPopulated[$i]['address']."</td>
                                                         <td><a href='stage_2_proprietors_partners_principapls_directors_profiles_edit.php?id=".@$ifPopulated[$i]['director_id']."'>[Edit]</a> | <a href ='./func/controller/directorsController.php?id=".@$ifPopulated[$i]['director_id']."class='delete' >[Delete]</a></td>

                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      
                                      ?>


                                      </tbody>
                                    </table>

                                    <input type ='hidden' value = '<?php echo $directors_count ; ?>' name ='directors_count'/>
                                </div>

                            </div>
                            </div>



                    </div>


                    <div class="login-footer">
                      <?php if($usertype=='applicant'){
                       ?>
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_1_review.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <button type="submit" class="btn green btn-outline pull-right" >[ Next >>] </button>
                            </div>
                        </div>
                        <?php } elseif ($usertype=='Administrator') {
                              ?>
                             <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <a href="applicantAlldetails.php?email=<?php echo $_GET['email'] ?>" class="btn green uppercase btn-outline pull-right">[<< Back]</a>
                            </div>
                            <?php } ?>

                    </div>
                </form>

                </div>
    <!--  First form of the dialog
          The Company’s Products/Services  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">PROPRIETORS, PARTNERS, PRINCIPALS AND DIRECTORS PROFILES</h4> </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Title  :</label>
                            <input type="text" class="form-control" id="recipient-name1"> </div>
                        <div class="form-group">
                            <label for="recipient-name2" class="control-label">Forename :</label>
                            <input type="text" class="form-control" id="recipient-name3"> </div>
                        <div class="form-group">
                            <label for="recipient-name4" class="control-label">Surname  :</label>
                            <input type="text" class="form-control" id="recipient-name5"> </div>
                        <div class="form-group">
                            <label for="recipient-name6" class="control-label">Age :</label>
                            <input type="text" class="form-control" id="recipient-name7"> </div>
                        <div class="form-group">
                            <label for="recipient-name8" class="control-label">Citizenship  :</label>
                            <input type="text" class="form-control" id="recipient-name9"> </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Address:</label>
                            <textarea class="form-control" id="message-text1"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>



<?php

require_once("require/footer.php") ;

