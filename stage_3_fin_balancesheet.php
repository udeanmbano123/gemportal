<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ;
$extra_css = "    <!-- Editable CSS -->
     <link href='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css' rel='stylesheet'>" ;
$page_number = "10" ;
$extra_js = "    <!-- Editable -->
  <script src='//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.js'></script>
  
    " ;
$thy_stage = "3" ;
$thy_qsn = "183/472" ;
$thy_qp = round(183/472 * 100, 2);
$my_title = "Balance Sheet  5 - 9" ;
@$msg=$_GET['msg'];
$balance_sheet = "true" ;
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg'];
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
if($others->searchBS($_SESSION['email']) == 0) {
    $others->setCompanyBalanceSheet($_SESSION['email'] , '2015') ;
    $others->setCompanyBalanceSheet($_SESSION['email'] , '2016') ;
    $others->setCompanyBalanceSheet($_SESSION['email'] , '2017') ;
    $others->setCompanyBalanceSheet($_SESSION['email'] , '2018') ;
    $others->setCompanyBalanceSheet($_SESSION['email'] , '2019') ;
    $others->setCompanyBalanceSheet($_SESSION['email'] , '2020') ;
    $others->setCompanyBalanceSheet($_SESSION['email'] , '2021') ;
    $others->setCompanyBalanceSheet($_SESSION['email'] , '2022') ;
}
$get_financials_cashflows=  (new controlDAO())->getlast2yrs()->selectBalanceSheet($_SESSION['email']) ;

$ifPopulated =  (new controlDAO())->getuploadFinancials()->selectOneuploadFinancialsByEmailBalanceSheet($_SESSION['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
    }
    $setOption .= "<input type ='hidden' value = '".@$id."' name ='id'/>" ;
    //echo $user_id ;
}


?>
<script language="javascript">
setInterval(function(){
   window.location.reload(1);
}, 30000);
</script>

                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;padding:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>

                            <!-- /row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="white-box">
                                        <h3 class="box-title">Balance Sheet Checked Items </h3>
                                        <form action="func/controller/finBalancesheetController.php" enctype="multipart/form-data"  class="login-form" method="post">
                                            <?=@$setOption;?>
                                            <input type="hidden" value="<?php echo $_SESSION['email']?>"  name="user_id">
                                            <table class="table table-bordered responsive">
                                                <thead>
                                                <th class="c-gray">#</th>
                                                <th class="c-gray">Is Audited</th>
                                                <th class="c-gray"> Upload File</th>
                                                <th class="c-gray"> </th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>  <input type="checkbox"
                                                            <?php
                                                            if(@$is_audited =="yes"){
                                                                echo "checked" ;
                                                            }

                                                        ?> value="yes" name="is_audited"  /></td>
                                                    <td>  <input type="file" name="upload_file"  /></td>
                                                    <td> <a target="_blank" href="<?=$doc_url;?>"> <?=@$doc_url;?></a> </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <button type="submit" class="btn green btn-outline pull-right" >[ Save ] </button>
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="white-box-main">
                                        <h3 class="box-title">CONSOLIDATED STATEMENT OF FINANCIAL POSITION</h3>
                                        <p class="text-muted">*Just click on word which you want to change and enter , page refreshes every 30 seconds to update figures</p>
                                        <div id ="div_t1">
                                        <table id="balancesheet_table" class="table editable-table table-bordered m-b-0">
                                            <thead class="text-uppercase">
                                                <th width="250" class="c-gray"></th>
                                                <th colspan="3" class="c-gray">HISTORICAL</th>
                                                <th colspan="5" class="c-gray">PROJECTED</th>
                                            </thead>
                                            <tbody>
                                            <colgroup>
                                                <col  style="background-color:#f5f5f5;">
                                                <col span="3" style="background-color:yellow">
                                                <col span="5" style="background-color:pink">
                                            </colgroup>

                                            <tr>
                                                <td>Year</td><td>2015</td><td>2016</td><td>2017</td><td>2018</td><td>2019</td><td>2020</td><td>2021</td><td>2022</td>
                                            </tr>
                                            <tr>
                                                <td>Property,plant and equipment</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'non_current_assests_Propertyplantandequipment' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['non_current_assests_Propertyplantandequipment']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Investment properties </td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'non_current_assests_Investmentproperties' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['non_current_assests_Investmentproperties']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr id ="ttl_aseti"></tr>
                                            <tr>
                                                <td>Intangible assets </td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'non_current_assests_Intangibleassets' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['non_current_assests_Intangibleassets']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Other NON CURRENT ASSETS</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'non_current_assests_Other' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['non_current_assests_Other']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr class="non_curr_ass_ttl" style="background-color: #000;color: #fff;"><tr>
                                            <tr>
                                                <td>Biological assets</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_assets_Biologicalassets' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_assets_Biologicalassets']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Inventories</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_assets_Inventories' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_assets_Inventories']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Trade & other receivables </td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_assets_Tradeotherreceivables' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_assets_Tradeotherreceivables']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Prepayments </td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_assets_Prepayments' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_assets_Prepayments']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Bank </td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_assets_Bank' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_assets_Bank']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Cash </td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_assets_Cash' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_assets_Cash']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Other CURRENT ASSETS</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_assets_Other' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_assets_Other']."</td>" ;
                                                }
                                                ?>


                                            </tr>

                                            <tr class="curr_ass_ttl" style="background-color: #000;color: #fff;"><tr>
    <tr class="ttl_equ" style="background-color: #000;color: #fff;"><tr>

                                         
                                            <tr>
                                                <td>Issued share capital</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'equity_attributed_Issuedsharecapital' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['equity_attributed_Issuedsharecapital']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Share premium</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'equity_attributed_Sharepremium' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['equity_attributed_Sharepremium']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Revenue reserves</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'equity_attributed_Revenuereserves' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['equity_attributed_Revenuereserves']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Capital reserves</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'equity_attributed_Capitalreserves' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['equity_attributed_Capitalreserves']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Other Capital and reserves</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'equity_attributed_Other' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['equity_attributed_Other']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr class="ttl_equattr" style="background-color: #000;color: #fff;"><tr>
                                            <tr>
                                                <td>Non controlling interest</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'non_controlling_interest' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['non_controlling_interest']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr class="ttl_equity" style="background-color: #000;color: #fff;"><tr>
                                            <tr>
                                                <td>Long term borrowings</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'non_current_liabilities_Longtermborrowings' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['non_current_liabilities_Longtermborrowings']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Defferred tax liabilities</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'non_current_liabilities_Defferredtaxliabilities' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['non_current_liabilities_Defferredtaxliabilities']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Other NON- CURRENT LIABILITIES</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'non_current_liabilities_Other' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['non_current_liabilities_Other']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr class="non_curr_lia_ttl" style="background-color: #000;color: #fff;"><tr>
                                            <tr>
                                                <td>Short term borrowings</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_liabilities_Shorttermborrowings' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_liabilities_Shorttermborrowings']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Trade and other payables</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_liabilities_Tradeotherpayables' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_liabilities_Tradeotherpayables']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Current tax liability</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_liabilities_Currenttaxliability' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_liabilities_Currenttaxliability']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr>
                                                <td>Other CURRENT LIABILITIES</td>

                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td data-name = 'current_liabilities_Other' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['current_liabilities_Other']."</td>" ;
                                                }
                                                ?>


                                            </tr>
                                            <tr class="curr_lia_ttl" style="background-color: #000;color: #fff;"><tr>
 <tr class="lia_ttl" style="background-color: #000;color: #fff;"><tr>
 <tr class="ttl_equlia" style="background-color: #000;color: #fff;"><tr>



                                            </tbody>
											
                                        </table>
                                    </div>
                                        <h3 class="box-title">OTHERS </h3>
                                        <div id ="div_t2">
                                        <table id="balancesheet_table_other" class="table editable-table table-bordered m-b-0">
                                            <thead class="text-uppercase">
                                                <th width="250" class="c-gray"></th>
                                                <th colspan="3" class="c-gray">HISTORICAL</th>
                                                <th colspan="5" class="c-gray">PROJECTED</th>
                                            </thead>
                                            <tbody>
                                            <colgroup>
                                                <col  style="background-color:#f5f5f5;">
                                                <col span="3" style="background-color:yellow">
                                                <col span="5" style="background-color:pink">
                                            </colgroup>
                                            <tr>
                                                <td>Year</td><td>2015</td><td>2016</td><td>2017</td><td>2018</td><td>2019</td><td>2020</td><td>2021</td><td>2022</td>
                                            </tr>
                                            <tr id='ttl_imwe'>
                                                <td>Total Ordinary shares</td>
                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td  data-name = 'OtherTotal_Ordinary_shares' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['OtherTotal_Ordinary_shares']."</td>" ;
                                                }
                                                ?>
                                            </tr>
                                            <tr id='ttl_imwe'>
                                                <td>Market price per share (Book Value of Share) </td>
                                                <?php

                                                for($i = 0  ; $i < count($get_financials_cashflows) ; $i++ ) {
                                                    echo "<td   data-name = 'OtherMarket_price_per_share' 
                                    class='name' data-type='text' data-pk='".$get_financials_cashflows[$i]['fin_balance_sheet_id']."
                                    '>".$get_financials_cashflows[$i]['OtherMarket_price_per_share']."</td>" ;
                                                }
                                                ?>
                                            </tr>
                                            <tr style="background-color: #000;color: #fff;"></tr>
                                            </tbody>
                                        </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->






                    </div>
                            <div class="login-footer">
                                <div class="row bs-reset">
                                    <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                        <a href ="stage_3_material_litigation_claims.php" class="btn green btn-outline">[<< Back]</a>
                                    </div>
                                    <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                        <a href ="stage_3_fin_profitandloss.php" type="submit" class="btn green btn-outline pull-right" >[ Next >>] </a>
                                    </div>
                                </div>
                            </div>

                </div>
<?php
$mee_test = "hello" ; 
require_once("require/footer.php") ;

?>