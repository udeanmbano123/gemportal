<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "" ; 
$page_number = "5" ; 
$extra_js = "" ;
$thy_stage = "2" ;
$main_page_title = "Intermediate Stage" ; 
@$label=$_GET['action'];
@$id=$_GET['id'];
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;

$ifPopulated =  (new controlDAO())->getcommittees()->selectcommitteeByDirectorId(@$id) ;
foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
?>
<div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                         <h1 class="text-uppercase "> <?php if ($label){
                            echo 'Add Committee';
                         }
                         else {
                          echo  'Edit Committee';
                         }  ; ?></h1>
                        <form action="func/controller/committeesController.php"  class="login-form" method="post">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Please complete all the required field with * asterisks  </span>
                            </div>
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                            <input type ="hidden" value= "<?= @$_GET['id'] ?>"  name ="id"/>
                            <div class="row">
                                
                                
                           
                                    	<div class="col-xs-4">
                                    *Committee Name
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Committee Name" name="name" value="<?=@$committee_name;?>" required/>
                                </div>
                                        <div class="col-xs-4">
                                            *Chairman
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Chairman" name="chairman"  value="<?=@$chairman;?>" required/>
                                        </div>
                                       <div class="col-xs-4">
                                            *Attendees
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Attendees" name="attendances"  value="<?=@$attendees;?>" required/>
                                        </div>

                                    

                                
                            </div>  
                            <div class="row">  
                                <div class="col-xs-4">
                                    *Quorum
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="quorum" name="quoram" value="<?=@$quoraum;?>" required/>
                                </div>
                                <div class="col-xs-4">
                                    *Monthly Meeting Frequency
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="number" min="0"  placeholder="Meeting frequency" name="frequency" value="<?=@$meeting_frequency;?>" required/>
                                    
                                </div>                                
                                <div class="col-xs-4">
                                    *Committee Responsibilities
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Responsibilities" name="responsibilities" value="<?=@$committee_responsibilities;?>" required/>
                                </div>
                                <div class="row"> 
                               <div class="col-xs-4">
                               	<button type="submit" class="btn btn-primary">Save</button>
                               </div>
                                </div>


                            </div>