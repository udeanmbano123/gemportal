<?php
require_once 'func/controlDAO.php';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Finsec | SME Portal</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">


    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
</head>
<body class="fix-sidebar fix-header">
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">

    <?php
    include("req/header_main.php");
    $extra_js = "";
    ?>


    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Finsec Admin </h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li class="active"><a href="#">Admin </a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>


            <!-- .row -->
            <div class="row">
                <div class="col-sm-12">

                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-2  inbox-panel">
                                <div>
                                    <?php
                                    require_once 'menu-sidebar.php';
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-8 ">
                                <!-- DASHBOARD -->

                                <div class="row col-12">
                                    <div class="col-md-6">
                                        <canvas id="line" width="400" height="400"></canvas>
                                    </div>
                                </div>

                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title" id="exampleModalLabel1">Countries are:</h4>
                                            </div>
                                            <form action="func/controller/countriesController.php" method="POST"
                                                  name="product_service">
                                                <input type="hidden" value="<?= $_SESSION['email'] ?>" name="user_id"/>
                                                <input type='hidden' value='true' name='create_prod' id="optn"/>
                                                <input type='hidden' name='country_id' id="cops_id"/>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label"> Name
                                                            :</label>
                                                        <input type="text" name="name" class="form-control" id="ename">
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">Save</button>
                                                    </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $extra_js = "
<script>
$(document).ready(function(){
    
 
  $(document).on('click', '.new_product', function(){
    $('input[name=edit_prod]').attr('name', 'create_prod');
    $('#exampleModal').modal('show');
    $('#eproduct_service_name').val('');
    $('#edescription_of_product').val('');
    $('#esales_volume').val('');
  });
  $(document).on('click', '.edit_product', function(){
    $('#ename').val('');
          
    var id=$(this).attr('id').split('_')[1];
    $('input[name=create_prod]').attr('name', 'edit_prod');
    var first=$('#name'+id).text();
    
    var address=$('#sales_volume'+id).text();
        //alert(id+first+last+address) ; 
    $('#exampleModal').modal('show');
    $('#ename').val(first.trim());
    var last=$('#idnum'+id).text();
    $('#cops_id').val(last.trim());
    
  });

  
});
</script>
";
                                ?>

                                <!-- /.container-fluid -->
                                <footer class="footer text-center"> 2017 &copy; Escrow System</footer>
                            </div>
                            <!-- /#page-wrapper -->
                        </div>
                        <!-- /#wrapper -->
                        <!-- jQuery -->
                        <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
                        <!-- Bootstrap Core JavaScript -->
                        <script src="bootstrap/dist/js/bootstrap.min.js"></script>
                        <!-- Menu Plugin JavaScript -->
                        <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
                        <!--slimscroll JavaScript -->
                        <script src="js/jquery.slimscroll.js"></script>
                        <!--Wave Effects -->
                        <script src="js/waves.js"></script>
                        <!--Counter js -->
                        <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
                        <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
                        <!-- Custom Theme JavaScript -->
                        <script src="js/custom.min.js"></script>
                        <script src="public/assets/global/plugins/moment.min.js"></script>
                        <!--Style Switcher -->
                        <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
                        <!-- Date Picker Plugin JavaScript -->
                        <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
                        <!-- Date range Plugin JavaScript -->
                        <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
                        <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
                        <script src="plugins/bower_components/morrisjs/morris.js"></script>
                        <script src="js/morris-data.js"></script>

                        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

                        <script>
                            var ctx = document.getElementById("line").getContext('2d');
                            var line = new Chart(ctx, {
                                type: 'line',
                                data: {
                                    labels: ["Micro", "Small", "Medium"],
                                    datasets: [{
                                        label: 'Thresholds',
                                        data: [12, 5, 17],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 159, 64, 0.2)'
                                        ],
                                        borderColor: [
                                            'rgba(255,99,132,1)',
                                            'rgba(153, 102, 255, 1)',
                                            'rgba(255, 159, 64, 1)'
                                        ],
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        </script>


                        <!--Style Switcher -->

                        <?php echo $extra_js; ?>
                        <script>
                            // Date Picker
                            jQuery('.mydatepicker, #datepicker').datepicker();
                            jQuery('#datepicker-autoclose').datepicker({
                                autoclose: true,
                                todayHighlight: true
                            });

                        </script>

</body>

</html>