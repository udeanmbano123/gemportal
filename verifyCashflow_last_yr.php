





<?php

require_once 'func/controlDAO.php' ;
$ifPopulated =  (new controlDAO())->getCorporateDirectory()->selectOnecorporateDirectoryByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
     foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>



<!DOCTYPE html>



<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

        <?php 
          include("req/header_main.php") ;
          

        ?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Finsec Admin [Acceptance]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-2  inbox-panel">
                                    <div>
                                        <div class="list-group mail-list "> 
                                          <a href="admin_home.php" class="list-group-item ">Home </a>
                                          <a href="f_dashboard.php" class="list-group-item ">Pending Acceptance </a>
                                          <a href="f_dashboard_accept.php" class="list-group-item active">Accepted </a>
                                          <a href="f_dashboard_rejected.php" class="list-group-item">Rejected </a>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-10 mail_listing">
                                    
                                         <div class="col-md-12 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #868e97;">
                        <div class="login-content" style ="margin-top:0px;">
                         <h4 class="text-uppercase">CASH  FLOW</h4>
                            <form action="func/controller/GroupPermissionController.php" class="login-form" method="post">
                            <input type ="hidden" value="signup" name="signup"/>
                            <input type ="hidden" value= "<?=@$id ?>"  name ="id"/>
                            

                            
                            
                                
                            <div class="form-group">
                              <div class="row">
                                <b>Year:</b> <?=@$ifPopulated[$i]['duration_in_stated_business_line'];?>&nbsp;&nbsp;&nbsp;&nbsp; <input   name="pem[]" type="checkbox"  value="7" />

                               
                              
                               </div>
                            </div>                            
                            <div class="form-group">
                                <b>Is Audited:</b> <?=@$ifPopulated[$i]['customer_basecorporates'];?>&nbsp;&nbsp;&nbsp;&nbsp;<input   name="pem[]" type="checkbox"  value="7" />
                                





                            </div>     
                            <div class="form-group">
                                <b> Operating Activities: </b><?php echo @$customer_indviduals;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  name="pem[]" type="checkbox"  value="4" />
                                 

                            </div>     
                            <div class="form-group">
                               <b> Profit or loss before tax from continuing operations: </b>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo @$business_year_end;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  name="pem[]"  type="checkbox"  value="5" />
                                
                            </div>     
                             <div class="form-group">
                              <b> Profit or loss before tax from discontinued  operations: </b>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo @$business_year_end;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  name="pem[]"  type="checkbox"  value="5" />
                                

                            </div>
                            <div class="form-group">
                              <b>  Profit or loss before tax:<?php echo @$nature_of_business;?> </b>&nbsp;&nbsp;&nbsp;&nbsp; <input  type="checkbox"  name="pem[]"  value ="8" />
                              

                            </div>


                            <div class="form-group">
                               <b> Adjusted for: </b> &nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$contact_person_name;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  name="pem[]"  type="checkbox"  value="5" />
                               




                            </div>     
                             <div class="form-group">
                               <b> Depreciation:</b>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo @$contact_person_position;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="9" />
                               

                            </div>
                            <div class="form-group">
                               <b> Impairments:</b>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo @$contact_person_cell;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />
                               

                            </div>
                            <div class="form-group">
                               <b> Ammortisations: </b>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo @$contact_person_telephone;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  name="pem[]"  type="checkbox"  value="5" />
                              

                            </div>     
                             <div class="form-group">
                                <b>Loss or Profit on Disposal of Assets:</b>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo @$contact_person_email;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="9" />
                                

                            </div>
                            <div class="form-group">
                                <b>Profit or Disposal of Assets</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$company_secretary_address;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                             <div class="form-group">
                                <b>Profit on Disposal of Assets Classified held for sale</b>: &nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$attorney_office_address;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="9" />

                            </div>
                            <div class="form-group">
                                <b>Finance Income</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$accountant_office_address;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Income Tax Paid</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$auditors_office_address;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                             <div class="form-group">
                                <b>Working Capital Adjustments</b>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo @$principal_bankers_address;?>&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="9" />

                            </div>
                            <div class="form-group">
                                <b>Increase or Decrease in Inventory</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$authorised_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Increase or Decrease in Debtors</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$authorised_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Increase or Decrease in Creditors</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$authorised_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Net Cash Flows From Operating Activity</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div> 
                             <div class="form-group">
                                <b>Purchase of fixed assets for maintaning operations</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div> 
                            <div class="form-group">
                                <b>Initial Capital paid in</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>(Purchase)/sale of long term financial securities</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                             <div class="form-group">
                                <b>Puchase of Land</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Increase/(decrease) in loans and investments</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                             <div class="form-group">
                                <b>Proceeds from disposal of property and equipment</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Interest received/(Paid)</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>NET CASHFLOWS FROM INVESTING ACTIVITIES</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                             <div class="form-group">
                                <b>FINANCING ACTIVITIES</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Dividends paid in cash</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                             <div class="form-group">
                                <b>Increase in shareholder funding</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Share buy back</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Repayment of borrowings</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div> 
                            <div class="form-group">
                                <b>NET CASHFLOW FROM FINANCING ACTIVITIES</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Net increase in cash and cash equivalents</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Cash and cash equivalents at the beginning of the year</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>
                            <div class="form-group">
                                <b>Cash and cash equivalents at the end of the period</b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo @$issued_share_capital;?>:&nbsp;&nbsp;&nbsp;&nbsp;<input  type="checkbox"  name="pem[]"  value ="8" />

                            </div>                                                                           
                            <div >
                                <button type="submit" class="btn btn-primary">Save</button>
                               </div>
                          </form>

                            </div>

                            





                    </div>

<!--                                         <tr>
                                           <td colspan="4" style="text-align:center">No Appliacction found</td>
                                        </tr>  -->
                                        
                                    
                                        
                                     
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>