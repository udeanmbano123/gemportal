<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ; 
$extra_css = "" ; 
$page_number = "10" ; 
$extra_js = "" ;
$thy_stage = "3" ;
$thy_qsn = "472/472" ;
$thy_qp = "100";
$my_title = "PERSONAL FINANCIAL COMMITMENTS  7 - 9" ;
@$msg=$_GET['msg'];
$main_page_title = "Advanced Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;

?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;padding:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_fin_profitandloss.php"> <strong>Income Statement</strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_fin_cashflow.php"> <strong>Cash Flow </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_financials_balancesheet.php"> <strong>Balance Sheet  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <form action="func/controller/stagefincommitController.php"  class="login-form" method="post">

                            <div class="white-box-main">
                            <div class="row">  
                                <div class="col-xs-12">
                                    <h5>Lists : <a href="stage_3_personal_financial_commitments_edit.php?action=add" >[ Add New ]</a></a> <p><?php echo "<br><font color='green'  >".@$msg. " </font>" ;?></p> </h5> 
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                         <th>PRINCIPAL / GUARANTOR’S NAME</th>
                                         <th>Financial Institution   </th>
                                         <th>Facility Type   </th>
                                         <th>Facility Amount </th>
                                         <th>Monthly Installment </th>
                                         <th>Balance </th>
                                         <th>Tenure (Months) </th>
                                         <th>Start Date</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <?php
                                      $ifPopulated =  (new controlDAO())->getpersonalFinancial()->selectpersonalFinancialByEmail($_SESSION['email']) ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="8" style="text-align:center">No entry found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# personal_financial_id, user_id, , , , , , , ,
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>
                                                         <input type ='hidden' value = 'set' name ='fin_commitment'/>
                                                         ".@$ifPopulated[$i]['guarantor_name']."</td>
                                                         <td>".@$ifPopulated[$i]['financial_institution']."</td>
                                                         <td>".@$ifPopulated[$i]['facility_type']."</td>
                                                         <td>".@$ifPopulated[$i]['facility_amount']."</td>
                                                         <td>".@$ifPopulated[$i]['monthly_installment']."</td>
                                                         <td>".@$ifPopulated[$i]['balance']."</td>
                                                         <td>".@$ifPopulated[$i]['tenure']."</td>
                                                         <td>".@$ifPopulated[$i]['start_date']."</td>
                                                          <td><a href='stage_3_personal_financial_commitments_edit.php?id=".@$ifPopulated[$i]['personal_financial_id']."'>[Edit]</a> | <a href ='./func/controller/personalFinancialController.php?id=".@$ifPopulated[$i]['personal_financial_id']."' >[Delete]</a></td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>

                                      </tbody>
                                    </table>
                                </div>

                            </div>
                            </div>




                    </div>


                    <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_3_fin_cashflow.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                <button type="submit" class="btn green btn-outline pull-right" >[ Next >>] </button>
                            </div>
                        </div>
                    </div>


                    </form>

                </div>


    <!--  Second form of the dialog
          PERSONAL FINANCIAL COMITMENTS  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">PERSONAL FINANCIAL COMITMENTS</h4> </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">PRINCIPAL / GUARANTOR’S NAME  :</label>
                            <input type="text" class="form-control" id="recipient-name1"> </div>
                        <div class="form-group">
                            <label for="recipient-name2" class="control-label">Financial Institution :</label>
                            <input type="text" class="form-control" id="recipient-name3"> </div>
                        <div class="form-group">
                            <label for="recipient-name4" class="control-label">Facility Type  :</label>
                            <input type="text" class="form-control" id="recipient-name5"> </div>
                        <div class="form-group">
                            <label for="recipient-name6" class="control-label">Facility Amount :</label>
                            <input type="text" class="form-control" id="recipient-name7"> </div>
                        <div class="form-group">
                            <label for="recipient-name8" class="control-label">Monthly Installment  :</label>
                            <input type="text" class="form-control" id="recipient-name9"> </div>
                        <div class="form-group">
                            <label for="recipient-name10" class="control-label">Balance  :</label>
                            <input type="text" class="form-control" id="recipient-name11"> </div>
                        <div class="form-group">
                            <label for="recipient-name12" class="control-label">Tenure (Months)  :</label>
                            <input type="text" class="form-control" id="recipient-name13"> </div>
                        <div class="form-group">
                            <label for="recipient-name14" class="control-label">Start Date  :</label>
                            <input type="text" class="form-control" id="recipient-name15"> </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>




<?php

require_once("require/footer.php") ;

?>