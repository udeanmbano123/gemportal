<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ;
$extra_css = "    <!-- Editable CSS -->
     <link href='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css' rel='stylesheet'>
     <link href='plugins/bower_components/summernote/dist/summernote.css' rel='stylesheet' />
     " ;
$page_number = "10" ;
$extra_js = "    <!-- Editable -->
  <script src='//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.js'></script>
      <script src='plugins/bower_components/summernote/dist/summernote.min.js'></script>
    <script>
    jQuery(document).ready(function() {
        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });
        $('.inline-editor').summernote({
            airMode: true
        });
    });
    window.edit = function() {
        $('.click2edit').summernote()
    }, window.save = function() {
        $('.click2edit').destroy()
    }
    </script>
    " ;
$thy_stage = "3" ;
$thy_qsn = "183/472" ;
$thy_qp = round(183/472 * 100, 2);
$my_title = "Business Plan  8 - 8" ;
@$msg=$_GET['msg'];
$balance_sheet = "true" ;
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg'];
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
if($others->searchBP($_SESSION['email']) == 0) {
    $others->setBusinessPlan($_SESSION['email']) ;
}
$get_BizList=$others->getBusinessPlan($_SESSION['email']);

?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;padding:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>

                            <!-- /row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="white-box-main">
                                        <h3 class="box-title">Provide the pending items of the business plan </h3>
                                        <p class="text-muted">*Just click on word which you want to change and enter</p>

                                        <table id="balancesheet_table_other" class="table editable-table table-bordered m-b-0">
                                            <thead class="text-uppercase">
                                            <th class="c-gray"><strong>Items</strong></th>
                                            <th class="c-gray"><strong>Topics </strong></th>
                                            <th class="c-gray"><strong>Status</strong></th>
                                            </thead>

                                            <tbody>

                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <strong>1.0 Executive Summary</strong>
                                                </td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    1.
                                                </td>
                                                <td >
                                                    1.1 Problem
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Executive_Problem']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Problem' data-title='Executive Summary' class='new_busplan'
                                                       id='Executive_Problem'>".$status."</a>";

                                  
                                ?>
                                                  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    2.
                                                </td>
                                                <td >
                                                    1.2 Solution
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Executive_Solution']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Solution' data-title='Executive Summary' class='new_busplan'
                                                       id='Executive_Solution'>".$status."</a>";

                                  
                                ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    3.
                                                </td>
                                                <td >
                                                    1.3 Operating Environment of Issuer and Business Units (PESTEL)
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Executive_PESTEL']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Operating Environment of Issuer and Business Units (PESTEL)' data-title='Executive Summary' class='new_busplan'
                                                       id='Executive_PESTEL'>".$status."</a>";

                                  
                                ?>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    4.
                                                </td>
                                                <td >
                                                    1.5 Financial Highlights
                                                </td>
                                                <td >
                                                    Available
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >&nbsp;</td>
                                                <td >
                                                    <strong>2.0 Opportunity</strong>
                                                </td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    5.
                                                </td>
                                                <td >
                                                    2.1 Problem Statement
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Opportunity_Problem_Statement']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Solution' data-title='Opportunity' class='new_busplan'
                                                       id='Opportunity_Problem_Statement'>".$status."</a>";

                                  
                                ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    6.
                                                </td>
                                                <td >
                                                    2.2 Proposed Solution
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Opportunity_Proposed_Solution']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Proposed Solution' data-title='Opportunity' class='new_busplan'
                                                       id='Opportunity_Proposed_Solution'>".$status."</a>";

                                  
                                ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    7.
                                                </td>
                                                <td >
                                                    2.3 Validation of Problem and Solution
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Opportunity_Validation_of_Problem_Solution']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Validation of Problem and Solution' data-title='Opportunity' class='new_busplan'
                                                       id='Opportunity_Validation_of_Problem_Solution'>".$status."</a>";

                                  
                                ?>
                                               </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    8.
                                                </td>
                                                <td >
                                                    2.4 Future Prospects of Issuer
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Opportunity_Future_Prospects_of_Issuer']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Future Prospects of Issuer' data-title='Opportunity' class='new_busplan'
                                                       id='Opportunity_Future_Prospects_of_Issuer'>".$status."</a>";

                                  
                                ?>
                                             
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >&nbsp;</td>
                                                <td >
                                                    <strong>3.0 Market Analysis Summary</strong>
                                                </td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    9.
                                                </td>
                                                <td >
                                                    3.1 Market Segmentation
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Market_Segmentation']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Market Segmentation' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Market_Segmentation'>".$status."</a>";

                                  
                                ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    10.
                                                </td>
                                                <td >
                                                    3.2 Target Market Segment Strategy
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Target_Market_Segment_Strategy']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Target Market Segment Strategy' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Target_Market_Segment_Strategy'>".$status."</a>";

                                  
                                ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    11.
                                                </td>
                                                <td >
                                                    3.2.1 Market Needs
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Market_Needs']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Market Needs' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Market_Needs'>".$status."</a>";

                                  
                                ?>
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    12.
                                                </td>
                                                <td >
                                                    3.2.2 Market Trends
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Market_Trends']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Market Trends' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Market_Trends'>".$status."</a>";

                                  
                                ?>
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    13.
                                                </td>
                                                <td >
                                                    3.2.3 Market Growth
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Market_Growth']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Market Growth' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Market_Growth'>".$status."</a>";

                                  
                                ?>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    14.
                                                </td>
                                                <td >
                                                    3.3 Key Customers
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Key_Customers']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Key Customers' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Key_Customers'>".$status."</a>";

                                  
                                ?>
                                               </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    15.
                                                </td>
                                                <td >
                                                    3.4 Future Markets
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Future_Markets']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Future Markets' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Future_Markets'>".$status."</a>";

                                  
                                ?>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    16.
                                                </td>
                                                <td >
                                                    3.5 Competition
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Competition']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Competition' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Competition'>".$status."</a>";

                                  
                                ?>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    17.
                                                </td>
                                                <td >
                                                    3.5.1 Competitors and Alternatives
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Competitors_and_Alternatives']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Competitors and Alternatives' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Competitors_and_Alternatives'>".$status."</a>";

                                  
                                ?>
                                
                                           </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    18.
                                                </td>
                                                <td >
                                                    3.5.2 Our Advantages
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Market_Analysis_Our_Advantages']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Our Advantages' data-title='Market Analysis Summary' class='new_busplan'
                                                       id='Market_Analysis_Our_Advantages'>".$status."</a>";

                                  
                                ?>
                                               </td>
                                            </tr>
                                            <tr>
                                                <td >&nbsp;</td>
                                                <td >
                                                    <strong>4.0 Execution </strong>
                                                </td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    19.
                                                </td>
                                                <td >
                                                    4.1 Marketing Plan
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Execution_Marketing_Plan']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Marketing Plan' data-title='Execution' class='new_busplan'
                                                       id='Execution_Marketing_Plan'>".$status."</a>";

                                  
                                ?>
                                                  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    20.
                                                </td>
                                                <td >
                                                    4.2 Sales Plan
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Execution_Sales_Plan']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Sales Plan' data-title='Execution' class='new_busplan'
                                                       id='Execution_Sales_Plan'>".$status."</a>";

                                  
                                ?>
                                           
                                             </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    21.
                                                </td>
                                                <td >
                                                    4.3 Location and Facilities
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Execution_Location_and_Facilities']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Location and Facilities' data-title='Execution' class='new_busplan'
                                                       id='Execution_Location_and_Facilities'>".$status."</a>";

                                  
                                ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    22.
                                                </td>
                                                <td >
                                                    4.4 Technology
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Execution_Technology']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Technology' data-title='Execution' class='new_busplan'
                                                       id='Execution_Technology'>".$status."</a>";
?>
                                  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    23.
                                                </td>
                                                <td >
                                                    4.5 Equipment and Tools
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Execution_Equipment_Tools']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Equipment and Tools' data-title='Execution' class='new_busplan'
                                                       id='Execution_Equipment_Tools'>".$status."</a>";
?>
                                                  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    24.
                                                </td>
                                                <td >
                                                    4.6 Milestones
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Execution_Milestones']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Milestones' data-title='Execution' class='new_busplan'
                                                       id='Execution_Milestones'>".$status."</a>";
?>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    25.
                                                </td>
                                                <td >
                                                    4.7 Key Metrics
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Execution_Key_Metrics']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Key Metrics' data-title='Execution' class='new_busplan'
                                                       id='Execution_Key_Metrics'>".$status."</a>";
?>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >&nbsp;</td>
                                                <td >
                                                    <strong>5.0 Company and Management Summary</strong>
                                                </td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    26.
                                                </td>
                                                <td >
                                                    5.1 Corporate Governance Framework
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Company_Management_Corporate_Governance_Framework']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Corporate Governance Framework' data-title='Company and Management Summary' class='new_busplan'
                                                       id='Company_Management_Corporate_Governance_Framework'>".$status."</a>";
?>
                                             
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    27.
                                                </td>
                                                <td >
                                                    5.2 Management Team
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Company_Management_Management_Team']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Management Team' data-title='Company and Management Summary' class='new_busplan'
                                                       id='Company_Management_Management_Team'>".$status."</a>";
?>
                                 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    28.
                                                </td>
                                                <td >
                                                    5.3 Management Team Gaps
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Company_Management_Management_Team_Gaps']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Management Team Gaps' data-title='Company and Management Summary' class='new_busplan'
                                                       id='Company_Management_Management_Team_Gaps'>".$status."</a>";
?>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    29.
                                                </td>
                                                <td >
                                                    5.4 Personnel Plan
                                                </td>
                                                <td >
<?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Company_Management_Personnel_Plan']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Personnel Plan' data-title='Company and Management Summary' class='new_busplan'
                                                       id='Company_Management_Personnel_Plan'>".$status."</a>";
?>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    30.
                                                </td>
                                                <td >
                                                    5.5 Company History and Ownership
                                                </td>
                                                <td >
<?php
                 $status="Not Available";
                                    for($i = 0  ; $i < count($get_BizList) ; $i++ ) {
										if($get_BizList[$i]['Company_Management_Company_History_Ownership']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Company History and Ownership' data-title='Company and Management Summary' class='new_busplan'
                                                       id='Company_Management_Company_History_Ownership'>".$status."</a>";
?>
                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >&nbsp;</td>
                                                <td >
                                                    <strong>6.0 Financial Plan</strong>
                                                </td>
                                                <td >&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    31.
                                                </td>
                                                <td >
                                                    6.1 Revenue/Sales Forecast
                                                </td>
                                                <td >
                                                    Available
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    32.
                                                </td>
                                                <td >
                                                    6.2 Expenses
                                                </td>
                                                <td >
                                                    Available
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    33.
                                                </td>
                                                <td >
                                                    6.3 Projected Profit and Loss
                                                </td>
                                                <td >
                                                    Available
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    34.
                                                </td>
                                                <td >
                                                    6.4 Projected Cashflow
                                                </td>
                                                <td >
                                                    Available
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    35.
                                                </td>
                                                <td >
                                                    6.5 Projected Balance Sheet
                                                </td>
                                                <td >
                                                    Available
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    36.
                                                </td>
                                                <td >
                                                    6.7 Business Ratios
                                                </td>
                                                <td >
                                                    Available
                                                </td>
                                            </tr>
                                            </tbody>

                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->


                    </div>
                            <div class="login-footer">
                                <div class="row bs-reset">
                                    <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                        <a href ="stage_3_prelisting.php" class="btn green btn-outline">[<< Back]</a>
                                    </div>
                                    <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                        <a href ="stage_3_review.php" type="submit" class="btn green btn-outline pull-right" >[ FINISH >>] </a>
                                    </div>
                                </div>
                            </div>

                </div>
    <!-- Major Suppliers and Supply Channels  -->
    <div class="modal fade" id="exampleModalthree" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4">
        <div class="modal-dialog my_diag" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1"> <span id = 'my_title_1'></span> </h4> </div>
                <form action ="func/controller/businessPlanController.php" method ="POST" name ="business_plan">
                    <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                    <input type ='hidden' name ='edit_buzplan' id="edit_value_id" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label"><span id='my_title'></span></label>
                            <textarea  name = "desc" class="summernote form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"><?=@$desc;?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>



<?php
$extra_js .= "
<script>
$(document).ready(function(){
    
    
    
	$(document).on('click', '.new_busplan', function(){
	    
		var subtitle = $(this).data('id');
		var title = $(this).data('title');
		var id=$(this).attr('id');
		$('#my_title_1').html( title );
		$('#my_title').html( subtitle );
		$('#edit_value_id').val( id );	    
		$.ajax({
              url: 'getbusiness_plan.php?col='+id,
              cache: false,
              success: function(html){
                   $('.panel-body').html(html);                             
                   $('#exampleModalthree').modal('show');
                    
              }
        });
		
        
	});
	
});
</script>
" ;
require_once("require/footer.php") ;

?>