<?php

$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ; 
$extra_css = "" ; 
$page_number = "10" ; 
$extra_js = "" ;
$thy_stage = "3" ;
$thy_qsn = "399/472" ;
$thy_qp = round(399/472 * 100, 2);
$my_title = "Cash Flow  5 - 7";
@$msg=$_GET['msg'];
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg']; 
require_once("require/header.php") ;
require_once 'func/controlDAO.php' ;
?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;padding:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_financials_balancesheet.php"> <strong>Balance Sheet  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>

                        <form action="func/controller/financialCashFlowController.php"  class="login-form" method="post">
                          <input type="hidden" value="<?php echo $_SESSION['email']?>"  name="user_id">
                        <table class="table table-bordered responsive">


                                <thead class="text-uppercase">
                                    <th class="c-gray">OPTION</th>
                                    <th colspan="3" class="c-gray">HISTORICAL</th>
                                    <th colspan="3" class="c-gray">PROJECTED</th>
                                </thead>
                            <tbody>
                            <colgroup>
                                <col  style="background-color:#f5f5f5;">
                                <col span="3" style="background-color:yellow">
                                <col span="3" style="background-color:pink">
                            </colgroup>

                            <tr>
                                <td>Years   </td>
                                <td> <input type = "text" value="2015" name="last_3_years"  /></td>
                                <td> <input type = "text" value="2016" name="last_2_years"  /></td>
                                <td> <input type = "text" value="2017" name="last_year"  /></td>
                                <td> <input type = "text" value="2018" name="this_year"    /> </td>
                                <td> <input type = "text" value="2019" name="next_year"    /> </td>
                                <td> <input type = "text" value="2020" name="next_2_years"   /> </td>
                            </tr>
                            <tr>
                                 <td> Is Audited  </td>
                                <td> <input type="checkbox"  value="yes" name="last_3_years_audited"  /></td>
                                <td>  <input type="checkbox"  value="yes" name="last_2_years_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="last_year_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="this_year_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="next_year_audited"   /></td>
                                <td>  <input type="checkbox"  value="yes" name="next_2_years_audited" /></td>

                            </tr>
                            
                            <tr>
                                <td> Upload File  </td>
                                <td> <input type="file"  value="yes" name="file_"  /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_"   /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>

                            </tr>                                
                            <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >OPERATING ACTIVITIES </font></b></td>
                            </tr>
                            
                             
                            <tr>
                                <td>(Loss)/Profit  before tax</td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_3_years_profit_or_loss_before_tax"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_profit_or_loss_before_tax"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_year_profit_or_loss_before_tax"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_profit_or_loss_before_tax"    /> </td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_profit_or_loss_before_tax"    /> </td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_profit_or_loss_before_tax"   /> </td>
                                
                            </tr>
                            
                            <tr>
                                <td>Adjustments to reconcile profit/loss before tax</td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_3_years_adjustments_to_reconcile_cash_before_tax_to_net_cashflows"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_djustments_to_reconcile_cash_before_tax_to_net_cashflows"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_year_adjustments_to_reconcile_cash_before_tax_to_net_cashflows"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_adjustments_to_reconcile_cash_before_tax_to_net_cashflows"    /> </td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_adjustments_to_reconcile_cash_before_tax_to_net_cashflows"    /> </td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_adjustments_to_reconcile_cash_before_tax_to_net_cashflows"   /> </td>
                                
                            </tr>
                            <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >INVESTING ACTIVITIES </font></b></td>
                            </tr>
                              <tr>
                                <td>Net Cash used in investing Activities</td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_3_years_net_cash_flows_used_in_investing_activities"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_net_cash_flows_used_in_investing_activities"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_year_net_cash_flows_used_in_investing_activities"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_net_cash_flows_used_in_investing_activities"    /> </td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_net_cash_flows_used_in_investing_activities"    /> </td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_net_cash_flows_used_in_investing_activities"   /> </td>
                                
                            </tr>
                             <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >FINANCING ACTIVITIES </font></b></td>
                            </tr>
                            <tr>
                                <td>Net Cash used in financing Activities</td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_net_cash_flows_used_in_financing_activities"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_net_cash_flows_used_in_financing_activities"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_year_net_cash_flows_used_in_financing_activities"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_net_cash_flows_used_in_financing_activities"    /> </td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_net_cash_flows_used_in_financing_activities"    /> </td>
                                <td> <input type = "text"class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_net_cash_flows_used_in_financing_activities"   /> </td>
                                
                            </tr>
                            <tr>
                                <td>Cash and cash equivalents at beginning of the year</td>
                               <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_3_years_cash_and_cash_equivalents_at_the_end_of_the_year"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_cash_and_cash_equivalents_at_the_end_of_the_year"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_year_cash_and_cash_equivalents_at_the_end_of_the_year"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_cash_and_cash_equivalents_at_the_end_of_the_year"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_cash_and_cash_equivalents_at_the_end_of_the_year"  /></td>
                                <td> <input type = "text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_cash_and_cash_equivalents_at_the_end_of_the_year"  /></td>
                            </tr>
                          
                            
                            </tbody>
                        </table>
                        <div class="login-footer">
                        <div class="row bs-reset">
                            <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                <a href ="stage_3_financials_balancesheet.php" class="btn green btn-outline">[<< Back]</a>
                            </div>
                            <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                
                                <button type="submit" class="btn green uppercase btn-outline pull-right">[Next>>]</button>
                            </div>
                        </div>
                    </div>

                        </form>


                    </div>


                    <!--  First form of the dialog
                          Income Statement  -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                          <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="exampleModalLabel1">Income Statement</h4> </div>
                                  <div class="modal-body">
                                      <form>
                                          <div class="form-group">
                                              <label for="recipient-name" class="control-label">Year  :</label>
                                              <input type="text" class="form-control" id="recipient-name1"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name2" class="control-label">% Gross Income :</label>
                                              <input type="text" class="form-control" id="recipient-name3"> </div>
                                          <div class="form-group">
                                              <label for="recipient-name4" class="control-label">Gross Income  :</label>
                                              <input type="text" class="form-control" id="recipient-name5"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name6" class="control-label">Cost of sales :</label>
                                              <input type="text" class="form-control" id="recipient-name7"> </div>
                                          <div class="form-group">
                                              <label for="recipient-name8" class="control-label">Gross Profit  :</label>
                                              <input type="text" class="form-control" id="recipient-name9"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name10" class="control-label">Net Profit  :</label>
                                              <input type="text" class="form-control" id="recipient-name11"> </div>                                                                                      
                                      </form>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save</button>
                                  </div>
                              </div>
                          </div>
                    </div>


                    <!--  Second form of the dialog
                          Balance sheet  -->
                    <div class="modal fade" id="exampleModaltwo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
                          <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="exampleModalLabel1">Balance sheet</h4> </div>
                                  <div class="modal-body">
                                      <form>
                                          <div class="form-group">
                                              <label for="recipient-name" class="control-label">Year  :</label>
                                              <input type="text" class="form-control" id="recipient-name1"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name2" class="control-label">Fixed Assets :</label>
                                              <input type="text" class="form-control" id="recipient-name3"> </div>
                                          <div class="form-group">
                                              <label for="recipient-name4" class="control-label">Current Assets  :</label>
                                              <input type="text" class="form-control" id="recipient-name5"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name6" class="control-label">Total Assets :</label>
                                              <input type="text" class="form-control" id="recipient-name7"> </div>
                                          <div class="form-group">
                                              <label for="recipient-name8" class="control-label">Current Liabilities  :</label>
                                              <input type="text" class="form-control" id="recipient-name9"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name10" class="control-label">Other Liabilities  :</label>
                                              <input type="text" class="form-control" id="recipient-name11"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name12" class="control-label">Total Liabilities  :</label>
                                              <input type="text" class="form-control" id="recipient-name13"> </div>                                        
                                          <div class="form-group">
                                              <label for="recipient-name14" class="control-label">NET ASSETS  :</label>
                                              <input type="text" class="form-control" id="recipient-name15"> </div>                                        
                                                 
                                      </form>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save</button>
                                  </div>
                              </div>
                          </div>
                    </div>                    


                    

                <
<?php

require_once("require/footer.php") ;

?>