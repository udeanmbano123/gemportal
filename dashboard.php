<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

      	<?php 
      		include("req/header_dash.php") ;
      	?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Financial Advisor </h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li class="active"><a href="#">HOME</a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">

                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-sm-6">
                                    <div class="row row-in">
                                        <!-- /.col -->
                                        <!--col -->
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="white-box">
                                                <div class="col-in row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="E" class="linea-icon linea-basic"></i>
                                                        <h5 class="text-muted vb">Applications</h5> </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h3 class="counter text-right m-t-15 text-danger">23</h3> </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <!--col -->
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="white-box">
                                                <div class="col-in row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
                                                        <h5 class="text-muted vb">APPROVED</h5> </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h3 class="counter text-right m-t-15 text-megna">169</h3> </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-megna" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <!--col -->
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="white-box">
                                                <div class="col-in row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
                                                        <h5 class="text-muted vb">REJECTED</h5> </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h3 class="counter text-right m-t-15 text-primary">157</h3> </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <!--col -->
                                        <div class="col-lg-6 col-sm-12">
                                            <div class="white-box">
                                                <div class="col-in row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon=""></i>
                                                        <h5 class="text-muted vb">PENDING</h5> </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <h3 class="counter text-right m-t-15 text-success">431</h3> </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 mail_listing">
                                    <h1 class="box-title">Recent Application</h1><span><?php echo @$_GET['sms'] ; ?></span>
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                         <th>Company name </th>
                                         <th>Contact person</th>
                                         <th>Email</th>
                                         <th>Phone number</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                        <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>                                          
                                        </tr>
                                      </tbody>
                                    </table>
                                </div>

                            </div>


                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>