<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

      	<?php 
      		include("req/header_main.php") ;
          require_once 'func/controlDAO.php' ;
          $id=$_GET['view'];
      	?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Financial Advisor [Acceptance]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="dashboard.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        
                            
                               <!--  <div class="col-md-2  inbox-panel">
                                    <div>
                                        <div class="list-group mail-list "> 
                                          <a href="f_dashboard.php" class="list-group-item active">Pending Acceptance </a>
                                          <a href="f_dashboard_accept.php" class="list-group-item ">Accepted </a>
                                          <a href="f_dashboard_rejected.php" class="list-group-item">Rejected </a>
                                        </div>

                                    </div> -->
                                <!-- </div> -->

                                <div class="col-md-10 mail_listing">
                                    <h6 class="box-title">Capital Requirements</h6><span><?php echo @$_GET['msg'] ; ?></span>
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                         <th>Company name </th>
                                         <th>Company Registration Number</th>
                                         <th>Incorporation Date</th>
                                         <th>Entity Type</th>
                                         <th>Business Sector</th>
                                         <th>Nature of Business</th>
                                         <th>Raised Equity</th>
                                         <th>Raised Debt </th>
                                         <th>Raised Other</th>
                                         <th>Purpose of Funds </th>
                                      </tr>
                                      </thead>
                                      <tbody>
<!--                                         <tr>
                                           <td colspan="4" style="text-align:center">No Appliacction found</td>
                                        </tr>  -->
                                        
                                    <?php
                                      $ifPopulated =  (new controlDAO())->getDetails()->getviewCapitalDetails($id);
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="3" style="text-align:center">No Financial  Advisors found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# directors_shareholding_id, user_id, , ,
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>".@$ifPopulated[$i]['company_name']."</td>
                                                         <td>".@$ifPopulated[$i]['company_registration_number']."</td>
                                                         <td>".@$ifPopulated[$i]['date_of_incorporation']."</td>
                                                         
                                                          <td>".@$ifPopulated[$i]['type_of_entity']."</td>
                                                           <td>".@$ifPopulated[$i]['business_sector']."</td>
                                                         <td>".@$ifPopulated[$i]['nature_of_business']."</td>
                                                           <td>".@$ifPopulated[$i]['raised_equity']."</td>
                                                           <td>".@$ifPopulated[$i]['raised_debt']."</td>
                                                           <td>".@$ifPopulated[$i]['raised_other']."</td>
                                                           <td>".@$ifPopulated[$i]['purpose_of_funds']."</td>
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>

















                                        <!-- <tr>
                                           <td>Escrow Systems</td>
                                           <td>Tinashe Makaza</td>
                                           <td>tinashe@escrogroup.org</td>
                                           <td>0772876187</td>
                                           <td><a href='#'>[View Details]</a> | <a href ='#'>[Accept]</a>| <a href ='#'>[Reject]</a></td>
                                        </tr> -->
                                      </tbody>
                                    </table>
                                </div>
                            

                        


                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                              
                    </div>
                  </div>
                <!-- /.row -->



            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>