<?php
require_once 'func/controlDAO.php' ;



$ifPopulated =  (new controlDAO())->getCapitalRequirements()->selectOneCapitalReqByEmail($_GET['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
    $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
    }
    $setOption .= "<input type ='hidden' value = '".$capital_req_id."' name ='capital_req_id'/>" ;
    //echo $user_id ;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Finsec | SME Portal</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- toast CSS -->
    <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
</head>
<body class="fix-sidebar fix-header">
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
    <?php
    include("req/header_main.php") ;
    @$email=$_GET['email'];
    $getratios =  (new controlDAO())->getfinRatios() ;

    ?>
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title" style="background: #9ea1f1;">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title"  style="color: #ffffff;" >Finsec Admin [Acceptance]</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-12">
                                <form name ="capital_requirements" method="post" action="func/controller/analyst_financial_ratios.php">
                                    <input type="hidden" name ="email" value="<?=@$_GET['email'];?>" >

                                    <div class="col-md-12 login-container bs-reset mt-login-5-bsfix">
                                        <div class="inbox-center">
                                            <h4> FINANCIAL RATIOS</h4>
                                            <table id="fin_ratios" class="table editable-table table-bordered m-b-0">
                                                <thead class="text-uppercase">
                                                <th width="330" class="c-gray"></th>
                                                <th colspan="3" class="c-gray">HISTORICAL</th>
                                                <th colspan="5" class="c-gray">PROJECTED</th>
                                                <th colspan="1" class="c-gray">AVRG</th>
                                                <th colspan="1" class="c-gray">RATING</th>
                                                </thead>
                                                <tbody>
                                                <colgroup>
                                                    <col  style="background-color:#f5f5f5;">
                                                    <col span="3" style="background-color:yellow">
                                                    <col span="5" style="background-color:pink">
                                                </colgroup>
                                                <tr>
                                                    <td></td>
                                                    <td>		YEAR 1	</td>
                                                    <td>	    YEAR 2</td>
                                                    <td>		YEAR 3</td>
                                                    <td>		YEAR 4</td>
                                                    <td>		YEAR 5</td>
                                                    <td>		YEAR 6</td>
                                                    <td>	    YEAR 7</td>
                                                    <td>        YEAR 8</td>
                                                </tr>
                                                <tr>
                                                    <td><h3>Short term solvency ratios</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>Current Ratio	</td>

                                                    <?php

                                                    $curr_ratio = $getratios->getCurrentRatio($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($curr_ratio) ; $mi++){
                                                        echo "<td>$curr_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average = array_sum($curr_ratio) / count($curr_ratio);
                                                        echo round($average, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getCurrentRatioValue($average); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Quick Ratio	</td>
                                                    <?php

                                                    $quick_ratio = $getratios->getQuickRatio($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($quick_ratio) ; $mi++){
                                                        echo "<td>$quick_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_ = array_sum($quick_ratio) / count($quick_ratio);
                                                         echo round($average_, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getQuickRatioValue($average_); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Cash Ratio	</td>
                                                    <?php

                                                    $cash_ratio = $getratios->getCashRatio($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($cash_ratio) ; $mi++){
                                                        echo "<td>$cash_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average__ = array_sum($cash_ratio) / count($cash_ratio);
                                                         echo round($average__, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getCashRatioValue($average__); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Net Working Capital to Current Liabilities</td>
                                                    <?php

                                                    $wccl_ratio = $getratios->getNetWorkingCapitalCurrentLiabilities($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($wccl_ratio) ; $mi++){
                                                        echo "<td>$wccl_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average___ = array_sum($wccl_ratio) / count($wccl_ratio);
                                                         echo round($average___, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getWCCLRatioValue($average___); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Cash as a % of Current Assets</td>
                                                    <?php

                                                    $wccl_ratio = $getratios->getPerNumber($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($wccl_ratio) ; $mi++){
                                                        echo "<td>$wccl_ratio[$mi]%</td>" ;
                                                    }
                                                    ?>
                                                    <td colspan="2"></td>

                                                </tr>
                                                <tr>
                                                    <td><h3>Asset Utilization or Turnover ratios</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>Average Collection Period</td>
                                                    <?php

                                                    $acp_ratio = $getratios->getAverageCollectionPeriod($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($acp_ratio) ; $mi++){
                                                        echo "<td>$acp_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average__o_ = array_sum($acp_ratio) / count($acp_ratio);
                                                         echo round($average__o_, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getAverageCollectionPeriodRatioValue($average__o_); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Inventory Turnover Ratios</td>
                                                    <?php

                                                    $it_ratio = $getratios->getInventoryTurnoverRatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($it_ratio) ; $mi++){
                                                        echo "<td>$it_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_ = array_sum($it_ratio) / count($it_ratio);
                                                        echo round($average_u_o_, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getInventoryTurnoverRatioValue($average_u_o_); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Receivable Turnover	</td>
                                                    <?php

                                                    $rt_ratio = $getratios->getReceivableTurnoverRatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($rt_ratio) ; $mi++){
                                                        echo "<td>$rt_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_x = array_sum($rt_ratio) / count($rt_ratio);
                                                        echo round($average_u_o_x, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getReceivableTurnoverRatioValue($average_u_o_x); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Fixed Asset Turnover</td>
                                                    <?php

                                                    $fat_ratio = $getratios->getFixedAssetTurnoverRatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($fat_ratio) ; $mi++){
                                                        echo "<td>$fat_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_e = array_sum($fat_ratio) / count($fat_ratio);
                                                        echo round($average_u_o_e, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getFixedAssetTurnoverRatioValue($average_u_o_e); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Total Asset Turnover</td>
                                                    <?php

                                                    $tat_ratio = $getratios->getTotalAssetTurnoverRatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($tat_ratio) ; $mi++){
                                                        echo "<td>$tat_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_gg = array_sum($tat_ratio) / count($tat_ratio);
                                                        echo round($average_u_o_gg, 4); 
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getTotalAssetTurnoverRatioValue($average_u_o_gg); ?></td>

                                                </tr>
                                                <tr>
                                                    <td><h3>Financial Leverage ratios</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>Total Debt Ratio	</td>
                                                    <?php

                                                    $tdrr_ratio = $getratios->getTotalDebtRatioRatio($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($tdrr_ratio) ; $mi++){
                                                        echo "<td>$tdrr_ratio[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_gdg = array_sum($tdrr_ratio) / count($tdrr_ratio);
                                                        echo round($average_u_o_gdg, 4);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getTotalDebtRatioRatioValue($average_u_o_gdg); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Debt/Equity</td>
                                                    <?php

                                                    $tdrr_ratiod = $getratios->getDebtEquityRatio($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($tdrr_ratiod) ; $mi++){
                                                        echo "<td>$tdrr_ratiod[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_gdgd = array_sum($tdrr_ratiod) / count($tdrr_ratiod);
                                                        echo round($average_u_o_gdgd, 4);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getDebtEquityRatioValue($average_u_o_gdgd); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Equity Ratio (TE/TA)</td>
                                                    <?php

                                                    $tdrr_ratiodfm = $getratios->getEquityRatio($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($tdrr_ratiodfm) ; $mi++){
                                                        echo "<td>$tdrr_ratiodfm[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_gdgdsa = array_sum($tdrr_ratiodfm) / count($tdrr_ratiodfm);
                                                        echo round($average_u_o_gdgdsa, 4);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getEquityRatioValue($average_u_o_gdgdsa); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Long-term Debt Ratio (LTD/TA)	</td>
                                                    <?php

                                                    $tdrr_ratiodfmv = $getratios->getLongTermDebtRatio($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($tdrr_ratiodfmv) ; $mi++){
                                                        echo "<td>$tdrr_ratiodfmv[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_gdgdsavd = array_sum($tdrr_ratiodfmv) / count($tdrr_ratiodfmv);
                                                        echo round($average_u_o_gdgdsavd, 4);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getLongTermDebtRatioValue($average_u_o_gdgdsavd); ?></td>

                                                </tr>
                                                <tr>
                                                    <td><h3>Profitability ratios</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>Gross Profit Margin 	</td>
                                                    <?php

                                                    $tdrr_ratiodfmvs = $getratios->getGrossProfitMarginRatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($tdrr_ratiodfmvs) ; $mi++){
                                                        echo "<td>$tdrr_ratiodfmvs[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_gdgdsavds = array_sum($tdrr_ratiodfmvs) / count($tdrr_ratiodfmvs);
                                                        echo round($average_u_o_gdgdsavds, 4);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getGrossProfitMarginRatioValue($average_u_o_gdgdsavds); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Net Profit Margin</td>
                                                    <?php

                                                    $tdrr_ratiodfmvc = $getratios->getNetProfitMarginRatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($tdrr_ratiodfmvc) ; $mi++){
                                                        echo "<td>$tdrr_ratiodfmvc[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $average_u_o_gdgdsavdc = array_sum($tdrr_ratiodfmvc) / count($tdrr_ratiodfmvc);
                                                        echo round($average_u_o_gdgdsavdc, 4);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getNetProfitMarginRatioValue($average_u_o_gdgdsavdc); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Return on assets (ROA)</td>
                                                    <?php

                                                    $wetdrr_ratiodfmvc = $getratios->getROARatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($wetdrr_ratiodfmvc) ; $mi++){
                                                        echo "<td>$wetdrr_ratiodfmvc[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $daverage_u_o_gdgdsavdc = array_sum($wetdrr_ratiodfmvc) / count($wetdrr_ratiodfmvc);
                                                         echo round($daverage_u_o_gdgdsavdc, 4);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getROARatioValue($daverage_u_o_gdgdsavdc); ?></td>

                                                </tr>
                                                <tr>
                                                    <td>Return on equity (ROE)</td>
                                                    <?php

                                                    $wetdrr_ratiodfmvcE = $getratios->getROERatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($wetdrr_ratiodfmvcE) ; $mi++){
                                                        echo "<td>$wetdrr_ratiodfmvcE[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $daverage_u_o_gdgdsavdcE = array_sum($wetdrr_ratiodfmvcE) / count($wetdrr_ratiodfmvcE);
                                                        echo round($daverage_u_o_gdgdsavdcE, 4);
                                                        ?>
                                                    </td>
                                                    <td><?php echo $getratios->getROERatioValue($daverage_u_o_gdgdsavdcE); ?></td>

                                                </tr>
                                                <tr>
                                                    <td><h3>Market value ratios</h3></td>
                                                </tr>
                                                <tr>
                                                    <td>Earnings per share		</td>
                                                    <?php

                                                    $wetdrr_ratiodfmvcE_us = $getratios->getEarningspershareRatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($wetdrr_ratiodfmvcE_us) ; $mi++){
                                                        echo "<td>$wetdrr_ratiodfmvcE_us[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td colspan="2"></td>

                                                </tr>
                                                <tr>
                                                    <td>Price/Earnings Ratio</td>
                                                    <?php

                                                    $wetdrr_ratiodfmvcE_me = $getratios->getPriceEarningspershareRatios($_GET['email']) ;
                                                    for ($mi = 0 ; $mi < count($wetdrr_ratiodfmvcE_me) ; $mi++){
                                                        echo "<td>$wetdrr_ratiodfmvcE_me[$mi]</td>" ;
                                                    }
                                                    ?>
                                                    <td colspan="2"></td>

                                                </tr>
                                            </table>
                                        </div>
                                    </div>


<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-7 m-t-20">-->
<!--                                            <div class="btn-group pull-left">-->
<!--                                                <a href="analyst_CompanyEnterpriseSizeCategorisation.php?email=--><?//=@$_GET['email'];?><!--" class="btn btn-default waves-effect">[ Enterprise Size Categorisation ]</a>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-5 m-t-20">-->
<!--                                            <div class="btn-group pull-right">-->
<!--                                                <button type="submit" class="btn btn-default waves-effect">[Enterprise Valuation Models >> ]</button>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="js/waves.js"></script>
<!--Counter js -->
<script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="js/custom.min.js"></script>
<script src="public/assets/global/plugins/moment.min.js"></script>
<!--Style Switcher -->
<script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

</script>
</body>
</html>