<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ; 
$extra_css = "" ; 
$page_number = "10" ; 
$extra_js = "" ;
$thy_stage = "3" ;
$thy_qsn = "183/472" ;
$thy_qp = round(183/472 * 100, 2);
$my_title = "Balance Sheet  4 - 7" ;
@$msg=$_GET['msg'];
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg']; 
require_once("require/header.php") ;
require_once 'func/controlDAO.php' ;
$ifPopulated =  (new controlDAO())->getBalanceSheetValues()->selectBalanceSheetLast3Years($_SESSION['email']) ;
if(!$ifPopulated){
    //echo "There is no data " ;
    $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

}else{
   
    foreach($ifPopulated as $i => $item) {
        extract($ifPopulated[$i]);
    }
   
}
$last_2yrs =  (new controlDAO())->getlast2yrs()->selectBalanceSheetLast2Years($_SESSION['email']) ;
if($last_2yrs){
    //echo "There is no data " ;
    foreach($last_2yrs as $a => $item) {
        extract($last_2yrs[$a]);
    }
   

}

$last_yr =  (new controlDAO())->getlastyr()->selectBalanceSheetLastYear($_SESSION['email']) ;
if($last_yr){
    //echo "There is no data " ;
    foreach($last_yr as $b => $item) {
        extract($last_yr[$b]);
    }
   

}




$this_yr =  (new controlDAO())->getthisyr()->selectBalanceSheetthisYear($_SESSION['email']) ;
if($this_yr){
    //echo "There is no data " ;
    foreach($this_yr as $c => $item) {
        extract($this_yr[$c]);
    }
   

}
$next_yr =  (new controlDAO())->getnextyr()->selectBalanceSheetnextYear($_SESSION['email']) ;
if($next_yr){
    //echo "There is no data " ;
    foreach($next_yr as $d => $item) {
        extract($next_yr[$d]);
    }
   

}
$next_2yrs =  (new controlDAO())->getnext2yrs()->selectBalanceSheetnext2Years($_SESSION['email']) ;
if($next_2yrs){
    //echo "There is no data " ;
    foreach($next_2yrs as $e => $item) {
        extract($next_2yrs[$e]);
    }
   

}


?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;padding:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>
                        <form action="func/controller/financialBalancesheetController.php"  class="login-form" method="post">
                           <input type="hidden" value="<?php echo $_SESSION['email']?>"  name="user_id">
                           
                        <table class="table table-bordered responsive">


                                <thead class="text-uppercase">
                                    <th class="c-gray">OPTION</th>
                                    <th colspan="3" class="c-gray">HISTORICAL</th>
                                    <th colspan="3" class="c-gray">PROJECTED</th>
                                </thead>
                            <tbody>
                            <colgroup>
                                <col  style="background-color:#f5f5f5;">
                                <col span="3" style="background-color:yellow">
                                <col span="3" style="background-color:pink">
                            </colgroup>

                            <tr>
                                <td>Years   </td>
                                <td> <input type = "text" value="2015" name="last_3_years" readonly /></td>
                                <td> <input type = "text" value="2016" name="last_2_years" readonly /></td>
                                <td> <input type = "text" value="2017" name="last_year" readonly /></td>
                                <td> <input type = "text" value="2018" name="this_year"    readonly/> </td>
                                <td> <input type = "text" value="2019" name="next_year"   readonly /> </td>
                                <td> <input type = "text" value="2020" name="next_2_years"  readonly /> </td>
                            </tr>
                            <tr>
                                <td> Is Audited  </td>
                                <td> <input type="checkbox"  value="yes" name="last_3_years_audited"  /></td>
                                <td>  <input type="checkbox"  value="yes" name="last_2_years_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="last_year_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="this_year_audited" /></td>
                                <td>  <input type="checkbox"  value="yes" name="next_year_audited"   /></td>
                                <td>  <input type="checkbox"  value="yes" name="next_2_years_audited" /></td>

                            </tr>
                            <tr>
                                <td> Upload File  </td>
                                <td> <input type="file"  value="yes" name="file_"  /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>
                                <td>  <input type="file"  value="yes" name="file_"   /></td>
                                <td>  <input type="file"  value="yes" name="file_" /></td>

                            </tr>
                            <tr>
                            <td colspan="6"><b><font color='blue' size='2' >ASSETS </font></b></td>
                            </tr>
                            <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >NON CURRENT ASSETS </font></b></td>
                            </tr>
                            <tr>
                                <td> Property,plant and equipment </td>
                                <td> <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_3_years_property_plant_and_equipment"  value="<?php echo $ifPopulated[$i]['property_plant_and_equipment'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_2_years_property_plant_and_equipment"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_year_property_plant_and_equipment" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="this_year_property_plant_and_equipment" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="next_year_property_plant_and_equipment"   /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="next_2_years_property_plant_and_equipment" /></td>

                            </tr>
                            <tr>
                                <td> Investment properties  </td>
                                <td> <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group"    name="last_3_years_investment_properties"  value="<?php echo $ifPopulated[$i]['investment_properties'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_2_years_investment_properties" value="<?php echo $last_2yrs[$a]['investment_properties'] ; ?>" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_investment_properties" value="<?php echo $last_yr[$c]['investment_properties'] ; ?>" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="this_year_investment_properties" value="<?php echo $this_yr[$c]['investment_properties'] ; ?>" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="next_year_investment_properties" value="<?php echo $next_yr[$d]['investment_properties'] ; ?>"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group" name="next_2_years_investment_properties"  value="<?php echo $next_2yrs[$e]['investment_properties'] ; ?>" /></td>
                            </tr>
                            <tr>
                                <td> Intangible assets </td>
                               <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_intangible_assets"  value="<?php echo $ifPopulated[$i]['intangible_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_intangible_assets"  value="<?php echo $last_2yrs[$a]['intangible_assets'] ; ?>"/> </td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_intangible_assets"  value="<?php echo $last_yr[$b]['intangible_assets'] ; ?>" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_intangible_assets" value="<?php echo $this_yr[$c]['intangible_assets'] ; ?>" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_intangible_assets"  value="<?php echo $next_yr[$d]['intangible_assets'] ; ?>"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_intangible_assets" value="<?php echo $next_2yrs[$e]['intangible_assets'] ; ?>" /></td>
                            </tr>
                            <tr>
                                <td>Other Fixed Assets </td>
                               <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_other_non_current_assets" value="<?php echo $ifPopulated[$i]['other_fixed_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_other_non_current_assets" value="<?php echo $last_2yrs[$a]['other_fixed_assets'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_other_non_current_assets" value="<?php echo $last_yr[$b]['other_fixed_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_other_non_current_assets" value="<?php echo $this_yr[$c]['other_fixed_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_other_non_current_assets"  value="<?php echo $next_yr[$d]['other_fixed_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_other_non_current_assets" value="<?php echo $next_2yrs[$e]['other_fixed_assets'] ; ?> " /></td>
                            </tr>
                             <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >CURRENT ASSETS </font></b></td>
                            </tr>

                            <tr>
                                <td> biological assets </td>
                               <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_biological_assets" value="<?php echo $ifPopulated[$i]['biological_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_biological_assets" value="<?php echo $last_2yrs[$a]['biological_assets'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_biological_assets" value="<?php echo $last_yr[$b]['other_fixed_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_biological_assets"  value="<?php echo $this_yr[$c]['other_fixed_assets'] ; ?> "/></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_biological_assets"  value="<?php echo $next_yr[$d]['other_fixed_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_biological_assets" value="<?php echo $next_2yrs[$e]['other_fixed_assets'] ; ?> "/></td>
                            </tr>
                            <tr>
                                <td>Inventories </td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_inventories" value="<?php echo $ifPopulated[$i]['inventories'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_inventories" value="<?php echo $last_2yrs[$a]['Inventories'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_inventories" value="<?php echo $last_yr[$b]['inventories'] ; ?> "/></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_inventories" value="<?php echo $this_yr[$c]['inventories'] ; ?> "/></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_inventories"  value="<?php echo $next_yr[$d]['inventories'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_inventories" value="<?php echo $next_2yrs[$a]['inventories'] ; ?> "/></td>
                            </tr>
                            <tr>
                                <td>Trade & other receivables </td>
                                 <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_trade_and_receivables" value="<?php echo $ifPopulated[$i]['trade_and_receivables'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_trade_and_receivables" value="<?php echo $last_2yrs[$a]['trade_and_receivables'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_trade_and_receivables" value="<?php echo $last_yr[$b]['trade_and_receivables'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_trade_and_receivables" value="<?php echo $this_yr[$c]['trade_and_receivables'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_trade_and_receivables"  value="<?php echo $next_yr[$d]['trade_and_receivables'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_trade_and_receivables" value="<?php echo $next_2yrs[$e]['trade_and_receivables'] ; ?> "/></td>
                            </tr>
                            <tr>
                                <td>Prepayments </td>
                                 <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_prepayments"   value="<?php echo $ifPopulated[$i]['prepayments'] ; ?> "  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_prepayments" value="<?php echo $last_2yrs[$a]['prepayments'] ; ?> "/></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_prepayments" value="<?php echo $last_yr[$b]['prepayments'] ; ?> "/> /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_prepayments" value="<?php echo $this_yr[$c]['prepayments'] ; ?> "/></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_prepayments"  value="<?php echo $next_yr[$d]['prepayments'] ; ?> "/> /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_prepayments" value="<?php echo $next_2yrs[$e]['prepayments'] ; ?> "/> /></td>
                            </tr>
                            
                            <tr>
                                <td> Bank  </td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_bank" value="<?php echo $ifPopulated[$i]['bank'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_equity_bank" value="<?php echo $last_2yrs[$a]['bank'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_equity_bank" value="<?php echo $last_yr[$b]['bank'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_equity_bank" value="<?php echo $this_yr[$c]['bank'] ; ?> "/></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_equity_bank"   value="<?php echo $next_yr[$d]['bank'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_equity_bank" value="<?php echo $next_2yrs[$e]['bank'] ; ?> " /></td>
                            </tr>
                            <tr>
                                <td> Cash  </td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_cash"  value="<?php echo $ifPopulated[$i]['cash'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_cash"  value="<?php echo $last_2yrs[$a]['bank'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_cash"  value="<?php echo $last_yr[$b]['bank'] ; ?> "/></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_cash"  value="<?php echo $this_yr[$c]['bank'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_cash"  value="<?php echo $next_yr[$d]['bank'] ; ?> "  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_cash" value="<?php echo $next_yr[$e]['bank'] ; ?> "  /></td>
                            </tr>
                            <tr>
                                <td> Other Current Assets </td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_other_current_assets" value="<?php echo $ifPopulated[$i]['other_current_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_other_current_assets" value="<?php echo $last_2yrs[$a]['other_current_assets'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_other_current_assets" value="<?php echo $last_yr[$b]['other_current_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_other_current_assets" value="<?php echo $this_yr[$c]['other_current_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_other_current_assets"  value="<?php echo $next_yr[$d]['other_current_assets'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_other_current_assets"  value="<?php echo $last_2yrs[$e]['other_current_assets'] ; ?> " /></td>
                            </tr>
                             <tr>
                            <td colspan="6"><b><font color='blue' size='2' >EQUITY AND LIABILITIES </font></b></td>
                            </tr>
                            <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >CAPITAL AND RESERVES </font></b></td>
                            </tr>
                            <tr>
                                <td> issue share capital </td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_issue_share_capital" value="<?php echo $ifPopulated[$i]['issue_share_capital'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_issue_share_capital"  value="<?php echo $last_2yrs[$a]['issue_share_capital'] ; ?> "/></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_issue_share_capital" value="<?php echo $last_yr[$b]['issue_share_capital'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_issue_share_capital" value="<?php echo $this_yr[$c]['issue_share_capital'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_issue_share_capital" value="<?php echo $next_yr[$d]['issue_share_capital'] ; ?> "  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_issue_share_capital" value="<?php echo $next_2yrs[$e]['issue_share_capital'] ; ?> "/></td>
                            </tr>
                            <tr>
                                <td> share premium</td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_share_premium" value="<?php echo $ifPopulated[$i]['share_premium'] ; ?> "   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_share_premium" value="<?php echo $last_2yrs[$a]['share_premium'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_share_premium" value="<?php echo $last_yr[$b]['share_premium'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_share_premium" value="<?php echo $this_yr[$c]['share_premium'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_share_premium"  value="<?php echo $next_yr[$d]['share_premium'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_share_premium" value="<?php echo $next_2yrs[$e]['share_premium'] ; ?> " /></td>
                            </tr>
                            <tr>
                                <td> capital reserves </td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_capital_reserves" value="<?php echo $ifPopulated[$i]['capital_reserves'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_capital_reserves" value="<?php echo $last_2yrs[$a]['capital_reserves'] ; ?> " /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_capital_reserves" value="<?php echo $last_yr[$b]['capital_reserves'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_capital_reserves" value="<?php echo $this_yr[$c]['capital_reserves'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_capital_reserves"  value="<?php echo $next_yr[$d]['capital_reserves'] ; ?> " /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_capital_reserves" value="<?php echo $next_2yrs[$e]['capital_reserves'] ; ?> " /></td>
                            </tr>
                            <tr>
                                <td> revenue reserves </td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_revenue_reserves"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_revenue_reserves"  /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_revenue_reserves" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_revenue_reserves" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_revenue_reserves"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_revenue_reserves" /></td>
                            </tr>
                            <tr>
                                <td> other reserves</td>
                                
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_other_capital_and_revenue_reserves"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_other_capital_and_revenue_reserves" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_other_capital_and_revenue_reserves" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_other_capital_and_revenue_reserves" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_other_capital_and_revenue_reserves"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_other_capital_and_revenue_reserves" /></td>
                            </tr>
                            <tr>
                                <td> non controlling interest </td>
                                 
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_non_controlling_interest"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_non_controlling_interest" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_non_controlling_interest" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_non_controlling_interest" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_non_controlling_interest"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_non_controlling_interest" /></td>
                            </tr>
                            <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >NON CURRENT ASSETS </font></b></td>
                            </tr>
                            <tr>
                                <td> long term borrowings </td>
                                <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_long_term_borrowings"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_long_term_borrowings" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_long_term_borrowings" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_long_term_borrowings" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_long_term_borrowings"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_long_term_borrowings" /></td>
                            </tr>
                            <tr>
                                <td> deffered tax liabilities </td>
                                 <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_deffered_tax_liabilities"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_deffered_tax_liabilities" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_deffered_tax_liabilities" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_deffered_tax_liabilities" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_deffered_tax_liabilities"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_deffered_tax_liabilities" /></td>
                            </tr>
                            <tr>
                                <td> other non current liabilities </td>
                                 <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_other_non_current_liabilities"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_other_non_current_liabilities" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_other_non_current_liabilities" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_other_non_current_liabilities" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_other_non_current_liabilities"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_other_non_current_liabilities" /></td>
                            </tr>
                            <tr>
                            <td colspan="6"><b><font color='black' size='1.5' >CURRENT LIABILITIES </font></b></td>
                            </tr>
                             <tr>
                                <td>short term borrowings </td>
                                 <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_short_term_borrowings"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_short_term_borrowings" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_short_term_borrowings" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_short_term_borrowings" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_short_term_borrowings"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_short_term_borrowings" /></td>
                            </tr>
                             
                            <tr>
                                <td>trade and other payables </td>
                                 <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_trade_and_other_payables"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_trade_and_other_payables" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_trade_and_other_payables" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_trade_and_other_payables" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_trade_and_other_payables"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_trade_and_other_payables" /></td>
                            </tr>
                            <tr>
                                <td>current tax liability</td>
                                 <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_current_tax_liability"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_current_tax_liability" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_current_tax_liability" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_current_tax_liability" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_current_tax_liability"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_current_tax_liability" /></td>
                            </tr>
                            <td>Other Current Liabilities</td>
                                 <td> <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"   name="last_3_years_other_current_liabilities"  /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="last_2_years_other_current_liabilities" /></td>
                                <td>  <input type="text"  class="form-control form-control-solid placeholder-no-fix form-group" name="last_year_other_current_liabilities" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="this_year_other_current_liabilities" /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_year_other_current_liabilities"   /></td>
                                <td>  <input type="text" class="form-control form-control-solid placeholder-no-fix form-group"  name="next_2_years_other_current_liabilities" /></td>
                            </tr>
                            
                            
                            </tbody>
                        </table>
                    </div>
                            <div class="login-footer">
                                <div class="row bs-reset">
                                    <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                        <a href ="stage_3_material_litigation_claims.php" class="btn green btn-outline">[<< Back]</a>
                                    </div>
                                    <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                        <button type="submit" class="btn green btn-outline pull-right" >[ Next >>] </button>
                                    </div>
                                </div>
                            </div>
                    </form>








                </div>
<?php

require_once("require/footer.php") ;

?>