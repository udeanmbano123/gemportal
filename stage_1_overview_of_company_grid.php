<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;

$page_title = "Finsec | Stage One" ;
$extra_css = "" ;
$thy_stage = "1" ;
$page_number = "4" ;
$thy_qsn = "43/472" ;
$thy_qp = round(43/472 * 100, 2);
$extra_js = "" ;
$main_page_title = "Basic Stage" ;
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
$my_title  = "COMPANY OVERVIEW OF <b class='font-green'>".$_SESSION['company_name']."</b> 5 - 5"   ;
@$msg=$_GET['msg'];
@$raw_msg=$_GET['raw_msg'];
@$supplier_msg=$_GET['supplier_msg'];
require("func/data/connect.php");
$user_email=$_SESSION['email'];
$sql2="SELECT * FROM `users` WHERE `users_email`='$user_email'";
$result2=mysqli_query($con,$sql2);
while($row2=mysqli_fetch_array($result2)){
    $usertype=$row2['users_type'];
}

if($usertype=='applicant'){
    $ifPopulated =  (new controlDAO())->getcompanyOverview()->selectOneCompanyOverviewByEmail($_SESSION['email']) ;
    if(!$ifPopulated){
        //echo "There is no data " ;
        $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

    }else{
        $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
        foreach($ifPopulated as $i => $item) {
            extract($ifPopulated[$i]);
        }
        $setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
    }
}
elseif($usertype=='Administrator'){
    $ifPopulated =  (new controlDAO())->getcompanyOverview()->selectOneCompanyOverviewByEmail(@$_GET['email']) ;
    if(!$ifPopulated){
        //echo "There is no data " ;
        $setOption = "<input type ='hidden' value = 'true' name ='create'/>" ;

    }else{
        $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
        foreach($ifPopulated as $i => $item) {
            extract($ifPopulated[$i]);
        }
        $setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
        //echo $user_id ;
    }
}


$count_products = 0 ;
$count_raw_materials = 0 ;



?>

    <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
    <div class="login-content" style ="margin-top:0px;">
        <div class="row bg-title" style="background-color: #f5f5f5;">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
            <?php if($usertype=='applicant')
            {
                ?>

                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">

                    <a href="index.php?logout=true" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                    <ol class="breadcrumb">
                        <li class="dropdown">
                            <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                [ Previous forms of this stage ]
                            </a>
                            <ul class="dropdown-menu mailbox animated ">
                                <li>
                                    <a class="text-left" href="stage_1_capital_requirements.php">1. CAPITAL REQUIREMENTS</a>
                                </li>
                                <li>
                                    <a class="text-left" href="stage_1_corporate_directory.php">2. CORPORATE DIRECTORY OF <b class='font-green'><?=$_SESSION['company_name']?></b></a>
                                </li>
                                <li>
                                    <a class="text-left" href="stage_1_entreprise_size_categorisation.php">3. ENTERPRISE SIZE CATEGORISATION</a>
                                </li>
                            </ul>
                            <!-- /.dropdown-messages -->
                        </li>
                        <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                    </ol>
                </div>
            <?php } ?>
            <!-- /.col-lg-12 -->
        </div>


        <form action="func/controller/companyOverviewController.php" enctype="multipart/form-data" class="login-form" method="post">
            <?=@$setOption;?>
            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
            <div class="white-box-main">
                <div class="row">
                    <div class="col-xs-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h5>The Company’s Products/Services :  <p><?php echo "<br><font color='green'  >".@$msg. " </font>" ;?></p>
                            <a href="javascript:void()" class="new_product">[ Add New ]</a>
                        </h5>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Product / Service Name</th>
                                <th>Description of Product</th>
                                <th>Sales Volume (% of Total Sales)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $ifPopulated =  (new controlDAO())->getcompanyOverviewProductsServices()->selectCompanyOverviewProductsServicesByEmail($_SESSION['email']) ;
                            if(!$ifPopulated){
                                //echo "There is no data " ;
                                $count_products = 0 ;
                                echo '<tr>
                                                         <td colspan="5" style="text-align:center">No Products found</td>
                                                      </tr>' ;
                            }else{
                                $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                foreach($ifPopulated as $i => $item) {
                                    //company_overview_products_services_id, user_id, company_overview_id, product_service_name, description_of_product, sales_volume
                                    extract($ifPopulated[$i]);
                                    $count_products++;
                                    echo "<tr>
                                                             <td>
                                                             <input type ='hidden' value= 'set'  name ='products'/>
                                                                <span id='product_service_name".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                                ".@$ifPopulated[$i]['product_service_name']."
                                                                </span>
                                                             </td>
                                                             <td>
                                                                 <span id='description_of_product".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                                 ".@$ifPopulated[$i]['description_of_product']."
                                                                 </span>
                                                             </td>
                                                             <td>
                                                                 <span id='sales_volume".$ifPopulated[$i]['company_overview_products_services_id']."' >
                                                                 ".@$ifPopulated[$i]['sales_volume']."
                                                                 </span>
                                                             </td>
                                                             <td><a href='#' id='editprod_".$ifPopulated[$i]['company_overview_products_services_id']."' class='edit_product'>[Edit]</a> | <a href='#' id='deleteprod_".$ifPopulated[$i]['company_overview_products_services_id']."' class='delete_product'>[Delete]</a> </td>
                                                          </tr>" ;
                                }
                                //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                //echo $user_id ;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <h5>The Company’s Major Raw Materials (In Respect to Manufacturers Only) :
                            <!--                                      <a href="javascript:void()" data-toggle="modal" data-target="#exampleModalone" data-whatever="@mdo">[ Add New ]</a>-->
                            <a href="javascript:void()" class="new_raw">[ Add New ] <p><?php echo "<br><font color='green'  >".@$raw_msg. " </font>" ;?></p></a>
                        </h5>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Raw Material Name</th>
                                <th>Description of Raw Material</th>
                                <th>Purchase Volume (% of Total Purchases)</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $ifPopulated =  (new controlDAO())->getcompanyOverviewRawMaterials()->selectcompanyOverviewRawMaterialsByEmail($_SESSION['email']) ;
                            if(!$ifPopulated){
                                //echo "There is no data " ;
                                $count_raw_materials = 0 ;
                                echo '<tr>
                                                         <td colspan="3" style="text-align:center">No Materials found</td>
                                                      </tr>' ;
                            }else{
                                $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                foreach($ifPopulated as $i => $item) {
                                    //company_overview_products_services_id, user_id, company_overview_id, product_service_name, description_of_product, sales_volume
                                    extract($ifPopulated[$i]);
                                    $count_raw_materials++;
                                    echo "<tr>
                                                             <td>
                                                             <input type ='hidden' value= 'set'  name ='rawmaterials'/>
                                                                 <span id='name".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                                 ".@$ifPopulated[$i]['name']."
                                                                 </span>
                                                             </td>
                                                             <td>
                                                                 <span id='desc".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                                 ".@$ifPopulated[$i]['desc']."
                                                                 </span>
                                                             </td>
                                                             <td>
                                                                 <span id='volume".$ifPopulated[$i]['company_overview_raw_materials_id']."' >
                                                                 ".@$ifPopulated[$i]['volume']."
                                                                 </span>
                                                             </td>
                                                             <td><a href='javascript:void();' id='editprod_".$ifPopulated[$i]['company_overview_raw_materials_id']."' class='edit_raw'>[Edit]</a> | <a href='#' id='deleteprod_".$ifPopulated[$i]['company_overview_raw_materials_id']."' class='delete_raw'>[Delete]</a> </td>
                                                          </tr>" ;
                                }
                                //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                //echo $user_id ;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <h5>Major Customers and Distribution Channels :
                            <a href="javascript:void()" class="new_customer">[ Add New ] <p><?php echo "<br><font color='green'  >".@$customer_msg. " </font>" ;?></p></a>
                        </h5>
                        <table class="table table-bordered">
                            <thead>

                            <tr>
                                <th>Customer (Buyer)’s Name</th>
                                <th>Location</th>
                                <th>Years of Relationship</th>
                                <th>% of Sales</th>
                                <th>Trade Terms</th>
                                <th>Distribution Channel</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $ifPopulated =  (new controlDAO())->getcompanyOverviewCustomers()->selectcompanyOverviewCustomersByEmail($_SESSION['email']) ;
                            if(!$ifPopulated){
                                //echo "There is no data " ;

                                echo '<tr>
                                                         <td colspan="6" style="text-align:center">No Customers  found</td>
                                                      </tr>' ;
                            }else{
                                $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                foreach($ifPopulated as $i => $item) {
                                    extract($ifPopulated[$i]);
                                    echo "<tr>
                                                             <td>
                                                             <input type ='hidden' value= 'set'  name ='distributionchannels'/>
                                                             <span id='customer_name".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_name']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_location".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_location']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_years_rel".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_years_rel']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_sales".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_sales']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_trade_terms".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_trade_terms']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='customer_distr_channel".$ifPopulated[$i]['company_overview_customers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_distr_channel']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <a href='javascript:void();' id='editprod_".$ifPopulated[$i]['company_overview_customers_id']."' class='edit_customer'>[Edit]</a> 
                                                             | 
                                                             <a href='#' id='deleteprod_".$ifPopulated[$i]['company_overview_customers_id']."' class='delete_customer'>[Delete]</a> </td>
                                                          </tr>" ;
                                }
                                //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                //echo $user_id ;
                            }
                            ?>

                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <h5>Major Suppliers and Supply Channels :
                            <a href="javascript:void()" class="new_supply">[ Add New ] </a><p><?php echo "<br><font color='green'  >".@$supplier_msg. " </font>" ;?></p>
                            <!--                                      <a href="javascript:void()" data-toggle="modal" data-target="#exampleModalthree" data-whatever="@mdo">[ Add New ]</a>-->
                        </h5>
                        <table class="table table-bordered">
                            <thead>

                            <tr>
                                <th>Customer (Buyer)’s Name</th>
                                <th>Location</th>
                                <th>Years of Relationship</th>
                                <th>% of Sales</th>
                                <th>Trade Terms</th>
                                <th>Distribution Channel</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $ifPopulated =  (new controlDAO())->getcompanyOverviewSuppliers()->selectcompanyOverviewCustomersByEmail($_SESSION['email']) ;
                            if(!$ifPopulated){
                                //echo "There is no data " ;

                                echo '<tr>
                                                         <td colspan="6" style="text-align:center">No Suppliers found</td>
                                                      </tr>' ;
                            }else{
                                $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                foreach($ifPopulated as $i => $item) {
                                    //company_overview_suppliers_id, user_id, company_overview_id, , , , , ,
                                    extract($ifPopulated[$i]);
                                    echo "<tr>
                                                             <td>
                                                             <input type ='hidden' value= 'set'  name ='supplychannels'/>
                                                             <span id='de_customer_name".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_name']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_location".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_location']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_years_rel".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_years_rel']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_sales".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_sales']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_trade_terms".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_trade_terms']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <span id='de_customer_distr_channel".$ifPopulated[$i]['company_overview_suppliers_id']."' >
                                                             ".@$ifPopulated[$i]['customer_distr_channel']."
                                                             </span>
                                                             </td>
                                                             <td>
                                                             <a href='javascript:void();' id='editprod_".$ifPopulated[$i]['company_overview_suppliers_id']."' class='edit_supply'>[Edit]</a> 
                                                             | 
                                                             <a href='#' id='deleteprod_".$ifPopulated[$i]['company_overview_suppliers_id']."' class='delete_supply'>[Delete]</a> 
                                                             </td>
                                                                                                                      
                                                          </tr>" ;
                                }
                                //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                //echo $user_id ;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12">
                    </div>

                </div>

            </div>


    </div>


    <div class="login-footer">
<?php if($usertype=='applicant'){
    ?>
    <div class="row bs-reset">
        <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">

            <a href ="stage_1_overview_of_company.php" class="btn green btn-outline">[<< Back]</a>
        </div>
        <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">

        <!--<button type="submit" class="btn green uppercase btn-outline pull-right"></button>                                -->
        <a href="stage_1_review.php"  class="btn green btn-outline pull-right" >[Finish Basic Stage>>]</a>
    </div>
    </div>
<?php } elseif ($usertype=='Administrator') {
    ?>
    <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
    <a href="applicantAlldetails.php?email=<?php echo $_GET['email'] ?>" class="btn green uppercase btn-outline pull-right">[<< Back]</a>
    </div>
<?php } ?>


    </div>


    </form>

    <!--  First form of the dialog
          The Company’s Products/Services  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">The Company’s Products/Services</h4> </div>
                <form action ="func/controller/companyOverviewProductsServicesController.php" method ="POST" name ="product_service">
                    <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                    <input type ='hidden' value = 'true' name ='create_prod' id="optn" />
                    <input type ='hidden' name ='company_overview_products_services_id' id="cops_id" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Product / Service Name :</label>
                            <input type="text" name ="product_service_name" class="form-control" id="eproduct_service_name" required> </div>
                        <div class="form-group">
                            <label for="message-text1" class="control-label">Description of Product:</label>
                            <textarea name ="description_of_product" class="form-control" id="edescription_of_product" required> </textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name3" class="control-label">Sales Volume (% of Total Sales):</label>
                            <input type="number" min="0" max="100" name ="sales_volume" class="form-control" id="esales_volume" required> </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--  Second form of the dialog
          The Company’s Major Raw Materials (In Respect to Manufacturers Only)  -->
    <div class="modal fade" id="exampleModalone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">The Company’s Major Raw Materials (Manufacturers Only)</h4> </div>
                <form action ="func/controller/companyOverviewRawMaterialsController.php" method ="POST" name ="raw_material">
                    <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                    <input type ='hidden' value = 'true' name ='create_raw' id="optn" />
                    <input type ='hidden' name ='company_overview_raw_materials_id' id="raw_id" />
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Raw Material Name  :</label>
                            <input type="text" name = "name"  class="form-control" id="ename" required> </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Description of Raw Material :</label>
                            <textarea class="form-control"  name = "desc" id="edesc" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name2" class="control-label">Purchase Volume (% of Total Purchases):</label>
                            <input type="number" min="0" max ="100" name = "volume"  class="form-control" id="evolume"> </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--  Third form of the dialog
          Major Customers and Distribution Channels  -->
    <div class="modal fade" id="exampleModaltwo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">Major Customers and Distribution Channels</h4> </div>
                <form action ="func/controller/companyOverviewCustomersController.php" method ="POST" name ="customer">
                    <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                    <input type ='hidden' value = 'true' name ='create_customer' id="optn" />


                    <input type ='hidden' name ='company_overview_customers_id' id="customer_id" />
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="ecustomer_name" class="control-label">Customer (Buyer)’s Name  :</label>
                            <input type="text" name ="customer_name" class="form-control" id="ecustomer_name" required> </div>
                        <div class="form-group">
                            <label for="ecustomer_location" class="control-label">Location:</label>
                            <input type="text" name ="customer_location" class="form-control" id="ecustomer_location" required> </div>
                        <div class="form-group">
                            <label for="ecustomer_years_rel" class="control-label">Years of Relationship:</label>
                            <input type="text" name ="customer_years_rel" class="form-control" id="ecustomer_years_rel" required> </div>
                        <div class="form-group">
                            <label for="ecustomer_sales" class="control-label">% of Sales:</label>
                            <input type="number" min ="0" max="100" name ="customer_sales" class="form-control" required id="ecustomer_sales"> </div>
                        <div class="form-group">
                            <label for="ecustomer_trade_terms" class="control-label">Trade Terms  :</label>
                            <input type="text" name ="customer_trade_terms" class="form-control" required id="ecustomer_trade_terms"> </div>
                        <div class="form-group">
                            <label for="ecustomer_distr_channele" class="control-label">Distribution Channel:</label>
                            <select name ="customer_distr_channel" class="form-control" required id="ecustomer_distr_channel">
                                <option Text="Direct selling" Value="Direct selling">Direct selling</option>
                                <option Text="Selling through intermediaries" Value="Selling through intermediaries">Selling through intermediaries</option>
                                <option Text="Dual distribution" Value="Dual distribution">Dual distribution</option>
                                <option Text="Reverse channels" Value="Reverse channels">Reverse channels</option>
                            </select>
                        </div>

                    </div>
                    <!--                                    , user_id, company_overview_id, , , , , , -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!--  Forth form of the dialog
          Major Suppliers and Supply Channels  -->
    <div class="modal fade" id="exampleModalthree" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1">Major Suppliers and Supply Channels</h4> </div>
                <form action ="func/controller/companyOverviewSuppliersController.php" method ="POST" name ="supply">
                    <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                    <input type ='hidden' value = 'true' name ='create_supply' id="optn" />
                    <input type ='hidden' name ='company_overview_suppliers_id' id="supply_id" />
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Customer (Buyer)’s Name  :</label>
                            <input type="text" name ="customer_name" required class="form-control" id="e_customer_name"> </div>
                        <div class="form-group">
                            <label for="recipient-name2" class="control-label">Location:</label>
                            <input type="text" name ="customer_location" required class="form-control" id="e_customer_location"> </div>
                        <div class="form-group">
                            <label for="recipient-name4" class="control-label">Years of Relationship:</label>
                            <input type="number" name ="customer_years_rel" min="0" class="form-control" id="e_customer_years_rel"> </div>
                        <div class="form-group">
                            <label for="recipient-name6" class="control-label">% of Sales:</label>
                            <input type="number" name ="customer_sales" min="0" max="100" class="form-control" id="e_customer_sales"> </div>
                        <div class="form-group">
                            <label for="recipient-name8" class="control-label">Trade Terms  :</label>
                            <input type="text" name ="customer_trade_terms" required class="form-control" id="e_customer_trade_terms"> </div>
                        <div class="form-group">
                            <label for="recipient-name10" class="control-label">Distribution Channel:</label>
                            <select name ="customer_distr_channel" class="form-control" required id="e_customer_distr_channel">
                                <option Text="Direct selling" Value="Direct selling">Direct selling</option>
                                <option Text="Selling through intermediaries" Value="Selling through intermediaries">Selling through intermediaries</option>
                                <option Text="Dual distribution" Value="Dual distribution">Dual distribution</option>
                                <option Text="Reverse channels" Value="Reverse channels">Reverse channels</option>
                            </select>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                </form>

            </div>
        </div>
    </div>


    </div>
<?php
$extra_js = "
<script>
$(document).ready(function(){
    
	$(document).on('click', '.edit_product', function(){
		$('#eproduct_service_name').val('');
		$('#edescription_of_product').val('');
		$('#esales_volume').val('');	    
		var id=$(this).attr('id').split('_')[1];
		$('input[name=create_prod]').attr('name', 'edit_prod');
		var first=$('#product_service_name'+id).text();
		var last=$('#description_of_product'+id).text();
		var address=$('#sales_volume'+id).text();
        //alert(id+first+last+address) ; 
		$('#exampleModal').modal('show');
		$('#eproduct_service_name').val(first.trim());
    $('#edescription_of_product').val(last.trim());
		$('#cops_id').val(id.trim());
		$('#esales_volume').val(address.trim());
	});
  $(document).on('click', '.new_product', function(){
    $('input[name=edit_prod]').attr('name', 'create_prod');
    $('#exampleModal').modal('show');
    $('#eproduct_service_name').val('');
    $('#edescription_of_product').val('');
    $('#esales_volume').val('');
  });
	$(document).on('click', '.delete_product', function(){
		var product_id=$(this).attr('id').split('_')[1];
		var first=$('#product_service_name'+product_id).text();
    var del = window.confirm('Are you sure you want to delete product  =>' + first) ;
    if(del){
      window.location.replace('func/controller/companyOverviewProductsServicesController.php?delete_prod=true&id='+ product_id);    
    }else{
        ;
    }

	});
	
	$(document).on('click', '.edit_raw', function(){
		$('#ename').val('');
		$('#edesc').val('');
		$('#evolume').val('');	    
		var id=$(this).attr('id').split('_')[1];
		$('input[name=create_raw]').attr('name', 'edit_raw');
		var first=$('#name'+id).text();
		var last=$('#desc'+id).text();
		var address=$('#volume'+id).text();
        //alert(id+first+last+address) ; 
		$('#exampleModalone').modal('show');
    $('#raw_id').val(id.trim());
		$('#ename').val(first.trim());
		$('#edesc').val(last.trim());
		$('#evolume').val(address.trim());
	});
	$(document).on('click', '.new_raw', function(){
		$('input[name=edit_raw]').attr('name', 'create_raw');
		$('#exampleModalone').modal('show');
		$('#ename').val('');
		$('#edesc').val('');
		$('#evolume').val('');
	});
  $(document).on('click', '.delete_raw', function(){
    var raw_id=$(this).attr('id').split('_')[1];
    var first=$('#name'+raw_id).text();
    var del = window.confirm('Are you sure you want to delete raw material =>' + first) ;
    if(del){
      window.location.replace('func/controller/companyOverviewRawMaterialsController.php?delete_raw=true&id='+ raw_id);    
    }else{
        ;
    }

  });
	
	$(document).on('click', '.edit_customer', function(){
		$('#ecustomer_name').val('');
		$('#ecustomer_location').val('');
		$('#ecustomer_years_rel').val('');	    
		$('#ecustomer_sales').val('');	    
		$('#ecustomer_trade_terms').val('');	    
		$('#ecustomer_distr_channel').val('');	    
		var id=$(this).attr('id').split('_')[1];
		$('input[name=create_customer]').attr('name', 'edit_customer');
		var first=$('#customer_name'+id).text();
		var last=$('#customer_location'+id).text();
		var address=$('#customer_years_rel'+id).text();
		var address1=$('#customer_sales'+id).text();
		var address2=$('#customer_trade_terms'+id).text();
		var address3=$('#customer_distr_channel'+id).text();
        //alert(id+first+last+address) ; 
		$('#exampleModaltwo').modal('show');
    $('#customer_id').val(id.trim());
		$('#ecustomer_name').val(first.trim());
		$('#ecustomer_location').val(last.trim());
		$('#ecustomer_years_rel').val(address.trim());
		$('#ecustomer_sales').val(address1.trim());
		$('#ecustomer_trade_terms').val(address2.trim());
		$('#ecustomer_distr_channel').val(address3.trim());
	});
	$(document).on('click', '.new_customer', function(){
		$('input[name=edit_customer]').attr('name', 'create_customer');
		$('#exampleModaltwo').modal('show');
		$('#ecustomer_name').val('');
		$('#ecustomer_location').val('');
		$('#ecustomer_years_rel').val('');	    
		$('#ecustomer_sales').val('');	    
		$('#ecustomer_trade_terms').val('');	    
		$('#ecustomer_distr_channel').val('');	
	});
  $(document).on('click', '.delete_customer', function(){
    var customer_id=$(this).attr('id').split('_')[1];
    var first=$('#customer_name'+customer_id).text();
    var del = window.confirm('Are you sure you want to delete customer =>' + first) ;
    if(del){
      window.location.replace('func/controller/companyOverviewCustomersController.php?delete_customer=true&id='+ customer_id);    
    }else{
        ;
    }

  });
	
	$(document).on('click', '.edit_supply', function(){

		$('#e_customer_name').val('');	    
		$('#e_customer_location').val('');	    
		$('#e_customer_years_rel').val('');	    
		$('#e_customer_sales').val('');	    
		$('#e_customer_trade_terms').val('');	    
		$('#e_customer_distr_channel').val('');	    
		var id=$(this).attr('id').split('_')[1];
//		alert(id) ;
		$('input[name=create_supply]').attr('name', 'edit_supply');
		var first=$('#de_customer_name'+id).text();
		var last=$('#de_customer_location'+id).text();
		var address=$('#de_customer_years_rel'+id).text();
		var address1=$('#de_customer_sales'+id).text();
		var address2=$('#de_customer_trade_terms'+id).text();
		var address3=$('#de_customer_distr_channel'+id).text();
       // alert(id+first+last+address) ; 
		$('#exampleModalthree').modal('show');
		$('#e_customer_name').val(first.trim());
		$('#e_customer_location').val(last.trim());
		$('#e_customer_years_rel').val(address.trim());
		$('#e_customer_sales').val(address1.trim());
		$('#e_customer_trade_terms').val(address2.trim());
		$('#e_customer_distr_channel').val(address3.trim());
    $('#supply_id').val(id.trim());
	});
	$(document).on('click', '.new_supply', function(){
		$('input[name=edit_supply]').attr('name', 'create_supply');
		$('#exampleModalthree').modal('show');
		$('#e_customer_name').val('');	    
		$('#e_customer_location').val('');	    
		$('#e_customer_years_rel').val('');	    
		$('#e_customer_sales').val('');	    
		$('#e_customer_trade_terms').val('');	    
		$('#e_customer_distr_channel').val('');	
	});
  $(document).on('click', '.delete_supply', function(){
    var supply_id=$(this).attr('id').split('_')[1];
    var first=$('#de_customer_name'+supply_id).text();
    var del = window.confirm('Are you sure you want to delete supplier =>' + first) ;
    if(del){
      window.location.replace('func/controller/companyOverviewSuppliersController.php?delete_supply=true&id='+ supply_id);    
    }else{
        ;
    }

  });
  
	
});
</script>
" ;
require_once("require/footer.php") ;
?>