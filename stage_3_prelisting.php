<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Three" ;
$extra_css = "    <!-- Editable CSS -->
     <link href='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.css' rel='stylesheet'>
     <link href='plugins/bower_components/summernote/dist/summernote.css' rel='stylesheet' />
     " ;
$page_number = "10" ;
$extra_js = "    <!-- Editable -->
  <script src='//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.js'></script>
      <script src='plugins/bower_components/summernote/dist/summernote.min.js'></script>
    <script>
    jQuery(document).ready(function() {
        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });
        $('.inline-editor').summernote({
            airMode: true
        });
    });
    window.edit = function() {
        $('.click2edit').summernote()
    }, window.save = function() {
        $('.click2edit').destroy()
    }
    </script>
    " ;
$thy_stage = "3" ;
$thy_qsn = "183/472" ;
$thy_qp = round(183/472 * 100, 2);
$my_title = "Pre-listing Statement  9 - 9" ;
@$msg=$_GET['msg'];
$balance_sheet = "true" ;
$main_page_title = "Advanced Stage" ;
@$balance_msg=$_GET['balance_msg'];
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;

if($others->searchPS($_SESSION['email']) == 0) {
    $others->setPrelisting($_SESSION['email']) ;
}
$get_PreList=$others->getPreList($_SESSION['email']);

?>
                <div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;padding:0px;">
                        <div class="row bg-title" style="background-color: #f5f5f5;margin-left: 0px;margin-right: 0px;">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <h5 class="page-title text-uppercase"><?php echo $my_title ; ?></h5> </div>
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                <a href="index.php?logout=true" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">LogOut</a>
                                <ol class="breadcrumb">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" aria-expanded="false">
                                            [ Previous forms of this stage ]
                                        </a>
                                        <ul class="dropdown-menu mailbox animated ">
                                            <li>
                                                <a class="text-left" href="stage_3_material_litigation_claims.php"> <strong>MATERIAL LITIGATION AND CLAIMS  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_asset_transactions.php"> <strong>MATERIAL ASSET TRANSACTIONS (INCLUDING PROPERTY)  </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_3_material_contracts.php"> <strong>Company CORPORATE DIRECTORY </strong></a>
                                            </li>
                                            <li>
                                                <a class="text-left" href="stage_2_review.php"> <strong>STAGE 2 REVIEW </strong></a>
                                            </li>
                                        </ul>
                                        <!-- /.dropdown-messages -->
                                    </li>
                                    <li class="active"><?php echo $_SESSION['name'] ; ?></li>
                                </ol>
                            </div>
                            <!-- /.col-lg-12 -->
                        </div>


                            <!-- /row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="white-box-main">
                                        <h3 class="box-title">Provide the pending items of the PRE-LISTING Statement </h3>
                                        <p class="text-muted">*Just click on word which you want to change and enter</p>

                                        <table id="balancesheet_table_other" class="table editable-table table-bordered m-b-0">
                                            <thead class="text-uppercase">
                                                <th>Item</th>
                                                <th>Topic</th>
                                                <th>Status</th>
                                            </thead>
                                            <tbody>

                                            <tr>
                                                <td>1</td>
                                                <td>Introduction</td>
                                                <td>
												 <?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_PreList) ; $i++ ) {
										if($get_PreList[$i]['pre_listing_Introduction']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Introduction' data-title='PRE-LISTING Statement' class='new_busplan'
                                                       id='pre_listing_Introduction'>".$status."</a>";

                                  
                                ?>

                                                 
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>2</td>
                                                <td>General Description of the programme</td>
                                                <td>
															 <?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_PreList) ; $i++ ) {
										if($get_PreList[$i]['pre_listing_General_Description_programme']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='General Description of the programme' data-title='PRE-LISTING Statement' class='new_busplan'
                                                       id='pre_listing_General_Description_programme'>".$status."</a>";

                                  
                                ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>3</td>
                                                <td>Statement of Profit & Loss and other comprehensive income for the year ended (5YRS)</td>
                                                <td>Available</td>
                                            </tr>

                                            <tr>
                                                <td>4</td>
                                                <td>Statement of Financial Position (5yrs)</td>
                                                <td>Available</td>
                                            </tr>

                                            <tr>
                                                <td>5</td>
                                                <td>Statement of changes in Equity</td>
                                                <td>
																					 <?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_PreList) ; $i++ ) {
										if($get_PreList[$i]['pre_listing_Statement_changes_Equity']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Statement of changes in Equity' data-title='PRE-LISTING Statement' class='new_busplan'
                                                       id='pre_listing_Statement_changes_Equity'>".$status."</a>";

                                  
                                ?>
           
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>6</td>
                                                <td>Directors</td>
                                                <td>Available</td>
                                            </tr>

                                            <tr>
                                                <td>7</td>
                                                <td>Directors interests in securities</td>
                                                <td>Available</td>
                                            </tr>

                                            <tr>
                                                <td>8</td>
                                                <td>Guarantee</td>
                                                <td>Available</td>
                                            </tr>

                                            <tr>
                                                <td>9</td>
                                                <td>Borrowing Powers</td>
                                                <td>
																								 <?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_PreList) ; $i++ ) {
										if($get_PreList[$i]['pre_listing_Borrowing_Powers']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Borrowing Powers' data-title='PRE-LISTING Statement' class='new_busplan'
                                                       id='pre_listing_Borrowing_Powers'>".$status."</a>";

                                  
                                ?>
                        
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>10</td>
                                                <td>Statement as to adequacy of capital</td>
                                                <td>
																											 <?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_PreList) ; $i++ ) {
										if($get_PreList[$i]['pre_listing_Statement_adequacy_capital']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Statement as to adequacy of capital' data-title='PRE-LISTING Statement' class='new_busplan'
                                                       id='pre_listing_Statement_adequacy_capital'>".$status."</a>";

                                  
                                ?>
  
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>11</td>
                                                <td>Members of board on the prospects of the issuer</td>
                                                <td>
																																						 <?php
                         $status="Not Available";
                                    for($i = 0  ; $i < count($get_PreList) ; $i++ ) {
										if($get_PreList[$i]['pre_listing_Members_board_prospects']!=""){
											$status="Available";
										}
										  }
										echo   "<a href='#' onclick='event.preventDefault();'
                                                       data-id='Members of board on the prospects of the issuer' data-title='PRE-LISTING Statement' class='new_busplan'
                                                       id='pre_listing_Members_board_prospects'>".$status."</a>";

                                  
                                ?>
  
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>12</td>
                                                <td>Major Shareholders</td>
                                                <td>Available</td>
                                            </tr>

                                            <tr>
                                                <td>13</td>
                                                <td>Responsibility statement of members of the board</td>
                                                <td>Available</td>
                                            </tr>

                                            <tr>
                                                <td>14</td>
                                                <td>Documents available</td>
                                                <td>Available</td>
                                            </tr>

                                            <tr>
                                                <td>15</td>
                                                <td>Registered Office</td>
                                                <td>Available</td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->


                    </div>
                            <div class="login-footer">
                                <div class="row bs-reset">
                                    <div  style="padding: 0 15px;" class="col-xs-3 bs-reset">
                                        <a href ="stage_3_personal_financial_commitments.php" class="btn green btn-outline">[<< Back]</a>
                                    </div>
                                    <div style="padding: 0 15px;"" class="col-xs-4 bs-reset">
                                        <a href ="stage_3_business_plan.php" type="submit" class="btn green btn-outline pull-right" >[ Next >>] </a>
                                    </div>
                                </div>
                            </div>

                </div>
    <!-- Major Suppliers and Supply Channels  -->
    <div class="modal fade" id="exampleModalthree" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4">
        <div class="modal-dialog my_diag" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel1"> <span id = 'my_title_1'></span> </h4> </div>
                <form action ="func/controller/preListingStatementController.php" method ="POST" name ="business_plan">
                    <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                    <input type ='hidden' name ='edit_buzplan' id="edit_value_id" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label"><span id='my_title'></span></label>
                            <textarea id ="desc_here" name = "desc" class="summernote form-control form-control-solid placeholder-no-fix form-group"  rows="4" cols="50"><?=@$desc;?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


<?php
$extra_js .= "
<script>
$(document).ready(function(){
	$(document).on('click', '.new_busplan', function(){
		var subtitle = $(this).data('id');
		var title = $(this).data('title');
		var id=$(this).attr('id');
		$('#my_title_1').html( title );
		$('#my_title').html( subtitle );
		$('#edit_value_id').val( id );	    
		$.ajax({
              url: 'getprelisting.php?col='+id,
              cache: false,
              success: function(html){
                   $('.panel-body').html(html);                             
                   $('#exampleModalthree').modal('show');
                    
              }
        });
	    

		
        
	});
	
});
</script>
" ;
require_once("require/footer.php") ;

?>