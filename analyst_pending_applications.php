<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

      	<?php 
      		include("req/header_main.php") ;
        require_once 'func/controlDAO.php' ;
        $others = (new controlDAO())->getOthers() ;
          $extra_js = "" ; 
      	?>


         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title" style="background: #9ea1f1;">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title" style="color: #ffffff;"  >Finsec Analyst[Applications]</h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="analyst_dashboard.php">Dashboard</a></li>
                        <li class="active"><a href="#">Acceptance </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">

                                <div class="col-md-12 mail_listing">
                                    <h1 class="box-title">Acceptance -> Company Details</h1><span><?php echo @$_GET['msg'] ; ?></span>
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                         <th>ID</th>
                                         <th>Company name </th>
                                         <th>Applicant Name</th>
                                          <th>Phone number</th>
                                         <th>Email</th>
                                         <th>Total Score (Threshold=<?=$others->getThreshHold();?>)</th>
                                         <th>Region</th>
                                         <th>Business Sector</th>
                                         <th>Level</th>
                                      </tr>
                                      </thead>
                                      <tbody>
<!--                                         <tr>
                                           <td colspan="4" style="text-align:center">No Appliacction found</td>
                                        </tr>  -->
                                        
                                    <?php
                                      $ifPopulated =  $others->getPendingApplications() ;
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="3" style="text-align:center">No pending applications found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# directors_shareholding_id, user_id, , ,
                                              //total_score, users_fullname, company_name, business_sector, region_of_incorporation, telephone, users_created_date, capital_required
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                          <td id = 'idnum".@$ifPopulated[$i]['capital_req_id']."'>".@($i+1)."</td>
                                                         <td>".@$ifPopulated[$i]['company_name']."</td>
                                                         <td>".@$ifPopulated[$i]['users_fullname']."</td>
                                                          <td>".@$ifPopulated[$i]['telephone']."</td>
                                                          <td>".@$ifPopulated[$i]['email']."</td>
                                                           <td>".@$ifPopulated[$i]['total_score']."</td>
                                                           <td>".@$ifPopulated[$i]['region_of_incorporation']."</td>
                                                           <td>".@$ifPopulated[$i]['business_sector']."</td>
                                                           <td>".@$ifPopulated[$i]['send_to']."</td>
                                                         <td>
                                                         <a href='downloads.php?email=".@$ifPopulated[$i]['email']."'>[Downloads]</a> | 
                                                         <a href='verifyFinancialRatiosViewer.php?email=".@$ifPopulated[$i]['email']."'>[View Ratios]</a> | 
                                                         <a href='analyst_CapitalRequirements.php?email=".$ifPopulated[$i]['email']."'>[Review and Verification]</a> |
                                                         <a href='javascript:void(0);' data-id='".$ifPopulated[$i]['email']."'                                                       
                                                         class ='new_busplan'>[Analysts Rejection]</a> |
                                                         <a href='view_stage_1_capital_requirements.php?email=".$ifPopulated[$i]['email']."'>[View Application]</a> </td>
                                                      </tr>" ;
                                          }

                                      }
                                      ?>

                                      </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>

             <div class="modal fade" id="exampleModalthree" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4">
                 <div class="modal-dialog my_diag" role="document">
                     <div class="modal-content">
                         <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                             <h4 class="modal-title" id="exampleModalLabel1"> Reject Application </h4> </div>
                         <form action ="analyst_manualRejection.php" method ="POST" name ="business_plan">
                             <input type ="hidden" id="user_id"   name ="user_id"/>
                             <input type ='hidden' name ='edit_buzplan' id="edit_value_id" />
                             <div class="modal-body">

                                 <div class="form-group">
                                     <label class="control-label">Select application level</label>
                                         <div class="radio-list">
                                             <label class="radio-inline">
                                                 <input type="radio" name="optionsRadios2" value="BASIC"> BASIC </label>
                                             <label class="radio-inline">
                                                 <input type="radio" name="optionsRadios2" value="INTERMEDIATE"> INTERMEDIATE </label>
                                             <label class="radio-inline">
                                                 <input type="radio" name="optionsRadios2" value="ADVANCED" checked=""> ADVANCED </label>
                                         </div>
                                 </div>
                                 <br>

                                 <div class="form-group">
                                     <label for="recipient-name" class="control-label">Reason for rejections</label>
                                     <textarea id ="desc_here" name = "desc"
                                               class="form-control form-control-solid placeholder-no-fix form-group"
                                               rows="4" cols="50"><?=@$desc;?></textarea>
                                 </div>

                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 <button type="submit" class="btn btn-primary">Save</button>
                             </div>
                         </form>

                     </div>
                 </div>
             </div>


            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script type="text/javascript">
        function popup(){
        window.open ("stage_1_overview_of_company.php","mywindow","menubar=1,resizable=1,width=350,height=250");
      }
      </script>
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
      <script>
          $(document).ready(function(){
              $(document).on('click', '.new_busplan', function(){
                  var subtitle = $(this).data('id');
                  $('#user_id').val( subtitle );
                  $('#exampleModalthree').modal('show');
              });

          });
      </script>

   </body>

</html>