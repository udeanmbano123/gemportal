<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Finsec | SME Portal</title>
      <!-- Bootstrap Core CSS -->
      <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- Menu CSS -->
      <link href="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
      <link href="plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
      <!-- animation CSS -->
      <link href="css/animate.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="css/style.css" rel="stylesheet">
      <!-- color CSS -->
      <link href="css/colors/blue.css" id="theme" rel="stylesheet">



      <!-- toast CSS -->
      <link href="plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
      <!-- morris CSS -->
          <link href="plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">

    <link href="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">




   </head>
   <body class="fix-sidebar fix-header">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">

      	<?php 
          include("req/header_main.php") ;
          require_once 'func/controlDAO.php' ;
        ?>

         <!-- Page Content -->
         <div id="page-wrapper">
            <div class="container-fluid">
               <div class="row bg-title">
                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                     <h4 class="page-title">Finsec Admin [Review & Verification] </h4>
                  </div>
                  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                     <ol class="breadcrumb">
                        <li><a href="admin_home.php">Dashboard</a></li>
                        <li class="active"><a href="#">Review & Verification  </a></li>
                     </ol>
                  </div>
                  <!-- /.col-lg-12 -->
               </div>
 

                <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="white-box">
                            <div class="row">


                                <div class="col-md-12 mail_listing">
                                    <h1 class="box-title">Review & Verification Company Details</h1><span><?php echo @$_GET['sms'] ; ?></span>
                                    <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                         <th>Company name </th>
                                         
                                         <th>Email</th>
                                         <th>Phone number</th>
                                          <th>Action</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                       <?php
                                      $ifPopulated =  (new controlDAO())->getApplications()->getAcceptedApplications();
                                      if(!$ifPopulated){
                                          //echo "There is no data " ;

                                          echo '<tr>
                                                     <td colspan="3" style="text-align:center">No Financial  Advisors found</td>
                                                  </tr>' ;
                                      }else{
                                          $setOption = "<input type ='hidden' value = 'true' name ='update'/>" ;
                                          foreach($ifPopulated as $i => $item) {
                                              //# directors_shareholding_id, user_id, , ,
                                              extract($ifPopulated[$i]);
                                              echo "<tr>
                                                         <td>".@$ifPopulated[$i]['company_name']."</td>
                                                        
                                                         <td>".@$ifPopulated[$i]['email']."</td>
                                                         
                                                          <td>".@$ifPopulated[$i]['telephone']."</td>
                                                           <td> <a href='verifyCapitalRequirements.php?email=".$ifPopulated[$i]['email']."'>[Review and Verification]</a> </td>
                                                         
                                                      </tr>" ;
                                          }
                                          //$setOption .= "<input type ='hidden' value = '".@$company_overview_id."' name ='company_overview_id'/>" ;
                                          //echo $user_id ;
                                      }
                                      ?>
                                      






                                          <!-- <td><a href='#'>[View Details]</a> <br> <a href ='#'>[Review & Verification Details]</a></td> -->
                                        </tr>
                                      </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Escrow System </footer>
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- jQuery -->
      <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Menu Plugin JavaScript -->
      <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
      <!--slimscroll JavaScript -->
      <script src="js/jquery.slimscroll.js"></script>
      <!--Wave Effects -->
      <script src="js/waves.js"></script>
      <!--Counter js -->
      <script src="plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
      <script src="plugins/bower_components/counterup/jquery.counterup.min.js"></script>
      <!-- Custom Theme JavaScript -->
      <script src="js/custom.min.js"></script>
      <script src="public/assets/global/plugins/moment.min.js"></script>
      <!--Style Switcher -->
      <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    </script>

   </body>

</html>