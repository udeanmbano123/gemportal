<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "" ; 
$page_number = "5" ; 
$extra_js = "" ;
$thy_stage = "3" ;
$main_page_title = "Advanced Stage" ;
@$label=$_GET['action'];
@$id=$_GET['id'];
require_once 'func/controlDAO.php' ;
$others = (new controlDAO())->getOthers() ;
require_once("require/header.php") ;
$ifPopulated =  (new controlDAO())-> getpersonalFinancial()->selectpersonalFinancialById(@$id) ;
foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
?>
<div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                         <h1 class="text-uppercase "> <?php if ($label){
                            echo 'Add Financial Commitments';
                         }
                         else {
                          echo  'Edit Financial Commitments';
                         }  ; ?></h1>
                        <form action="func/controller/personalFinancialController.php"  class="login-form" method="post">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Please complete all the required field with * asterisks  </span>
                            </div>
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                             <input type ="hidden" value= "<?= @$_GET['id'] ?>"  name ="id"/>
                            <div class="row">
                                
                                
                           
                                      <div class="col-xs-4">
                                    *Principal/Gurantor Name
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Principal name" name="name" value="<?=@$guarantor_name;?>" required/>
                                </div>
                                        <div class="col-xs-4">
                                            *Financial Institution
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Financial Institution" name="institution"  value="<?=@$financial_institution;?>" required/>
                                        </div>
                                       <div class="col-xs-4">
                                            *Facility type
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Facility Type" name="type"  value="<?=@$facility_type;?>" required/>
                                        </div>

                                    

                                
                            </div>  
                            <div class="row">  
                                <div class="col-xs-4">
                                    *Facility Amount:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="number"  placeholder="Facility Amount" name="amount" value="<?=@$facility_amount;?>" required/>
                                </div>
                                <div class="col-xs-4">
                                    *Monthly Installment
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="number"  placeholder="Monthly Installment" name="installment" value="<?=@$monthly_installment;?>" required/>
                                    
                                </div>                                
                                <div class="col-xs-4">
                                    *Balance
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="number"  placeholder="Balance" name="balance" value="<?=@$balance;?>" required/>
                                </div>
                                <div class="row">  
                                <div class="col-xs-6">
                                    *Tenure (Months):
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="number"  placeholder="Tenure" name="tenure" value="<?=@$tenure;?>" required/>
                                </div>
                                <div class="col-xs-6">
                                    *Start Date
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="date" autocomplete="off" placeholder="Start Date"  name="date" value="<?=@$start_date;?>" required/>

                                    
                                </div>                                
                                
                            </div>
                                <div class="row"> 
                               <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary">Save</button>
                               </div>
                                </div>


                            </div>