<?php
$message  = "A free registration facility for entrepreneurs, business owners and senior representatives of large non-listed enterprises, small to medium sized enterprises and highly potent start-ups. The enterprises must be located in Zimbabwe.
                        Kindly create your User Account here. Please note that your Company and Personal Profiles are not accessible to the public and are strictly for the assessment and ordinary business of the Financial Securities Exchange and licenced participants on the FINSEC ATP. " ;
$page_title = "Finsec | Stage Two" ; 
$extra_css = "" ; 
$page_number = "5" ; 
$extra_js = "" ;
$thy_stage = "3" ;
$main_page_title = "Intermediate Stage" ;
@$label=$_GET['action'];
@$id=$_GET['id'];
require_once("require/header.php") ;
require_once 'func/controlDAO.php' ;
$ifPopulated =  (new controlDAO())->getbalanceSheet()->selectBalanceSheetById(@$id) ;
foreach($ifPopulated as $i => $item) {
         extract($ifPopulated[$i]);
     }
?>

<div class="col-md-7 login-container bs-reset mt-login-5-bsfix" style ="border-left: 1px solid #74d0a9;">
                    <div class="login-content" style ="margin-top:0px;">
                         <h1 class="text-uppercase "> <?php if ($label){
                            echo 'Add Balancesheet';
                         }
                         else {
                          echo  'Edit Blancesheet';
                         }  ; ?></h1>
                        <form action="func/controller/projectedbalanceSheetController.php"  class="login-form" method="post">
                            <div class="alert alert-danger display-hide">
                                <button class="close" data-close="alert"></button>
                                <span>Please complete all the required field with * asterisks  </span>
                            </div>
                            <?=@$setOption;?>
                            <input type ="hidden" value= "<?= $_SESSION['email'] ?>"  name ="user_id"/>
                             <input type ="hidden" value= "<?= @$_GET['id'] ?>"  name ="id"/>
                             <input type ="hidden" value= "future"  name ="type"/>
                            <div class="row">
                                
                                
                           
                                        <div class="col-xs-3">
                                    *Year:
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Year" name="year" value="<?=@$year;?>" required/>
                                </div>
                                        <div class="col-xs-3">
                                            Fixed  Assets
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Fixed Assets" name="fassets"  value="<?=@$fixed_assets;?>" required/>
                                        </div>
                                       <div class="col-xs-3">
                                            *Current Assets
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Current Assets" name="cassets"  value="<?=@$current_assets;?>" required/>
                                        </div>
                                        <div class="col-xs-3">
                                            *Total Assets
                                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Total Assets" name="tassets"  value="<?=@$total_assets;?>" required/>
                                        </div>


                                    

                                
                            </div>  
                            <div class="row">  
                                <div class="col-xs-3">
                                    *Net Assets :
                                    <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Net  Assets" name="netassets" value="<?=@$net_assets;?>" required/>
                                </div>
                                <div class="col-xs-3">
                                    *Current Liabilities
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Current Liabilities" name="cliabilities" value="<?=@$current_liabilities;?>" required/>
                                    
                                </div>
                                <div class="col-xs-3">
                                    *Total Liabilities
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Total Liabilities" name="toliabilities" value="<?=@$total_liabilities;?>" required/>
                                    
                                </div>
                                  <div class="col-xs-3">
                                    *Other Liabilities
                                     <input class="form-control form-control-solid placeholder-no-fix form-group" type="text"  placeholder="Other Liabilities" name="oliabilities" value="<?=@$other_liabilities;?>" required/>
                                    
                                </div>                                     
                                
                                <div class="row"> 
                               <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary">Save</button>
                               </div>
                                </div>


                            </div>



      